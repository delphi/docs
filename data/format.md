DELPHI Data Format

DELPHI Physics Data Structures
==============================

*The Data Format*
-----------------

All DELPHI data sets, from raw data to mini-DSTs are written in
[ZEBRA](http://cern.ch/wwwasdoc/zebra_html3/zebramain.html) FZ exchange
format. This allows members of the collaboration to transport data
easily from one computing architecture to another without having to
convert the data from one format to another.

*From Raw data to processed DST's*
----------------------------------

There are five types of DELPHI data sets, all but one of which is
derived from one or more of the other types of data sets. The types of
DELPHI data sets and the point in the DELPHI data analysis and
production stream where they are produced is as follows:

1.  [RAW data](rawdata.html), collected either by the DELPHI [online
    system](http://delonline.cern.ch/delphi$www/public/delphionline.html).
    or produced by
    [DELSIM](http://delphiwww.cern.ch/offline/analysis/delsim.html), the
    DELPHI detector simulation program contain the electronic data read
    out by the detector and, by size, are the largest of the DELPHI data
    sets
2.  [Full DST data](http://delphiwww.cern.ch/~sacquin/dst_content.ps),
    produced by
    [DELANA](http://delphiwww.cern.ch/offline/analysis/delana.html), ,
    the DELPHI Data Reconstruction and Analysis Program from the DELPHI
    RAW data, contain information about reconstructed charged tracks and
    neutrals which may be used in physics analyses
3.  [Short DST data](http://delphiwww.cern.ch/spassoff/des/shortdes.ps),
    produced by DSTANA, the DST Analysis and fixing package, and written
    by PHDST, the DELPHI package for DST productions, from the DELPHI
    DSTs are a more compact data set which allow faster analysis of the
    physics data. These are used primarily by the hadronic teams in
    DELPHI
4.  [Long DST data](http://delphiwww.cern.ch/spassoff/des/longdes.ps),
    produced using the same software as the Short DST's, but contain the
    Full DST information (with fixes and particle identifications),
    followed by the Short DST structure. These are used primarily by the
    DELPHI leptonic teams.
5.  [Mini DST data](http://delphiwww.cern.ch/spassoff/des/minides.ps),
    produced by PHDST, are highly reduced data sets, containing the
    minimum of information needed for a physics analysis. These data are
    intended to be copied and used at the home institutes.

------------------------------------------------------------------------

*Last update 1 feb 1999*

[Hansjoerg Klein](http://delphiwww.cern.ch/offline/hkl.html)
