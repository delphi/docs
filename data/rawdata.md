DELPHI Raw Data from the pit

DELPHI Raw Data taken since the beginning of LEP
================================================

*Please note: all raw data have been copied to Castor. The tape number
given here are not normally available. If you really need to acceess
them, contact delphi-core.*

------------------------------------------------------------------------

Raw Data of 2000 (Fatmen name *RAWD00*)
---------------------------------------

### Copies done on ED1767-ED1863 DLT2000 multifile

EY1767-EY1863 \*\* ROBOT \*\*

ED1767-ED1863 \*\* VAULT \*\*

### COSMICS *Start with run 108066 on 13.03.00*

-   EY1767.04 - 78 multifile STK Robot cassette
-   EY1768.01 - 72 multifile STK Robot cassette
-   EY1769.01 - 83 multifile STK Robot cassette
-   EY1770.01 - 94 multifile STK Robot cassette
-   EY1771.01 - 12 multifile STK Robot cassette (stop run 108297 on
    31.3) Output:Y16376.1-72

    ------------------------------------------------------------------------

### Energy at 91 Gev *Start with run 108390 on 03.04.00*

-   EY1771.13 - 72 multifile STK Robot cassette
-   EY1772.01 - 75 multifile STK Robot cassette
-   EY1773.01 - 22 multifile STK Robot cassette (stop 07.04)
-   EY1773.41 - -- multifile STK Robot cassette (start 08.04)
-   EY1773.-- - 69 multifile STK Robot cassette (stop 09.04)
-   EY1773.80 - 90 multifile STK Robot cassette (start 09.04)
-   EY1774.01 - 37 multifile STK Robot cassette (stop 11.04)
-   EY1792.34 - 77 multifile STK Robot cassette (start 23.05)
-   EY1793.01 - 35 multifile STK Robot cassette (stop 24.05)
-   EY1836.64 - 95 multifile STK Robot cassette (start 16.08)
-   EY1837.01 - 08 multifile STK Robot cassette (stop) 17.08)
-   EY1842.96 - 96 multifile STK Robot cassette (start 14.09)
-   EY1843.93 - 99 multifile STK Robot cassette
-   EY1844.01 - 48 multifile STK Robot cassette (stop) 15.09)

------------------------------------------------------------------------

### Energy at 100-104 Gev *Start with run 108613 on 08.04.00*

-   EY1773.23 - 40 multifile STK Robot cassette (stop 08.04)
-   EY1773.70 - 79 multifile STK Robot cassette (start/stop 09.04)
-   EY1774.38 - 75 multifile STK Robot cassette (start 11.04)
-   EY1775.01 - 91 multifile STK Robot cassette
-   EY1776.01 - 99 multifile STK Robot cassette
-   EY1777.01 - 99 multifile STK Robot cassette
-   EY1778.01 - 99 multifile STK Robot cassette
-   EY1779.01 - 99 multifile STK Robot cassette
-   EY1780.01 - 76 multifile STK Robot cassette
-   EY1781.01 - 94 multifile STK Robot cassette
-   EY1782.01 - 98 multifile STK Robot cassette
-   EY1783.01 - 97 multifile STK Robot cassette
-   EY1784.01 - 94 multifile STK Robot cassette
-   EY1785.01 - 99 multifile STK Robot cassette
-   EY1786.01 - 99 multifile STK Robot cassette
-   EY1787.01 - 99 multifile STK Robot cassette
-   EY1788.01 - 94 multifile STK Robot cassette
-   EY1789.01 - 91 multifile STK Robot cassette
-   EY1790.01 - 96 multifile STK Robot cassette
-   EY1791.01 - 91 multifile STK Robot cassette
-   EY1792.01 - 33 multifile STK Robot cassette (stop 23.05)
-   EY1793.36 - 82 multifile STK Robot cassette (start 25.05)
-   EY1794 01 - 90 multifile STK Robot cassette
-   EY1795 01 - 94 multifile STK Robot cassette
-   EY1796 01 - 82 multifile STK Robot cassette
-   EY1797 01 - 90 multifile STK Robot cassette
-   EY1800 01 - 99 multifile STK Robot cassette
-   EY1801 01 - 74 multifile STK Robot cassette
-   EY1802 01 - 93 multifile STK Robot cassette
-   EY1803 01 - 99 multifile STK Robot cassette
-   EY1804 01 - 96 multifile STK Robot cassette
-   EY1805 01 - 81 multifile STK Robot cassette
-   EY1806 01 - 99 multifile STK Robot cassette
-   EY1807 01 - 88 multifile STK Robot cassette
-   EY1808 01 - 98 multifile STK Robot cassette
-   EY1809 01 - 83 multifile STK Robot cassette
-   EY1810 01 - 98 multifile STK Robot cassette
-   EY1811 01 - 98 multifile STK Robot cassette
-   EY1812 01 - 99 multifile STK Robot cassette
-   EY1813 01 - 99 multifile STK Robot cassette
-   EY1814 01 - 99 multifile STK Robot cassette
-   EY1815 01 - 99 multifile STK Robot cassette
-   EY1816 01 - 99 multifile STK Robot cassette
-   EY1817 01 - 99 multifile STK Robot cassette
-   EY1818 01 - 99 multifile STK Robot cassette
-   EY1819 01 - 99 multifile STK Robot cassette
-   EY1820 01 - 99 multifile STK Robot cassette
-   EY1821 01 - 99 multifile STK Robot cassette
-   EY1822 01 - 99 multifile STK Robot cassette
-   EY1823 01 - 99 multifile STK Robot cassette
-   EY1824 01 - 99 multifile STK Robot cassette
-   EY1825 01 - 99 multifile STK Robot cassette
-   EY1826 01 - 99 multifile STK Robot cassette
-   EY1827 01 - 99 multifile STK Robot cassette
-   EY1828 01 - 99 multifile STK Robot cassette
-   EY1829 01 - 76 multifile STK Robot cassette
-   EY1830 01 - 99 multifile STK Robot cassette
-   EY1831 01 - 97 multifile STK Robot cassette
-   EY1832 01 - 99 multifile STK Robot cassette
-   EY1833 01 - 56 multifile STK Robot cassette
-   EY1834 01 - 94 multifile STK Robot cassette
-   EY1835 01 - 92 multifile STK Robot cassette
-   EY1836 01 - 63 multifile STK Robot cassette (stop 16.08)
-   EY1837 09 - 96 multifile STK Robot cassette (start 17.08)
-   EY1838 01 - 90 multifile STK Robot cassette
-   EY1839 01 - 99 multifile STK Robot cassette
-   EY1840 01 - 97 multifile STK Robot cassette
-   EY1841 01 - 95 multifile STK Robot cassette
-   EY1842 01 - 95 multifile STK Robot cassette
-   EY1843 01 - 92 multifile STK Robot cassette (stop 11.09)
-   EY1844 49 - 74 multifile STK Robot cassette (start 15.09)
-   EY1845 01 - 99 multifile STK Robot cassette
-   EY1846 01 - 99 multifile STK Robot cassette
-   EY1847 01 - 84 multifile STK Robot cassette
-   EY1848 01 - 99 multifile STK Robot cassette
-   EY1849 01 - 94 multifile STK Robot cassette
-   EY1850 01 - 61 multifile STK Robot cassette
-   EY1851 01 - 85 multifile STK Robot cassette
-   EY1852 01 - 92 multifile STK Robot cassette
-   EY1853 01 - 90 multifile STK Robot cassette
-   EY1854 01 - 97 multifile STK Robot cassette
-   EY1855 01 - 98 multifile STK Robot cassette
-   EY1856 01 - 36 multifile STK Robot cassette
-   EY1857 01 - 96 multifile STK Robot cassette
-   EY1858 01 - 99 multifile STK Robot cassette
-   EY1859 01 - 65 multifile STK Robot cassette
-   EY1860 01 - 99 multifile STK Robot cassette
-   EY1861 01 - 99 multifile STK Robot cassette
-   EY1862 01 - 58 multifile STK Robot cassette
-   EY1863 01 - 55 multifile STK Robot cassette (stop 02.11)

### END OF DATA TAKING last run 117840 on 2.11.00

------------------------------------------------------------------------

Raw Data of 1999 (Fatmen name *RAWD99*)
---------------------------------------

### Copies done on ED1701-ED1766 DLT2000 multifile

EY1701-EY1766 \*\* ROBOT \*\*

ED1701-ED1766 \*\* VAULT \*\*

see [Vaxnews/Delphi.offline of Nov. 9: 99 Real data in
Fatmen](http://delnews.cern.ch/VAXNEWS/DELPHI.OFFLINE?001519)

### COSMICS *Start with run 100175 on 12.04.99*

-   EY1701.01 - 99 multifile STK Robot cassette
-   EY1702.01 - 99 multifile STK Robot cassette
-   EY1703.01 - 19 multifile STK Robot cassette (stop run 100843
    on 27.04.99)
-   EY1721.62 - 72 multifile STK Robot cassette (start run 103383
    on 1.7.99)
-   EY1721.62 - 72 multifile STK Robot cassette (stop run 103394
    on 2.7.99)
-   EY1734.47 - 59 multifile STK Robot cassette (start/stop run
    104383-409 on 28.7.99)

------------------------------------------------------------------------

### Energy at 91 Gev *Start with run 101413 on 04.05.99*

-   EY1703.20 - 99 multifile STK Robot cassette
-   EY1704.01 - 84 multifile STK Robot cassette
-   EY1705.01 - 66 multifile STK Robot cassette
-   EY1706.01 - 79 multifile STK Robot cassette (stop run 101779
    on 14.05.99)
-   EY1734.45 - 46 multifile STK Robot cassette (start/stop run
    104375-376 on 28.7.99)
-   EY1734.60 - 86 multifile STK Robot cassette (start run
    104426 29.7.99)
-   EY1735.01 - 07 multifile STK Robot cassette (stop run
    104448 30.7.99)
-   EY1755.19 - 31 multifile STK Robot cassette (start run 106484
    on 28.10.99)
-   EY1756.01 - 71 multifile STK Robot cassette
-   EY1757.01 - 05 multifile STK Robot cassette (stop run 106573
    on 3.10.99)

------------------------------------------------------------------------

### Energy at 192 Gev *Start with run 101800 on 14.05.99*

-   EY1706.80 - 81 multifile STK Robot cassette
-   EY1707.01 - 79 multifile STK Robot cassette
-   EY1708.01 - 98 multifile STK Robot cassette
-   EY1709.01 - 74 multifile STK Robot cassette
-   EY1710.01 - 92 multifile STK Robot cassette
-   EY1711.01 - 86 multifile STK Robot cassette
-   EY1712.01 - 82 multifile STK Robot cassette
-   EY1713.01 - 68 multifile STK Robot cassette (stop run 102650
    on 4.6.99)
-   EY1714.02 - 68 multifile STK Robot cassette (start run 102665 on
    5.6.99 )
-   EY1715.01 - 33 multifile STK Robot cassette (stop run 102735
    on 7.6.99)
-   EY1716.66 - 67 multifile STK Robot cassette (start/stop run
    102920-932 on 14.6.99)
-   EY1721.73 - 75 multifile STK Robot cassette (start run 103444
    on 3.7.99)
-   EY1722.01 - 02 multifile STK Robot cassette (stop run 103450
    on 3.7.99)
-   EY1722.05 - 10 multifile STK Robot cassette (start/stop run
    103464-465 on 3.7.99)
-   EY1722.64 - 73 multifile STK Robot cassette (start/stop run
    103525-528 on 5.7.99)
-   EY1724.50 - 67 multifile STK Robot cassette (start/stop run
    103634-639\_12 on 9.7.99)
-   EY1731.36 - 39 multifile STK Robot cassette (start/stop run
    104110-111 on 22.7.99)

------------------------------------------------------------------------

### Energy at 196 Gev *Start with run 102657 on 04.06.99*

-   EY1713.69 - 70 multifile STK Robot cassette
-   EY1714.01 - 01 multifile STK Robot cassette (stop run 102657
    on 4.6.99)
-   EY1715.34 - 76 multifile STK Robot cassette (start run 102755
    on 8.6.99)
-   EY1716.01 - 65 multifile STK Robot cassette (stop run 102910
    on 13.6.99)
-   EY1716.68 - 70 multifile STK Robot cassette (start run 1029881
    on 15.6.99)
-   EY1717.01 - 70 multifile STK Robot cassette
-   EY1718.01 - 74 multifile STK Robot cassette
-   EY1719.01 - 68 multifile STK Robot cassette
-   EY1720.01 - 71 multifile STK Robot cassette
-   EY1721.01 - 72 multifile STK Robot cassette (stop run 103378
    on 2.7.99)
-   EY1722.03 - 04 multifile STK Robot cassette (start/stop run 103455
    on 3.7.99)
-   EY1722.11 - 63 multifile STK Robot cassette (start/stop run
    103469-519 on 3.7.99)
-   EY1722.74 - 76 multifile STK Robot cassette (start run 103532
    on 5.7.99)
-   EY1723.01 - 69 multifile STK Robot cassette
-   EY1724.01 - 49 multifile STK Robot cassette (stop run 103623
    on 8.7.99)
-   EY1724.68 - 71 multifile STK Robot cassette (start run 103644
    on 9.7.99)
-   EY1725.01 - 79 multifile STK Robot cassette
-   EY1726.01 - 68 multifile STK Robot cassette
-   EY1727.01 - 70 multifile STK Robot cassette
-   EY1728.01 - 66 multifile STK Robot cassette
-   EY1729.01 - 68 multifile STK Robot cassette
-   EY1730.01 - 68 multifile STK Robot cassette
-   EY1731.01 - 35 multifile STK Robot cassette (stop run 104104
    on 22.7.99)
-   EY1731.40 - 71 multifile STK Robot cassette (start run 104161
    on 23.7.99)
-   EY1732.01 - 69 multifile STK Robot cassette
-   EY1733.01 - 68 multifile STK Robot cassette
-   EY1734.01 - 44 multifile STK Robot cassette (stop run 104363
    on 28.7.99)
-   EY1735.08 - 73 multifile STK Robot cassette (start run on
    104470 31.07.99)
-   EY1736.01 - 31 multifile STK Robot cassette (stop run 104535
    on 2.8.99)
-   EY1738.02 - 16 multifile STK Robot cassette (start/stop run
    104778-785 on 7.8.99)
-   EY1741.04 - 09 multifile STK Robot cassette (start/stop run
    105096.1-5 on 18.8.99)

------------------------------------------------------------------------

### Energy at 200 GeV *Start with run 104541 on 2.8.99*

-   EY1736.32 - 75 multifile STK Robot cassette
-   EY1737.01 - 81 multifile STK Robot cassette
-   EY1738.01 - 01 multifile STK Robot cassette (stop run 104744
    on 6.8.99)
-   EY1738.17 - 81 multifile STK Robot cassette (start run 104790
    on 7.8.99)
-   EY1739.01 - 69 multifile STK Robot cassette
-   EY1740.01 - 71 multifile STK Robot cassette
-   EY1741.01 - 03 multifile STK Robot cassette (stop run 105046
    on 16.8.99)
-   EY1741.10 - 73 multifile STK Robot cassette (start run 105114
    on 19.8.99)
-   EY1742.01 - 79 multifile STK Robot cassette
-   EY1743.01 - 73 multifile STK Robot cassette
-   EY1744.01 - 75 multifile STK Robot cassette
-   EY1745.01 - 70 multifile STK Robot cassette
-   EY1746.01 - 75 multifile STK Robot cassette
-   EY1747.01 - 68 multifile STK Robot cassette
-   EY1748.01 - 70 multifile STK Robot cassette
-   EY1749.01 - 73 multifile STK Robot cassette
-   EY1750.01 - 71 multifile STK Robot cassette
-   EY1752.01 - 78 multifile STK Robot cassette
-   EY1753.01 - 68 multifile STK Robot cassette
-   EY1754.01 - 75 multifile STK Robot cassette
-   EY1755.01 - 09 multifile STK Robot cassette (stop run 106390
    on 24.9.99)
-   EY1757.06 - -- multifile STK Robot cassette (start run 106580
    on 3.10.99)
-   EY1757.-- - 29 multifile STK Robot cassette (stop run 106604
    on 4.10.99)
-   EY1757.39 - 46 multifile STK Robot cassette (start/stop run
    106645-649 on 5.10.99)
-   EY1751.14 - 30 multifile STK Robot cassette (start/stop run
    106924-929 on 16.10.99)
-   EY1755.38 - -- multifile STK Robot cassette (start run 107002
    on 19.10.99)
-   EY1755.-- - 74 multifile STK Robot cassette (stop run 107039
    on 21.10.99)
-   EY1760.10 - 27 multifile STK Robot cassette (start/stop run
    107078-089 on 22.10.99)
-   EY1761.76 - 80 multifile STK Robot cassette (start run 107282
    on 27.10.99)
-   EY1762.01 - 24 multifile STK Robot cassette (stop run 107316
    on 28.10.99)
-   EY1764.23 - -- multifile STK Robot cassette (start run 107513
    on 2.11.99)
-   EY1764.-- - 43 multifile STK Robot cassette (stop run 107531
    on 3.11.99)
-   EY1764.57 - 63 multifile STK Robot cassette (start/stop run
    107555\_1-7 on 4.11.99)
-   EY1766.16 - -- multifile STK Robot cassette (start run 107691
    on 7.11.99)
-   EY1766.-- - 30 multifile STK Robot cassette (stop run 107702
    on 8.11.99)

### END OF DATA TAKING last run 107702 on 8.11.99

------------------------------------------------------------------------

### Energy at 202 GeV *Start with run 106428 on 24.9.99*

-   EY1755.10 - 18 multifile STK Robot cassette (stop run 106438
    on 25.10.99)
-   EY1757.30 - 38 multifile STK Robot cassette (start/stop run
    106610-623 on
-   EY1757.47 - 72 multifile STK Robot cassette (start run 106654
    on 5.10.99)
-   EY1758.01 - 70 multifile STK Robot cassette
-   EY1759.01 - 61 multifile STK Robot cassette
-   EY1751.01 - 13 multifile STK Robot cassette (stop run 106909
    on 15.10.99)
-   EY1751.31 - 77 multifile STK Robot cassette (start run 106932
    on 16.10.99)
-   EY1755.32 - 37 multifile STK Robot cassette (stop run 106993
    on 19.10.99)
-   EY1755.75 - 76 multifile STK Robot cassette (start run 107043
    on 21.10.99)
-   EY1759.62 - 79 multifile STK Robot cassette
-   EY1760.01 - 09 multifile STK Robot cassette (stop run 107075
    on 21.10.99)
-   EY1760.28 - 79 multifile STK Robot cassette (start run 107092
    on 22.10.99)
-   EY1761.01 - 75 multifile STK Robot cassette (stop run 107268
    on 26.10.99)
-   EY1762.25 - 76 multifile STK Robot cassette (start run 107320
    on 28.10.99)
-   EY1763.01 - 74 multifile STK Robot cassette
-   EY1764.01 - 22 multifile STK Robot cassette (stop run 107508
    on 2.11.99)
-   EY1764.44 - -- multifile STK Robot cassette (start run 107536
    on 3.11.99)
-   EY1764.-- - 56 multifile STK Robot cassette (stop run 107550
    on 4.11.99)
-   EY1764.64 - 76 multifile STK Robot cassette (start run 107564
    on 4.11.99)
-   EY1765.01 - 76 multifile STK Robot cassette
-   EY1766.01 - 15 multifile STK Robot cassette (stop run 107683
    on 7.11.99)

### Note: ONE run, 107680\_1 is 204 GeV, on EY1766.12

------------------------------------------------------------------------

Raw Data of 1998 (Fatmen name *RAWD98*)
---------------------------------------

### Copies done on ED0372-ED0427 DLT2000 multifile

ED0372-ED0437 \*\* VAULT \*\*

EN9372-EN9437 \*\* ARCHIVED \*\*

### COSMICS *Start with run 81567 on 20.04.98*

-   EN9372.01 - 99 multifile NTP Robot cassette
-   EN9373.01 - 99 multifile NTP Robot cassette
-   EN9374.01 - 32 multifile NTP Robot cassette (last run 82047
    on 05.05.98)

### COSMOLEP *Start with run 89813 on 03.11.98*

-   EN9437.27 - 77 multifile NTP Robot cassette (last run 89881
    on 10.11.98)

------------------------------------------------------------------------

### Energy at 91 Gev *Start with run 82393 on 14.05.98*

-   EN9374.33 - 99 multifile NTP Robot cassette
-   EN9375.01 - 91 multifile NTP Robot cassette
-   EN9376.01 - 99 multifile NTP Robot cassette
-   EN9377.01 - 99 multifile NTP Robot cassette
-   EN9378.01 - 91 multifile NTP Robot cassette
-   EN9379.01 - 58 multifile NTP Robot cassette (last run 83222
    on 24.05.98)
-   EN9433.40 - 88 multifile NTP Robot cassette (first run 89226
    on 19.10.98)
-   EN9434.01 - 18 multifile NTP Robot cassette (last run 89271
    on 21.10.98)

------------------------------------------------------------------------

### Energy at 189 Gev *Start with run 83231 on 25.05.98*

-   EN9379.59 - 98 multifile NTP Robot cassette
-   EN9380.01 - 87 multifile NTP Robot cassette
-   EN9381.01 - 99 multifile NTP Robot cassette
-   EN9382.01 - 80 multifile NTP Robot cassette
-   EN9383.01 - 93 multifile NTP Robot cassette
-   EN9384.01 - 78 multifile NTP Robot cassette
-   EN9385.01 - 82 multifile NTP Robot cassette
-   EN9386.01 - 84 multifile NTP Robot cassette
-   EN9387.01 - 80 multifile NTP Robot cassette
-   EN9388.01 - 83 multifile NTP Robot cassette
-   EN9389.01 - 76 multifile NTP Robot cassette
-   EN9390.01 - 82 multifile NTP Robot cassette
-   EN9391.01 - 87 multifile NTP Robot cassette
-   EN9392.01 - 80 multifile NTP Robot cassette
-   EN9393.01 - 81 multifile NTP Robot cassette
-   EN9394.01 - 83 multifile NTP Robot cassette
-   EN9395.01 - 84 multifile NTP Robot cassette
-   EN9396.01 - 78 multifile NTP Robot cassette
-   EN9397.01 - 81 multifile NTP Robot cassette
-   EN9398.01 - 78 multifile NTP Robot cassette
-   EN9399.01 - 79 multifile NTP Robot cassette
-   EN9400.01 - 87 multifile NTP Robot cassette
-   EN9401.01 - 88 multifile NTP Robot cassette
-   EN9402.01 - 83 multifile NTP Robot cassette
-   EN9403.01 - 84 multifile NTP Robot cassette
-   EN9404.01 - 91 multifile NTP Robot cassette
-   EN9405.01 - 84 multifile NTP Robot cassette
-   EN9406.01 - 79 multifile NTP Robot cassette
-   EN9407.01 - 93 multifile NTP Robot cassette
-   EN9408.01 - 87 multifile NTP Robot cassette
-   EN9409.01 - 90 multifile NTP Robot cassette
-   EN9410.01 - 85 multifile NTP Robot cassette
-   EN9411.01 - 89 multifile NTP Robot cassette
-   EN9412.01 - 99 multifile NTP Robot cassette
-   EN9413.01 - 85 multifile NTP Robot cassette
-   EN9414.01 - 92 multifile NTP Robot cassette
-   EN9415.01 - 99 multifile NTP Robot cassette
-   EN9416.01 - 96 multifile NTP Robot cassette
-   EN9417.01 - 87 multifile NTP Robot cassette
-   EN9418.01 - 86 multifile NTP Robot cassette
-   EN9419.01 - 85 multifile NTP Robot cassette
-   EN9420.01 - 96 multifile NTP Robot cassette
-   EN9421.01 - 90 multifile NTP Robot cassette
-   EN9422.01 - 84 multifile NTP Robot cassette
-   EN9423.01 - 86 multifile NTP Robot cassette
-   EN9424.01 - 84 multifile NTP Robot cassette
-   EN9425.01 - 88 multifile NTP Robot cassette
-   EN9426.01 - 86 multifile NTP Robot cassette
-   EN9427.01 - 92 multifile NTP Robot cassette
-   EN9428.01 - 91 multifile NTP Robot cassette
-   EN9429.01 - 95 multifile NTP Robot cassette
-   EN9430.01 - 99 multifile NTP Robot cassette
-   EN9431.01 - 92 multifile NTP Robot cassette
-   EN9432.01 - 90 multifile NTP Robot cassette
-   EN9433.01 - 39 multifile NTP Robot cassette
-   EN9434.19 - 95 multifile NTP Robot cassette
-   EN9435.01 - 88 multifile NTP Robot cassette
-   EN9436.01 - 89 multifile NTP Robot cassette
-   EN9437.01 - 26 multifile NTP Robot cassette (last run 89752
    on 02.11.98)

------------------------------------------------------------------------

Raw Data of 1997 (Fatmen name *RAWD97*)
---------------------------------------

### Copies done on ED0341-ED0371 DLT2000 multifile

ED0341-ED0371 \*\* VAULT \*\*

EN9341-EN9371 \*\* ARCHIVED \*\*

### COSMICS *Start date 17.5.97*

-   EN9341.02 - 99 multifile NTP Robot cassette
-   EN9342.01 - 70 multifile NTP Robot cassette
-   EN9343.01 - 19 multifile NTP Robot cassette

------------------------------------------------------------------------

### Energy at 91 Gev physics Z-zeroes *Start date 20.7.97*

-   EN9343.20 - 25 multifile NTP Robot cassette
-   EN9344.01 - 99 multifile NTP Robot cassette
-   EN9345.01 - 63 multifile NTP Robot cassette
-   EN9346.01 - 98 multifile NTP Robot cassette
-   EN9342.71 - 87 multifile NTP Robot cassette

------------------------------------------------------------------------

### Energy at 183 Gev *Start date 31.7.97, stop 28.09.97*

-   EN9342.88 - 99 multifile NTP Robot cassette
-   EN9343.26 - 99 multifile NTP Robot cassette
-   EN9345.64 - 99 multifile NTP Robot cassette
-   EN9347.01 - 99 multifile NTP Robot cassette
-   EN9348.01 - 99 multifile NTP Robot cassette
-   EN9349.01 - 85 multifile NTP Robot cassette
-   EN9350.01 - 15 multifile NTP Robot cassette
-   EN9350.01 - 88 multifile NTP Robot cassette
-   EN9351.01 - 85 multifile NTP Robot cassette
-   EN9352.01 - 99 multifile NTP Robot cassette
-   EN9353.01 - 91 multifile NTP Robot cassette
-   EN9354.01 - 80 multifile NTP Robot cassette
-   EN9355.01 - 85 multifile NTP Robot cassette
-   EN9356.01 - 83 multifile NTP Robot cassette
-   EN9357.01 - 79 multifile NTP Robot cassette
-   EN9358.01 - 91 multifile NTP Robot cassette
-   EN9359.01 - 99 multifile NTP Robot cassette
-   EN9360.01 - 97 multifile NTP Robot cassette
-   EN9361.01 - 99 multifile NTP Robot cassette
-   EN9362.01 - 30 multifile NTP Robot cassette
-   EN9363.01 - 66 multifile NTP Robot cassette

------------------------------------------------------------------------

### Energy at 130-136 Gev *Start 3 Oct 1997, end 10 Oct 1997*

-   EN9363.68 - 99 multifile NTP Robot cassette
-   EN9364.01 - 98 multifile NTP Robot cassette
-   EN9362.31 - 96 multifile NTP Robot cassette
-   EN9365.01 - 03 multifile NTP Robot cassette
-   EN9366.01 - 17 multifile NTP Robot cassette

------------------------------------------------------------------------

### Energy at 183-184 Gev *Start date 11.10.97 , end 9.11.97*

-   EN9365.04 - 99 multifile NTP Robot cassette
-   EN9366.01 - 94 multifile NTP Robot cassette
-   EN9367.01 - 99 multifile NTP Robot cassette
-   EN9368.01 - 96 multifile NTP Robot cassette
-   EN9369.01 - 99 multifile NTP robot cassette
-   EN9370.01 - 95 multifile NTP robot cassette
-   EN9371.01 - 44 multifile NTP robot cassette

------------------------------------------------------------------------

Raw Data of 1996 (Fatmen name *RAWD96*)
---------------------------------------

### Copies done on corresponding EK0310-EK0335

ED0310-ED0335 \*\* VAULT \*\*

EK0331-EK0335 \*\* ARCHIVED \*\*

### P1...COSMICS *Start date 3.6.96*

-   ED0310.01 - 93 multifile DLT2000 cassette
-   ED0311.01 - 99 multifile DLT2000 cassette
-   ED0312.01 - 38 multifile DLT2000 cassette
-   ED0320.11 - 14 multifile DLT2000 cassette

### P2...COSMICS *Start date 3.10.96*

-   ED0327.01 - 99 multifile DLT2000 cassette
-   ED0328.01 - 54 multifile DLT2000 cassette
-   ED0329.02 - 09 multifile DLT2000 cassette

------------------------------------------------------------------------

### P1...Energy at 91 Gev physics Z-zeroes *Start date 28.6.96*

-   ED0312.39 - 99 multifile DLT2000 cassette
-   ED0313.01 - 94 multifile DLT2000 cassette
-   ED0314.01 - 52 multifile DLT2000 cassette
-   ED0315.01 - 03 multifile DLT2000 cassette
-   ED0326.25 - 33 multifile DLT2000 cassette

### P2...Energy at 91 Gev physics Z-zeroes *Start date 17.10.96*

-   ED0328.55 - 94 multifile DLT2000 cassette
-   ED0329.10 - 36 multifile DLT2000 cassette
-   ED0333.56 - 60 multifile DLT2000 cassette
-   ED0334.33 - 36 multifile DLT2000 cassette
-   ED0334.94 - 96 multifile DLT2000 cassette
-   ED0335.01 - 04 multifile DLT2000 cassette

------------------------------------------------------------------------

### P1...Energy at 161 Gev *Start date 9.7.96*

-   ED0315.04 - 99 multifile DLT2000 cassette
-   ED0316.01 - 74 multifile DLT2000 cassette
-   ED0317.01 - 94 multifile DLT2000 cassette
-   ED0318.01 - 86 multifile DLT2000 cassette
-   ED0319.01 - 91 multifile DLT2000 cassette
-   ED0320.01 - 10 multifile DLT2000 cassette
-   ED0320.15 - 99 multifile DLT2000 cassette
-   ED0321.01 - 93 multifile DLT2000 cassette
-   ED0322.01 - 94 multifile DLT2000 cassette
-   ED0323.01 - 85 multifile DLT2000 cassette
-   ED0324.01 - 80 multifile DLT2000 cassette
-   ED0325.01 - 78 multifile DLT2000 cassette
-   ED0326.01 - 24 multifile DLT2000 cassette

------------------------------------------------------------------------

### P2...Energy at 172 Gev *Start date 19.10.96*

-   ED0328.95 - 99 multifile DLT2000 cassette
-   ED0329.37 - 97 multifile DLT2000 cassette
-   ED0330.01 - 98 multifile DLT2000 cassette
-   ED0331.01 - 89 multifile DLT2000 cassette
-   ED0332.01 - 84 multifile DLT2000 cassette
-   ED0333.01 - 55 multifile DLT2000 cassette
-   ED0333.61 - 99 multifile DLT2000 cassette
-   ED0334.01 - 32 multifile DLT2000 cassette
-   ED0334.37 - 93 multifile DLT2000 cassette

------------------------------------------------------------------------

### *Shutdown 22 November 1996*

------------------------------------------------------------------------

### From year 1995 on, the raw data are written via CDR (Central Data Recording)

------------------------------------------------------------------------

Raw Data of 1995 (Fatmen name *RAWD95*)
---------------------------------------

### Energy at 91 Gev (P1+P2)

-   ED0251 - ED0295 multifile DLT2000 cassettes \*\* VAULT \*\*
-   EK0251 - EK0295 multifile DLT2000 cassettes \*\* ARCHIVE \*\*
-   ED0296 - ED0298 multifile DLT2000 cassettes \*\* VAULT \*\*
-   EK0296 - EK0298 multifile DLT2000 cassettes \*\* ARCHIVE \*\*

### Energy at 130 Gev (P3)

-   ED0300 - ED0306 multifile DLT2000 cassettes \*\* VAULT \*\*
-   EK0300 - EK0306 multifile DLT2000 cassettes \*\* ARCHIVED \*

### Background data with random triggers MD

-   ED0307 multifile DLT2000 cassette \*\* VAULT \*\*
-   EK0307 multifile DLT2000 cassette \*\*ARCHIVE \*\*

------------------------------------------------------------------------

Raw Data of 1994 (Fatmen name *RAWD94*)
---------------------------------------

-   EQ5291 - EQ9999 single file 3480 cassettes \*\* ARCHIVED \*\*
-   ER0001 - ER0671 single file 3480 cassettes \*\* ARCHIVED \*\*
-   ED0151 - ED0238 multifile DLT2000 cassettes 60 files each \*\* VAULT
    \*\*

### [Details of copies onto DLT's](http://delphiwww.cern.ch/offline/data/raw94.html)

------------------------------------------------------------------------

Raw Data of 1993 (Fatmen name *RAWD93*)
---------------------------------------

-   EQ2401 - EQ5284 single file 3480 cassettes \*\* ARCHIVED \*\*
-   ED0105 - ED0150 multifile DLT2000 cassettes 60 files each \*\* VAULT
    \*\*

### [Details of copies onto DLT's](http://delphiwww.cern.ch/offline/data/raw93.html)

------------------------------------------------------------------------

Raw Data of 1992 (Fatmen name *RAWD92*)
---------------------------------------

-   EP8013 - EP9999 single file 3480 cassettes \*\* ARCHIVED \*\*
-   EQ0001 - EQ2364 single file 3480 cassettes \*\* ARCHIVED \*\*
-   ED0018 - ED0104 multifile DLT2000 cassettes 50 files each \*\* VAULT
    \*\*

### [Details of copies onto DLT's](http://delphiwww.cern.ch/offline/data/raw92.html)

------------------------------------------------------------------------

Raw Data of 1991 (Fatmen name *RAWD91* or *CRAW91*)
---------------------------------------------------

-   EP4001 - EP8000 single file 3480 cassettes \*\* ARCHIVED \*\*
-   ED0005 - ED0017 multifile DLT2000 cas.50 files each DELANA selection
    \*\* VAULT \*\*

### [Details of copies onto DLT's](http://delphiwww.cern.ch/offline/data/raw91.html)

------------------------------------------------------------------------

Raw Data of 1990 (Fatmen name *RAWD90* or *CRAWHAD90*)
------------------------------------------------------

-   EP1006 - EP3334 single file 3480 cassettes \*\* ARCHIVED \*\*
-   ED0001 - ED0004 multifile DLT2000 cas.50 files each DELANA selection
    \*\* VAULT \*\*

### [Details of copies onto DLT's](http://delphiwww.cern.ch/offline/data/raw90.html)

------------------------------------------------------------------------

Raw Data of 1989
----------------

-   EP0001 - EP0969 single file 3480 cassettes \*\* ARCHIVED \*\*

------------------------------------------------------------------------

*Last update Mars 22 2001*

[Anita Bjorkebo](http://consult.cern.ch/xwho/people/00358)\

