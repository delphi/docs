Final LEP2 data set
===================

The final LEP2 data set corresponds to the following reprocessings:

-   **97G**
-   **98E**
-   **99E**
-   **A0E**
-   **A0U** (no TPC sector 6 period)\

For each year two distinct DSTANA versions are available:\

-   **Version 1 corresponds to XSDST format 1.07**, on which most of the
    DELPHI LEP2 analysis are based
-   **Version 2 corresponds to XSDST format 1.08** ( see the
    [description](http://delphiwww.cern.ch/spassoff/des/xshortdes.ps))

The latter includes:\

-    [REMCLU](http://delphiwww.cern.ch/pubxx/delnote/public/2000_164_prog_242.ps.gz)
    clusters in the PV-PA structure, which makes it not generally
    backward compatible with analysis codes based on version 1.07; this
    difference is explained at page 8 of the XSDST documentation;
-   all the software packages are run during the production in their own
    final version, so the information written in the structure is
    supposed to be the best available (while in version 1 several
    packages, i.e. b-tagging, RICH software, REMCLU, should be rerun as
    usual at analysis time)

**Both versions will be kept**. In order to use the version 2 it is
**customary** to use the DSTANA software distribution in the last
prerelease. This can be used also on version 1, although differences are
to be expected with respect to the last release. Both will be kept for
backward compatibility needs of already frozen analyses. A list of news
appeared in the *DELPHI.OFFLINE* newsgroup concerning the last
reprocessing, software and data sets available is given in the
following.\

-   [Sotware Steering Panel of
    24/09/2001](http://delphiwww.cern.ch/pubxx.delwww/news/DELPHI_OFFLINE1777.html)
-   [New XshortDST version
    1.08](http://delphiwww.cern.ch/pubxx.delwww/news/DELPHI_OFFLINE1826.html)
-   [New DSTANA
    software](http://delphiwww.cern.ch/pubxx.delwww/news/DELPHI_OFFLINE1836.html)
-   [A0E2 Monte Carlo
    sets](http://delphiwww.cern.ch/pubxx.delwww/news/DELPHI_OFFLINE1844.html)

------------------------------------------------------------------------

*Last update 15 October 2002,
[F.Cossutti](mailto:fabio.cossutti@cern.ch)*
