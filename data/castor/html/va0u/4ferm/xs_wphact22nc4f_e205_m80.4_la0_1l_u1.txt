*
*   Nickname     : xs_wphact22nc4f_e205_m80.4_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22NC4F/LYON/SUMT/C001-50
*   Description  :  Extended Short DST simulation a0u1 205 , Lyon
*---
*   Comments     : in total 49991 events in 50 files time stamp: Sat Mar  2 18:10:08 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91001.xsdst ! RUN = 91001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91002.xsdst ! RUN = 91002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91003.xsdst ! RUN = 91003 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91004.xsdst ! RUN = 91004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91005.xsdst ! RUN = 91005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91006.xsdst ! RUN = 91006 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91007.xsdst ! RUN = 91007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91008.xsdst ! RUN = 91008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91009.xsdst ! RUN = 91009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91010.xsdst ! RUN = 91010 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91011.xsdst ! RUN = 91011 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91012.xsdst ! RUN = 91012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91013.xsdst ! RUN = 91013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91014.xsdst ! RUN = 91014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91015.xsdst ! RUN = 91015 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91016.xsdst ! RUN = 91016 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91017.xsdst ! RUN = 91017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91018.xsdst ! RUN = 91018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91019.xsdst ! RUN = 91019 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91020.xsdst ! RUN = 91020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91021.xsdst ! RUN = 91021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91022.xsdst ! RUN = 91022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91023.xsdst ! RUN = 91023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91024.xsdst ! RUN = 91024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91025.xsdst ! RUN = 91025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91026.xsdst ! RUN = 91026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91027.xsdst ! RUN = 91027 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91028.xsdst ! RUN = 91028 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91029.xsdst ! RUN = 91029 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91030.xsdst ! RUN = 91030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91031.xsdst ! RUN = 91031 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91032.xsdst ! RUN = 91032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91033.xsdst ! RUN = 91033 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91034.xsdst ! RUN = 91034 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91035.xsdst ! RUN = 91035 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91036.xsdst ! RUN = 91036 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91037.xsdst ! RUN = 91037 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91038.xsdst ! RUN = 91038 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91039.xsdst ! RUN = 91039 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91040.xsdst ! RUN = 91040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91041.xsdst ! RUN = 91041 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91042.xsdst ! RUN = 91042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91043.xsdst ! RUN = 91043 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91044.xsdst ! RUN = 91044 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91045.xsdst ! RUN = 91045 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91046.xsdst ! RUN = 91046 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91047.xsdst ! RUN = 91047 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91048.xsdst ! RUN = 91048 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91049.xsdst ! RUN = 91049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_nc4f_205_80.4_91050.xsdst ! RUN = 91050 ! NEVT = 1000
