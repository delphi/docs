*
*   Nickname     : xs_bdkeeeelt1gev_e206.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/BDKEEEELT1GEV/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 49606 events in 10 files time stamp: Tue Feb  5 11:34:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60441.xsdst ! RUN = 60441 ! NEVT = 4902
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60442.xsdst ! RUN = 60442 ! NEVT = 4896
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60443.xsdst ! RUN = 60443 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60444.xsdst ! RUN = 60444 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60445.xsdst ! RUN = 60445 ! NEVT = 4917
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60446.xsdst ! RUN = 60446 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60447.xsdst ! RUN = 60447 ! NEVT = 4967
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60448.xsdst ! RUN = 60448 ! NEVT = 4926
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60449.xsdst ! RUN = 60449 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/bdk/va0u/206.5/bdk_eeee_lt1gev_e206.5_60450.xsdst ! RUN = 60450 ! NEVT = 5000
