*
*   Nickname     : xs_bdk02eeee_e205_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/BDK02EEEE/RAL/SUMT/C001-15
*   Description  :  Extended Short DST simulation a0u1 205 , RAL
*---
*   Comments     : in total 75000 events in 15 files time stamp: Sat Mar  2 01:10:13 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61002.xsdst ! RUN = 61002 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61003.xsdst ! RUN = 61003 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61004.xsdst ! RUN = 61004 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61005.xsdst ! RUN = 61005 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61006.xsdst ! RUN = 61006 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61007.xsdst ! RUN = 61007 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61008.xsdst ! RUN = 61008 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61010.xsdst ! RUN = 61010 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61011.xsdst ! RUN = 61011 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61012.xsdst ! RUN = 61012 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61013.xsdst ! RUN = 61013 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61014.xsdst ! RUN = 61014 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61018.xsdst ! RUN = 61018 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61019.xsdst ! RUN = 61019 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/bdk02/va0u/205/bdk02_eeee_205_61020.xsdst ! RUN = 61020 ! NEVT = 5000
