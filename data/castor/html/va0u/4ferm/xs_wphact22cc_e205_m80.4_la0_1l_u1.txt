*
*   Nickname     : xs_wphact22cc_e205_m80.4_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CC/LYON/SUMT/C001-22
*   Description  :  Extended Short DST simulation a0u1 205 , Lyon
*---
*   Comments     : in total 22384 events in 22 files time stamp: Wed Mar  6 15:10:20 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90007.xsdst ! RUN = 90007 ! NEVT = 977
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90008.xsdst ! RUN = 90008 ! NEVT = 418
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90009.xsdst ! RUN = 90009 ! NEVT = 1805
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90010.xsdst ! RUN = 90010 ! NEVT = 1817
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90011.xsdst ! RUN = 90011 ! NEVT = 148
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90012.xsdst ! RUN = 90012 ! NEVT = 201
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90013.xsdst ! RUN = 90013 ! NEVT = 291
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90014.xsdst ! RUN = 90014 ! NEVT = 212
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90015.xsdst ! RUN = 90015 ! NEVT = 219
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90018.xsdst ! RUN = 90018 ! NEVT = 51
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90019.xsdst ! RUN = 90019 ! NEVT = 177
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90020.xsdst ! RUN = 90020 ! NEVT = 1892
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90021.xsdst ! RUN = 90021 ! NEVT = 666
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90022.xsdst ! RUN = 90022 ! NEVT = 1925
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90024.xsdst ! RUN = 90024 ! NEVT = 1806
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90025.xsdst ! RUN = 90025 ! NEVT = 1151
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90026.xsdst ! RUN = 90026 ! NEVT = 1032
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90027.xsdst ! RUN = 90027 ! NEVT = 1948
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90029.xsdst ! RUN = 90029 ! NEVT = 1936
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90031.xsdst ! RUN = 90031 ! NEVT = 1630
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90033.xsdst ! RUN = 90033 ! NEVT = 1299
FILE = /castor/cern.ch/delphi/MCprod/lyon/wphact22/va0u/205/wphact22_cc_205_80.4_90036.xsdst ! RUN = 90036 ! NEVT = 783
