*
*   Nickname     : xs_hzha03pyth6156hsee_e206.5_m72.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1993 events in 4 files time stamp: Fri Nov 23 14:40:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_72.5_67250.xsdst ! RUN = 67250 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_72.5_67251.xsdst ! RUN = 67251 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_72.5_67252.xsdst ! RUN = 67252 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_72.5_67253.xsdst ! RUN = 67253 ! NEVT = 499
