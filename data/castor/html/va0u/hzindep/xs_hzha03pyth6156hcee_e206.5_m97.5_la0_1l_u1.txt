*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m97.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sat Nov 24 10:15:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_97.5_69754.xsdst ! RUN = 69754 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_97.5_69755.xsdst ! RUN = 69755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_97.5_69756.xsdst ! RUN = 69756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_97.5_69757.xsdst ! RUN = 69757 ! NEVT = 499
