*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m105.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1997 events in 4 files time stamp: Tue Nov 27 18:17:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_105.0_110508.xsdst ! RUN = 110508 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_105.0_110509.xsdst ! RUN = 110509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_105.0_110510.xsdst ! RUN = 110510 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_105.0_110511.xsdst ! RUN = 110511 ! NEVT = 500
