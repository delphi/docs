*
*   Nickname     : xs_hzha03pyth6156hgee_e206.5_m85.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1996 events in 4 files time stamp: Fri Nov 23 14:41:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_85.0_68508.xsdst ! RUN = 68508 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_85.0_68509.xsdst ! RUN = 68509 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_85.0_68510.xsdst ! RUN = 68510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_85.0_68511.xsdst ! RUN = 68511 ! NEVT = 499
