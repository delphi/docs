*
*   Nickname     : xs_hzha03pyth6156hcnn_e206.5_m45.0_ka0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Tue Jan 15 15:39:22 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_45.0_7911.xsdst ! RUN = 7911 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_45.0_7912.xsdst ! RUN = 7912 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_45.0_7913.xsdst ! RUN = 7913 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_45.0_7914.xsdst ! RUN = 7914 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_45.0_7915.xsdst ! RUN = 7915 ! NEVT = 400
