*
*   Nickname     : xs_hzha03pyth6156hgee_e206.5_m75.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E206.5/LYON/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1499 events in 3 files time stamp: Fri Nov 23 14:42:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_75.0_67509.xsdst ! RUN = 67509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_75.0_67510.xsdst ! RUN = 67510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_75.0_67511.xsdst ! RUN = 67511 ! NEVT = 499
