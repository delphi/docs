*
*   Nickname     : xs_hzha03pyth6156hcnn_e206.5_m77.5_ka0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Tue Jan 15 15:39:26 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_77.5_7976.xsdst ! RUN = 7976 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_77.5_7977.xsdst ! RUN = 7977 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_77.5_7978.xsdst ! RUN = 7978 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_77.5_7979.xsdst ! RUN = 7979 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_77.5_7980.xsdst ! RUN = 7980 ! NEVT = 400
