*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m50.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1994 events in 4 files time stamp: Mon Nov 26 16:16:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_50.0_105008.xsdst ! RUN = 105008 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_50.0_105009.xsdst ! RUN = 105009 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_50.0_105010.xsdst ! RUN = 105010 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_50.0_105011.xsdst ! RUN = 105011 ! NEVT = 498
