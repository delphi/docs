*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m52.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1999 events in 4 files time stamp: Fri Nov 23 12:22:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_52.5_65254.xsdst ! RUN = 65254 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_52.5_65255.xsdst ! RUN = 65255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_52.5_65256.xsdst ! RUN = 65256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_52.5_65257.xsdst ! RUN = 65257 ! NEVT = 500
