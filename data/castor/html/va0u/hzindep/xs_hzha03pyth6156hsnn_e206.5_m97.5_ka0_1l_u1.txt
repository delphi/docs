*
*   Nickname     : xs_hzha03pyth6156hsnn_e206.5_m97.5_ka0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation a0_u1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Tue Jan 15 15:39:36 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsnn_206.5_97.5_7861.xsdst ! RUN = 7861 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsnn_206.5_97.5_7862.xsdst ! RUN = 7862 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsnn_206.5_97.5_7863.xsdst ! RUN = 7863 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsnn_206.5_97.5_7864.xsdst ! RUN = 7864 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsnn_206.5_97.5_7865.xsdst ! RUN = 7865 ! NEVT = 400
