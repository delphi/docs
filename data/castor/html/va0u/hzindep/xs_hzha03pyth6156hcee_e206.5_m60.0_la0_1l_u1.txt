*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m60.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1994 events in 4 files time stamp: Fri Nov 23 12:21:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_60.0_66004.xsdst ! RUN = 66004 ! NEVT = 496
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_60.0_66005.xsdst ! RUN = 66005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_60.0_66006.xsdst ! RUN = 66006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_60.0_66007.xsdst ! RUN = 66007 ! NEVT = 499
