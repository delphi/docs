*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m107.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/LYON/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1498 events in 3 files time stamp: Wed Nov 28 06:28:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_107.5_110759.xsdst ! RUN = 110759 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_107.5_110760.xsdst ! RUN = 110760 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_107.5_110761.xsdst ! RUN = 110761 ! NEVT = 498
