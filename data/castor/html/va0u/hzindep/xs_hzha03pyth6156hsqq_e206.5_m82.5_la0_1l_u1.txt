*
*   Nickname     : xs_hzha03pyth6156hsqq_e206.5_m82.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1998 events in 4 files time stamp: Tue Nov 27 08:18:19 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_82.5_108250.xsdst ! RUN = 108250 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_82.5_108251.xsdst ! RUN = 108251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_82.5_108252.xsdst ! RUN = 108252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_82.5_108253.xsdst ! RUN = 108253 ! NEVT = 499
