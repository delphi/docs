*
*   Nickname     : xs_hzha03pyth6156hcmm_e206.5_m105.0_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Dec  6 06:43:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_105.0_90505.xsdst ! RUN = 90505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_105.0_90506.xsdst ! RUN = 90506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_105.0_90507.xsdst ! RUN = 90507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_105.0_90508.xsdst ! RUN = 90508 ! NEVT = 500
