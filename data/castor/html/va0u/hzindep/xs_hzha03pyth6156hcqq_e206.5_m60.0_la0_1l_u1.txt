*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m60.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1999 events in 4 files time stamp: Mon Nov 26 16:16:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_60.0_106004.xsdst ! RUN = 106004 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_60.0_106005.xsdst ! RUN = 106005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_60.0_106006.xsdst ! RUN = 106006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_60.0_106007.xsdst ! RUN = 106007 ! NEVT = 500
