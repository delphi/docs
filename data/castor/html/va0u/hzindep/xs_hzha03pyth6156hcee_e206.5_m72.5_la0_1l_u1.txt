*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m72.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 23 14:40:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_72.5_67254.xsdst ! RUN = 67254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_72.5_67255.xsdst ! RUN = 67255 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_72.5_67256.xsdst ! RUN = 67256 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_72.5_67257.xsdst ! RUN = 67257 ! NEVT = 500
