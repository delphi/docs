*
*   Nickname     : xs_hzha03pyth6156hgmm_e206.5_m42.5_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E206.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Dec  6 06:43:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_42.5_84259.xsdst ! RUN = 84259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_42.5_84260.xsdst ! RUN = 84260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_42.5_84261.xsdst ! RUN = 84261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_42.5_84262.xsdst ! RUN = 84262 ! NEVT = 500
