*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m82.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1999 events in 4 files time stamp: Tue Nov 27 12:16:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_82.5_108254.xsdst ! RUN = 108254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_82.5_108255.xsdst ! RUN = 108255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_82.5_108256.xsdst ! RUN = 108256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_82.5_108257.xsdst ! RUN = 108257 ! NEVT = 499
