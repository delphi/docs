*
*   Nickname     : xs_hzha03pyth6156hsmm_e206.5_m105.0_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E206.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Dec  5 18:33:19 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsmm_206.5_105.0_90501.xsdst ! RUN = 90501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsmm_206.5_105.0_90502.xsdst ! RUN = 90502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsmm_206.5_105.0_90503.xsdst ! RUN = 90503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsmm_206.5_105.0_90504.xsdst ! RUN = 90504 ! NEVT = 500
