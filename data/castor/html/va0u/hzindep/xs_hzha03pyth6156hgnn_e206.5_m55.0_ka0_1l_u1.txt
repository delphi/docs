*
*   Nickname     : xs_hzha03pyth6156hgnn_e206.5_m55.0_ka0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation a0_u1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1998 events in 5 files time stamp: Tue Jan 15 15:39:26 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgnn_206.5_55.0_7621.xsdst ! RUN = 7621 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgnn_206.5_55.0_7622.xsdst ! RUN = 7622 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgnn_206.5_55.0_7623.xsdst ! RUN = 7623 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgnn_206.5_55.0_7624.xsdst ! RUN = 7624 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgnn_206.5_55.0_7625.xsdst ! RUN = 7625 ! NEVT = 399
