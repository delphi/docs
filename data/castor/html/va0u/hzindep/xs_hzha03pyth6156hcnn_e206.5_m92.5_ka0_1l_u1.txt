*
*   Nickname     : xs_hzha03pyth6156hcnn_e206.5_m92.5_ka0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1998 events in 5 files time stamp: Tue Jan 15 15:39:19 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_92.5_8006.xsdst ! RUN = 8006 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_92.5_8007.xsdst ! RUN = 8007 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_92.5_8008.xsdst ! RUN = 8008 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_92.5_8009.xsdst ! RUN = 8009 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcnn_206.5_92.5_8010.xsdst ! RUN = 8010 ! NEVT = 399
