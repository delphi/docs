*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m67.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1998 events in 4 files time stamp: Fri Nov 23 14:40:17 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_67.5_66754.xsdst ! RUN = 66754 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_67.5_66755.xsdst ! RUN = 66755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_67.5_66756.xsdst ! RUN = 66756 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_67.5_66757.xsdst ! RUN = 66757 ! NEVT = 500
