*
*   Nickname     : xs_hzha03pyth6156hsee_e206.5_m82.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E206.5/LYON/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1499 events in 3 files time stamp: Sun Nov 25 06:21:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_82.5_68251.xsdst ! RUN = 68251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_82.5_68252.xsdst ! RUN = 68252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_82.5_68253.xsdst ! RUN = 68253 ! NEVT = 500
