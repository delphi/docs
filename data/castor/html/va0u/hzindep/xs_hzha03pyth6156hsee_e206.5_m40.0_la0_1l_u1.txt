*
*   Nickname     : xs_hzha03pyth6156hsee_e206.5_m40.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1999 events in 4 files time stamp: Wed Nov 21 14:24:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_40.0_64000.xsdst ! RUN = 64000 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_40.0_64001.xsdst ! RUN = 64001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_40.0_64002.xsdst ! RUN = 64002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_40.0_64003.xsdst ! RUN = 64003 ! NEVT = 499
