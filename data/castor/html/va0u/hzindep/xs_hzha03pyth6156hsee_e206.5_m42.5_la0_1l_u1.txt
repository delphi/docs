*
*   Nickname     : xs_hzha03pyth6156hsee_e206.5_m42.5_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E206.5/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 1997 events in 4 files time stamp: Tue Nov 20 22:15:54 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_42.5_64250.xsdst ! RUN = 64250 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_42.5_64251.xsdst ! RUN = 64251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_42.5_64252.xsdst ! RUN = 64252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_42.5_64253.xsdst ! RUN = 64253 ! NEVT = 498
