*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m90.0_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 2000 events in 4 files time stamp: Tue Nov 27 18:18:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_90.0_109004.xsdst ! RUN = 109004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_90.0_109005.xsdst ! RUN = 109005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_90.0_109006.xsdst ! RUN = 109006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_90.0_109007.xsdst ! RUN = 109007 ! NEVT = 500
