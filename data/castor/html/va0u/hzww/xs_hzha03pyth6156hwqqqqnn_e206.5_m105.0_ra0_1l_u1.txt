*
*   Nickname     : xs_hzha03pyth6156hwqqqqnn_e206.5_m105.0_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQNN/E206.5/RAL/SUMT/C001-11
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 5498 events in 11 files time stamp: Sat Aug  3 15:13:47 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107503.xsdst ! RUN = 107503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107504.xsdst ! RUN = 107504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107507.xsdst ! RUN = 107507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107508.xsdst ! RUN = 107508 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107509.xsdst ! RUN = 107509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107510.xsdst ! RUN = 107510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107511.xsdst ! RUN = 107511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107512.xsdst ! RUN = 107512 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107513.xsdst ! RUN = 107513 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107514.xsdst ! RUN = 107514 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_105.0_107515.xsdst ! RUN = 107515 ! NEVT = 499
