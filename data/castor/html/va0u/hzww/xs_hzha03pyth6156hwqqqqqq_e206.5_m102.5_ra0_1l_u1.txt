*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e206.5_m102.5_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E206.5/RAL/SUMT/C001-9
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4497 events in 9 files time stamp: Sat Aug  3 01:14:11 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106251.xsdst ! RUN = 106251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106252.xsdst ! RUN = 106252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106254.xsdst ! RUN = 106254 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106255.xsdst ! RUN = 106255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106256.xsdst ! RUN = 106256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106259.xsdst ! RUN = 106259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106260.xsdst ! RUN = 106260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106262.xsdst ! RUN = 106262 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_106264.xsdst ! RUN = 106264 ! NEVT = 500
