*
*   Nickname     : xs_hzha03pyth6156hwqqqqnn_e206.5_m110.0_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQNN/E206.5/RAL/SUMT/C001-12
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 6000 events in 12 files time stamp: Sat Aug  3 15:13:45 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108001.xsdst ! RUN = 108001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108002.xsdst ! RUN = 108002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108003.xsdst ! RUN = 108003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108004.xsdst ! RUN = 108004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108005.xsdst ! RUN = 108005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108006.xsdst ! RUN = 108006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108007.xsdst ! RUN = 108007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108008.xsdst ! RUN = 108008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108010.xsdst ! RUN = 108010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108012.xsdst ! RUN = 108012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108013.xsdst ! RUN = 108013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqqqnn_206.5_110.0_108014.xsdst ! RUN = 108014 ! NEVT = 500
