*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e206.5_m120.0_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E206.5/RAL/SUMT/C001-12
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 5812 events in 12 files time stamp: Sat Aug  3 06:13:55 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110001.xsdst ! RUN = 110001 ! NEVT = 312
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110002.xsdst ! RUN = 110002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110003.xsdst ! RUN = 110003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110004.xsdst ! RUN = 110004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110005.xsdst ! RUN = 110005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110006.xsdst ! RUN = 110006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110007.xsdst ! RUN = 110007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110008.xsdst ! RUN = 110008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110009.xsdst ! RUN = 110009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110010.xsdst ! RUN = 110010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110014.xsdst ! RUN = 110014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hwqqlnqq_206.5_120.0_110015.xsdst ! RUN = 110015 ! NEVT = 500
