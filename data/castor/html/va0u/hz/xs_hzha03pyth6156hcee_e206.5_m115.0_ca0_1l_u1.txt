*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m115.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1995 events in 4 files time stamp: Wed Mar 27 10:11:09 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_115.0_91525.xsdst ! RUN = 91525 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_115.0_91526.xsdst ! RUN = 91526 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_115.0_91527.xsdst ! RUN = 91527 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_115.0_91528.xsdst ! RUN = 91528 ! NEVT = 499
