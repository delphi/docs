*
*   Nickname     : xs_hzha03pyth6156hcmm_e206.5_m77.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sun Mar 24 15:10:48 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_77.5_87755.xsdst ! RUN = 87755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_77.5_87756.xsdst ! RUN = 87756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_77.5_87757.xsdst ! RUN = 87757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_77.5_87758.xsdst ! RUN = 87758 ! NEVT = 500
