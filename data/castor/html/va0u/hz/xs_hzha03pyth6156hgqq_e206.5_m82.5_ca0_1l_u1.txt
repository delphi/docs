*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m82.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1997 events in 4 files time stamp: Tue Mar 26 06:10:52 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_82.5_88299.xsdst ! RUN = 88299 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_82.5_88300.xsdst ! RUN = 88300 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_82.5_88301.xsdst ! RUN = 88301 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_82.5_88302.xsdst ! RUN = 88302 ! NEVT = 499
