*
*   Nickname     : xs_hzha03pyth6156hzqq_e206.5_m120_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Wed Aug 22 10:11:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22101.xsdst ! RUN = 22101 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22102.xsdst ! RUN = 22102 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22103.xsdst ! RUN = 22103 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22104.xsdst ! RUN = 22104 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22105.xsdst ! RUN = 22105 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22106.xsdst ! RUN = 22106 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22107.xsdst ! RUN = 22107 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22108.xsdst ! RUN = 22108 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22109.xsdst ! RUN = 22109 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_120_22110.xsdst ! RUN = 22110 ! NEVT = 499
