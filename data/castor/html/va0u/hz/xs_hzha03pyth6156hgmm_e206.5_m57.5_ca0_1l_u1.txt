*
*   Nickname     : xs_hzha03pyth6156hgmm_e206.5_m57.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1999 events in 4 files time stamp: Mon Mar 25 06:11:17 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_57.5_85759.xsdst ! RUN = 85759 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_57.5_85760.xsdst ! RUN = 85760 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_57.5_85761.xsdst ! RUN = 85761 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_57.5_85762.xsdst ! RUN = 85762 ! NEVT = 500
