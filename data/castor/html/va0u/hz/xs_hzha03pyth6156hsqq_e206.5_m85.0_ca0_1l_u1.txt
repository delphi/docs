*
*   Nickname     : xs_hzha03pyth6156hsqq_e206.5_m85.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1999 events in 4 files time stamp: Thu Mar 28 18:13:00 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_85.0_88541.xsdst ! RUN = 88541 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_85.0_88542.xsdst ! RUN = 88542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_85.0_88543.xsdst ! RUN = 88543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsqq_206.5_85.0_88544.xsdst ! RUN = 88544 ! NEVT = 499
