*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m85.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/CERN/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1497 events in 3 files time stamp: Tue Mar 26 06:10:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_85.0_88549.xsdst ! RUN = 88549 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_85.0_88550.xsdst ! RUN = 88550 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_85.0_88551.xsdst ! RUN = 88551 ! NEVT = 499
