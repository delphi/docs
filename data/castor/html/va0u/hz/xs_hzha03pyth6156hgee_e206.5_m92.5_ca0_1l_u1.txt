*
*   Nickname     : xs_hzha03pyth6156hgee_e206.5_m92.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Sun Mar 31 06:12:35 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_92.5_89279.xsdst ! RUN = 89279 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_92.5_89280.xsdst ! RUN = 89280 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_92.5_89281.xsdst ! RUN = 89281 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_92.5_89282.xsdst ! RUN = 89282 ! NEVT = 500
