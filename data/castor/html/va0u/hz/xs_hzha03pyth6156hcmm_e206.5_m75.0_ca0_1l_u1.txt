*
*   Nickname     : xs_hzha03pyth6156hcmm_e206.5_m75.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sun Mar 24 15:10:52 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_75.0_87505.xsdst ! RUN = 87505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_75.0_87506.xsdst ! RUN = 87506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_75.0_87507.xsdst ! RUN = 87507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcmm_206.5_75.0_87508.xsdst ! RUN = 87508 ! NEVT = 500
