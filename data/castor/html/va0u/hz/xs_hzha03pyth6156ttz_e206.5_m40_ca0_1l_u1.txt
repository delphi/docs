*
*   Nickname     : xs_hzha03pyth6156ttz_e206.5_m40_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Wed Aug 29 00:11:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14151.xsdst ! RUN = 14151 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14152.xsdst ! RUN = 14152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14153.xsdst ! RUN = 14153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14154.xsdst ! RUN = 14154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14155.xsdst ! RUN = 14155 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14156.xsdst ! RUN = 14156 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14157.xsdst ! RUN = 14157 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14158.xsdst ! RUN = 14158 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14159.xsdst ! RUN = 14159 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_40_14160.xsdst ! RUN = 14160 ! NEVT = 500
