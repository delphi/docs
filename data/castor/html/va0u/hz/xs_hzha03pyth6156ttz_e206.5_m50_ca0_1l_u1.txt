*
*   Nickname     : xs_hzha03pyth6156ttz_e206.5_m50_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Wed Aug 29 02:12:30 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15151.xsdst ! RUN = 15151 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15152.xsdst ! RUN = 15152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15153.xsdst ! RUN = 15153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15154.xsdst ! RUN = 15154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15155.xsdst ! RUN = 15155 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15156.xsdst ! RUN = 15156 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15157.xsdst ! RUN = 15157 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15158.xsdst ! RUN = 15158 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15159.xsdst ! RUN = 15159 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ttz_206.5_50_15160.xsdst ! RUN = 15160 ! NEVT = 499
