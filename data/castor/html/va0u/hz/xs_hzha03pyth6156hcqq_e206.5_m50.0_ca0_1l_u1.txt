*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m50.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Fri Mar 29 12:14:13 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_50.0_85045.xsdst ! RUN = 85045 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_50.0_85046.xsdst ! RUN = 85046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_50.0_85047.xsdst ! RUN = 85047 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_50.0_85048.xsdst ! RUN = 85048 ! NEVT = 500
