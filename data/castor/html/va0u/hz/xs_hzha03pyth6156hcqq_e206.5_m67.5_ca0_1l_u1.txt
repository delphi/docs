*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m67.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Fri Mar 29 18:11:51 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_67.5_86795.xsdst ! RUN = 86795 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_67.5_86796.xsdst ! RUN = 86796 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_67.5_86797.xsdst ! RUN = 86797 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_67.5_86798.xsdst ! RUN = 86798 ! NEVT = 500
