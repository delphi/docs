*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m87.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1996 events in 4 files time stamp: Wed Mar 27 01:11:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_87.5_88775.xsdst ! RUN = 88775 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_87.5_88776.xsdst ! RUN = 88776 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_87.5_88777.xsdst ! RUN = 88777 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_87.5_88778.xsdst ! RUN = 88778 ! NEVT = 499
