*
*   Nickname     : xs_hzha03pyth6156hgmm_e206.5_m92.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Mar 25 10:11:38 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_92.5_89259.xsdst ! RUN = 89259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_92.5_89260.xsdst ! RUN = 89260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_92.5_89261.xsdst ! RUN = 89261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_92.5_89262.xsdst ! RUN = 89262 ! NEVT = 500
