*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m112.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Mar 30 10:13:19 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_112.5_91295.xsdst ! RUN = 91295 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_112.5_91296.xsdst ! RUN = 91296 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_112.5_91297.xsdst ! RUN = 91297 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_112.5_91298.xsdst ! RUN = 91298 ! NEVT = 500
