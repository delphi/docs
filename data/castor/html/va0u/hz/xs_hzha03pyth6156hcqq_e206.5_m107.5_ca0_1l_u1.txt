*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m107.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Sat Mar 30 12:22:16 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_107.5_90795.xsdst ! RUN = 90795 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_107.5_90796.xsdst ! RUN = 90796 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_107.5_90797.xsdst ! RUN = 90797 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcqq_206.5_107.5_90798.xsdst ! RUN = 90798 ! NEVT = 500
