*
*   Nickname     : xs_hzha03pyth6156hgmm_e206.5_m97.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E206.5/CERN/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1500 events in 3 files time stamp: Mon Mar 25 10:11:23 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_97.5_89759.xsdst ! RUN = 89759 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_97.5_89760.xsdst ! RUN = 89760 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgmm_206.5_97.5_89761.xsdst ! RUN = 89761 ! NEVT = 500
