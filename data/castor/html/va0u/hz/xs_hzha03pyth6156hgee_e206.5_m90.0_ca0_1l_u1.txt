*
*   Nickname     : xs_hzha03pyth6156hgee_e206.5_m90.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E206.5/CERN/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1499 events in 3 files time stamp: Sun Mar 31 06:11:59 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_90.0_89029.xsdst ! RUN = 89029 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_90.0_89030.xsdst ! RUN = 89030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgee_206.5_90.0_89031.xsdst ! RUN = 89031 ! NEVT = 499
