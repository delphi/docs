*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m45.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1999 events in 4 files time stamp: Tue Mar 26 12:14:56 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_45.0_84525.xsdst ! RUN = 84525 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_45.0_84526.xsdst ! RUN = 84526 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_45.0_84527.xsdst ! RUN = 84527 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hcee_206.5_45.0_84528.xsdst ! RUN = 84528 ! NEVT = 499
