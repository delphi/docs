*
*   Nickname     : xs_hzha03pyth6156hsee_e206.5_m60.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1996 events in 4 files time stamp: Mon Mar 25 22:10:40 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_60.0_86021.xsdst ! RUN = 86021 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_60.0_86022.xsdst ! RUN = 86022 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_60.0_86023.xsdst ! RUN = 86023 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_60.0_86024.xsdst ! RUN = 86024 ! NEVT = 499
