*
*   Nickname     : xs_hzha03pyth6156hsee_e206.5_m70.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 1996 events in 4 files time stamp: Mon Mar 25 22:10:49 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_70.0_87021.xsdst ! RUN = 87021 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_70.0_87022.xsdst ! RUN = 87022 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_70.0_87023.xsdst ! RUN = 87023 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hsee_206.5_70.0_87024.xsdst ! RUN = 87024 ! NEVT = 499
