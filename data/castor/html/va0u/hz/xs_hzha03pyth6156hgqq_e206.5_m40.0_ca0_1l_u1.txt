*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m40.0_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Mar 30 10:12:43 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_40.0_84049.xsdst ! RUN = 84049 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_40.0_84050.xsdst ! RUN = 84050 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_40.0_84051.xsdst ! RUN = 84051 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hgqq_206.5_40.0_84052.xsdst ! RUN = 84052 ! NEVT = 500
