*
*   Nickname     : xs_hzha03pyth6156hzqq_e206.5_m100_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Tue Aug 21 17:13:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20101.xsdst ! RUN = 20101 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20102.xsdst ! RUN = 20102 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20103.xsdst ! RUN = 20103 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20104.xsdst ! RUN = 20104 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20105.xsdst ! RUN = 20105 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20106.xsdst ! RUN = 20106 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20107.xsdst ! RUN = 20107 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20108.xsdst ! RUN = 20108 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20109.xsdst ! RUN = 20109 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzqq_206.5_100_20110.xsdst ! RUN = 20110 ! NEVT = 499
