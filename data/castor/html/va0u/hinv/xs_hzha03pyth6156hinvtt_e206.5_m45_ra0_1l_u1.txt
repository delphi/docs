*
*   Nickname     : xs_hzha03pyth6156hinvtt_e206.5_m45_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 12 14:14:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvtt_206.5_45_34660.xsdst ! RUN = 34660 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvtt_206.5_45_34661.xsdst ! RUN = 34661 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvtt_206.5_45_34662.xsdst ! RUN = 34662 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvtt_206.5_45_34663.xsdst ! RUN = 34663 ! NEVT = 500
