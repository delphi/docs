*
*   Nickname     : xs_hzha03pyth6156hinvee_e206.5_m60_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 12 12:12:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_60_36120.xsdst ! RUN = 36120 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_60_36121.xsdst ! RUN = 36121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_60_36122.xsdst ! RUN = 36122 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_60_36123.xsdst ! RUN = 36123 ! NEVT = 500
