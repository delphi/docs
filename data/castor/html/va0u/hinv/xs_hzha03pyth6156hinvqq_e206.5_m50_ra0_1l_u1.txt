*
*   Nickname     : xs_hzha03pyth6156hinvqq_e206.5_m50_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E206.5/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Tue Sep 11 12:11:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35100.xsdst ! RUN = 35100 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35101.xsdst ! RUN = 35101 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35102.xsdst ! RUN = 35102 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35103.xsdst ! RUN = 35103 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35104.xsdst ! RUN = 35104 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35105.xsdst ! RUN = 35105 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35106.xsdst ! RUN = 35106 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35107.xsdst ! RUN = 35107 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35108.xsdst ! RUN = 35108 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvqq_206.5_50_35109.xsdst ! RUN = 35109 ! NEVT = 500
