*
*   Nickname     : xs_hzha03pyth6156hinvee_e206.5_m115_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Sep 12 13:11:58 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_115_41620.xsdst ! RUN = 41620 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_115_41621.xsdst ! RUN = 41621 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_115_41622.xsdst ! RUN = 41622 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvee_206.5_115_41623.xsdst ! RUN = 41623 ! NEVT = 500
