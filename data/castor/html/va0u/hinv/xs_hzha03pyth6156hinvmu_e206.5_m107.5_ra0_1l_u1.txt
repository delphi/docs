*
*   Nickname     : xs_hzha03pyth6156hinvmu_e206.5_m107.5_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 12 13:11:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvmu_206.5_107.5_40890.xsdst ! RUN = 40890 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvmu_206.5_107.5_40891.xsdst ! RUN = 40891 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvmu_206.5_107.5_40892.xsdst ! RUN = 40892 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hinvmu_206.5_107.5_40893.xsdst ! RUN = 40893 ! NEVT = 500
