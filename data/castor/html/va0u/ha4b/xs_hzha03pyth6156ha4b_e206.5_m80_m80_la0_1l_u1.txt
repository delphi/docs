*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m80_m80_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/LYON/SUMT/C001-13
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 6496 events in 13 files time stamp: Tue Oct  2 07:15:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20253.xsdst ! RUN = 20253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20255.xsdst ! RUN = 20255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20256.xsdst ! RUN = 20256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20481.xsdst ! RUN = 20481 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20482.xsdst ! RUN = 20482 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20483.xsdst ! RUN = 20483 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20484.xsdst ! RUN = 20484 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20485.xsdst ! RUN = 20485 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20486.xsdst ! RUN = 20486 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20487.xsdst ! RUN = 20487 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20488.xsdst ! RUN = 20488 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20489.xsdst ! RUN = 20489 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_80_80_20490.xsdst ! RUN = 20490 ! NEVT = 500
