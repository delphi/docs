*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m30_m50_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/LYON/SUMT/C001-11
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 5494 events in 11 files time stamp: Mon Oct  1 22:09:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20170.xsdst ! RUN = 20170 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20171.xsdst ! RUN = 20171 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20172.xsdst ! RUN = 20172 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20173.xsdst ! RUN = 20173 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20174.xsdst ! RUN = 20174 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20175.xsdst ! RUN = 20175 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20176.xsdst ! RUN = 20176 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20177.xsdst ! RUN = 20177 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20178.xsdst ! RUN = 20178 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20179.xsdst ! RUN = 20179 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_50_20180.xsdst ! RUN = 20180 ! NEVT = 500
