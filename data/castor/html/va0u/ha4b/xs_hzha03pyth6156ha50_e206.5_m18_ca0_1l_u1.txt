*
*   Nickname     : xs_hzha03pyth6156ha50_e206.5_m18_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Thu Nov  8 06:19:33 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21901.xsdst ! RUN = 21901 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21902.xsdst ! RUN = 21902 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21903.xsdst ! RUN = 21903 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21904.xsdst ! RUN = 21904 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21905.xsdst ! RUN = 21905 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21906.xsdst ! RUN = 21906 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21907.xsdst ! RUN = 21907 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21908.xsdst ! RUN = 21908 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21909.xsdst ! RUN = 21909 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50_206.5_18_21910.xsdst ! RUN = 21910 ! NEVT = 500
