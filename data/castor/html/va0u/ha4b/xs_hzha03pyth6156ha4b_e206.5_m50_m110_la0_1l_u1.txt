*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m50_m110_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/LYON/SUMT/C001-11
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 5491 events in 11 files time stamp: Mon Oct  1 22:09:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20310.xsdst ! RUN = 20310 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20311.xsdst ! RUN = 20311 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20312.xsdst ! RUN = 20312 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20313.xsdst ! RUN = 20313 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20314.xsdst ! RUN = 20314 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20315.xsdst ! RUN = 20315 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20316.xsdst ! RUN = 20316 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20317.xsdst ! RUN = 20317 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20318.xsdst ! RUN = 20318 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20319.xsdst ! RUN = 20319 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_50_110_20320.xsdst ! RUN = 20320 ! NEVT = 500
