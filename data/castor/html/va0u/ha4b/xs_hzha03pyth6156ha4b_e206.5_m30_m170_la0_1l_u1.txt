*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m30_m170_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/LYON/SUMT/C001-11
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 5487 events in 11 files time stamp: Mon Oct  1 21:09:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20230.xsdst ! RUN = 20230 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20231.xsdst ! RUN = 20231 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20232.xsdst ! RUN = 20232 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20233.xsdst ! RUN = 20233 ! NEVT = 496
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20234.xsdst ! RUN = 20234 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20235.xsdst ! RUN = 20235 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20236.xsdst ! RUN = 20236 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20237.xsdst ! RUN = 20237 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20238.xsdst ! RUN = 20238 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20239.xsdst ! RUN = 20239 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_30_170_20240.xsdst ! RUN = 20240 ! NEVT = 500
