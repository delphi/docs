*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m90_m110_la0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/LYON/SUMT/C001-20
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_u1 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 9883 events in 20 files time stamp: Tue Oct  2 13:12:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20351.xsdst ! RUN = 20351 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20352.xsdst ! RUN = 20352 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20353.xsdst ! RUN = 20353 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20355.xsdst ! RUN = 20355 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20356.xsdst ! RUN = 20356 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20357.xsdst ! RUN = 20357 ! NEVT = 397
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20358.xsdst ! RUN = 20358 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20359.xsdst ! RUN = 20359 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20360.xsdst ! RUN = 20360 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20580.xsdst ! RUN = 20580 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20581.xsdst ! RUN = 20581 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20582.xsdst ! RUN = 20582 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20583.xsdst ! RUN = 20583 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20584.xsdst ! RUN = 20584 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20585.xsdst ! RUN = 20585 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20586.xsdst ! RUN = 20586 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20587.xsdst ! RUN = 20587 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20588.xsdst ! RUN = 20588 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20589.xsdst ! RUN = 20589 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha4b_206.5_90_110_20590.xsdst ! RUN = 20590 ! NEVT = 499
