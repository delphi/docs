*
*   Nickname     : xs_hzha03pyth6156hzbq_e206.5_m105_m20_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZBQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9929 events in 10 files time stamp: Thu Apr 18 06:14:25 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10707.xsdst ! RUN = 10707 ! NEVT = 935
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10711.xsdst ! RUN = 10711 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10715.xsdst ! RUN = 10715 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10719.xsdst ! RUN = 10719 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10723.xsdst ! RUN = 10723 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10727.xsdst ! RUN = 10727 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10731.xsdst ! RUN = 10731 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10735.xsdst ! RUN = 10735 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10739.xsdst ! RUN = 10739 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_105_20_10743.xsdst ! RUN = 10743 ! NEVT = 999
