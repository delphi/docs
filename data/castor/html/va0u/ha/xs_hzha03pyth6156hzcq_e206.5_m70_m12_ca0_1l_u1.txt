*
*   Nickname     : xs_hzha03pyth6156hzcq_e206.5_m70_m12_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9086 events in 10 files time stamp: Sat Apr 13 15:11:51 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7128.xsdst ! RUN = 7128 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7132.xsdst ! RUN = 7132 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7136.xsdst ! RUN = 7136 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7140.xsdst ! RUN = 7140 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7144.xsdst ! RUN = 7144 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7148.xsdst ! RUN = 7148 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7152.xsdst ! RUN = 7152 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7156.xsdst ! RUN = 7156 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7160.xsdst ! RUN = 7160 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_12_7164.xsdst ! RUN = 7164 ! NEVT = 93
