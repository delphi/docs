*
*   Nickname     : xs_hzha03pyth6156hzbq_e206.5_m50_m12_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZBQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9014 events in 10 files time stamp: Thu Apr 18 10:14:26 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5127.xsdst ! RUN = 5127 ! NEVT = 140
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5131.xsdst ! RUN = 5131 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5135.xsdst ! RUN = 5135 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5139.xsdst ! RUN = 5139 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5143.xsdst ! RUN = 5143 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5147.xsdst ! RUN = 5147 ! NEVT = 878
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5151.xsdst ! RUN = 5151 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5155.xsdst ! RUN = 5155 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5159.xsdst ! RUN = 5159 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzbq_206.5_50_12_5163.xsdst ! RUN = 5163 ! NEVT = 999
