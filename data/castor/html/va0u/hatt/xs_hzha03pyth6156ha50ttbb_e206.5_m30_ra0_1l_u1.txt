*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e206.5_m30_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E206.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Nov 25 20:47:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33030.xsdst ! RUN = 33030 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33031.xsdst ! RUN = 33031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33032.xsdst ! RUN = 33032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33033.xsdst ! RUN = 33033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33034.xsdst ! RUN = 33034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33035.xsdst ! RUN = 33035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33036.xsdst ! RUN = 33036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33037.xsdst ! RUN = 33037 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33038.xsdst ! RUN = 33038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_30_33039.xsdst ! RUN = 33039 ! NEVT = 500
