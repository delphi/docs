*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m90_m90_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/RAL/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sun Nov 25 20:47:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_90_90_21551.xsdst ! RUN = 21551 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_90_90_21552.xsdst ! RUN = 21552 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_90_90_21553.xsdst ! RUN = 21553 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_90_90_21554.xsdst ! RUN = 21554 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_90_90_21555.xsdst ! RUN = 21555 ! NEVT = 500
