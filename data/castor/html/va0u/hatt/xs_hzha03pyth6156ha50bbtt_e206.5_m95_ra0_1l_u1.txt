*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e206.5_m95_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E206.5/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Sun Nov 25 20:48:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39540.xsdst ! RUN = 39540 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39541.xsdst ! RUN = 39541 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39542.xsdst ! RUN = 39542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39543.xsdst ! RUN = 39543 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39544.xsdst ! RUN = 39544 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39545.xsdst ! RUN = 39545 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39546.xsdst ! RUN = 39546 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39547.xsdst ! RUN = 39547 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39548.xsdst ! RUN = 39548 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_95_39549.xsdst ! RUN = 39549 ! NEVT = 500
