*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e206.5_m60_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E206.5/RAL/SUMT/C001-5
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 25 20:47:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_60_36030.xsdst ! RUN = 36030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_60_36031.xsdst ! RUN = 36031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_60_36032.xsdst ! RUN = 36032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_60_36035.xsdst ! RUN = 36035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_60_36039.xsdst ! RUN = 36039 ! NEVT = 500
