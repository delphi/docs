*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m18_m18_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/RAL/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 25 20:47:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_18_18_21111.xsdst ! RUN = 21111 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_18_18_21112.xsdst ! RUN = 21112 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_18_18_21113.xsdst ! RUN = 21113 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_18_18_21114.xsdst ! RUN = 21114 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_18_18_21115.xsdst ! RUN = 21115 ! NEVT = 500
