*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e206.5_m80_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E206.5/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sun Nov 25 20:46:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38040.xsdst ! RUN = 38040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38041.xsdst ! RUN = 38041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38042.xsdst ! RUN = 38042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38043.xsdst ! RUN = 38043 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38044.xsdst ! RUN = 38044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38045.xsdst ! RUN = 38045 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38046.xsdst ! RUN = 38046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38047.xsdst ! RUN = 38047 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38048.xsdst ! RUN = 38048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50bbtt_206.5_80_38049.xsdst ! RUN = 38049 ! NEVT = 500
