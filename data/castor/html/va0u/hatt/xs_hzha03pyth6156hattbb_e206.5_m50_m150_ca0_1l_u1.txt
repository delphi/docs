*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m50_m150_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Fri Oct 12 20:16:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_50_150_22331.xsdst ! RUN = 22331 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_50_150_22332.xsdst ! RUN = 22332 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_50_150_22333.xsdst ! RUN = 22333 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_50_150_22334.xsdst ! RUN = 22334 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_50_150_22335.xsdst ! RUN = 22335 ! NEVT = 500
