*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m70_m80_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Fri Oct 12 16:16:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_70_80_21411.xsdst ! RUN = 21411 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_70_80_21412.xsdst ! RUN = 21412 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_70_80_21413.xsdst ! RUN = 21413 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_70_80_21414.xsdst ! RUN = 21414 ! NEVT = 500
