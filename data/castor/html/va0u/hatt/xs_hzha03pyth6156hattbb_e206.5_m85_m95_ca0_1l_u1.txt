*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m85_m95_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sat Oct 13 04:15:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_95_22541.xsdst ! RUN = 22541 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_95_22542.xsdst ! RUN = 22542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_95_22543.xsdst ! RUN = 22543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_95_22544.xsdst ! RUN = 22544 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_95_22545.xsdst ! RUN = 22545 ! NEVT = 500
