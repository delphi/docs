*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m85_m90_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sat Oct 13 04:15:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_90_22531.xsdst ! RUN = 22531 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_90_22532.xsdst ! RUN = 22532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_90_22533.xsdst ! RUN = 22533 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_90_22534.xsdst ! RUN = 22534 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_85_90_22535.xsdst ! RUN = 22535 ! NEVT = 500
