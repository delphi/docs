*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e206.5_m90_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E206.5/RAL/SUMT/C001-9
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4496 events in 9 files time stamp: Sun Nov 25 20:47:10 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39031.xsdst ! RUN = 39031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39032.xsdst ! RUN = 39032 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39033.xsdst ! RUN = 39033 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39034.xsdst ! RUN = 39034 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39035.xsdst ! RUN = 39035 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39036.xsdst ! RUN = 39036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39037.xsdst ! RUN = 39037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39038.xsdst ! RUN = 39038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_ha50ttbb_206.5_90_39039.xsdst ! RUN = 39039 ! NEVT = 500
