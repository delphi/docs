*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m80_m90_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/RAL/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 25 20:47:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_80_90_21501.xsdst ! RUN = 21501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_80_90_21502.xsdst ! RUN = 21502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_80_90_21503.xsdst ! RUN = 21503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_80_90_21504.xsdst ! RUN = 21504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_80_90_21505.xsdst ! RUN = 21505 ! NEVT = 500
