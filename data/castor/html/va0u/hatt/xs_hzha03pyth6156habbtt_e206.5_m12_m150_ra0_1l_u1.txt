*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m12_m150_ra0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/RAL/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2498 events in 5 files time stamp: Fri Oct 12 10:20:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_12_150_21071.xsdst ! RUN = 21071 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_12_150_21072.xsdst ! RUN = 21072 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_12_150_21073.xsdst ! RUN = 21073 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_12_150_21074.xsdst ! RUN = 21074 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0u/206.5/hzha03pyth6156_habbtt_206.5_12_150_21075.xsdst ! RUN = 21075 ! NEVT = 500
