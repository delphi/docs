*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m90_m110_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2496 events in 5 files time stamp: Sat Oct 13 04:15:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_90_110_22581.xsdst ! RUN = 22581 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_90_110_22582.xsdst ! RUN = 22582 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_90_110_22583.xsdst ! RUN = 22583 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_90_110_22584.xsdst ! RUN = 22584 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hattbb_206.5_90_110_22585.xsdst ! RUN = 22585 ! NEVT = 500
