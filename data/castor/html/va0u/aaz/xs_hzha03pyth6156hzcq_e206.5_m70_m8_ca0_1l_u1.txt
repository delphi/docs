*   Nickname :     xs_hzha03pyth6156hzcq_e206.5_m70_m8_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E206.5/CERN/SUMT/C001-10
*   Description :   Extended Short DST simulation a0u  done at ecm=206.5 GeV , CERN
*   Comments :     in total 8458 events in 10 files, time stamp: Sat Mai 17 4:12:56 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7052.xsdst ! RUN = 7052 ! NEVT = 924
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7053.xsdst ! RUN = 7053 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7054.xsdst ! RUN = 7054 ! NEVT = 709
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7055.xsdst ! RUN = 7055 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7056.xsdst ! RUN = 7056 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7057.xsdst ! RUN = 7057 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7058.xsdst ! RUN = 7058 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7059.xsdst ! RUN = 7059 ! NEVT = 9
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7060.xsdst ! RUN = 7060 ! NEVT = 819
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0u/206.5/hzha03pyth6156_hzcq_206.5_70_8_7061.xsdst ! RUN = 7061 ! NEVT = 1000
