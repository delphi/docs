*
*   Nickname     : xs_kk2f4146qqardcy_e91.25_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4146QQARDCY/E91.25/CERN/SUMT/C001-39
*   Description  :  Extended Short DST simulation a0u1 done at ecms=91.25 , CERN
*---
*   Comments     : in total 116991 events in 39 files time stamp: Sat Aug  3 11:16:44 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65001.xsdst ! RUN = 65001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65002.xsdst ! RUN = 65002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65003.xsdst ! RUN = 65003 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65004.xsdst ! RUN = 65004 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65005.xsdst ! RUN = 65005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65006.xsdst ! RUN = 65006 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65007.xsdst ! RUN = 65007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65009.xsdst ! RUN = 65009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65010.xsdst ! RUN = 65010 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65011.xsdst ! RUN = 65011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65012.xsdst ! RUN = 65012 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65013.xsdst ! RUN = 65013 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65014.xsdst ! RUN = 65014 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65015.xsdst ! RUN = 65015 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65016.xsdst ! RUN = 65016 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65017.xsdst ! RUN = 65017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65018.xsdst ! RUN = 65018 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65019.xsdst ! RUN = 65019 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65020.xsdst ! RUN = 65020 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65021.xsdst ! RUN = 65021 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65022.xsdst ! RUN = 65022 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65023.xsdst ! RUN = 65023 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65024.xsdst ! RUN = 65024 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65025.xsdst ! RUN = 65025 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65026.xsdst ! RUN = 65026 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65027.xsdst ! RUN = 65027 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65028.xsdst ! RUN = 65028 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65029.xsdst ! RUN = 65029 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65030.xsdst ! RUN = 65030 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65031.xsdst ! RUN = 65031 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65032.xsdst ! RUN = 65032 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65033.xsdst ! RUN = 65033 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65034.xsdst ! RUN = 65034 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65035.xsdst ! RUN = 65035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65036.xsdst ! RUN = 65036 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65037.xsdst ! RUN = 65037 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65038.xsdst ! RUN = 65038 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65039.xsdst ! RUN = 65039 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/91.25/kk2f4146_qqardcy_91.25_65040.xsdst ! RUN = 65040 ! NEVT = 3000
