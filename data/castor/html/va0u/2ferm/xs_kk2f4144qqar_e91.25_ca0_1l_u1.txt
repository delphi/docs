*   Nickname :     xs_kk2f4144qqar_e91.25_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4144QQAR/E91.25/CERN/SUMT/C001-11
*   Description :   Extended Short DST simulation a0u  done at ecm=91.25 GeV , CERN
*   Comments :     in total 32996 events in 11 files, time stamp: Tue Jun 8 21:46:38 2010
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50000.xsdst ! RUN = 50000 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50001.xsdst ! RUN = 50001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50002.xsdst ! RUN = 50002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50003.xsdst ! RUN = 50003 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50004.xsdst ! RUN = 50004 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50005.xsdst ! RUN = 50005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50006.xsdst ! RUN = 50006 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50007.xsdst ! RUN = 50007 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50008.xsdst ! RUN = 50008 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50009.xsdst ! RUN = 50009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/va0u/91.25/kk2f4144_qqar_91.25_50010.xsdst ! RUN = 50010 ! NEVT = 3000
