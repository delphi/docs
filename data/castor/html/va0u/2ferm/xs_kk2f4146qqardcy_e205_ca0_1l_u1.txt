*
*   Nickname     : xs_kk2f4146qqardcy_e205_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4146QQARDCY/CERN/SUMT/C001-15
*   Description  :  Extended Short DST simulation a0u1 205 , CERN
*---
*   Comments     : in total 26521 events in 15 files time stamp: Sat Aug  3 22:11:51 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75100.xsdst ! RUN = 75100 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75101.xsdst ! RUN = 75101 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75102.xsdst ! RUN = 75102 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75103.xsdst ! RUN = 75103 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75104.xsdst ! RUN = 75104 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75105.xsdst ! RUN = 75105 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75106.xsdst ! RUN = 75106 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75107.xsdst ! RUN = 75107 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75108.xsdst ! RUN = 75108 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75109.xsdst ! RUN = 75109 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75110.xsdst ! RUN = 75110 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75111.xsdst ! RUN = 75111 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75112.xsdst ! RUN = 75112 ! NEVT = 2996
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75113.xsdst ! RUN = 75113 ! NEVT = 1155
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/205/kk2f4146_qqardcy_205_75114.xsdst ! RUN = 75114 ! NEVT = 2382
