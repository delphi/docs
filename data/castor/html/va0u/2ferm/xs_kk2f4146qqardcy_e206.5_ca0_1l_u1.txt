*
*   Nickname     : xs_kk2f4146qqardcy_e206.5_ca0_1l_u1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4146QQARDCY/E206.5/CERN/SUMT/C001-80
*   Description  :  Extended Short DST simulation a0u1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 154183 events in 80 files time stamp: Sat Aug  3 22:11:51 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75000.xsdst ! RUN = 75000 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75001.xsdst ! RUN = 75001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75002.xsdst ! RUN = 75002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75003.xsdst ! RUN = 75003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75004.xsdst ! RUN = 75004 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75005.xsdst ! RUN = 75005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75006.xsdst ! RUN = 75006 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75007.xsdst ! RUN = 75007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75008.xsdst ! RUN = 75008 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75009.xsdst ! RUN = 75009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75010.xsdst ! RUN = 75010 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75011.xsdst ! RUN = 75011 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75012.xsdst ! RUN = 75012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75013.xsdst ! RUN = 75013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75014.xsdst ! RUN = 75014 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75015.xsdst ! RUN = 75015 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75016.xsdst ! RUN = 75016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75017.xsdst ! RUN = 75017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75018.xsdst ! RUN = 75018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75019.xsdst ! RUN = 75019 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75020.xsdst ! RUN = 75020 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75021.xsdst ! RUN = 75021 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75022.xsdst ! RUN = 75022 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75023.xsdst ! RUN = 75023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75024.xsdst ! RUN = 75024 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75025.xsdst ! RUN = 75025 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75026.xsdst ! RUN = 75026 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75027.xsdst ! RUN = 75027 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75028.xsdst ! RUN = 75028 ! NEVT = 126
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75029.xsdst ! RUN = 75029 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75030.xsdst ! RUN = 75030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75031.xsdst ! RUN = 75031 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75032.xsdst ! RUN = 75032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75033.xsdst ! RUN = 75033 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75034.xsdst ! RUN = 75034 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75035.xsdst ! RUN = 75035 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75036.xsdst ! RUN = 75036 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75037.xsdst ! RUN = 75037 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75038.xsdst ! RUN = 75038 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75039.xsdst ! RUN = 75039 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75040.xsdst ! RUN = 75040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75041.xsdst ! RUN = 75041 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75042.xsdst ! RUN = 75042 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75043.xsdst ! RUN = 75043 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75044.xsdst ! RUN = 75044 ! NEVT = 856
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75045.xsdst ! RUN = 75045 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75046.xsdst ! RUN = 75046 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75047.xsdst ! RUN = 75047 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75048.xsdst ! RUN = 75048 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75049.xsdst ! RUN = 75049 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75050.xsdst ! RUN = 75050 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75051.xsdst ! RUN = 75051 ! NEVT = 2995
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75052.xsdst ! RUN = 75052 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75054.xsdst ! RUN = 75054 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75055.xsdst ! RUN = 75055 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75056.xsdst ! RUN = 75056 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75057.xsdst ! RUN = 75057 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75058.xsdst ! RUN = 75058 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75059.xsdst ! RUN = 75059 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75060.xsdst ! RUN = 75060 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75061.xsdst ! RUN = 75061 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75062.xsdst ! RUN = 75062 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75063.xsdst ! RUN = 75063 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75064.xsdst ! RUN = 75064 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75065.xsdst ! RUN = 75065 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75066.xsdst ! RUN = 75066 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75067.xsdst ! RUN = 75067 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75068.xsdst ! RUN = 75068 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75069.xsdst ! RUN = 75069 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75070.xsdst ! RUN = 75070 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75071.xsdst ! RUN = 75071 ! NEVT = 2996
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75072.xsdst ! RUN = 75072 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75073.xsdst ! RUN = 75073 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75074.xsdst ! RUN = 75074 ! NEVT = 2284
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75075.xsdst ! RUN = 75075 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75076.xsdst ! RUN = 75076 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75077.xsdst ! RUN = 75077 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75078.xsdst ! RUN = 75078 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75079.xsdst ! RUN = 75079 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/va0u/206.5/kk2f4146_qqardcy_206.5_75080.xsdst ! RUN = 75080 ! NEVT = 2996
