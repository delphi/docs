*
*   Nickname     : xs_susygen200_e205_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/SUSYGEN200/CERN/SUMT
*   Description  :  Extended Short DST simulation 99_a1 205 , CERN
*---
*   Comments     :  time stamp: Sun Mar 25 15:01:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_5.xsdst ! RUN = 7978 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_10.xsdst ! RUN = 7972 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_20.xsdst ! RUN = 7975 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_30.xsdst ! RUN = 7976 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_40.xsdst ! RUN = 7977 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_50.xsdst ! RUN = 7979 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_60.xsdst ! RUN = 7980 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_70.xsdst ! RUN = 7981 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_80.xsdst ! RUN = 7982 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_85.xsdst ! RUN = 7983 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_90.xsdst ! RUN = 7984 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_94.xsdst ! RUN = 7985 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_95.xsdst ! RUN = 7986 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_97.xsdst ! RUN = 7987 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_99.xsdst ! RUN = 7988 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_100.xsdst ! RUN = 7973 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/sb_205_101.xsdst ! RUN = 7974 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_5.xsdst ! RUN = 8132 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_10.xsdst ! RUN = 8126 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_20.xsdst ! RUN = 8129 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_30.xsdst ! RUN = 8130 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_40.xsdst ! RUN = 8131 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_50.xsdst ! RUN = 8133 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_60.xsdst ! RUN = 8134 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_70.xsdst ! RUN = 8135 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_80.xsdst ! RUN = 8136 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_85.xsdst ! RUN = 8137 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_90.xsdst ! RUN = 8138 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_94.xsdst ! RUN = 8139 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_95.xsdst ! RUN = 8140 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_97.xsdst ! RUN = 8141 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_99.xsdst ! RUN = 8142 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_100.xsdst ! RUN = 8127 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/205/st_205_101.xsdst ! RUN = 8128 ! NEVT = 1000
