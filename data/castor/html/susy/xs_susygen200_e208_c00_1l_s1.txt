*
*   Nickname     : xs_susygen200_e208_c00_1l_s1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/SUSYGEN200/CERN/SUMT
*   Description  :  Extended Short DST simulation 00_s1 208 , CERN
*---
*   Comments     :  time stamp: Sun Mar 25 15:01:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_5.xsdst ! RUN = 8045 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_10.xsdst ! RUN = 8039 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_20.xsdst ! RUN = 8042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_30.xsdst ! RUN = 8043 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_40.xsdst ! RUN = 8044 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_50.xsdst ! RUN = 8046 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_60.xsdst ! RUN = 8047 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_70.xsdst ! RUN = 8048 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_80.xsdst ! RUN = 8049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_85.xsdst ! RUN = 8050 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_90.xsdst ! RUN = 8051 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_94.xsdst ! RUN = 8052 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_95.xsdst ! RUN = 8053 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_97.xsdst ! RUN = 8054 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_99.xsdst ! RUN = 8055 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_100.xsdst ! RUN = 8040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/sb_208s1_101.xsdst ! RUN = 8041 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_5.xsdst ! RUN = 8200 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_10.xsdst ! RUN = 8194 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_20.xsdst ! RUN = 8197 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_30.xsdst ! RUN = 8198 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_40.xsdst ! RUN = 8199 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_50.xsdst ! RUN = 8201 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_60.xsdst ! RUN = 8202 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_70.xsdst ! RUN = 8203 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_80.xsdst ! RUN = 8204 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_85.xsdst ! RUN = 8205 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_90.xsdst ! RUN = 8206 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_94.xsdst ! RUN = 8207 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_95.xsdst ! RUN = 8208 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_97.xsdst ! RUN = 8209 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_100.xsdst ! RUN = 8195 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/208/st_208s1_101.xsdst ! RUN = 8196 ! NEVT = 1000

