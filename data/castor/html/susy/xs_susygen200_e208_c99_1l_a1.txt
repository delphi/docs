*
*   Nickname     : xs_susygen200_e208_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/SUSYGEN200/CERN/SUMT
*   Description  :  Extended Short DST simulation 99_a1 208 , CERN
*---
*   Comments     :  time stamp: Sun Mar 25 15:01:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_10.xsdst ! RUN = 8022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_100.xsdst ! RUN = 8023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_101.xsdst ! RUN = 8024 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_20.xsdst ! RUN = 8025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_30.xsdst ! RUN = 8026 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_40.xsdst ! RUN = 8027 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_5.xsdst ! RUN = 8028 ! NEVT = 995
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_50.xsdst ! RUN = 8029 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_60.xsdst ! RUN = 8030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_70.xsdst ! RUN = 8031 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_80.xsdst ! RUN = 8032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_85.xsdst ! RUN = 8033 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_90.xsdst ! RUN = 8034 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_94.xsdst ! RUN = 8035 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_95.xsdst ! RUN = 8036 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_97.xsdst ! RUN = 8037 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/sb_208_99.xsdst ! RUN = 8038 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_10.xsdst ! RUN = 8177 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_100.xsdst ! RUN = 8178 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_101.xsdst ! RUN = 8179 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_20.xsdst ! RUN = 8180 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_30.xsdst ! RUN = 8181 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_40.xsdst ! RUN = 8182 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_5.xsdst ! RUN = 8183 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_50.xsdst ! RUN = 8184 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_60.xsdst ! RUN = 8185 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_70.xsdst ! RUN = 8186 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_80.xsdst ! RUN = 8187 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_85.xsdst ! RUN = 8188 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_90.xsdst ! RUN = 8189 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_94.xsdst ! RUN = 8190 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_95.xsdst ! RUN = 8191 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_97.xsdst ! RUN = 8192 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v99_4/208/st_208_99.xsdst ! RUN = 8193 ! NEVT = 1000
