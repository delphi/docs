*
*   Nickname     : xs_susygen200_e206.7_c00_1l_s1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/SUSYGEN200/E206.7/CERN/SUMT
*   Description  :  Extended Short DST simulation 00_s1 done at ecms=206.7 , CERN
*---
*   Comments     :  time stamp: Sun Mar 25 15:01:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_5.xsdst ! RUN = 8012 ! NEVT = 996
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_10.xsdst ! RUN = 8006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_20.xsdst ! RUN = 8009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_30.xsdst ! RUN = 8010 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_40.xsdst ! RUN = 8011 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_50.xsdst ! RUN = 8013 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_60.xsdst ! RUN = 8014 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_70.xsdst ! RUN = 8015 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_80.xsdst ! RUN = 8016 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_85.xsdst ! RUN = 8017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_94.xsdst ! RUN = 8018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_95.xsdst ! RUN = 8019 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_97.xsdst ! RUN = 8020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_99.xsdst ! RUN = 8021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_100.xsdst ! RUN = 8007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/sb_2067s1_101.xsdst ! RUN = 8008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_5.xsdst ! RUN = 8166 ! NEVT = 996
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_10.xsdst ! RUN = 8160 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_20.xsdst ! RUN = 8163 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_30.xsdst ! RUN = 8164 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_40.xsdst ! RUN = 8165 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_50.xsdst ! RUN = 8167 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_60.xsdst ! RUN = 8168 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_70.xsdst ! RUN = 8169 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_80.xsdst ! RUN = 8170 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_85.xsdst ! RUN = 8171 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_90.xsdst ! RUN = 8172 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_94.xsdst ! RUN = 8173 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_95.xsdst ! RUN = 8174 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_97.xsdst ! RUN = 8175 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_99.xsdst ! RUN = 8176 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_100.xsdst ! RUN = 8161 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/susygen200/v00_s/206.7/st_2067s1_101.xsdst ! RUN = 8162 ! NEVT = 1000
