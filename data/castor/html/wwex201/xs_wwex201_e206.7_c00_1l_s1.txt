*
*   Nickname     : xs_wwex201_e206.7_c00_1l_s1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WWEX201/E206.7/CERN, FEB/MAR. 2000 US/SUMT
*   Description  : XSHORT WW like Excalibur events, version 201. Reprocessed from raw simulation Extended Short DST simulation 00_s1 done at ecms=206.7 , CERN, Feb/Mar. 2000 US
*---
*   Comments     :  time stamp: Fri Jun 15 09:30:49 2001
*---
*
*  Comments     : same events as in  xs_wwex201_e206.7_c99_1l_a1
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52001.raw.xsdst ! RUN =   52001 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52002.raw.xsdst ! RUN =   52002 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52003.raw.xsdst ! RUN =   52003 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52004.raw.xsdst ! RUN =   52004 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52005.raw.xsdst ! RUN =   52005 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52006.raw.xsdst ! RUN =   52006 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52007.raw.xsdst ! RUN =   52007 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52008.raw.xsdst ! RUN =   52008 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52009.raw.xsdst ! RUN =   52009 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52010.raw.xsdst ! RUN =   52010 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52011.raw.xsdst ! RUN =   52011 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52012.raw.xsdst ! RUN =   52012 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52013.raw.xsdst ! RUN =   52013 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52014.raw.xsdst ! RUN =   52014 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52015.raw.xsdst ! RUN =   52015 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52016.raw.xsdst ! RUN =   52016 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52017.raw.xsdst ! RUN =   52017 ! NEVT =     998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52018.raw.xsdst ! RUN =   52018 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52019.raw.xsdst ! RUN =   52019 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52020.raw.xsdst ! RUN =   52020 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52021.raw.xsdst ! RUN =   52021 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52022.raw.xsdst ! RUN =   52022 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52023.raw.xsdst ! RUN =   52023 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52024.raw.xsdst ! RUN =   52024 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52025.raw.xsdst ! RUN =   52025 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52026.raw.xsdst ! RUN =   52026 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52027.raw.xsdst ! RUN =   52027 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52028.raw.xsdst ! RUN =   52028 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52029.raw.xsdst ! RUN =   52029 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52030.raw.xsdst ! RUN =   52030 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52031.raw.xsdst ! RUN =   52031 ! NEVT =     997
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52032.raw.xsdst ! RUN =   52032 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52033.raw.xsdst ! RUN =   52033 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52034.raw.xsdst ! RUN =   52034 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52035.raw.xsdst ! RUN =   52035 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52036.raw.xsdst ! RUN =   52036 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52037.raw.xsdst ! RUN =   52037 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52038.raw.xsdst ! RUN =   52038 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52039.raw.xsdst ! RUN =   52039 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52040.raw.xsdst ! RUN =   52040 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52041.raw.xsdst ! RUN =   52041 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52042.raw.xsdst ! RUN =   52042 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52043.raw.xsdst ! RUN =   52043 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52044.raw.xsdst ! RUN =   52044 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52045.raw.xsdst ! RUN =   52045 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52046.raw.xsdst ! RUN =   52046 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52047.raw.xsdst ! RUN =   52047 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52048.raw.xsdst ! RUN =   52048 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52049.raw.xsdst ! RUN =   52049 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52050.raw.xsdst ! RUN =   52050 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52051.raw.xsdst ! RUN =   52051 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52052.raw.xsdst ! RUN =   52052 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52053.raw.xsdst ! RUN =   52053 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52054.raw.xsdst ! RUN =   52054 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52055.raw.xsdst ! RUN =   52055 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52056.raw.xsdst ! RUN =   52056 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52057.raw.xsdst ! RUN =   52057 ! NEVT =     997
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52058.raw.xsdst ! RUN =   52058 ! NEVT =     998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52059.raw.xsdst ! RUN =   52059 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52060.raw.xsdst ! RUN =   52060 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52061.raw.xsdst ! RUN =   52061 ! NEVT =     998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52062.raw.xsdst ! RUN =   52062 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52063.raw.xsdst ! RUN =   52063 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52064.raw.xsdst ! RUN =   52064 ! NEVT =     998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52065.raw.xsdst ! RUN =   52065 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52066.raw.xsdst ! RUN =   52066 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52067.raw.xsdst ! RUN =   52067 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52068.raw.xsdst ! RUN =   52068 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52069.raw.xsdst ! RUN =   52069 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52070.raw.xsdst ! RUN =   52070 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52071.raw.xsdst ! RUN =   52071 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52072.raw.xsdst ! RUN =   52072 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52073.raw.xsdst ! RUN =   52073 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52074.raw.xsdst ! RUN =   52074 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52075.raw.xsdst ! RUN =   52075 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52076.raw.xsdst ! RUN =   52076 ! NEVT =     998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52077.raw.xsdst ! RUN =   52077 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52078.raw.xsdst ! RUN =   52078 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52079.raw.xsdst ! RUN =   52079 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52080.raw.xsdst ! RUN =   52080 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52081.raw.xsdst ! RUN =   52081 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52082.raw.xsdst ! RUN =   52082 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52083.raw.xsdst ! RUN =   52083 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52084.raw.xsdst ! RUN =   52084 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52085.raw.xsdst ! RUN =   52085 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52086.raw.xsdst ! RUN =   52086 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52087.raw.xsdst ! RUN =   52087 ! NEVT =     998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52088.raw.xsdst ! RUN =   52088 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52089.raw.xsdst ! RUN =   52089 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52090.raw.xsdst ! RUN =   52090 ! NEVT =     999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52091.raw.xsdst ! RUN =   52091 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52092.raw.xsdst ! RUN =   52092 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52093.raw.xsdst ! RUN =   52093 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52094.raw.xsdst ! RUN =   52094 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52095.raw.xsdst ! RUN =   52095 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52096.raw.xsdst ! RUN =   52096 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52097.raw.xsdst ! RUN =   52097 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52098.raw.xsdst ! RUN =   52098 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52099.raw.xsdst ! RUN =   52099 ! NEVT =    1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex201/v00_s/206.7/run52100.raw.xsdst ! RUN =   52100 ! NEVT =     999
