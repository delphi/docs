*
*   Nickname     : xs_hzha03pyth6156hcmm_e201.6_m67.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Sun Dec 23 17:08:20 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcmm_201.6_67.5_4701.xsdst ! RUN = 4701 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcmm_201.6_67.5_4702.xsdst ! RUN = 4702 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcmm_201.6_67.5_4703.xsdst ! RUN = 4703 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcmm_201.6_67.5_4704.xsdst ! RUN = 4704 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcmm_201.6_67.5_4705.xsdst ! RUN = 4705 ! NEVT = 400
