*
*   Nickname     : xs_hzha03pyth6156hsee_e201.6_m82.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1995 events in 4 files time stamp: Thu Nov 15 10:26:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_82.5_48251.xsdst ! RUN = 48251 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_82.5_48252.xsdst ! RUN = 48252 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_82.5_48253.xsdst ! RUN = 48253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_82.5_48254.xsdst ! RUN = 48254 ! NEVT = 499
