*
*   Nickname     : xs_hzha03pyth6156hcee_e201.6_m65.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Thu Nov 15 15:00:15 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_65.0_46505.xsdst ! RUN = 46505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_65.0_46506.xsdst ! RUN = 46506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_65.0_46507.xsdst ! RUN = 46507 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_65.0_46508.xsdst ! RUN = 46508 ! NEVT = 500
