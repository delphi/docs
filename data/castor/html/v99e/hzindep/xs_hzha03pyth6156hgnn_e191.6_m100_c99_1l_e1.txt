*
*   Nickname     : xs_hzha03pyth6156hgnn_e191.6_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Wed Jun  5 10:16:02 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130001.xsdst ! RUN = 130001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130002.xsdst ! RUN = 130002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130003.xsdst ! RUN = 130003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130004.xsdst ! RUN = 130004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130005.xsdst ! RUN = 130005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130006.xsdst ! RUN = 130006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130007.xsdst ! RUN = 130007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130008.xsdst ! RUN = 130008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130009.xsdst ! RUN = 130009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgnn_191.6_100_130010.xsdst ! RUN = 130010 ! NEVT = 500
