*
*   Nickname     : xs_hzha03pyth6156hgee_e195.5_m62.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E195.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 16 00:27:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_62.5_48259.xsdst ! RUN = 48259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_62.5_48260.xsdst ! RUN = 48260 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_62.5_48261.xsdst ! RUN = 48261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_62.5_48262.xsdst ! RUN = 48262 ! NEVT = 498
