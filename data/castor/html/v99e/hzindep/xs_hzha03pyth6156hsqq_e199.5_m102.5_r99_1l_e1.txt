*
*   Nickname     : xs_hzha03pyth6156hsqq_e199.5_m102.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Mon Nov 26 23:53:54 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_102.5_91251.xsdst ! RUN = 91251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_102.5_91252.xsdst ! RUN = 91252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_102.5_91253.xsdst ! RUN = 91253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_102.5_91254.xsdst ! RUN = 91254 ! NEVT = 500
