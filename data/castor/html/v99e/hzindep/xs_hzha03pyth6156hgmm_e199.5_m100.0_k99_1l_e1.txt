*
*   Nickname     : xs_hzha03pyth6156hgmm_e199.5_m100.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E199.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=199.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Fri Dec 14 06:54:33 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgmm_199.5_100.0_3371.xsdst ! RUN = 3371 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgmm_199.5_100.0_3372.xsdst ! RUN = 3372 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgmm_199.5_100.0_3373.xsdst ! RUN = 3373 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgmm_199.5_100.0_3374.xsdst ! RUN = 3374 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgmm_199.5_100.0_3375.xsdst ! RUN = 3375 ! NEVT = 400
