*
*   Nickname     : xs_hzha03pyth6156hgee_e201.6_m100.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Fri Dec  7 16:42:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_100.0_50009.xsdst ! RUN = 50009 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_100.0_50010.xsdst ! RUN = 50010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_100.0_50011.xsdst ! RUN = 50011 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_100.0_50012.xsdst ! RUN = 50012 ! NEVT = 500
