*
*   Nickname     : xs_hzha03pyth6156hcmm_e195.5_m87.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Dec 20 16:04:27 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_87.5_4431.xsdst ! RUN = 4431 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_87.5_4432.xsdst ! RUN = 4432 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_87.5_4433.xsdst ! RUN = 4433 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_87.5_4434.xsdst ! RUN = 4434 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_87.5_4435.xsdst ! RUN = 4435 ! NEVT = 399
