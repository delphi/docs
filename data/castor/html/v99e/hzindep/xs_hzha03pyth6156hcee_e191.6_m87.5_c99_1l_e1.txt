*
*   Nickname     : xs_hzha03pyth6156hcee_e191.6_m87.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E191.6/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1996 events in 4 files time stamp: Fri Nov 16 04:38:18 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_87.5_51755.xsdst ! RUN = 51755 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_87.5_51756.xsdst ! RUN = 51756 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_87.5_51757.xsdst ! RUN = 51757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_87.5_51758.xsdst ! RUN = 51758 ! NEVT = 499
