*
*   Nickname     : xs_hzha03pyth6156hcee_e191.6_m90.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E191.6/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1994 events in 4 files time stamp: Fri Nov 16 00:27:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_90.0_52005.xsdst ! RUN = 52005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_90.0_52006.xsdst ! RUN = 52006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_90.0_52007.xsdst ! RUN = 52007 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_90.0_52008.xsdst ! RUN = 52008 ! NEVT = 498
