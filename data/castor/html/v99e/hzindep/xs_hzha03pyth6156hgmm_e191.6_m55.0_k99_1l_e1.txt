*
*   Nickname     : xs_hzha03pyth6156hgmm_e191.6_m55.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Dec  5 13:26:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_55.0_2971.xsdst ! RUN = 2971 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_55.0_2972.xsdst ! RUN = 2972 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_55.0_2973.xsdst ! RUN = 2973 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_55.0_2974.xsdst ! RUN = 2974 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_55.0_2975.xsdst ! RUN = 2975 ! NEVT = 400
