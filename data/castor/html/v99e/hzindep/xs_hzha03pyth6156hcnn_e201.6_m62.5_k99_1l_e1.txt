*
*   Nickname     : xs_hzha03pyth6156hcnn_e201.6_m62.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Fri Jan 11 19:26:37 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_62.5_7481.xsdst ! RUN = 7481 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_62.5_7482.xsdst ! RUN = 7482 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_62.5_7483.xsdst ! RUN = 7483 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_62.5_7484.xsdst ! RUN = 7484 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_62.5_7485.xsdst ! RUN = 7485 ! NEVT = 400
