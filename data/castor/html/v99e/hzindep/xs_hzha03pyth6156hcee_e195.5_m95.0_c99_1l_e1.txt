*
*   Nickname     : xs_hzha03pyth6156hcee_e195.5_m95.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E195.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Nov 15 15:03:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_95.0_51505.xsdst ! RUN = 51505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_95.0_51506.xsdst ! RUN = 51506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_95.0_51507.xsdst ! RUN = 51507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_95.0_51508.xsdst ! RUN = 51508 ! NEVT = 500
