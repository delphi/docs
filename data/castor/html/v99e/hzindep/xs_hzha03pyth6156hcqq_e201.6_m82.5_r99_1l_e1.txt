*
*   Nickname     : xs_hzha03pyth6156hcqq_e201.6_m82.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1995 events in 4 files time stamp: Sun Nov 25 05:01:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_82.5_88255.xsdst ! RUN = 88255 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_82.5_88256.xsdst ! RUN = 88256 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_82.5_88257.xsdst ! RUN = 88257 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_82.5_88258.xsdst ! RUN = 88258 ! NEVT = 499
