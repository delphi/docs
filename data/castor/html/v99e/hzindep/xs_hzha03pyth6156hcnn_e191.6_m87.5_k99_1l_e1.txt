*
*   Nickname     : xs_hzha03pyth6156hcnn_e191.6_m87.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 1998 events in 5 files time stamp: Thu Jan 10 12:54:16 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcnn_191.6_87.5_7066.xsdst ! RUN = 7066 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcnn_191.6_87.5_7067.xsdst ! RUN = 7067 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcnn_191.6_87.5_7068.xsdst ! RUN = 7068 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcnn_191.6_87.5_7069.xsdst ! RUN = 7069 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcnn_191.6_87.5_7070.xsdst ! RUN = 7070 ! NEVT = 400
