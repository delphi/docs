*
*   Nickname     : xs_hzha03pyth6156hsmm_e201.6_m107.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Tue Dec 18 15:52:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsmm_201.6_107.5_4161.xsdst ! RUN = 4161 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsmm_201.6_107.5_4162.xsdst ! RUN = 4162 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsmm_201.6_107.5_4163.xsdst ! RUN = 4163 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsmm_201.6_107.5_4164.xsdst ! RUN = 4164 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsmm_201.6_107.5_4165.xsdst ! RUN = 4165 ! NEVT = 400
