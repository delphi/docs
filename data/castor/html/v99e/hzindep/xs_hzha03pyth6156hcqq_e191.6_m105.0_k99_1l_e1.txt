*
*   Nickname     : xs_hzha03pyth6156hcqq_e191.6_m105.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 1839 events in 5 files time stamp: Sun Feb  3 15:17:25 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_105.0_10056.xsdst ! RUN = 10056 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_105.0_10057.xsdst ! RUN = 10057 ! NEVT = 239
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_105.0_10058.xsdst ! RUN = 10058 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_105.0_10059.xsdst ! RUN = 10059 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_105.0_10060.xsdst ! RUN = 10060 ! NEVT = 400
