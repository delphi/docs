*
*   Nickname     : xs_hzha03pyth6156hgqq_e195.5_m107.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Sun Jan 27 01:11:18 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgqq_195.5_107.5_9121.xsdst ! RUN = 9121 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgqq_195.5_107.5_9122.xsdst ! RUN = 9122 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgqq_195.5_107.5_9123.xsdst ! RUN = 9123 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgqq_195.5_107.5_9124.xsdst ! RUN = 9124 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgqq_195.5_107.5_9125.xsdst ! RUN = 9125 ! NEVT = 399
