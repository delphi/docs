*
*   Nickname     : xs_hzha03pyth6156hsqq_e191.6_m80.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Mon Jan 28 01:13:29 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsqq_191.6_80.0_9376.xsdst ! RUN = 9376 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsqq_191.6_80.0_9377.xsdst ! RUN = 9377 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsqq_191.6_80.0_9378.xsdst ! RUN = 9378 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsqq_191.6_80.0_9379.xsdst ! RUN = 9379 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsqq_191.6_80.0_9380.xsdst ! RUN = 9380 ! NEVT = 400
