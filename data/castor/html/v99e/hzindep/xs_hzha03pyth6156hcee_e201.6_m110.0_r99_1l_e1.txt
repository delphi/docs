*
*   Nickname     : xs_hzha03pyth6156hcee_e201.6_m110.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 16 00:24:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_110.0_51005.xsdst ! RUN = 51005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_110.0_51006.xsdst ! RUN = 51006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_110.0_51007.xsdst ! RUN = 51007 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcee_201.6_110.0_51008.xsdst ! RUN = 51008 ! NEVT = 499
