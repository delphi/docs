*
*   Nickname     : xs_hzha03pyth6156hgee_e199.5_m65.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Sat Nov 17 04:01:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgee_199.5_65.0_47509.xsdst ! RUN = 47509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgee_199.5_65.0_47510.xsdst ! RUN = 47510 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgee_199.5_65.0_47511.xsdst ! RUN = 47511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgee_199.5_65.0_47512.xsdst ! RUN = 47512 ! NEVT = 500
