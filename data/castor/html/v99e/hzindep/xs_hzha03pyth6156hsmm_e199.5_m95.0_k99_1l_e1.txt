*
*   Nickname     : xs_hzha03pyth6156hsmm_e199.5_m95.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E199.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=199.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Mon Dec 17 23:38:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_95.0_3981.xsdst ! RUN = 3981 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_95.0_3982.xsdst ! RUN = 3982 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_95.0_3983.xsdst ! RUN = 3983 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_95.0_3984.xsdst ! RUN = 3984 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_95.0_3985.xsdst ! RUN = 3985 ! NEVT = 400
