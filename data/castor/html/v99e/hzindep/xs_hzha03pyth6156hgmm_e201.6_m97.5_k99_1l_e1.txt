*
*   Nickname     : xs_hzha03pyth6156hgmm_e201.6_m97.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Dec 13 16:01:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_97.5_3521.xsdst ! RUN = 3521 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_97.5_3522.xsdst ! RUN = 3522 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_97.5_3523.xsdst ! RUN = 3523 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_97.5_3524.xsdst ! RUN = 3524 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_97.5_3525.xsdst ! RUN = 3525 ! NEVT = 400
