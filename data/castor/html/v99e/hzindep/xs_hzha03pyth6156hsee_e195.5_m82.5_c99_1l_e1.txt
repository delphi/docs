*
*   Nickname     : xs_hzha03pyth6156hsee_e195.5_m82.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E195.5/CERN/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 1500 events in 3 files time stamp: Thu Nov 15 06:37:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_82.5_50251.xsdst ! RUN = 50251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_82.5_50252.xsdst ! RUN = 50252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_82.5_50254.xsdst ! RUN = 50254 ! NEVT = 500
