*
*   Nickname     : xs_hzha03pyth6156hsmm_e191.6_m65.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Dec 13 18:52:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsmm_191.6_65.0_3611.xsdst ! RUN = 3611 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsmm_191.6_65.0_3612.xsdst ! RUN = 3612 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsmm_191.6_65.0_3613.xsdst ! RUN = 3613 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsmm_191.6_65.0_3614.xsdst ! RUN = 3614 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsmm_191.6_65.0_3615.xsdst ! RUN = 3615 ! NEVT = 400
