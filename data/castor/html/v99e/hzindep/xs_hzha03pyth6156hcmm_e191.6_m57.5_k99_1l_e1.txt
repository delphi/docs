*
*   Nickname     : xs_hzha03pyth6156hcmm_e191.6_m57.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Dec 19 11:14:17 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcmm_191.6_57.5_4216.xsdst ! RUN = 4216 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcmm_191.6_57.5_4217.xsdst ! RUN = 4217 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcmm_191.6_57.5_4218.xsdst ! RUN = 4218 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcmm_191.6_57.5_4219.xsdst ! RUN = 4219 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcmm_191.6_57.5_4220.xsdst ! RUN = 4220 ! NEVT = 400
