*
*   Nickname     : xs_hzha03pyth6156hcnn_e195.5_m60.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Jan 10 14:50:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_60.0_7166.xsdst ! RUN = 7166 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_60.0_7167.xsdst ! RUN = 7167 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_60.0_7168.xsdst ! RUN = 7168 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_60.0_7169.xsdst ! RUN = 7169 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_60.0_7170.xsdst ! RUN = 7170 ! NEVT = 400
