*
*   Nickname     : xs_hzha03pyth6156hcqq_e201.6_m52.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Nov 24 16:42:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_52.5_85255.xsdst ! RUN = 85255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_52.5_85256.xsdst ! RUN = 85256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_52.5_85257.xsdst ! RUN = 85257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_52.5_85258.xsdst ! RUN = 85258 ! NEVT = 500
