*
*   Nickname     : xs_hzha03pyth6156hcee_e199.5_m52.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Fri Nov 16 14:53:37 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_52.5_46255.xsdst ! RUN = 46255 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_52.5_46256.xsdst ! RUN = 46256 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_52.5_46257.xsdst ! RUN = 46257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_52.5_46258.xsdst ! RUN = 46258 ! NEVT = 500
