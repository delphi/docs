*
*   Nickname     : xs_hzha03pyth6156hsqq_e199.5_m92.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Mon Nov 26 16:47:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_92.5_90251.xsdst ! RUN = 90251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_92.5_90252.xsdst ! RUN = 90252 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_92.5_90253.xsdst ! RUN = 90253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_92.5_90254.xsdst ! RUN = 90254 ! NEVT = 500
