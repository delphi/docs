*
*   Nickname     : xs_hzha03pyth6156hsmm_e199.5_m57.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E199.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=199.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Mon Dec 17 23:19:07 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_57.5_3906.xsdst ! RUN = 3906 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_57.5_3907.xsdst ! RUN = 3907 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_57.5_3908.xsdst ! RUN = 3908 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_57.5_3909.xsdst ! RUN = 3909 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsmm_199.5_57.5_3910.xsdst ! RUN = 3910 ! NEVT = 400
