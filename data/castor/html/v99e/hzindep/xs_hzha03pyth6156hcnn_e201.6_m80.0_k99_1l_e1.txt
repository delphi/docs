*
*   Nickname     : xs_hzha03pyth6156hcnn_e201.6_m80.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Fri Jan 11 19:24:21 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_80.0_7516.xsdst ! RUN = 7516 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_80.0_7517.xsdst ! RUN = 7517 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_80.0_7518.xsdst ! RUN = 7518 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_80.0_7519.xsdst ! RUN = 7519 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcnn_201.6_80.0_7520.xsdst ! RUN = 7520 ! NEVT = 400
