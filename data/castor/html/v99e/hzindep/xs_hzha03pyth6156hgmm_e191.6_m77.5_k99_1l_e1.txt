*
*   Nickname     : xs_hzha03pyth6156hgmm_e191.6_m77.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Dec 12 13:10:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_77.5_3016.xsdst ! RUN = 3016 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_77.5_3017.xsdst ! RUN = 3017 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_77.5_3018.xsdst ! RUN = 3018 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_77.5_3019.xsdst ! RUN = 3019 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgmm_191.6_77.5_3020.xsdst ! RUN = 3020 ! NEVT = 400
