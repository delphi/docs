*
*   Nickname     : xs_hzha03pyth6156hgqq_e201.6_m110.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Mon Nov 26 15:03:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_110.0_91009.xsdst ! RUN = 91009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_110.0_91010.xsdst ! RUN = 91010 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_110.0_91011.xsdst ! RUN = 91011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_110.0_91012.xsdst ! RUN = 91012 ! NEVT = 500
