*
*   Nickname     : xs_hzha03pyth6156hsee_e199.5_m72.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1996 events in 4 files time stamp: Fri Nov 16 04:30:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsee_199.5_72.5_48251.xsdst ! RUN = 48251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsee_199.5_72.5_48252.xsdst ! RUN = 48252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsee_199.5_72.5_48253.xsdst ! RUN = 48253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsee_199.5_72.5_48254.xsdst ! RUN = 48254 ! NEVT = 498
