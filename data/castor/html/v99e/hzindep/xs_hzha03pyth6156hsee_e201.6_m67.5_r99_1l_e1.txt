*
*   Nickname     : xs_hzha03pyth6156hsee_e201.6_m67.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1995 events in 4 files time stamp: Thu Nov 15 04:37:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_67.5_46751.xsdst ! RUN = 46751 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_67.5_46752.xsdst ! RUN = 46752 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_67.5_46753.xsdst ! RUN = 46753 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_67.5_46754.xsdst ! RUN = 46754 ! NEVT = 499
