*
*   Nickname     : xs_hzha03pyth6156hcnn_e195.5_m95.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Jan 10 17:36:10 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_95.0_7236.xsdst ! RUN = 7236 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_95.0_7237.xsdst ! RUN = 7237 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_95.0_7238.xsdst ! RUN = 7238 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_95.0_7239.xsdst ! RUN = 7239 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcnn_195.5_95.0_7240.xsdst ! RUN = 7240 ! NEVT = 399
