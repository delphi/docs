*
*   Nickname     : xs_hzha03pyth6156hgmm_e201.6_m65.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Dec 12 15:55:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_65.0_3456.xsdst ! RUN = 3456 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_65.0_3457.xsdst ! RUN = 3457 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_65.0_3458.xsdst ! RUN = 3458 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_65.0_3459.xsdst ! RUN = 3459 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgmm_201.6_65.0_3460.xsdst ! RUN = 3460 ! NEVT = 400
