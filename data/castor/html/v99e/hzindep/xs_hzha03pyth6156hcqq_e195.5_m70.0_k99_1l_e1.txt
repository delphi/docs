*
*   Nickname     : xs_hzha03pyth6156hcqq_e195.5_m70.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Sun Feb  3 22:18:28 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcqq_195.5_70.0_11061.xsdst ! RUN = 11061 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcqq_195.5_70.0_11062.xsdst ! RUN = 11062 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcqq_195.5_70.0_11063.xsdst ! RUN = 11063 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcqq_195.5_70.0_11064.xsdst ! RUN = 11064 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcqq_195.5_70.0_11065.xsdst ! RUN = 11065 ! NEVT = 400
