*
*   Nickname     : xs_hzha03pyth6156hcee_e195.5_m105.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E195.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Thu Nov 15 12:40:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_105.0_52505.xsdst ! RUN = 52505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_105.0_52506.xsdst ! RUN = 52506 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_105.0_52507.xsdst ! RUN = 52507 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_105.0_52508.xsdst ! RUN = 52508 ! NEVT = 500
