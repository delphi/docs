*
*   Nickname     : xs_hzha03pyth6156hgqq_e199.5_m102.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Fri Nov 30 10:47:37 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_102.5_91259.xsdst ! RUN = 91259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_102.5_91260.xsdst ! RUN = 91260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_102.5_91261.xsdst ! RUN = 91261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_102.5_91262.xsdst ! RUN = 91262 ! NEVT = 500
