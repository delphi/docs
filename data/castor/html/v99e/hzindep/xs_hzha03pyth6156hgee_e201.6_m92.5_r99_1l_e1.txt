*
*   Nickname     : xs_hzha03pyth6156hgee_e201.6_m92.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Fri Dec  7 11:27:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_92.5_49259.xsdst ! RUN = 49259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_92.5_49260.xsdst ! RUN = 49260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_92.5_49261.xsdst ! RUN = 49261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgee_201.6_92.5_49262.xsdst ! RUN = 49262 ! NEVT = 500
