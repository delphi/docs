*
*   Nickname     : xs_hzha03pyth6156hcee_e199.5_m45.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E199.5/RAL/SUMT/C001-3
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1498 events in 3 files time stamp: Fri Nov 16 12:33:40 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_45.0_45505.xsdst ! RUN = 45505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_45.0_45506.xsdst ! RUN = 45506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_45.0_45508.xsdst ! RUN = 45508 ! NEVT = 499
