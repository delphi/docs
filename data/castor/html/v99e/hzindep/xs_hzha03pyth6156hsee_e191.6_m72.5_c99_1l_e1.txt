*
*   Nickname     : xs_hzha03pyth6156hsee_e191.6_m72.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E191.6/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1545 events in 4 files time stamp: Fri Nov 16 02:36:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_72.5_50251.xsdst ! RUN = 50251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_72.5_50252.xsdst ! RUN = 50252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_72.5_50253.xsdst ! RUN = 50253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_72.5_50254.xsdst ! RUN = 50254 ! NEVT = 46
