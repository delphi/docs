*
*   Nickname     : xs_hzha03pyth6156hsee_e195.5_m42.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E195.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 1997 events in 4 files time stamp: Thu Nov 15 06:37:49 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_42.5_46251.xsdst ! RUN = 46251 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_42.5_46252.xsdst ! RUN = 46252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_42.5_46253.xsdst ! RUN = 46253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsee_195.5_42.5_46254.xsdst ! RUN = 46254 ! NEVT = 499
