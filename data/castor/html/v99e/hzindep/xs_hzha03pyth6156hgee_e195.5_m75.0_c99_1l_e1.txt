*
*   Nickname     : xs_hzha03pyth6156hgee_e195.5_m75.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E195.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Nov 15 18:37:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_75.0_49509.xsdst ! RUN = 49509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_75.0_49510.xsdst ! RUN = 49510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_75.0_49511.xsdst ! RUN = 49511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_75.0_49512.xsdst ! RUN = 49512 ! NEVT = 500
