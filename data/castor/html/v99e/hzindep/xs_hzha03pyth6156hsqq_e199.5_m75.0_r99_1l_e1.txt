*
*   Nickname     : xs_hzha03pyth6156hsqq_e199.5_m75.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Mon Nov 26 16:45:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_75.0_88501.xsdst ! RUN = 88501 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_75.0_88502.xsdst ! RUN = 88502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_75.0_88503.xsdst ! RUN = 88503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_75.0_88504.xsdst ! RUN = 88504 ! NEVT = 500
