*
*   Nickname     : xs_hzha03pyth6156hgee_e191.6_m112.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E191.6/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1999 events in 4 files time stamp: Fri Nov 16 02:36:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_112.5_54259.xsdst ! RUN = 54259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_112.5_54260.xsdst ! RUN = 54260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_112.5_54261.xsdst ! RUN = 54261 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_112.5_54262.xsdst ! RUN = 54262 ! NEVT = 500
