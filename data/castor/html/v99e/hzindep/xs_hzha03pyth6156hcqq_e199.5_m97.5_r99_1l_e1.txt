*
*   Nickname     : xs_hzha03pyth6156hcqq_e199.5_m97.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Tue Nov 27 19:10:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_97.5_90755.xsdst ! RUN = 90755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_97.5_90756.xsdst ! RUN = 90756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_97.5_90757.xsdst ! RUN = 90757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_97.5_90758.xsdst ! RUN = 90758 ! NEVT = 500
