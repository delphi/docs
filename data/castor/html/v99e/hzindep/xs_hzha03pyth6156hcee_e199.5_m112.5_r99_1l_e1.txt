*
*   Nickname     : xs_hzha03pyth6156hcee_e199.5_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 16 22:34:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_112.5_52255.xsdst ! RUN = 52255 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_112.5_52256.xsdst ! RUN = 52256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_112.5_52257.xsdst ! RUN = 52257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_112.5_52258.xsdst ! RUN = 52258 ! NEVT = 499
