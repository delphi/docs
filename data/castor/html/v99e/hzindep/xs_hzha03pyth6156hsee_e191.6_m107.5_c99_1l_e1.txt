*
*   Nickname     : xs_hzha03pyth6156hsee_e191.6_m107.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E191.6/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Thu Nov 15 22:39:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_107.5_53751.xsdst ! RUN = 53751 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_107.5_53752.xsdst ! RUN = 53752 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_107.5_53753.xsdst ! RUN = 53753 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hsee_191.6_107.5_53754.xsdst ! RUN = 53754 ! NEVT = 500
