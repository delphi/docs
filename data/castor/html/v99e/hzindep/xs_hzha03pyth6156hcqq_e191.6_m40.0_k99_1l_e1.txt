*
*   Nickname     : xs_hzha03pyth6156hcqq_e191.6_m40.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 1997 events in 5 files time stamp: Sun Feb  3 15:16:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_40.0_9926.xsdst ! RUN = 9926 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_40.0_9927.xsdst ! RUN = 9927 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_40.0_9928.xsdst ! RUN = 9928 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_40.0_9929.xsdst ! RUN = 9929 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcqq_191.6_40.0_9930.xsdst ! RUN = 9930 ! NEVT = 399
