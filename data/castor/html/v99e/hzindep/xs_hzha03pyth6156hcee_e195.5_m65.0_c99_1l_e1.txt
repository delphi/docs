*
*   Nickname     : xs_hzha03pyth6156hcee_e195.5_m65.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E195.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 1693 events in 4 files time stamp: Thu Nov 15 12:40:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_65.0_48505.xsdst ! RUN = 48505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_65.0_48506.xsdst ! RUN = 48506 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_65.0_48507.xsdst ! RUN = 48507 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcee_195.5_65.0_48508.xsdst ! RUN = 48508 ! NEVT = 499
