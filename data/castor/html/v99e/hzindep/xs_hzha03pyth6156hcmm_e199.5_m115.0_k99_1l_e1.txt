*
*   Nickname     : xs_hzha03pyth6156hcmm_e199.5_m115.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E199.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Sun Dec 23 17:08:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcmm_199.5_115.0_4641.xsdst ! RUN = 4641 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcmm_199.5_115.0_4642.xsdst ! RUN = 4642 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcmm_199.5_115.0_4643.xsdst ! RUN = 4643 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcmm_199.5_115.0_4644.xsdst ! RUN = 4644 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcmm_199.5_115.0_4645.xsdst ! RUN = 4645 ! NEVT = 399
