*
*   Nickname     : xs_hzha03pyth6156hcee_e199.5_m110.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 16 22:33:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_110.0_52005.xsdst ! RUN = 52005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_110.0_52006.xsdst ! RUN = 52006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_110.0_52007.xsdst ! RUN = 52007 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcee_199.5_110.0_52008.xsdst ! RUN = 52008 ! NEVT = 499
