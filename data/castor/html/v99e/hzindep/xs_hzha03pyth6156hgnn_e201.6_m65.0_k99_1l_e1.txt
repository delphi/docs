*
*   Nickname     : xs_hzha03pyth6156hgnn_e201.6_m65.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Jan  9 14:44:13 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgnn_201.6_65.0_5626.xsdst ! RUN = 5626 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgnn_201.6_65.0_5627.xsdst ! RUN = 5627 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgnn_201.6_65.0_5628.xsdst ! RUN = 5628 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgnn_201.6_65.0_5629.xsdst ! RUN = 5629 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgnn_201.6_65.0_5630.xsdst ! RUN = 5630 ! NEVT = 400
