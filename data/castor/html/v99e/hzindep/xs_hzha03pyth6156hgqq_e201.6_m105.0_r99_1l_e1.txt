*
*   Nickname     : xs_hzha03pyth6156hgqq_e201.6_m105.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sun Nov 25 22:50:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_105.0_90509.xsdst ! RUN = 90509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_105.0_90510.xsdst ! RUN = 90510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_105.0_90511.xsdst ! RUN = 90511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_105.0_90512.xsdst ! RUN = 90512 ! NEVT = 499
