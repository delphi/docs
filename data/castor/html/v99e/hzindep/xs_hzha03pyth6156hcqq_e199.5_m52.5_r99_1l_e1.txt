*
*   Nickname     : xs_hzha03pyth6156hcqq_e199.5_m52.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Tue Nov 27 06:02:57 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_52.5_86255.xsdst ! RUN = 86255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_52.5_86256.xsdst ! RUN = 86256 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_52.5_86257.xsdst ! RUN = 86257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_52.5_86258.xsdst ! RUN = 86258 ! NEVT = 499
