*
*   Nickname     : xs_hzha03pyth6156hsnn_e199.5_m87.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSNN/E199.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_e1 done at ecms=199.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Jan  9 12:09:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsnn_199.5_87.5_6446.xsdst ! RUN = 6446 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsnn_199.5_87.5_6447.xsdst ! RUN = 6447 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsnn_199.5_87.5_6448.xsdst ! RUN = 6448 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsnn_199.5_87.5_6449.xsdst ! RUN = 6449 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsnn_199.5_87.5_6450.xsdst ! RUN = 6450 ! NEVT = 400
