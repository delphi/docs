*
*   Nickname     : xs_hzha03pyth6156hsqq_e199.5_m65.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Mon Nov 26 16:49:14 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_65.0_87501.xsdst ! RUN = 87501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_65.0_87502.xsdst ! RUN = 87502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_65.0_87503.xsdst ! RUN = 87503 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hsqq_199.5_65.0_87504.xsdst ! RUN = 87504 ! NEVT = 500
