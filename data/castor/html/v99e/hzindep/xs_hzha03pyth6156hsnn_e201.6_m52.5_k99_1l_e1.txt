*
*   Nickname     : xs_hzha03pyth6156hsnn_e201.6_m52.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSNN/E201.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_e1 done at ecms=201.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Jan  9 14:45:34 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsnn_201.6_52.5_6531.xsdst ! RUN = 6531 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsnn_201.6_52.5_6532.xsdst ! RUN = 6532 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsnn_201.6_52.5_6533.xsdst ! RUN = 6533 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsnn_201.6_52.5_6534.xsdst ! RUN = 6534 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsnn_201.6_52.5_6535.xsdst ! RUN = 6535 ! NEVT = 400
