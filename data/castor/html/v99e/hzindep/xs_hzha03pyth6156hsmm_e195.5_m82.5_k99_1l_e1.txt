*
*   Nickname     : xs_hzha03pyth6156hsmm_e195.5_m82.5_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Mon Dec 17 23:23:19 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsmm_195.5_82.5_3801.xsdst ! RUN = 3801 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsmm_195.5_82.5_3802.xsdst ! RUN = 3802 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsmm_195.5_82.5_3803.xsdst ! RUN = 3803 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsmm_195.5_82.5_3804.xsdst ! RUN = 3804 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hsmm_195.5_82.5_3805.xsdst ! RUN = 3805 ! NEVT = 400
