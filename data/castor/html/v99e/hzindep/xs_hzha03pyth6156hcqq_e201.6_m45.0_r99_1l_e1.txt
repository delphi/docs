*
*   Nickname     : xs_hzha03pyth6156hcqq_e201.6_m45.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1996 events in 4 files time stamp: Sat Nov 24 16:43:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_45.0_84505.xsdst ! RUN = 84505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_45.0_84506.xsdst ! RUN = 84506 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_45.0_84507.xsdst ! RUN = 84507 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hcqq_201.6_45.0_84508.xsdst ! RUN = 84508 ! NEVT = 499
