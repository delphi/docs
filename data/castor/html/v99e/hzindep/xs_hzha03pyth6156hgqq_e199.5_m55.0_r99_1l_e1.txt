*
*   Nickname     : xs_hzha03pyth6156hgqq_e199.5_m55.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E199.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Thu Nov 29 10:42:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_55.0_86509.xsdst ! RUN = 86509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_55.0_86510.xsdst ! RUN = 86510 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_55.0_86511.xsdst ! RUN = 86511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hgqq_199.5_55.0_86512.xsdst ! RUN = 86512 ! NEVT = 500
