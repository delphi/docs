*
*   Nickname     : xs_hzha03pyth6156hcee_e191.6_m85.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E191.6/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1605 events in 4 files time stamp: Fri Nov 16 06:58:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_85.0_51505.xsdst ! RUN = 51505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_85.0_51506.xsdst ! RUN = 51506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_85.0_51507.xsdst ! RUN = 51507 ! NEVT = 107
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hcee_191.6_85.0_51508.xsdst ! RUN = 51508 ! NEVT = 498
