*
*   Nickname     : xs_hzha03pyth6156hcqq_e199.5_m102.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Wed Nov 28 00:54:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_102.5_91255.xsdst ! RUN = 91255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_102.5_91256.xsdst ! RUN = 91256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_102.5_91257.xsdst ! RUN = 91257 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hcqq_199.5_102.5_91258.xsdst ! RUN = 91258 ! NEVT = 500
