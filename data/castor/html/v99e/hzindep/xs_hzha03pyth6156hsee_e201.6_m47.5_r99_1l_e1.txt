*
*   Nickname     : xs_hzha03pyth6156hsee_e201.6_m47.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Thu Nov 15 00:24:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_47.5_44751.xsdst ! RUN = 44751 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_47.5_44752.xsdst ! RUN = 44752 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_47.5_44753.xsdst ! RUN = 44753 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_47.5_44754.xsdst ! RUN = 44754 ! NEVT = 500
