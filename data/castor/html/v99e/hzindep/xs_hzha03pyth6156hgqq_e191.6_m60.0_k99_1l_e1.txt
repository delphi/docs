*
*   Nickname     : xs_hzha03pyth6156hgqq_e191.6_m60.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E191.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=191.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Fri Jan 25 15:11:22 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgqq_191.6_60.0_8716.xsdst ! RUN = 8716 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgqq_191.6_60.0_8717.xsdst ! RUN = 8717 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgqq_191.6_60.0_8718.xsdst ! RUN = 8718 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgqq_191.6_60.0_8719.xsdst ! RUN = 8719 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgqq_191.6_60.0_8720.xsdst ! RUN = 8720 ! NEVT = 400
