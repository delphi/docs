*
*   Nickname     : xs_hzha03pyth6156hcmm_e195.5_m45.0_k99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E195.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Dec 19 18:56:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_45.0_4346.xsdst ! RUN = 4346 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_45.0_4347.xsdst ! RUN = 4347 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_45.0_4348.xsdst ! RUN = 4348 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_45.0_4349.xsdst ! RUN = 4349 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hcmm_195.5_45.0_4350.xsdst ! RUN = 4350 ! NEVT = 400
