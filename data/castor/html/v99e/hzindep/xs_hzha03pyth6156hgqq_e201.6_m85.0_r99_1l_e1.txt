*
*   Nickname     : xs_hzha03pyth6156hgqq_e201.6_m85.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sun Nov 25 15:14:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_85.0_88509.xsdst ! RUN = 88509 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_85.0_88510.xsdst ! RUN = 88510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_85.0_88511.xsdst ! RUN = 88511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hgqq_201.6_85.0_88512.xsdst ! RUN = 88512 ! NEVT = 500
