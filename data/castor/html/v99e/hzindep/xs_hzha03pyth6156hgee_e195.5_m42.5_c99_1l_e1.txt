*
*   Nickname     : xs_hzha03pyth6156hgee_e195.5_m42.5_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E195.5/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Thu Nov 15 15:03:20 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_42.5_46259.xsdst ! RUN = 46259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_42.5_46260.xsdst ! RUN = 46260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_42.5_46261.xsdst ! RUN = 46261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hgee_195.5_42.5_46262.xsdst ! RUN = 46262 ! NEVT = 498
