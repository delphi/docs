*
*   Nickname     : xs_hzha03pyth6156hgee_e191.6_m75.0_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E191.6/CERN/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 1916 events in 4 files time stamp: Fri Nov 16 00:27:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_75.0_50509.xsdst ! RUN = 50509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_75.0_50510.xsdst ! RUN = 50510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_75.0_50511.xsdst ! RUN = 50511 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hgee_191.6_75.0_50512.xsdst ! RUN = 50512 ! NEVT = 417
