*
*   Nickname     : xs_hzha03pyth6156hsee_e201.6_m70.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E201.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Thu Nov 15 06:33:18 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_70.0_47001.xsdst ! RUN = 47001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_70.0_47002.xsdst ! RUN = 47002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_70.0_47003.xsdst ! RUN = 47003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hsee_201.6_70.0_47004.xsdst ! RUN = 47004 ! NEVT = 499
