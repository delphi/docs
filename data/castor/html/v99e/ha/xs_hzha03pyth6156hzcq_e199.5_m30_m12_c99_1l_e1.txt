*
*   Nickname     : xs_hzha03pyth6156hzcq_e199.5_m30_m12_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E199.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 9996 events in 10 files time stamp: Thu Apr 11 12:10:44 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3128.xsdst ! RUN = 3128 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3132.xsdst ! RUN = 3132 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3136.xsdst ! RUN = 3136 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3140.xsdst ! RUN = 3140 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3144.xsdst ! RUN = 3144 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3148.xsdst ! RUN = 3148 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3152.xsdst ! RUN = 3152 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3156.xsdst ! RUN = 3156 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3160.xsdst ! RUN = 3160 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_30_12_3164.xsdst ! RUN = 3164 ! NEVT = 1000
