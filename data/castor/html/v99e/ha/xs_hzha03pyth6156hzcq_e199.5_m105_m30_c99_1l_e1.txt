*
*   Nickname     : xs_hzha03pyth6156hzcq_e199.5_m105_m30_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E199.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 9995 events in 10 files time stamp: Thu Apr 11 22:10:48 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10808.xsdst ! RUN = 10808 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10812.xsdst ! RUN = 10812 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10816.xsdst ! RUN = 10816 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10820.xsdst ! RUN = 10820 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10824.xsdst ! RUN = 10824 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10828.xsdst ! RUN = 10828 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10832.xsdst ! RUN = 10832 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10836.xsdst ! RUN = 10836 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10840.xsdst ! RUN = 10840 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_105_30_10844.xsdst ! RUN = 10844 ! NEVT = 1000
