*
*   Nickname     : xs_hzhahphmwatn_e195.5_m80_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHAHPHMWATN/E195.5/CERN/SUMT/C001-2
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 3999 events in 2 files time stamp: Sat May  4 10:10:25 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/195.5/hzha_hphm_watn_e195.5_80_50.xsdst ! RUN = 50 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/195.5/hzha_hphm_watn_e195.5_80_70.xsdst ! RUN = 70 ! NEVT = 2000
