*
*   Nickname     : xs_hzhahphmwatn_e199.5_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHAHPHMWATN/E199.5/CERN/SUMT/C001-3
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 5998 events in 3 files time stamp: Thu Apr 25 12:10:21 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/199.5/hzha_hphm_watn_e199.5_90_50.xsdst ! RUN = 50 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/199.5/hzha_hphm_watn_e199.5_90_70.xsdst ! RUN = 70 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/199.5/hzha_hphm_watn_e199.5_90_80.xsdst ! RUN = 80 ! NEVT = 1999
