*
*   Nickname     : xs_hzha03pyth6156ha4t_e199.5_m50_m4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E199.5/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Wed Feb 27 01:10:26 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5045.xsdst ! RUN = 5045 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5046.xsdst ! RUN = 5046 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5047.xsdst ! RUN = 5047 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5048.xsdst ! RUN = 5048 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5049.xsdst ! RUN = 5049 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5050.xsdst ! RUN = 5050 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5051.xsdst ! RUN = 5051 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_4_5052.xsdst ! RUN = 5052 ! NEVT = 250
