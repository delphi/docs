*
*   Nickname     : xs_hzha03pyth6156hzcq_e199.5_m70_m30_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E199.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 7784 events in 10 files time stamp: Mon Apr 15 01:10:55 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7308.xsdst ! RUN = 7308 ! NEVT = 941
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7312.xsdst ! RUN = 7312 ! NEVT = 154
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7316.xsdst ! RUN = 7316 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7320.xsdst ! RUN = 7320 ! NEVT = 406
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7324.xsdst ! RUN = 7324 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7328.xsdst ! RUN = 7328 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7332.xsdst ! RUN = 7332 ! NEVT = 285
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7336.xsdst ! RUN = 7336 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7340.xsdst ! RUN = 7340 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_70_30_7344.xsdst ! RUN = 7344 ! NEVT = 1000
