*
*   Nickname     : xs_hzhahphmwawa_e195.5_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHAHPHMWAWA/E195.5/CERN/SUMT/C001-3
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 5964 events in 3 files time stamp: Sat May  4 18:10:29 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/195.5/hzha_hphm_wawa_e195.5_90_50.xsdst ! RUN = 50 ! NEVT = 1992
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/195.5/hzha_hphm_wawa_e195.5_90_70.xsdst ! RUN = 70 ! NEVT = 1987
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/195.5/hzha_hphm_wawa_e195.5_90_80.xsdst ! RUN = 80 ! NEVT = 1985
