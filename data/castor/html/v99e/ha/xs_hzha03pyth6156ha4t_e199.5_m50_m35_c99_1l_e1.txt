*
*   Nickname     : xs_hzha03pyth6156ha4t_e199.5_m50_m35_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E199.5/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Wed Feb 27 01:10:27 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5355.xsdst ! RUN = 5355 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5356.xsdst ! RUN = 5356 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5357.xsdst ! RUN = 5357 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5358.xsdst ! RUN = 5358 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5359.xsdst ! RUN = 5359 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5360.xsdst ! RUN = 5360 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5361.xsdst ! RUN = 5361 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hA4t_199.5_50_35_5362.xsdst ! RUN = 5362 ! NEVT = 250
