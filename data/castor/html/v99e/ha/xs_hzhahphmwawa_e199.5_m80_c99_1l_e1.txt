*
*   Nickname     : xs_hzhahphmwawa_e199.5_m80_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHAHPHMWAWA/E199.5/CERN/SUMT/C001-2
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 3992 events in 2 files time stamp: Mon Apr 29 10:51:28 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/199.5/hzha_hphm_wawa_e199.5_80_50.xsdst ! RUN = 50 ! NEVT = 1996
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha/v99e/199.5/hzha_hphm_wawa_e199.5_80_70.xsdst ! RUN = 70 ! NEVT = 1996
