*
*   Nickname     : xs_hzha03pyth6156hzcq_e199.5_m90_m30_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E199.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 9847 events in 10 files time stamp: Fri Apr 12 18:10:48 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9308.xsdst ! RUN = 9308 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9312.xsdst ! RUN = 9312 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9316.xsdst ! RUN = 9316 ! NEVT = 851
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9320.xsdst ! RUN = 9320 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9324.xsdst ! RUN = 9324 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9328.xsdst ! RUN = 9328 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9332.xsdst ! RUN = 9332 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9336.xsdst ! RUN = 9336 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9340.xsdst ! RUN = 9340 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_90_30_9344.xsdst ! RUN = 9344 ! NEVT = 1000
