*
*   Nickname     : xs_hzha03pyth6156hattbb_e201.6_m50_m150_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sat Nov 10 19:05:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_150_31301.xsdst ! RUN = 31301 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_150_31302.xsdst ! RUN = 31302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_150_31303.xsdst ! RUN = 31303 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_150_31304.xsdst ! RUN = 31304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_150_31305.xsdst ! RUN = 31305 ! NEVT = 500
