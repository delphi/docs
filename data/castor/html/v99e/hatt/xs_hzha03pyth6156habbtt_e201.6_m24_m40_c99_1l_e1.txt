*
*   Nickname     : xs_hzha03pyth6156habbtt_e201.6_m24_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Nov 10 19:05:25 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_24_40_30121.xsdst ! RUN = 30121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_24_40_30122.xsdst ! RUN = 30122 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_24_40_30123.xsdst ! RUN = 30123 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_24_40_30124.xsdst ! RUN = 30124 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_24_40_30125.xsdst ! RUN = 30125 ! NEVT = 499
