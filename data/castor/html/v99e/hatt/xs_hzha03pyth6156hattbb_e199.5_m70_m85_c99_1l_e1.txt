*
*   Nickname     : xs_hzha03pyth6156hattbb_e199.5_m70_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sun Nov 11 11:04:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_70_85_33361.xsdst ! RUN = 33361 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_70_85_33362.xsdst ! RUN = 33362 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_70_85_33363.xsdst ! RUN = 33363 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_70_85_33364.xsdst ! RUN = 33364 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_70_85_33365.xsdst ! RUN = 33365 ! NEVT = 499
