*
*   Nickname     : xs_hzha03pyth6156hattbb_e199.5_m12_m110_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 11 04:42:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_110_33051.xsdst ! RUN = 33051 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_110_33052.xsdst ! RUN = 33052 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_110_33053.xsdst ! RUN = 33053 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_110_33054.xsdst ! RUN = 33054 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_110_33055.xsdst ! RUN = 33055 ! NEVT = 500
