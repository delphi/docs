*
*   Nickname     : xs_hzha03pyth6156hattbb_e201.6_m24_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sat Nov 10 19:04:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_24_40_31121.xsdst ! RUN = 31121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_24_40_31122.xsdst ! RUN = 31122 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_24_40_31123.xsdst ! RUN = 31123 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_24_40_31124.xsdst ! RUN = 31124 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_24_40_31125.xsdst ! RUN = 31125 ! NEVT = 500
