*
*   Nickname     : xs_hzha03pyth6156habbtt_e195.5_m90_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Mon Nov 12 12:54:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_90_90_34471.xsdst ! RUN = 34471 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_90_90_34472.xsdst ! RUN = 34472 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_90_90_34473.xsdst ! RUN = 34473 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_90_90_34474.xsdst ! RUN = 34474 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_90_90_34475.xsdst ! RUN = 34475 ! NEVT = 500
