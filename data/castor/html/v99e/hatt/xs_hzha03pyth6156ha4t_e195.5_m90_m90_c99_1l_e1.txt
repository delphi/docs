*   Nickname :     xs_hzha03pyth6156ha4t_e195.5_m90_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E195.5/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation 99e  done at ecm=195.5 GeV , CERN
*   Comments :     in total 1999 events in 2 files, time stamp: Tue Nov 12 4:17:45 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4t_195.5_90_90_9905.xsdst ! RUN = 9905 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4t_195.5_90_90_9906.xsdst ! RUN = 9906 ! NEVT = 999
