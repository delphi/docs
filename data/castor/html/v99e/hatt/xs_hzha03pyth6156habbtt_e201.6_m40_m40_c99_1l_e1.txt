*
*   Nickname     : xs_hzha03pyth6156habbtt_e201.6_m40_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Nov 10 19:04:40 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_40_40_30221.xsdst ! RUN = 30221 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_40_40_30222.xsdst ! RUN = 30222 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_40_40_30223.xsdst ! RUN = 30223 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_40_40_30224.xsdst ! RUN = 30224 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_40_40_30225.xsdst ! RUN = 30225 ! NEVT = 499
