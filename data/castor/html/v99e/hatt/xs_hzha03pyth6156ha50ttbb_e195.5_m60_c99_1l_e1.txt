*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e195.5_m60_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E195.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sat Nov 10 19:05:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96031.xsdst ! RUN = 96031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96032.xsdst ! RUN = 96032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96033.xsdst ! RUN = 96033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96034.xsdst ! RUN = 96034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96035.xsdst ! RUN = 96035 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96036.xsdst ! RUN = 96036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96037.xsdst ! RUN = 96037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96038.xsdst ! RUN = 96038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96039.xsdst ! RUN = 96039 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_60_96040.xsdst ! RUN = 96040 ! NEVT = 500
