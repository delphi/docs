*
*   Nickname     : xs_hzha03pyth6156hattbb_e191.6_m30_m150_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 11 20:57:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_150_37201.xsdst ! RUN = 37201 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_150_37202.xsdst ! RUN = 37202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_150_37203.xsdst ! RUN = 37203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_150_37204.xsdst ! RUN = 37204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_150_37205.xsdst ! RUN = 37205 ! NEVT = 500
