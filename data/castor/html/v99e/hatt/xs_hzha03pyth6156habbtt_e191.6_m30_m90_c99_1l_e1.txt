*
*   Nickname     : xs_hzha03pyth6156habbtt_e191.6_m30_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sun Nov 11 18:56:58 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_90_36171.xsdst ! RUN = 36171 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_90_36172.xsdst ! RUN = 36172 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_90_36173.xsdst ! RUN = 36173 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_90_36174.xsdst ! RUN = 36174 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_90_36175.xsdst ! RUN = 36175 ! NEVT = 499
