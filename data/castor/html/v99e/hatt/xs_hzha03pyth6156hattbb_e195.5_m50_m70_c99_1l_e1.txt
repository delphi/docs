*
*   Nickname     : xs_hzha03pyth6156hattbb_e195.5_m50_m70_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sun Nov 11 20:58:07 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_70_35251.xsdst ! RUN = 35251 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_70_35252.xsdst ! RUN = 35252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_70_35253.xsdst ! RUN = 35253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_70_35254.xsdst ! RUN = 35254 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_70_35255.xsdst ! RUN = 35255 ! NEVT = 499
