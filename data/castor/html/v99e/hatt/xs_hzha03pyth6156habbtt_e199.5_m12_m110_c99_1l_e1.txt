*
*   Nickname     : xs_hzha03pyth6156habbtt_e199.5_m12_m110_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sat Nov 10 23:37:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_110_32051.xsdst ! RUN = 32051 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_110_32052.xsdst ! RUN = 32052 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_110_32053.xsdst ! RUN = 32053 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_110_32054.xsdst ! RUN = 32054 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_110_32055.xsdst ! RUN = 32055 ! NEVT = 500
