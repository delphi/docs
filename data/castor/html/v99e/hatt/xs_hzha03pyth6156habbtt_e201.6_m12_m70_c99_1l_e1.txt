*
*   Nickname     : xs_hzha03pyth6156habbtt_e201.6_m12_m70_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sat Nov 10 19:04:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_70_30031.xsdst ! RUN = 30031 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_70_30032.xsdst ! RUN = 30032 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_70_30033.xsdst ! RUN = 30033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_70_30034.xsdst ! RUN = 30034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_70_30035.xsdst ! RUN = 30035 ! NEVT = 500
