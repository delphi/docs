*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e191.6_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E191.6/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Sat Nov 10 19:05:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108541.xsdst ! RUN = 108541 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108542.xsdst ! RUN = 108542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108543.xsdst ! RUN = 108543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108544.xsdst ! RUN = 108544 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108545.xsdst ! RUN = 108545 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108546.xsdst ! RUN = 108546 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108547.xsdst ! RUN = 108547 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108548.xsdst ! RUN = 108548 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108549.xsdst ! RUN = 108549 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_85_108550.xsdst ! RUN = 108550 ! NEVT = 500
