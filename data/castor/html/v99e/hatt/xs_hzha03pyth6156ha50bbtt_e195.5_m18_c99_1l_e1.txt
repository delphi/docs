*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e195.5_m18_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E195.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sat Nov 10 19:05:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91841.xsdst ! RUN = 91841 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91842.xsdst ! RUN = 91842 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91843.xsdst ! RUN = 91843 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91844.xsdst ! RUN = 91844 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91845.xsdst ! RUN = 91845 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91846.xsdst ! RUN = 91846 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91847.xsdst ! RUN = 91847 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91848.xsdst ! RUN = 91848 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91849.xsdst ! RUN = 91849 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_18_91850.xsdst ! RUN = 91850 ! NEVT = 500
