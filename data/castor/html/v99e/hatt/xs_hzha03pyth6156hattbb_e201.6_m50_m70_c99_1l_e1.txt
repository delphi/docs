*
*   Nickname     : xs_hzha03pyth6156hattbb_e201.6_m50_m70_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Nov 10 19:05:15 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_70_31261.xsdst ! RUN = 31261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_70_31262.xsdst ! RUN = 31262 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_70_31263.xsdst ! RUN = 31263 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_70_31264.xsdst ! RUN = 31264 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_70_31265.xsdst ! RUN = 31265 ! NEVT = 499
