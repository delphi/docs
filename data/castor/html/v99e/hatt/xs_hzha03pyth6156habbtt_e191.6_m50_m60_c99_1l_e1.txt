*
*   Nickname     : xs_hzha03pyth6156habbtt_e191.6_m50_m60_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sun Nov 11 19:00:17 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_50_60_36241.xsdst ! RUN = 36241 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_50_60_36242.xsdst ! RUN = 36242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_50_60_36243.xsdst ! RUN = 36243 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_50_60_36244.xsdst ! RUN = 36244 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_50_60_36245.xsdst ! RUN = 36245 ! NEVT = 499
