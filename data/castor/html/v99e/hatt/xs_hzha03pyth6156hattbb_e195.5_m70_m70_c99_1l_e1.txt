*
*   Nickname     : xs_hzha03pyth6156hattbb_e195.5_m70_m70_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Mon Nov 12 00:58:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_70_35341.xsdst ! RUN = 35341 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_70_35342.xsdst ! RUN = 35342 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_70_35343.xsdst ! RUN = 35343 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_70_35344.xsdst ! RUN = 35344 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_70_35345.xsdst ! RUN = 35345 ! NEVT = 500
