*
*   Nickname     : xs_hzha03pyth6156habbtt_e201.6_m95_m95_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sat Nov 10 19:05:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_95_95_30541.xsdst ! RUN = 30541 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_95_95_30542.xsdst ! RUN = 30542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_95_95_30543.xsdst ! RUN = 30543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_95_95_30544.xsdst ! RUN = 30544 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_95_95_30545.xsdst ! RUN = 30545 ! NEVT = 500
