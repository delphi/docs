*
*   Nickname     : xs_hzha03pyth6156habbtt_e195.5_m40_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 11 20:59:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_40_40_34211.xsdst ! RUN = 34211 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_40_40_34212.xsdst ! RUN = 34212 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_40_40_34213.xsdst ! RUN = 34213 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_40_40_34214.xsdst ! RUN = 34214 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_40_40_34215.xsdst ! RUN = 34215 ! NEVT = 500
