*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e199.5_m95_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E199.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sat Nov 10 19:05:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89541.xsdst ! RUN = 89541 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89542.xsdst ! RUN = 89542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89543.xsdst ! RUN = 89543 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89544.xsdst ! RUN = 89544 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89545.xsdst ! RUN = 89545 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89546.xsdst ! RUN = 89546 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89547.xsdst ! RUN = 89547 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89548.xsdst ! RUN = 89548 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89549.xsdst ! RUN = 89549 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_95_89550.xsdst ! RUN = 89550 ! NEVT = 500
