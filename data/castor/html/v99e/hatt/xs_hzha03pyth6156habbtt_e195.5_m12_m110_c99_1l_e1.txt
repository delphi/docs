*
*   Nickname     : xs_hzha03pyth6156habbtt_e195.5_m12_m110_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sun Nov 11 15:00:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_110_34051.xsdst ! RUN = 34051 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_110_34052.xsdst ! RUN = 34052 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_110_34053.xsdst ! RUN = 34053 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_110_34054.xsdst ! RUN = 34054 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_110_34055.xsdst ! RUN = 34055 ! NEVT = 500
