*
*   Nickname     : xs_hzha03pyth6156habbtt_e191.6_m90_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Tue Nov 13 11:47:56 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_90_90_36471.xsdst ! RUN = 36471 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_90_90_36472.xsdst ! RUN = 36472 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_90_90_36473.xsdst ! RUN = 36473 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_90_90_36474.xsdst ! RUN = 36474 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_90_90_36475.xsdst ! RUN = 36475 ! NEVT = 500
