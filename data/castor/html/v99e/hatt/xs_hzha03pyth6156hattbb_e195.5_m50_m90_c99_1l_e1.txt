*
*   Nickname     : xs_hzha03pyth6156hattbb_e195.5_m50_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 20:56:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_90_35261.xsdst ! RUN = 35261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_90_35262.xsdst ! RUN = 35262 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_90_35263.xsdst ! RUN = 35263 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_90_35264.xsdst ! RUN = 35264 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_50_90_35265.xsdst ! RUN = 35265 ! NEVT = 500
