*   Nickname :     xs_hzha03pyth6156ha4t_e199.5_m4_m25_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E199.5/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation 99e  done at ecm=199.5 GeV , CERN
*   Comments :     in total 2000 events in 2 files, time stamp: Tue Nov 12 4:17:43 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4t_199.5_4_25_655.xsdst ! RUN = 655 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4t_199.5_4_25_656.xsdst ! RUN = 656 ! NEVT = 1000
