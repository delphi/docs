*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e201.6_m30_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E201.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Fri Nov 23 12:55:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73031.xsdst ! RUN = 73031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73032.xsdst ! RUN = 73032 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73033.xsdst ! RUN = 73033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73034.xsdst ! RUN = 73034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73035.xsdst ! RUN = 73035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73036.xsdst ! RUN = 73036 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73037.xsdst ! RUN = 73037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73038.xsdst ! RUN = 73038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73039.xsdst ! RUN = 73039 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha50ttbb_201.6_30_73040.xsdst ! RUN = 73040 ! NEVT = 499
