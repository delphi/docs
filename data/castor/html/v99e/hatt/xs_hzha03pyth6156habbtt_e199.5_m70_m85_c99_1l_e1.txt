*
*   Nickname     : xs_hzha03pyth6156habbtt_e199.5_m70_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 04:42:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_70_85_32361.xsdst ! RUN = 32361 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_70_85_32362.xsdst ! RUN = 32362 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_70_85_32363.xsdst ! RUN = 32363 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_70_85_32364.xsdst ! RUN = 32364 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_70_85_32365.xsdst ! RUN = 32365 ! NEVT = 500
