*
*   Nickname     : xs_hzha03pyth6156hattbb_e199.5_m30_m150_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sun Nov 11 11:01:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_30_150_33201.xsdst ! RUN = 33201 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_30_150_33202.xsdst ! RUN = 33202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_30_150_33203.xsdst ! RUN = 33203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_30_150_33204.xsdst ! RUN = 33204 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_30_150_33205.xsdst ! RUN = 33205 ! NEVT = 499
