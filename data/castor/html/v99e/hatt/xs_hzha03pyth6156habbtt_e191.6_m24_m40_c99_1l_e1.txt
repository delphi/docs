*
*   Nickname     : xs_hzha03pyth6156habbtt_e191.6_m24_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Tue Nov 13 09:05:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_24_40_36121.xsdst ! RUN = 36121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_24_40_36122.xsdst ! RUN = 36122 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_24_40_36123.xsdst ! RUN = 36123 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_24_40_36124.xsdst ! RUN = 36124 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_24_40_36125.xsdst ! RUN = 36125 ! NEVT = 500
