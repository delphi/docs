*   Nickname :     xs_hzha03pyth6156ha4t_e199.5_m8_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E199.5/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation 99e  done at ecm=199.5 GeV , CERN
*   Comments :     in total 1997 events in 2 files, time stamp: Thu Mar 6 13:19:42 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4t_199.5_8_90_1705.xsdst ! RUN = 1705 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4t_199.5_8_90_1706.xsdst ! RUN = 1706 ! NEVT = 998
