*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e191.6_m24_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E191.6/CERN/SUMT/C001-7
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 3499 events in 7 files time stamp: Sat Nov 10 19:04:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102441.xsdst ! RUN = 102441 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102442.xsdst ! RUN = 102442 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102446.xsdst ! RUN = 102446 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102447.xsdst ! RUN = 102447 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102448.xsdst ! RUN = 102448 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102449.xsdst ! RUN = 102449 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50bbtt_191.6_24_102450.xsdst ! RUN = 102450 ! NEVT = 499
