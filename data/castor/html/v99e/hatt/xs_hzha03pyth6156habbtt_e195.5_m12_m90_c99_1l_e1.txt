*
*   Nickname     : xs_hzha03pyth6156habbtt_e195.5_m12_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 15:00:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_90_34041.xsdst ! RUN = 34041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_90_34042.xsdst ! RUN = 34042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_90_34043.xsdst ! RUN = 34043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_90_34044.xsdst ! RUN = 34044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_12_90_34045.xsdst ! RUN = 34045 ! NEVT = 499
