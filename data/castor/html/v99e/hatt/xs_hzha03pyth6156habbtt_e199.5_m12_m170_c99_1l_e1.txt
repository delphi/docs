*
*   Nickname     : xs_hzha03pyth6156habbtt_e199.5_m12_m170_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Sat Nov 10 23:39:14 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_170_32081.xsdst ! RUN = 32081 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_170_32082.xsdst ! RUN = 32082 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_170_32083.xsdst ! RUN = 32083 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_170_32084.xsdst ! RUN = 32084 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_12_170_32085.xsdst ! RUN = 32085 ! NEVT = 500
