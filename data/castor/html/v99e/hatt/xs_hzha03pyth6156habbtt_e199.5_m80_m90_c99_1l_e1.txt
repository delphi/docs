*
*   Nickname     : xs_hzha03pyth6156habbtt_e199.5_m80_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 11:04:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_80_90_32421.xsdst ! RUN = 32421 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_80_90_32422.xsdst ! RUN = 32422 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_80_90_32423.xsdst ! RUN = 32423 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_80_90_32424.xsdst ! RUN = 32424 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_80_90_32425.xsdst ! RUN = 32425 ! NEVT = 500
