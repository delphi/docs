*
*   Nickname     : xs_hzha03pyth6156hattbb_e191.6_m30_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 17:06:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_40_37141.xsdst ! RUN = 37141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_40_37142.xsdst ! RUN = 37142 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_40_37143.xsdst ! RUN = 37143 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_40_37144.xsdst ! RUN = 37144 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_30_40_37145.xsdst ! RUN = 37145 ! NEVT = 500
