*
*   Nickname     : xs_hzha03pyth6156hattbb_e201.6_m60_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Nov 10 19:04:58 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_60_100_31351.xsdst ! RUN = 31351 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_60_100_31352.xsdst ! RUN = 31352 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_60_100_31353.xsdst ! RUN = 31353 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_60_100_31354.xsdst ! RUN = 31354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_60_100_31355.xsdst ! RUN = 31355 ! NEVT = 500
