*
*   Nickname     : xs_hzha03pyth6156habbtt_e195.5_m70_m110_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Mon Nov 12 03:09:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_70_110_34391.xsdst ! RUN = 34391 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_70_110_34392.xsdst ! RUN = 34392 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_70_110_34393.xsdst ! RUN = 34393 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_70_110_34394.xsdst ! RUN = 34394 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_habbtt_195.5_70_110_34395.xsdst ! RUN = 34395 ! NEVT = 500
