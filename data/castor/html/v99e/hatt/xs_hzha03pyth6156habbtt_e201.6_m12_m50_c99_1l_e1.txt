*
*   Nickname     : xs_hzha03pyth6156habbtt_e201.6_m12_m50_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Nov 10 19:05:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_50_30021.xsdst ! RUN = 30021 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_50_30022.xsdst ! RUN = 30022 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_50_30023.xsdst ! RUN = 30023 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_50_30024.xsdst ! RUN = 30024 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_habbtt_201.6_12_50_30025.xsdst ! RUN = 30025 ! NEVT = 500
