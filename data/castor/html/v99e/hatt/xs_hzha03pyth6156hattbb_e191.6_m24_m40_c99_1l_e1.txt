*
*   Nickname     : xs_hzha03pyth6156hattbb_e191.6_m24_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 11 15:00:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_24_40_37121.xsdst ! RUN = 37121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_24_40_37122.xsdst ! RUN = 37122 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_24_40_37123.xsdst ! RUN = 37123 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_24_40_37124.xsdst ! RUN = 37124 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_24_40_37125.xsdst ! RUN = 37125 ! NEVT = 500
