*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e195.5_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E195.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sat Nov 10 19:05:27 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100041.xsdst ! RUN = 100041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100042.xsdst ! RUN = 100042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100043.xsdst ! RUN = 100043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100044.xsdst ! RUN = 100044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100045.xsdst ! RUN = 100045 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100046.xsdst ! RUN = 100046 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100047.xsdst ! RUN = 100047 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100048.xsdst ! RUN = 100048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100049.xsdst ! RUN = 100049 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_100_100050.xsdst ! RUN = 100050 ! NEVT = 499
