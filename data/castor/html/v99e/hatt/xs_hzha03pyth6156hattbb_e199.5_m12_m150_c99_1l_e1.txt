*
*   Nickname     : xs_hzha03pyth6156hattbb_e199.5_m12_m150_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 18:56:49 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_150_33071.xsdst ! RUN = 33071 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_150_33072.xsdst ! RUN = 33072 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_150_33073.xsdst ! RUN = 33073 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_150_33074.xsdst ! RUN = 33074 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_12_150_33075.xsdst ! RUN = 33075 ! NEVT = 500
