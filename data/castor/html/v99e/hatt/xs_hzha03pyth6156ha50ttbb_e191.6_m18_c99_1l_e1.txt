*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e191.6_m18_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Nov 12 07:26:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101831.xsdst ! RUN = 101831 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101832.xsdst ! RUN = 101832 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101833.xsdst ! RUN = 101833 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101834.xsdst ! RUN = 101834 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101835.xsdst ! RUN = 101835 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101836.xsdst ! RUN = 101836 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101837.xsdst ! RUN = 101837 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101838.xsdst ! RUN = 101838 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101839.xsdst ! RUN = 101839 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha50ttbb_191.6_18_101840.xsdst ! RUN = 101840 ! NEVT = 500
