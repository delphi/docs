*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e199.5_m50_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E199.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4993 events in 10 files time stamp: Sat Nov 10 19:04:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85031.xsdst ! RUN = 85031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85032.xsdst ! RUN = 85032 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85033.xsdst ! RUN = 85033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85034.xsdst ! RUN = 85034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85035.xsdst ! RUN = 85035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85036.xsdst ! RUN = 85036 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85037.xsdst ! RUN = 85037 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85038.xsdst ! RUN = 85038 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85039.xsdst ! RUN = 85039 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_50_85040.xsdst ! RUN = 85040 ! NEVT = 498
