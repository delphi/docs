*
*   Nickname     : xs_hzha03pyth6156hattbb_e201.6_m50_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E201.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sat Nov 10 19:04:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_90_31271.xsdst ! RUN = 31271 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_90_31272.xsdst ! RUN = 31272 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_90_31273.xsdst ! RUN = 31273 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_90_31274.xsdst ! RUN = 31274 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hattbb_201.6_50_90_31275.xsdst ! RUN = 31275 ! NEVT = 500
