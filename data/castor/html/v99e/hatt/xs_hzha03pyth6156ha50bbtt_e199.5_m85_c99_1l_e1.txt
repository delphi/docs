*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e199.5_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E199.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Sat Nov 10 19:05:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88541.xsdst ! RUN = 88541 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88542.xsdst ! RUN = 88542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88543.xsdst ! RUN = 88543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88544.xsdst ! RUN = 88544 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88545.xsdst ! RUN = 88545 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88546.xsdst ! RUN = 88546 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88547.xsdst ! RUN = 88547 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88548.xsdst ! RUN = 88548 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88549.xsdst ! RUN = 88549 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50bbtt_199.5_85_88550.xsdst ! RUN = 88550 ! NEVT = 500
