*   Nickname :     xs_hzha03pyth6156ha4t_e199.5_m15_m25_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E199.5/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation 99e  done at ecm=199.5 GeV , CERN
*   Comments :     in total 2000 events in 2 files, time stamp: Mon Nov 11 4:12:30 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4t_199.5_15_25_1755.xsdst ! RUN = 1755 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4t_199.5_15_25_1756.xsdst ! RUN = 1756 ! NEVT = 1000
