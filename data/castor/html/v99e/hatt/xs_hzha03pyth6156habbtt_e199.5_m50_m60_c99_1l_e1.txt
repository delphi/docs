*
*   Nickname     : xs_hzha03pyth6156habbtt_e199.5_m50_m60_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Nov 11 11:02:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_50_60_32241.xsdst ! RUN = 32241 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_50_60_32242.xsdst ! RUN = 32242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_50_60_32243.xsdst ! RUN = 32243 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_50_60_32244.xsdst ! RUN = 32244 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_habbtt_199.5_50_60_32245.xsdst ! RUN = 32245 ! NEVT = 500
