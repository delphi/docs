*
*   Nickname     : xs_hzha03pyth6156hattbb_e191.6_m70_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Mon Nov 12 07:27:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_70_100_37381.xsdst ! RUN = 37381 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_70_100_37382.xsdst ! RUN = 37382 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_70_100_37383.xsdst ! RUN = 37383 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_70_100_37384.xsdst ! RUN = 37384 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hattbb_191.6_70_100_37385.xsdst ! RUN = 37385 ! NEVT = 500
