*
*   Nickname     : xs_hzha03pyth6156hattbb_e195.5_m70_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Mon Nov 12 04:55:18 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_100_35381.xsdst ! RUN = 35381 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_100_35382.xsdst ! RUN = 35382 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_100_35383.xsdst ! RUN = 35383 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_100_35384.xsdst ! RUN = 35384 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_70_100_35385.xsdst ! RUN = 35385 ! NEVT = 500
