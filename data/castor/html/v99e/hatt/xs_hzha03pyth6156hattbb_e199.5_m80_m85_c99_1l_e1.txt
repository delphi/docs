*
*   Nickname     : xs_hzha03pyth6156hattbb_e199.5_m80_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E199.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 12:42:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_80_85_33411.xsdst ! RUN = 33411 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_80_85_33412.xsdst ! RUN = 33412 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_80_85_33413.xsdst ! RUN = 33413 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_80_85_33414.xsdst ! RUN = 33414 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hattbb_199.5_80_85_33415.xsdst ! RUN = 33415 ! NEVT = 500
