*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e195.5_m80_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E195.5/CERN/SUMT/C001-9
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4496 events in 9 files time stamp: Tue Nov 13 07:43:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98041.xsdst ! RUN = 98041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98043.xsdst ! RUN = 98043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98044.xsdst ! RUN = 98044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98045.xsdst ! RUN = 98045 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98046.xsdst ! RUN = 98046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98047.xsdst ! RUN = 98047 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98048.xsdst ! RUN = 98048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98049.xsdst ! RUN = 98049 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50bbtt_195.5_80_98050.xsdst ! RUN = 98050 ! NEVT = 500
