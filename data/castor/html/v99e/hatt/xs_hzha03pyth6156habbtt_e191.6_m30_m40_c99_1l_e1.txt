*
*   Nickname     : xs_hzha03pyth6156habbtt_e191.6_m30_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E191.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sun Nov 11 17:05:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_40_36141.xsdst ! RUN = 36141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_40_36142.xsdst ! RUN = 36142 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_40_36143.xsdst ! RUN = 36143 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_40_36144.xsdst ! RUN = 36144 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_habbtt_191.6_30_40_36145.xsdst ! RUN = 36145 ! NEVT = 500
