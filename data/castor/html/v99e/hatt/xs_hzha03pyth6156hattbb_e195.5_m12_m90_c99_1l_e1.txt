*
*   Nickname     : xs_hzha03pyth6156hattbb_e195.5_m12_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E195.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sun Nov 11 14:58:37 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_12_90_35041.xsdst ! RUN = 35041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_12_90_35042.xsdst ! RUN = 35042 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_12_90_35043.xsdst ! RUN = 35043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_12_90_35044.xsdst ! RUN = 35044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hattbb_195.5_12_90_35045.xsdst ! RUN = 35045 ! NEVT = 499
