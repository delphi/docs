*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e195.5_m30_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E195.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sat Nov 10 19:05:29 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93031.xsdst ! RUN = 93031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93032.xsdst ! RUN = 93032 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93033.xsdst ! RUN = 93033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93034.xsdst ! RUN = 93034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93035.xsdst ! RUN = 93035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93036.xsdst ! RUN = 93036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93037.xsdst ! RUN = 93037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93038.xsdst ! RUN = 93038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93039.xsdst ! RUN = 93039 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50ttbb_195.5_30_93040.xsdst ! RUN = 93040 ! NEVT = 500
