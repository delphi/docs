*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e199.5_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E199.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4788 events in 10 files time stamp: Sat Nov 10 19:04:58 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88531.xsdst ! RUN = 88531 ! NEVT = 292
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88532.xsdst ! RUN = 88532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88533.xsdst ! RUN = 88533 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88534.xsdst ! RUN = 88534 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88535.xsdst ! RUN = 88535 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88536.xsdst ! RUN = 88536 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88537.xsdst ! RUN = 88537 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88538.xsdst ! RUN = 88538 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88539.xsdst ! RUN = 88539 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha50ttbb_199.5_85_88540.xsdst ! RUN = 88540 ! NEVT = 500
