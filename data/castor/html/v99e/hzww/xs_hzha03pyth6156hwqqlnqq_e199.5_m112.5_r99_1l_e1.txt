*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e199.5_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E199.5/RAL/SUMT/C001-13
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 6495 events in 13 files time stamp: Mon Aug  5 06:15:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23251.xsdst ! RUN = 23251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23252.xsdst ! RUN = 23252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23253.xsdst ! RUN = 23253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23254.xsdst ! RUN = 23254 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23255.xsdst ! RUN = 23255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23256.xsdst ! RUN = 23256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23257.xsdst ! RUN = 23257 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23258.xsdst ! RUN = 23258 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23259.xsdst ! RUN = 23259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23260.xsdst ! RUN = 23260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23261.xsdst ! RUN = 23261 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23263.xsdst ! RUN = 23263 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_112.5_23264.xsdst ! RUN = 23264 ! NEVT = 499
