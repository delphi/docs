*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e199.5_m120.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E199.5/RAL/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 5497 events in 11 files time stamp: Mon Aug  5 06:15:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22003.xsdst ! RUN = 22003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22004.xsdst ! RUN = 22004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22005.xsdst ! RUN = 22005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22006.xsdst ! RUN = 22006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22007.xsdst ! RUN = 22007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22008.xsdst ! RUN = 22008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22009.xsdst ! RUN = 22009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22010.xsdst ! RUN = 22010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22011.xsdst ! RUN = 22011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22014.xsdst ! RUN = 22014 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_120.0_22015.xsdst ! RUN = 22015 ! NEVT = 499
