*
*   Nickname     : xs_hzha03pyth6156hwqqqqnn_e201.6_m115.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQNN/E201.6/RAL/SUMT/C001-13
*   Description  :  Extended Short DST simulation 99e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 6498 events in 13 files time stamp: Tue Aug  6 01:15:11 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32501.xsdst ! RUN = 32501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32502.xsdst ! RUN = 32502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32503.xsdst ! RUN = 32503 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32504.xsdst ! RUN = 32504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32505.xsdst ! RUN = 32505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32506.xsdst ! RUN = 32506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32508.xsdst ! RUN = 32508 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32509.xsdst ! RUN = 32509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32510.xsdst ! RUN = 32510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32512.xsdst ! RUN = 32512 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32513.xsdst ! RUN = 32513 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32514.xsdst ! RUN = 32514 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hwqqqqnn_201.6_115.0_32515.xsdst ! RUN = 32515 ! NEVT = 500
