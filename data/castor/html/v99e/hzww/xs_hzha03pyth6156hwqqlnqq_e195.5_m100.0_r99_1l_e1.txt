*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e195.5_m100.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E195.5/RAL/SUMT/C001-14
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 6998 events in 14 files time stamp: Mon Aug  5 18:17:07 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32002.xsdst ! RUN = 32002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32003.xsdst ! RUN = 32003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32004.xsdst ! RUN = 32004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32005.xsdst ! RUN = 32005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32006.xsdst ! RUN = 32006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32007.xsdst ! RUN = 32007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32008.xsdst ! RUN = 32008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32009.xsdst ! RUN = 32009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32010.xsdst ! RUN = 32010 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32011.xsdst ! RUN = 32011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32012.xsdst ! RUN = 32012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32013.xsdst ! RUN = 32013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32014.xsdst ! RUN = 32014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_100.0_32015.xsdst ! RUN = 32015 ! NEVT = 499
