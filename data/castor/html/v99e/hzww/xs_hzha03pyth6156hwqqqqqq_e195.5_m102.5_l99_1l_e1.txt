*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e195.5_m102.5_l99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E195.5/LYON/SUMT/C001-9
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , Lyon
*---
*   Comments     : in total 4499 events in 9 files time stamp: Sun Jul 28 15:13:16 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30250.xsdst ! RUN = 30250 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30252.xsdst ! RUN = 30252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30253.xsdst ! RUN = 30253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30254.xsdst ! RUN = 30254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30255.xsdst ! RUN = 30255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30256.xsdst ! RUN = 30256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30257.xsdst ! RUN = 30257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30258.xsdst ! RUN = 30258 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_102.5_30259.xsdst ! RUN = 30259 ! NEVT = 499
