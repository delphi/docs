*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e199.5_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E199.5/RAL/SUMT/C001-9
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4499 events in 9 files time stamp: Sun Aug  4 22:14:55 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21251.xsdst ! RUN = 21251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21252.xsdst ! RUN = 21252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21253.xsdst ! RUN = 21253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21255.xsdst ! RUN = 21255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21256.xsdst ! RUN = 21256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21258.xsdst ! RUN = 21258 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21259.xsdst ! RUN = 21259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21260.xsdst ! RUN = 21260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqqq_199.5_112.5_21262.xsdst ! RUN = 21262 ! NEVT = 500
