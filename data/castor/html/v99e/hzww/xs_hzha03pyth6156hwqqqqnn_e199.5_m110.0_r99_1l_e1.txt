*
*   Nickname     : xs_hzha03pyth6156hwqqqqnn_e199.5_m110.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQNN/E199.5/RAL/SUMT/C001-15
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 7499 events in 15 files time stamp: Mon Aug  5 01:15:40 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22001.xsdst ! RUN = 22001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22002.xsdst ! RUN = 22002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22003.xsdst ! RUN = 22003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22004.xsdst ! RUN = 22004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22005.xsdst ! RUN = 22005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22006.xsdst ! RUN = 22006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22007.xsdst ! RUN = 22007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22008.xsdst ! RUN = 22008 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22009.xsdst ! RUN = 22009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22010.xsdst ! RUN = 22010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22011.xsdst ! RUN = 22011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22012.xsdst ! RUN = 22012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22013.xsdst ! RUN = 22013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22014.xsdst ! RUN = 22014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_110.0_22015.xsdst ! RUN = 22015 ! NEVT = 500
