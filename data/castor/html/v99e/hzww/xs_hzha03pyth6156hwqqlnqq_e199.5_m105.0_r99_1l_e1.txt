*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e199.5_m105.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E199.5/RAL/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 5497 events in 11 files time stamp: Mon Aug  5 11:16:13 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22501.xsdst ! RUN = 22501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22502.xsdst ! RUN = 22502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22503.xsdst ! RUN = 22503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22505.xsdst ! RUN = 22505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22507.xsdst ! RUN = 22507 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22508.xsdst ! RUN = 22508 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22510.xsdst ! RUN = 22510 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22512.xsdst ! RUN = 22512 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22513.xsdst ! RUN = 22513 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22514.xsdst ! RUN = 22514 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_105.0_22515.xsdst ! RUN = 22515 ! NEVT = 500
