*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e195.5_m105.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Mon Aug  5 11:16:14 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_105.0_30511.xsdst ! RUN = 30511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_105.0_30512.xsdst ! RUN = 30512 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_105.0_30513.xsdst ! RUN = 30513 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_105.0_30515.xsdst ! RUN = 30515 ! NEVT = 500
