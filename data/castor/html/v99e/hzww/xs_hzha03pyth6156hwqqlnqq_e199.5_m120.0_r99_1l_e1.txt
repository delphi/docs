*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e199.5_m120.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E199.5/RAL/SUMT/C001-14
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 6999 events in 14 files time stamp: Mon Aug  5 11:16:10 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24001.xsdst ! RUN = 24001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24002.xsdst ! RUN = 24002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24003.xsdst ! RUN = 24003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24004.xsdst ! RUN = 24004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24005.xsdst ! RUN = 24005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24006.xsdst ! RUN = 24006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24007.xsdst ! RUN = 24007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24008.xsdst ! RUN = 24008 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24009.xsdst ! RUN = 24009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24010.xsdst ! RUN = 24010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24011.xsdst ! RUN = 24011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24012.xsdst ! RUN = 24012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24014.xsdst ! RUN = 24014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqlnqq_199.5_120.0_24015.xsdst ! RUN = 24015 ! NEVT = 500
