*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e195.5_m107.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E195.5/RAL/SUMT/C001-3
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 1498 events in 3 files time stamp: Mon Aug  5 11:16:07 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_107.5_30763.xsdst ! RUN = 30763 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_107.5_30764.xsdst ! RUN = 30764 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_107.5_30765.xsdst ! RUN = 30765 ! NEVT = 500
