*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e195.5_m115.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E195.5/RAL/SUMT/C001-14
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 6996 events in 14 files time stamp: Mon Aug  5 22:15:06 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33501.xsdst ! RUN = 33501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33502.xsdst ! RUN = 33502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33504.xsdst ! RUN = 33504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33505.xsdst ! RUN = 33505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33506.xsdst ! RUN = 33506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33507.xsdst ! RUN = 33507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33508.xsdst ! RUN = 33508 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33509.xsdst ! RUN = 33509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33510.xsdst ! RUN = 33510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33511.xsdst ! RUN = 33511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33512.xsdst ! RUN = 33512 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33513.xsdst ! RUN = 33513 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33514.xsdst ! RUN = 33514 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqlnqq_195.5_115.0_33515.xsdst ! RUN = 33515 ! NEVT = 498
