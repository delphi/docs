*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e195.5_m100.0_l99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E195.5/LYON/SUMT/C001-7
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , Lyon
*---
*   Comments     : in total 3495 events in 7 files time stamp: Sun Jul 28 11:13:22 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30000.xsdst ! RUN = 30000 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30001.xsdst ! RUN = 30001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30003.xsdst ! RUN = 30003 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30005.xsdst ! RUN = 30005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30007.xsdst ! RUN = 30007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30008.xsdst ! RUN = 30008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hwqqqqqq_195.5_100.0_30009.xsdst ! RUN = 30009 ! NEVT = 500
