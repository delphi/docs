*
*   Nickname     : xs_hzha03pyth6156hwqqqqnn_e199.5_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQNN/E199.5/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4500 events in 10 files time stamp: Mon Aug  5 01:15:43 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22251.xsdst ! RUN = 22251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22252.xsdst ! RUN = 22252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22254.xsdst ! RUN = 22254 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22255.xsdst ! RUN = 22255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22256.xsdst ! RUN = 22256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22257.xsdst ! RUN = 22257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22259.xsdst ! RUN = 22259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22260.xsdst ! RUN = 22260 ! NEVT = 1
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22263.xsdst ! RUN = 22263 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hwqqqqnn_199.5_112.5_22264.xsdst ! RUN = 22264 ! NEVT = 500
