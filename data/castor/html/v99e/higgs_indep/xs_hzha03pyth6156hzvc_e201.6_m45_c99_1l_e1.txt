*   Nickname :     xs_hzha03pyth6156hzvc_e201.6_m45_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVC/E201.6/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu c c Extended Short DST simulation 99e  done at ecm=201.6 GeV , CERN
*---
*   Comments :     in total 1999 events in 2 files, time stamp: Sat Aug 31 18:12:24 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzvc_201.6_45_4803.xsdst ! RUN = 4803 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzvc_201.6_45_4804.xsdst ! RUN = 4804 ! NEVT = 999
