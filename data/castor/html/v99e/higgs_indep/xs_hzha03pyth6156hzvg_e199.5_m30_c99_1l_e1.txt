*   Nickname :     xs_hzha03pyth6156hzvg_e199.5_m30_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVG/E199.5/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu gluon gluon Extended Short DST simulation 99e  done at ecm=199.5 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Sat Aug 31 6:12:12 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzvg_199.5_30_3301.xsdst ! RUN = 3301 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzvg_199.5_30_3302.xsdst ! RUN = 3302 ! NEVT = 1000
