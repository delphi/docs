*   Nickname :     xs_hzha03pyth6156hzvg_e191.6_m25_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVG/E191.6/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu gluon gluon Extended Short DST simulation 99e  done at ecm=191.6 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Sat Aug 31 1:12:11 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzvg_191.6_25_2801.xsdst ! RUN = 2801 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzvg_191.6_25_2802.xsdst ! RUN = 2802 ! NEVT = 1000
