*   Nickname :     xs_hzha03pyth6156hzvs_e201.6_m40_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVS/E201.6/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu s s Extended Short DST simulation 99e  done at ecm=201.6 GeV , CERN
*---
*   Comments :     in total 1999 events in 2 files, time stamp: Sat Aug 31 15:12:3 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzvs_201.6_40_4302.xsdst ! RUN = 4302 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzvs_201.6_40_4303.xsdst ! RUN = 4303 ! NEVT = 999
