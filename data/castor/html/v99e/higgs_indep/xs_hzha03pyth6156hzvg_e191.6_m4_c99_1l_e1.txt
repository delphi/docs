*   Nickname :     xs_hzha03pyth6156hzvg_e191.6_m4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVG/E191.6/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu gluon gluon Extended Short DST simulation 99e  done at ecm=191.6 GeV , CERN
*---
*   Comments :     in total 1999 events in 2 files, time stamp: Sat Aug 31 1:12:10 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzvg_191.6_4_701.xsdst ! RUN = 701 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzvg_191.6_4_702.xsdst ! RUN = 702 ! NEVT = 999
