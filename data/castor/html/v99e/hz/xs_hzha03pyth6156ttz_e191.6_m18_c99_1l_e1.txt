*
*   Nickname     : xs_hzha03pyth6156ttz_e191.6_m18_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Fri Sep 14 06:40:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32151.xsdst ! RUN = 32151 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32152.xsdst ! RUN = 32152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32153.xsdst ! RUN = 32153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32154.xsdst ! RUN = 32154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32155.xsdst ! RUN = 32155 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32156.xsdst ! RUN = 32156 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32157.xsdst ! RUN = 32157 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32158.xsdst ! RUN = 32158 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32159.xsdst ! RUN = 32159 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ttz_191.6_18_32160.xsdst ! RUN = 32160 ! NEVT = 500
