*
*   Nickname     : xs_hzha03pyth6156hzmu_e199.5_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/E199.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Fri Sep  7 21:42:57 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40131.xsdst ! RUN = 40131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40132.xsdst ! RUN = 40132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40133.xsdst ! RUN = 40133 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40134.xsdst ! RUN = 40134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40135.xsdst ! RUN = 40135 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40136.xsdst ! RUN = 40136 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40137.xsdst ! RUN = 40137 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40138.xsdst ! RUN = 40138 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40139.xsdst ! RUN = 40139 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzmu_199.5_100_40140.xsdst ! RUN = 40140 ! NEVT = 500
