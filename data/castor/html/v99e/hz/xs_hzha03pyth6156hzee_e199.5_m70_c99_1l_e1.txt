*
*   Nickname     : xs_hzha03pyth6156hzee_e199.5_m70_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZEE/E199.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-)(HZ+interference+fusion) Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4993 events in 10 files time stamp: Fri Sep  7 22:34:29 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37121.xsdst ! RUN = 37121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37122.xsdst ! RUN = 37122 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37123.xsdst ! RUN = 37123 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37124.xsdst ! RUN = 37124 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37125.xsdst ! RUN = 37125 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37126.xsdst ! RUN = 37126 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37127.xsdst ! RUN = 37127 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37128.xsdst ! RUN = 37128 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37129.xsdst ! RUN = 37129 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzee_199.5_70_37130.xsdst ! RUN = 37130 ! NEVT = 500
