*
*   Nickname     : xs_hzha03pyth6156hzmu_e191.6_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Mon Sep 10 19:28:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38831.xsdst ! RUN = 38831 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38832.xsdst ! RUN = 38832 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38833.xsdst ! RUN = 38833 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38834.xsdst ! RUN = 38834 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38835.xsdst ! RUN = 38835 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38836.xsdst ! RUN = 38836 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38837.xsdst ! RUN = 38837 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38838.xsdst ! RUN = 38838 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38839.xsdst ! RUN = 38839 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_85_38840.xsdst ! RUN = 38840 ! NEVT = 500
