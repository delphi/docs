*
*   Nickname     : xs_hzha03pyth6156hzmu_e191.6_m60_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Sep 10 18:30:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36331.xsdst ! RUN = 36331 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36332.xsdst ! RUN = 36332 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36333.xsdst ! RUN = 36333 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36334.xsdst ! RUN = 36334 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36335.xsdst ! RUN = 36335 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36336.xsdst ! RUN = 36336 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36337.xsdst ! RUN = 36337 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36338.xsdst ! RUN = 36338 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36339.xsdst ! RUN = 36339 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hzmu_191.6_60_36340.xsdst ! RUN = 36340 ! NEVT = 500
