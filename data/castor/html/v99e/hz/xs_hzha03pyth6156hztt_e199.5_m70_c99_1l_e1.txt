*
*   Nickname     : xs_hzha03pyth6156hztt_e199.5_m70_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZTT/E199.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Fri Sep  7 17:51:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37141.xsdst ! RUN = 37141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37142.xsdst ! RUN = 37142 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37143.xsdst ! RUN = 37143 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37144.xsdst ! RUN = 37144 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37145.xsdst ! RUN = 37145 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37146.xsdst ! RUN = 37146 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37147.xsdst ! RUN = 37147 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37148.xsdst ! RUN = 37148 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37149.xsdst ! RUN = 37149 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_70_37150.xsdst ! RUN = 37150 ! NEVT = 500
