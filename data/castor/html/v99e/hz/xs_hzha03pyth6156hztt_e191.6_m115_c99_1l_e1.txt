*
*   Nickname     : xs_hzha03pyth6156hztt_e191.6_m115_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZTT/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Tue Sep 11 14:31:40 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41841.xsdst ! RUN = 41841 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41842.xsdst ! RUN = 41842 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41843.xsdst ! RUN = 41843 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41844.xsdst ! RUN = 41844 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41845.xsdst ! RUN = 41845 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41846.xsdst ! RUN = 41846 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41847.xsdst ! RUN = 41847 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41848.xsdst ! RUN = 41848 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41849.xsdst ! RUN = 41849 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_115_41850.xsdst ! RUN = 41850 ! NEVT = 500
