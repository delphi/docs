*
*   Nickname     : xs_hzha03pyth6156hztt_e199.5_m100_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZTT/E199.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Sat Sep  8 00:38:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40141.xsdst ! RUN = 40141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40142.xsdst ! RUN = 40142 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40143.xsdst ! RUN = 40143 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40144.xsdst ! RUN = 40144 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40145.xsdst ! RUN = 40145 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40146.xsdst ! RUN = 40146 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40147.xsdst ! RUN = 40147 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40148.xsdst ! RUN = 40148 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40149.xsdst ! RUN = 40149 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hztt_199.5_100_40150.xsdst ! RUN = 40150 ! NEVT = 499
