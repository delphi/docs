*
*   Nickname     : xs_hzha03pyth6156ttz_e195.5_m24_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E195.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Sun Sep  9 02:24:13 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32651.xsdst ! RUN = 32651 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32652.xsdst ! RUN = 32652 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32653.xsdst ! RUN = 32653 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32654.xsdst ! RUN = 32654 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32655.xsdst ! RUN = 32655 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32656.xsdst ! RUN = 32656 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32657.xsdst ! RUN = 32657 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32658.xsdst ! RUN = 32658 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32659.xsdst ! RUN = 32659 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ttz_195.5_24_32660.xsdst ! RUN = 32660 ! NEVT = 500
