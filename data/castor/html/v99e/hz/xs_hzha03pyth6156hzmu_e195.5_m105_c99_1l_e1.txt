*
*   Nickname     : xs_hzha03pyth6156hzmu_e195.5_m105_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/E195.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Fri Sep 14 09:35:30 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40731.xsdst ! RUN = 40731 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40732.xsdst ! RUN = 40732 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40733.xsdst ! RUN = 40733 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40734.xsdst ! RUN = 40734 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40735.xsdst ! RUN = 40735 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40736.xsdst ! RUN = 40736 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40737.xsdst ! RUN = 40737 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40738.xsdst ! RUN = 40738 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40739.xsdst ! RUN = 40739 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzmu_195.5_105_40740.xsdst ! RUN = 40740 ! NEVT = 500
