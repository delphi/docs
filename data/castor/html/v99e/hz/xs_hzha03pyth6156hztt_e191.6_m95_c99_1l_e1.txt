*
*   Nickname     : xs_hzha03pyth6156hztt_e191.6_m95_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZTT/E191.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Fri Sep 14 07:42:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39841.xsdst ! RUN = 39841 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39842.xsdst ! RUN = 39842 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39843.xsdst ! RUN = 39843 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39844.xsdst ! RUN = 39844 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39845.xsdst ! RUN = 39845 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39846.xsdst ! RUN = 39846 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39847.xsdst ! RUN = 39847 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39848.xsdst ! RUN = 39848 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39849.xsdst ! RUN = 39849 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hztt_191.6_95_39850.xsdst ! RUN = 39850 ! NEVT = 500
