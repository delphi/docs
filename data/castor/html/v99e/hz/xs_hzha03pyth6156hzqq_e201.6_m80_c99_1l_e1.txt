*
*   Nickname     : xs_hzha03pyth6156hzqq_e201.6_m80_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/E201.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Thu Sep  6 00:25:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38001.xsdst ! RUN = 38001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38002.xsdst ! RUN = 38002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38003.xsdst ! RUN = 38003 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38004.xsdst ! RUN = 38004 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38005.xsdst ! RUN = 38005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38006.xsdst ! RUN = 38006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38007.xsdst ! RUN = 38007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38008.xsdst ! RUN = 38008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38009.xsdst ! RUN = 38009 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hzqq_201.6_80_38010.xsdst ! RUN = 38010 ! NEVT = 499
