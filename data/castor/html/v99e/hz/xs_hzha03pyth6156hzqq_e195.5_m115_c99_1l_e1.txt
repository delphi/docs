*
*   Nickname     : xs_hzha03pyth6156hzqq_e195.5_m115_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/E195.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Mon Sep 10 07:34:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41701.xsdst ! RUN = 41701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41702.xsdst ! RUN = 41702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41703.xsdst ! RUN = 41703 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41704.xsdst ! RUN = 41704 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41705.xsdst ! RUN = 41705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41706.xsdst ! RUN = 41706 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41707.xsdst ! RUN = 41707 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41708.xsdst ! RUN = 41708 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41709.xsdst ! RUN = 41709 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hzqq_195.5_115_41710.xsdst ! RUN = 41710 ! NEVT = 500
