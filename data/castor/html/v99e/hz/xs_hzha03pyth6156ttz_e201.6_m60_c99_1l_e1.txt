*
*   Nickname     : xs_hzha03pyth6156ttz_e201.6_m60_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E201.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Wed Sep  5 05:28:58 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36051.xsdst ! RUN = 36051 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36052.xsdst ! RUN = 36052 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36053.xsdst ! RUN = 36053 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36054.xsdst ! RUN = 36054 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36055.xsdst ! RUN = 36055 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36056.xsdst ! RUN = 36056 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36057.xsdst ! RUN = 36057 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36058.xsdst ! RUN = 36058 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36059.xsdst ! RUN = 36059 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ttz_201.6_60_36060.xsdst ! RUN = 36060 ! NEVT = 500
