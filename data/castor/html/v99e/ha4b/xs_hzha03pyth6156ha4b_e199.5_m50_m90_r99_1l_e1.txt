*
*   Nickname     : xs_hzha03pyth6156ha4b_e199.5_m50_m90_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E199.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4994 events in 10 files time stamp: Thu Nov  1 17:01:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70261.xsdst ! RUN = 70261 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70262.xsdst ! RUN = 70262 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70263.xsdst ! RUN = 70263 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70264.xsdst ! RUN = 70264 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70265.xsdst ! RUN = 70265 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70266.xsdst ! RUN = 70266 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70267.xsdst ! RUN = 70267 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70268.xsdst ! RUN = 70268 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70269.xsdst ! RUN = 70269 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_50_90_70270.xsdst ! RUN = 70270 ! NEVT = 500
