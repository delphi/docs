*
*   Nickname     : xs_hzha03pyth6156ha4b_e191.6_m60_m90_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E191.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Tue Nov  6 14:57:56 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90321.xsdst ! RUN = 90321 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90322.xsdst ! RUN = 90322 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90323.xsdst ! RUN = 90323 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90324.xsdst ! RUN = 90324 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90325.xsdst ! RUN = 90325 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90326.xsdst ! RUN = 90326 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90327.xsdst ! RUN = 90327 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90328.xsdst ! RUN = 90328 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90329.xsdst ! RUN = 90329 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_60_90_90330.xsdst ! RUN = 90330 ! NEVT = 499
