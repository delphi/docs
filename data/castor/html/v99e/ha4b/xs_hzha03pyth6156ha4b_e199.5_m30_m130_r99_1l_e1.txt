*
*   Nickname     : xs_hzha03pyth6156ha4b_e199.5_m30_m130_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E199.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4781 events in 10 files time stamp: Sun Nov 25 11:01:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70191.xsdst ! RUN = 70191 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70192.xsdst ! RUN = 70192 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70193.xsdst ! RUN = 70193 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70194.xsdst ! RUN = 70194 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70195.xsdst ! RUN = 70195 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70196.xsdst ! RUN = 70196 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70197.xsdst ! RUN = 70197 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70198.xsdst ! RUN = 70198 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70199.xsdst ! RUN = 70199 ! NEVT = 283
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_30_130_70200.xsdst ! RUN = 70200 ! NEVT = 500
