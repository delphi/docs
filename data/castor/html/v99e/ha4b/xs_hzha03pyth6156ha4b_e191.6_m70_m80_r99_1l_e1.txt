*
*   Nickname     : xs_hzha03pyth6156ha4b_e191.6_m70_m80_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E191.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Mon Nov 12 19:02:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90351.xsdst ! RUN = 90351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90352.xsdst ! RUN = 90352 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90353.xsdst ! RUN = 90353 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90354.xsdst ! RUN = 90354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90355.xsdst ! RUN = 90355 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90356.xsdst ! RUN = 90356 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90357.xsdst ! RUN = 90357 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90358.xsdst ! RUN = 90358 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90359.xsdst ! RUN = 90359 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_80_90360.xsdst ! RUN = 90360 ! NEVT = 500
