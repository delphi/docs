*
*   Nickname     : xs_hzha03pyth6156ha4b_e201.6_m30_m90_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E201.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Wed Oct  3 03:41:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60171.xsdst ! RUN = 60171 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60172.xsdst ! RUN = 60172 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60173.xsdst ! RUN = 60173 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60174.xsdst ! RUN = 60174 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60175.xsdst ! RUN = 60175 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60176.xsdst ! RUN = 60176 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60177.xsdst ! RUN = 60177 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60178.xsdst ! RUN = 60178 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60179.xsdst ! RUN = 60179 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_30_90_60180.xsdst ! RUN = 60180 ! NEVT = 500
