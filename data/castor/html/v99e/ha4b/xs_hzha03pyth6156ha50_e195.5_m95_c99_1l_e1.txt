*
*   Nickname     : xs_hzha03pyth6156ha50_e195.5_m95_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E195.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 4899 events in 10 files time stamp: Sun Sep  9 23:40:25 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49701.xsdst ! RUN = 49701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49702.xsdst ! RUN = 49702 ! NEVT = 404
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49703.xsdst ! RUN = 49703 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49704.xsdst ! RUN = 49704 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49705.xsdst ! RUN = 49705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49706.xsdst ! RUN = 49706 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49707.xsdst ! RUN = 49707 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49708.xsdst ! RUN = 49708 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49709.xsdst ! RUN = 49709 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha50_195.5_95_49710.xsdst ! RUN = 49710 ! NEVT = 500
