*
*   Nickname     : xs_hzha03pyth6156ha4b_e199.5_m70_m70_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E199.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4994 events in 10 files time stamp: Fri Nov  2 11:00:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70341.xsdst ! RUN = 70341 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70342.xsdst ! RUN = 70342 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70343.xsdst ! RUN = 70343 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70344.xsdst ! RUN = 70344 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70345.xsdst ! RUN = 70345 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70346.xsdst ! RUN = 70346 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70347.xsdst ! RUN = 70347 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70348.xsdst ! RUN = 70348 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70349.xsdst ! RUN = 70349 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_70_70350.xsdst ! RUN = 70350 ! NEVT = 500
