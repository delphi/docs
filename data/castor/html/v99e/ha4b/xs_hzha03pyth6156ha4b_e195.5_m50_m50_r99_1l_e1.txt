*
*   Nickname     : xs_hzha03pyth6156ha4b_e195.5_m50_m50_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E195.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sat Nov  3 18:25:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80231.xsdst ! RUN = 80231 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80232.xsdst ! RUN = 80232 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80233.xsdst ! RUN = 80233 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80234.xsdst ! RUN = 80234 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80235.xsdst ! RUN = 80235 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80236.xsdst ! RUN = 80236 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80237.xsdst ! RUN = 80237 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80238.xsdst ! RUN = 80238 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80239.xsdst ! RUN = 80239 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_50_80240.xsdst ! RUN = 80240 ! NEVT = 500
