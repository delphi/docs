*
*   Nickname     : xs_hzha03pyth6156ha4b_e191.6_m70_m110_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E191.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Wed Nov 14 01:03:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90391.xsdst ! RUN = 90391 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90392.xsdst ! RUN = 90392 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90393.xsdst ! RUN = 90393 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90394.xsdst ! RUN = 90394 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90395.xsdst ! RUN = 90395 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90396.xsdst ! RUN = 90396 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90397.xsdst ! RUN = 90397 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90398.xsdst ! RUN = 90398 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90399.xsdst ! RUN = 90399 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_70_110_90400.xsdst ! RUN = 90400 ! NEVT = 500
