*
*   Nickname     : xs_hzha03pyth6156ha4b_e195.5_m60_m80_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E195.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 4992 events in 10 files time stamp: Sun Nov  4 02:27:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80311.xsdst ! RUN = 80311 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80312.xsdst ! RUN = 80312 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80313.xsdst ! RUN = 80313 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80314.xsdst ! RUN = 80314 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80315.xsdst ! RUN = 80315 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80316.xsdst ! RUN = 80316 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80317.xsdst ! RUN = 80317 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80318.xsdst ! RUN = 80318 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80319.xsdst ! RUN = 80319 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_60_80_80320.xsdst ! RUN = 80320 ! NEVT = 499
