*
*   Nickname     : xs_hzha03pyth6156ha4b_e195.5_m30_m110_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E195.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Thu Nov  8 09:03:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80181.xsdst ! RUN = 80181 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80182.xsdst ! RUN = 80182 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80183.xsdst ! RUN = 80183 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80184.xsdst ! RUN = 80184 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80185.xsdst ! RUN = 80185 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80186.xsdst ! RUN = 80186 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80187.xsdst ! RUN = 80187 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80188.xsdst ! RUN = 80188 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80189.xsdst ! RUN = 80189 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_30_110_80190.xsdst ! RUN = 80190 ! NEVT = 499
