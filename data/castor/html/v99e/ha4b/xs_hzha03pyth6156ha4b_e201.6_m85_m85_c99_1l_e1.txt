*
*   Nickname     : xs_hzha03pyth6156ha4b_e201.6_m85_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E201.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 4993 events in 10 files time stamp: Wed Oct  3 23:39:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60471.xsdst ! RUN = 60471 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60472.xsdst ! RUN = 60472 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60473.xsdst ! RUN = 60473 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60474.xsdst ! RUN = 60474 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60475.xsdst ! RUN = 60475 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60476.xsdst ! RUN = 60476 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60477.xsdst ! RUN = 60477 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60478.xsdst ! RUN = 60478 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60479.xsdst ! RUN = 60479 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_85_85_60480.xsdst ! RUN = 60480 ! NEVT = 500
