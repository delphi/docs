*
*   Nickname     : xs_hzha03pyth6156ha4b_e199.5_m70_m90_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E199.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4994 events in 10 files time stamp: Fri Nov  2 11:00:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70371.xsdst ! RUN = 70371 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70372.xsdst ! RUN = 70372 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70373.xsdst ! RUN = 70373 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70374.xsdst ! RUN = 70374 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70375.xsdst ! RUN = 70375 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70376.xsdst ! RUN = 70376 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70377.xsdst ! RUN = 70377 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70378.xsdst ! RUN = 70378 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70379.xsdst ! RUN = 70379 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_70_90_70380.xsdst ! RUN = 70380 ! NEVT = 500
