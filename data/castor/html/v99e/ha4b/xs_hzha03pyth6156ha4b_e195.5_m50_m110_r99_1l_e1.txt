*
*   Nickname     : xs_hzha03pyth6156ha4b_e195.5_m50_m110_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E195.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Nov  4 02:27:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80271.xsdst ! RUN = 80271 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80272.xsdst ! RUN = 80272 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80273.xsdst ! RUN = 80273 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80274.xsdst ! RUN = 80274 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80275.xsdst ! RUN = 80275 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80276.xsdst ! RUN = 80276 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80277.xsdst ! RUN = 80277 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80278.xsdst ! RUN = 80278 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80279.xsdst ! RUN = 80279 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_50_110_80280.xsdst ! RUN = 80280 ! NEVT = 500
