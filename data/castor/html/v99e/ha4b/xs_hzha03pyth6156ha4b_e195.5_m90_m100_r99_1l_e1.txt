*
*   Nickname     : xs_hzha03pyth6156ha4b_e195.5_m90_m100_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E195.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Nov  5 00:40:54 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80491.xsdst ! RUN = 80491 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80492.xsdst ! RUN = 80492 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80493.xsdst ! RUN = 80493 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80494.xsdst ! RUN = 80494 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80495.xsdst ! RUN = 80495 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80496.xsdst ! RUN = 80496 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80497.xsdst ! RUN = 80497 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80498.xsdst ! RUN = 80498 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80499.xsdst ! RUN = 80499 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_ha4b_195.5_90_100_80500.xsdst ! RUN = 80500 ! NEVT = 500
