*
*   Nickname     : xs_hzha03pyth6156ha4b_e201.6_m12_m150_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E201.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 4988 events in 10 files time stamp: Wed Oct  3 03:44:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60071.xsdst ! RUN = 60071 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60072.xsdst ! RUN = 60072 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60073.xsdst ! RUN = 60073 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60074.xsdst ! RUN = 60074 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60075.xsdst ! RUN = 60075 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60076.xsdst ! RUN = 60076 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60077.xsdst ! RUN = 60077 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60078.xsdst ! RUN = 60078 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60079.xsdst ! RUN = 60079 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_ha4b_201.6_12_150_60080.xsdst ! RUN = 60080 ! NEVT = 497
