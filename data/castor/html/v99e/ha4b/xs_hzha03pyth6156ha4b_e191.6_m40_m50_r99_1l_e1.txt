*
*   Nickname     : xs_hzha03pyth6156ha4b_e191.6_m40_m50_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E191.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Tue Nov  6 04:46:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90221.xsdst ! RUN = 90221 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90222.xsdst ! RUN = 90222 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90223.xsdst ! RUN = 90223 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90224.xsdst ! RUN = 90224 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90225.xsdst ! RUN = 90225 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90226.xsdst ! RUN = 90226 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90227.xsdst ! RUN = 90227 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90228.xsdst ! RUN = 90228 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90229.xsdst ! RUN = 90229 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_50_90230.xsdst ! RUN = 90230 ! NEVT = 500
