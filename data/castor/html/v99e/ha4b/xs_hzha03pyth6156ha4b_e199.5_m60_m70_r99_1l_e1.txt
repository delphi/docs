*
*   Nickname     : xs_hzha03pyth6156ha4b_e199.5_m60_m70_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E199.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 4994 events in 10 files time stamp: Wed Nov  7 12:53:33 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70301.xsdst ! RUN = 70301 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70302.xsdst ! RUN = 70302 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70303.xsdst ! RUN = 70303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70304.xsdst ! RUN = 70304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70305.xsdst ! RUN = 70305 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70306.xsdst ! RUN = 70306 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70307.xsdst ! RUN = 70307 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70308.xsdst ! RUN = 70308 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70309.xsdst ! RUN = 70309 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_ha4b_199.5_60_70_70310.xsdst ! RUN = 70310 ! NEVT = 498
