*
*   Nickname     : xs_hzha03pyth6156ha4b_e191.6_m40_m40_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E191.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Tue Nov  6 02:24:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90211.xsdst ! RUN = 90211 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90212.xsdst ! RUN = 90212 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90213.xsdst ! RUN = 90213 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90214.xsdst ! RUN = 90214 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90215.xsdst ! RUN = 90215 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90216.xsdst ! RUN = 90216 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90217.xsdst ! RUN = 90217 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90218.xsdst ! RUN = 90218 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90219.xsdst ! RUN = 90219 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_ha4b_191.6_40_40_90220.xsdst ! RUN = 90220 ! NEVT = 500
