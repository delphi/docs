*
*   Nickname     : xs_wphact22ccbe32i_e201.6_m80.4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32I/E201.6/CERN/SUMT/C001-25
*   Description  :  Extended Short DST simulation 99e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 49975 events in 25 files time stamp: Thu Mar  7 10:20:48 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140201.xsdst ! RUN = 140201 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140202.xsdst ! RUN = 140202 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140203.xsdst ! RUN = 140203 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140204.xsdst ! RUN = 140204 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140205.xsdst ! RUN = 140205 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140206.xsdst ! RUN = 140206 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140207.xsdst ! RUN = 140207 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140208.xsdst ! RUN = 140208 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140209.xsdst ! RUN = 140209 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140210.xsdst ! RUN = 140210 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140211.xsdst ! RUN = 140211 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140212.xsdst ! RUN = 140212 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140213.xsdst ! RUN = 140213 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140214.xsdst ! RUN = 140214 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140215.xsdst ! RUN = 140215 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140216.xsdst ! RUN = 140216 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140217.xsdst ! RUN = 140217 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140218.xsdst ! RUN = 140218 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140219.xsdst ! RUN = 140219 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140220.xsdst ! RUN = 140220 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140221.xsdst ! RUN = 140221 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140222.xsdst ! RUN = 140222 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140223.xsdst ! RUN = 140223 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140224.xsdst ! RUN = 140224 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/201.6/wphact22_ccbe32i_201.6_80.4_140225.xsdst ! RUN = 140225 ! NEVT = 1997
