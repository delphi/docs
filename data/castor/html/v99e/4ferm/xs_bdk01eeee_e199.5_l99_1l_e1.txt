*
*   Nickname     : xs_bdk01eeee_e199.5_l99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/BDK01EEEE/E199.5/LYON/SUMT/C001-100
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , Lyon
*---
*   Comments     : in total 199999 events in 100 files time stamp: Sun Oct 21 12:18:55 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29001.xsdst ! RUN = 29001 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29002.xsdst ! RUN = 29002 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29003.xsdst ! RUN = 29003 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29004.xsdst ! RUN = 29004 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29005.xsdst ! RUN = 29005 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29006.xsdst ! RUN = 29006 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29007.xsdst ! RUN = 29007 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29008.xsdst ! RUN = 29008 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29009.xsdst ! RUN = 29009 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29010.xsdst ! RUN = 29010 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29011.xsdst ! RUN = 29011 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29012.xsdst ! RUN = 29012 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29013.xsdst ! RUN = 29013 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29014.xsdst ! RUN = 29014 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29015.xsdst ! RUN = 29015 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29016.xsdst ! RUN = 29016 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29017.xsdst ! RUN = 29017 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29018.xsdst ! RUN = 29018 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29019.xsdst ! RUN = 29019 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29020.xsdst ! RUN = 29020 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29021.xsdst ! RUN = 29021 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29022.xsdst ! RUN = 29022 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29023.xsdst ! RUN = 29023 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29024.xsdst ! RUN = 29024 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29025.xsdst ! RUN = 29025 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29026.xsdst ! RUN = 29026 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29027.xsdst ! RUN = 29027 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29028.xsdst ! RUN = 29028 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29029.xsdst ! RUN = 29029 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29030.xsdst ! RUN = 29030 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29031.xsdst ! RUN = 29031 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29032.xsdst ! RUN = 29032 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29033.xsdst ! RUN = 29033 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29034.xsdst ! RUN = 29034 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29035.xsdst ! RUN = 29035 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29036.xsdst ! RUN = 29036 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29037.xsdst ! RUN = 29037 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29038.xsdst ! RUN = 29038 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29039.xsdst ! RUN = 29039 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29040.xsdst ! RUN = 29040 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29041.xsdst ! RUN = 29041 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29042.xsdst ! RUN = 29042 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29043.xsdst ! RUN = 29043 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29044.xsdst ! RUN = 29044 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29045.xsdst ! RUN = 29045 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29046.xsdst ! RUN = 29046 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29047.xsdst ! RUN = 29047 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29048.xsdst ! RUN = 29048 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29049.xsdst ! RUN = 29049 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29050.xsdst ! RUN = 29050 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29051.xsdst ! RUN = 29051 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29052.xsdst ! RUN = 29052 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29053.xsdst ! RUN = 29053 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29054.xsdst ! RUN = 29054 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29055.xsdst ! RUN = 29055 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29056.xsdst ! RUN = 29056 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29057.xsdst ! RUN = 29057 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29058.xsdst ! RUN = 29058 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29059.xsdst ! RUN = 29059 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29060.xsdst ! RUN = 29060 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29061.xsdst ! RUN = 29061 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29062.xsdst ! RUN = 29062 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29063.xsdst ! RUN = 29063 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29064.xsdst ! RUN = 29064 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29065.xsdst ! RUN = 29065 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29066.xsdst ! RUN = 29066 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29067.xsdst ! RUN = 29067 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29068.xsdst ! RUN = 29068 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29069.xsdst ! RUN = 29069 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29070.xsdst ! RUN = 29070 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29071.xsdst ! RUN = 29071 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29072.xsdst ! RUN = 29072 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29073.xsdst ! RUN = 29073 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29074.xsdst ! RUN = 29074 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29075.xsdst ! RUN = 29075 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29076.xsdst ! RUN = 29076 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29077.xsdst ! RUN = 29077 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29078.xsdst ! RUN = 29078 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29079.xsdst ! RUN = 29079 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29080.xsdst ! RUN = 29080 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29081.xsdst ! RUN = 29081 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29082.xsdst ! RUN = 29082 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29083.xsdst ! RUN = 29083 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29084.xsdst ! RUN = 29084 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29085.xsdst ! RUN = 29085 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29086.xsdst ! RUN = 29086 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29087.xsdst ! RUN = 29087 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29088.xsdst ! RUN = 29088 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29089.xsdst ! RUN = 29089 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29090.xsdst ! RUN = 29090 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29091.xsdst ! RUN = 29091 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29092.xsdst ! RUN = 29092 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29093.xsdst ! RUN = 29093 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29094.xsdst ! RUN = 29094 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29095.xsdst ! RUN = 29095 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29096.xsdst ! RUN = 29096 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29097.xsdst ! RUN = 29097 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29098.xsdst ! RUN = 29098 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29099.xsdst ! RUN = 29099 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/199.5/bdk01_eeee_199.5_29100.xsdst ! RUN = 29100 ! NEVT = 2000
