*
*   Nickname     : xs_wphact22ccbe32a_e195.5_m80.4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32A/E195.5/CERN/SUMT/C001-49
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 97961 events in 49 files time stamp: Mon Mar  4 15:18:47 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160001.xsdst ! RUN = 160001 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160002.xsdst ! RUN = 160002 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160003.xsdst ! RUN = 160003 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160004.xsdst ! RUN = 160004 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160005.xsdst ! RUN = 160005 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160006.xsdst ! RUN = 160006 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160007.xsdst ! RUN = 160007 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160008.xsdst ! RUN = 160008 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160009.xsdst ! RUN = 160009 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160010.xsdst ! RUN = 160010 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160011.xsdst ! RUN = 160011 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160012.xsdst ! RUN = 160012 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160013.xsdst ! RUN = 160013 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160014.xsdst ! RUN = 160014 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160015.xsdst ! RUN = 160015 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160016.xsdst ! RUN = 160016 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160017.xsdst ! RUN = 160017 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160018.xsdst ! RUN = 160018 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160019.xsdst ! RUN = 160019 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160020.xsdst ! RUN = 160020 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160021.xsdst ! RUN = 160021 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160022.xsdst ! RUN = 160022 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160023.xsdst ! RUN = 160023 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160024.xsdst ! RUN = 160024 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160025.xsdst ! RUN = 160025 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160101.xsdst ! RUN = 160101 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160102.xsdst ! RUN = 160102 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160103.xsdst ! RUN = 160103 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160104.xsdst ! RUN = 160104 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160105.xsdst ! RUN = 160105 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160106.xsdst ! RUN = 160106 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160107.xsdst ! RUN = 160107 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160108.xsdst ! RUN = 160108 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160109.xsdst ! RUN = 160109 ! NEVT = 1996
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160110.xsdst ! RUN = 160110 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160111.xsdst ! RUN = 160111 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160113.xsdst ! RUN = 160113 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160114.xsdst ! RUN = 160114 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160115.xsdst ! RUN = 160115 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160116.xsdst ! RUN = 160116 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160117.xsdst ! RUN = 160117 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160118.xsdst ! RUN = 160118 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160119.xsdst ! RUN = 160119 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160120.xsdst ! RUN = 160120 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160121.xsdst ! RUN = 160121 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160122.xsdst ! RUN = 160122 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160123.xsdst ! RUN = 160123 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160124.xsdst ! RUN = 160124 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32a_195.5_80.4_160125.xsdst ! RUN = 160125 ! NEVT = 1999
