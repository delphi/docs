*
*   Nickname     : xs_wphact22ccbe32a_e191.6_m80.4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32A/E191.6/CERN/SUMT/C001-49
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , CERN
*---
*   Comments     : in total 96898 events in 49 files time stamp: Mon Mar  4 15:18:47 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170001.xsdst ! RUN = 170001 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170002.xsdst ! RUN = 170002 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170003.xsdst ! RUN = 170003 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170004.xsdst ! RUN = 170004 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170005.xsdst ! RUN = 170005 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170006.xsdst ! RUN = 170006 ! NEVT = 930
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170007.xsdst ! RUN = 170007 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170008.xsdst ! RUN = 170008 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170009.xsdst ! RUN = 170009 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170010.xsdst ! RUN = 170010 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170011.xsdst ! RUN = 170011 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170012.xsdst ! RUN = 170012 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170013.xsdst ! RUN = 170013 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170015.xsdst ! RUN = 170015 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170016.xsdst ! RUN = 170016 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170017.xsdst ! RUN = 170017 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170018.xsdst ! RUN = 170018 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170019.xsdst ! RUN = 170019 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170020.xsdst ! RUN = 170020 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170021.xsdst ! RUN = 170021 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170022.xsdst ! RUN = 170022 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170023.xsdst ! RUN = 170023 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170024.xsdst ! RUN = 170024 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170025.xsdst ! RUN = 170025 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170101.xsdst ! RUN = 170101 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170102.xsdst ! RUN = 170102 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170103.xsdst ! RUN = 170103 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170104.xsdst ! RUN = 170104 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170105.xsdst ! RUN = 170105 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170106.xsdst ! RUN = 170106 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170107.xsdst ! RUN = 170107 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170108.xsdst ! RUN = 170108 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170109.xsdst ! RUN = 170109 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170110.xsdst ! RUN = 170110 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170111.xsdst ! RUN = 170111 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170112.xsdst ! RUN = 170112 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170113.xsdst ! RUN = 170113 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170114.xsdst ! RUN = 170114 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170115.xsdst ! RUN = 170115 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170116.xsdst ! RUN = 170116 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170117.xsdst ! RUN = 170117 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170118.xsdst ! RUN = 170118 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170119.xsdst ! RUN = 170119 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170120.xsdst ! RUN = 170120 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170121.xsdst ! RUN = 170121 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170122.xsdst ! RUN = 170122 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170123.xsdst ! RUN = 170123 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170124.xsdst ! RUN = 170124 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/191.6/wphact22_ccbe32a_191.6_80.4_170125.xsdst ! RUN = 170125 ! NEVT = 2000
