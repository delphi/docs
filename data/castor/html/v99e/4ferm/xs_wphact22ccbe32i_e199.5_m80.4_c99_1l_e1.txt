*
*   Nickname     : xs_wphact22ccbe32i_e199.5_m80.4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32I/E199.5/CERN/SUMT/C001-23
*   Description  :  Extended Short DST simulation 99e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 45974 events in 23 files time stamp: Thu Mar  7 06:28:59 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150201.xsdst ! RUN = 150201 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150202.xsdst ! RUN = 150202 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150203.xsdst ! RUN = 150203 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150204.xsdst ! RUN = 150204 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150205.xsdst ! RUN = 150205 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150207.xsdst ! RUN = 150207 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150208.xsdst ! RUN = 150208 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150210.xsdst ! RUN = 150210 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150211.xsdst ! RUN = 150211 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150212.xsdst ! RUN = 150212 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150213.xsdst ! RUN = 150213 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150214.xsdst ! RUN = 150214 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150215.xsdst ! RUN = 150215 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150216.xsdst ! RUN = 150216 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150217.xsdst ! RUN = 150217 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150218.xsdst ! RUN = 150218 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150219.xsdst ! RUN = 150219 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150220.xsdst ! RUN = 150220 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150221.xsdst ! RUN = 150221 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150222.xsdst ! RUN = 150222 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150223.xsdst ! RUN = 150223 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150224.xsdst ! RUN = 150224 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/199.5/wphact22_ccbe32i_199.5_80.4_150225.xsdst ! RUN = 150225 ! NEVT = 1996
