*
*   Nickname     : xs_wphact22ccbe32i_e195.5_m80.4_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32I/E195.5/CERN/SUMT/C001-23
*   Description  :  Extended Short DST simulation 99e1 done at ecms=195.5 , CERN
*---
*   Comments     : in total 45986 events in 23 files time stamp: Sat Mar  9 15:17:32 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160202.xsdst ! RUN = 160202 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160203.xsdst ! RUN = 160203 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160204.xsdst ! RUN = 160204 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160205.xsdst ! RUN = 160205 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160206.xsdst ! RUN = 160206 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160207.xsdst ! RUN = 160207 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160208.xsdst ! RUN = 160208 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160210.xsdst ! RUN = 160210 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160211.xsdst ! RUN = 160211 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160212.xsdst ! RUN = 160212 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160213.xsdst ! RUN = 160213 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160214.xsdst ! RUN = 160214 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160215.xsdst ! RUN = 160215 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160216.xsdst ! RUN = 160216 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160217.xsdst ! RUN = 160217 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160218.xsdst ! RUN = 160218 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160219.xsdst ! RUN = 160219 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160220.xsdst ! RUN = 160220 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160221.xsdst ! RUN = 160221 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160222.xsdst ! RUN = 160222 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160223.xsdst ! RUN = 160223 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160224.xsdst ! RUN = 160224 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v99e/195.5/wphact22_ccbe32i_195.5_80.4_160225.xsdst ! RUN = 160225 ! NEVT = 2000
