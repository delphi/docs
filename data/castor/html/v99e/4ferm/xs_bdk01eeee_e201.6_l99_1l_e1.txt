*
*   Nickname     : xs_bdk01eeee_e201.6_l99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/BDK01EEEE/E201.6/LYON/SUMT/C001-100
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , Lyon
*---
*   Comments     : in total 199462 events in 100 files time stamp: Mon Oct 22 10:31:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32001.xsdst ! RUN = 32001 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32002.xsdst ! RUN = 32002 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32003.xsdst ! RUN = 32003 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32004.xsdst ! RUN = 32004 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32005.xsdst ! RUN = 32005 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32006.xsdst ! RUN = 32006 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32007.xsdst ! RUN = 32007 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32008.xsdst ! RUN = 32008 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32009.xsdst ! RUN = 32009 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32010.xsdst ! RUN = 32010 ! NEVT = 1463
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32011.xsdst ! RUN = 32011 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32012.xsdst ! RUN = 32012 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32013.xsdst ! RUN = 32013 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32014.xsdst ! RUN = 32014 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32015.xsdst ! RUN = 32015 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32016.xsdst ! RUN = 32016 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32017.xsdst ! RUN = 32017 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32018.xsdst ! RUN = 32018 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32019.xsdst ! RUN = 32019 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32020.xsdst ! RUN = 32020 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32021.xsdst ! RUN = 32021 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32022.xsdst ! RUN = 32022 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32023.xsdst ! RUN = 32023 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32024.xsdst ! RUN = 32024 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32025.xsdst ! RUN = 32025 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32026.xsdst ! RUN = 32026 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32027.xsdst ! RUN = 32027 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32028.xsdst ! RUN = 32028 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32029.xsdst ! RUN = 32029 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32030.xsdst ! RUN = 32030 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32031.xsdst ! RUN = 32031 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32032.xsdst ! RUN = 32032 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32033.xsdst ! RUN = 32033 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32034.xsdst ! RUN = 32034 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32035.xsdst ! RUN = 32035 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32036.xsdst ! RUN = 32036 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32037.xsdst ! RUN = 32037 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32038.xsdst ! RUN = 32038 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32039.xsdst ! RUN = 32039 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32040.xsdst ! RUN = 32040 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32041.xsdst ! RUN = 32041 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32042.xsdst ! RUN = 32042 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32043.xsdst ! RUN = 32043 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32044.xsdst ! RUN = 32044 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32045.xsdst ! RUN = 32045 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32046.xsdst ! RUN = 32046 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32047.xsdst ! RUN = 32047 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32048.xsdst ! RUN = 32048 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32049.xsdst ! RUN = 32049 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32050.xsdst ! RUN = 32050 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32051.xsdst ! RUN = 32051 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32052.xsdst ! RUN = 32052 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32053.xsdst ! RUN = 32053 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32054.xsdst ! RUN = 32054 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32055.xsdst ! RUN = 32055 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32056.xsdst ! RUN = 32056 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32057.xsdst ! RUN = 32057 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32058.xsdst ! RUN = 32058 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32059.xsdst ! RUN = 32059 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32060.xsdst ! RUN = 32060 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32061.xsdst ! RUN = 32061 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32062.xsdst ! RUN = 32062 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32063.xsdst ! RUN = 32063 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32064.xsdst ! RUN = 32064 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32065.xsdst ! RUN = 32065 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32066.xsdst ! RUN = 32066 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32067.xsdst ! RUN = 32067 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32068.xsdst ! RUN = 32068 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32069.xsdst ! RUN = 32069 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32070.xsdst ! RUN = 32070 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32071.xsdst ! RUN = 32071 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32072.xsdst ! RUN = 32072 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32073.xsdst ! RUN = 32073 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32074.xsdst ! RUN = 32074 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32075.xsdst ! RUN = 32075 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32076.xsdst ! RUN = 32076 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32077.xsdst ! RUN = 32077 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32078.xsdst ! RUN = 32078 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32079.xsdst ! RUN = 32079 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32080.xsdst ! RUN = 32080 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32081.xsdst ! RUN = 32081 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32082.xsdst ! RUN = 32082 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32083.xsdst ! RUN = 32083 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32084.xsdst ! RUN = 32084 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32085.xsdst ! RUN = 32085 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32086.xsdst ! RUN = 32086 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32087.xsdst ! RUN = 32087 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32088.xsdst ! RUN = 32088 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32089.xsdst ! RUN = 32089 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32090.xsdst ! RUN = 32090 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32091.xsdst ! RUN = 32091 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32092.xsdst ! RUN = 32092 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32093.xsdst ! RUN = 32093 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32094.xsdst ! RUN = 32094 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32095.xsdst ! RUN = 32095 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32096.xsdst ! RUN = 32096 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32097.xsdst ! RUN = 32097 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32098.xsdst ! RUN = 32098 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32099.xsdst ! RUN = 32099 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk01/v99e/201.6/bdk01_eeee_201.6_32100.xsdst ! RUN = 32100 ! NEVT = 2000
