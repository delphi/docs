*   Nickname :     xs_hzha03pyth6156hzcq_e199.5_m20_m8_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E199.5/CERN/SUMT/C001-10
*   Description :   Extended Short DST simulation 99e  done at ecm=199.5 GeV , CERN
*   Comments :     in total 9128 events in 10 files, time stamp: Sat Mai 17 4:13:50 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2088.xsdst ! RUN = 2088 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2089.xsdst ! RUN = 2089 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2090.xsdst ! RUN = 2090 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2091.xsdst ! RUN = 2091 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2092.xsdst ! RUN = 2092 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2093.xsdst ! RUN = 2093 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2094.xsdst ! RUN = 2094 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2095.xsdst ! RUN = 2095 ! NEVT = 162
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2096.xsdst ! RUN = 2096 ! NEVT = 976
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hzcq_199.5_20_8_2097.xsdst ! RUN = 2097 ! NEVT = 1000
