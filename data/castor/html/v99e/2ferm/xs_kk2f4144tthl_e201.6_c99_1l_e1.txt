*
*   Nickname     : xs_kk2f4144tthl_e201.6_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4144TTHL/E201.6/CERN/SUMT/C001-40
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 199940 events in 40 files time stamp: Fri Jan 25 12:12:13 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27301.xsdst ! RUN = 27301 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27302.xsdst ! RUN = 27302 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27303.xsdst ! RUN = 27303 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27304.xsdst ! RUN = 27304 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27305.xsdst ! RUN = 27305 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27306.xsdst ! RUN = 27306 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27307.xsdst ! RUN = 27307 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27308.xsdst ! RUN = 27308 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27309.xsdst ! RUN = 27309 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27310.xsdst ! RUN = 27310 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27311.xsdst ! RUN = 27311 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27312.xsdst ! RUN = 27312 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27313.xsdst ! RUN = 27313 ! NEVT = 4995
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27314.xsdst ! RUN = 27314 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27315.xsdst ! RUN = 27315 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27316.xsdst ! RUN = 27316 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27317.xsdst ! RUN = 27317 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27318.xsdst ! RUN = 27318 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27319.xsdst ! RUN = 27319 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27320.xsdst ! RUN = 27320 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27321.xsdst ! RUN = 27321 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27322.xsdst ! RUN = 27322 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27323.xsdst ! RUN = 27323 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27324.xsdst ! RUN = 27324 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27325.xsdst ! RUN = 27325 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27326.xsdst ! RUN = 27326 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27327.xsdst ! RUN = 27327 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27328.xsdst ! RUN = 27328 ! NEVT = 4994
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27329.xsdst ! RUN = 27329 ! NEVT = 4996
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27330.xsdst ! RUN = 27330 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27331.xsdst ! RUN = 27331 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27332.xsdst ! RUN = 27332 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27333.xsdst ! RUN = 27333 ! NEVT = 4996
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27334.xsdst ! RUN = 27334 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27335.xsdst ! RUN = 27335 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27336.xsdst ! RUN = 27336 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27337.xsdst ! RUN = 27337 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27338.xsdst ! RUN = 27338 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27339.xsdst ! RUN = 27339 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/201.6/kk2f4144_tthl_201.6_27340.xsdst ! RUN = 27340 ! NEVT = 4999
