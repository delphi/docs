*
*   Nickname     : xs_kk2f4143mumu_e201.6_l99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4143MUMU/E201.6/LYON/SUMT/C001-23
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , Lyon
*---
*   Comments     : in total 110526 events in 23 files time stamp: Wed Nov 14 22:24:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10035.xsdst ! RUN = 10035 ! NEVT = 2755
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10036.xsdst ! RUN = 10036 ! NEVT = 2773
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10061.xsdst ! RUN = 10061 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10062.xsdst ! RUN = 10062 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10063.xsdst ! RUN = 10063 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10064.xsdst ! RUN = 10064 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10065.xsdst ! RUN = 10065 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10066.xsdst ! RUN = 10066 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10067.xsdst ! RUN = 10067 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10068.xsdst ! RUN = 10068 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10069.xsdst ! RUN = 10069 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10070.xsdst ! RUN = 10070 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10071.xsdst ! RUN = 10071 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10072.xsdst ! RUN = 10072 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10073.xsdst ! RUN = 10073 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10074.xsdst ! RUN = 10074 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10075.xsdst ! RUN = 10075 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10076.xsdst ! RUN = 10076 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10077.xsdst ! RUN = 10077 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10078.xsdst ! RUN = 10078 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10079.xsdst ! RUN = 10079 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_10080.xsdst ! RUN = 10080 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/lyon/kk2f4143/v99e/201.6/kk2f4143_mumu_201.6_23483.xsdst ! RUN = 23483 ! NEVT = 5000
