*
*   Nickname     : xs_kk2f4144qqar_e91.25_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4144QQAR/E91.25/CERN/SUMT/C001-73
*   Description  :  Extended Short DST simulation 99e1 done at ecms=91.25 , CERN
*---
*   Comments     : in total 218981 events in 73 files time stamp: Wed Mar  6 22:14:41 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40001.xsdst ! RUN = 40001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40002.xsdst ! RUN = 40002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40003.xsdst ! RUN = 40003 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40004.xsdst ! RUN = 40004 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40005.xsdst ! RUN = 40005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40006.xsdst ! RUN = 40006 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40007.xsdst ! RUN = 40007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40008.xsdst ! RUN = 40008 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40009.xsdst ! RUN = 40009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40010.xsdst ! RUN = 40010 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40011.xsdst ! RUN = 40011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40012.xsdst ! RUN = 40012 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40013.xsdst ! RUN = 40013 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40014.xsdst ! RUN = 40014 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40015.xsdst ! RUN = 40015 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40016.xsdst ! RUN = 40016 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40017.xsdst ! RUN = 40017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40018.xsdst ! RUN = 40018 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40019.xsdst ! RUN = 40019 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40020.xsdst ! RUN = 40020 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40021.xsdst ! RUN = 40021 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40022.xsdst ! RUN = 40022 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40023.xsdst ! RUN = 40023 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40024.xsdst ! RUN = 40024 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40025.xsdst ! RUN = 40025 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40026.xsdst ! RUN = 40026 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40027.xsdst ! RUN = 40027 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40028.xsdst ! RUN = 40028 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40029.xsdst ! RUN = 40029 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40030.xsdst ! RUN = 40030 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40031.xsdst ! RUN = 40031 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40032.xsdst ! RUN = 40032 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40033.xsdst ! RUN = 40033 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40034.xsdst ! RUN = 40034 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40035.xsdst ! RUN = 40035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40036.xsdst ! RUN = 40036 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40037.xsdst ! RUN = 40037 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40038.xsdst ! RUN = 40038 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40039.xsdst ! RUN = 40039 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40040.xsdst ! RUN = 40040 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40041.xsdst ! RUN = 40041 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40042.xsdst ! RUN = 40042 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40043.xsdst ! RUN = 40043 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40044.xsdst ! RUN = 40044 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40045.xsdst ! RUN = 40045 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40046.xsdst ! RUN = 40046 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40047.xsdst ! RUN = 40047 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40048.xsdst ! RUN = 40048 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40049.xsdst ! RUN = 40049 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40050.xsdst ! RUN = 40050 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40051.xsdst ! RUN = 40051 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40052.xsdst ! RUN = 40052 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40053.xsdst ! RUN = 40053 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40054.xsdst ! RUN = 40054 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40056.xsdst ! RUN = 40056 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40058.xsdst ! RUN = 40058 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40059.xsdst ! RUN = 40059 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40060.xsdst ! RUN = 40060 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40061.xsdst ! RUN = 40061 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40062.xsdst ! RUN = 40062 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40063.xsdst ! RUN = 40063 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40064.xsdst ! RUN = 40064 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40065.xsdst ! RUN = 40065 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40066.xsdst ! RUN = 40066 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40067.xsdst ! RUN = 40067 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40068.xsdst ! RUN = 40068 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40069.xsdst ! RUN = 40069 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40070.xsdst ! RUN = 40070 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40071.xsdst ! RUN = 40071 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40072.xsdst ! RUN = 40072 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40073.xsdst ! RUN = 40073 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40074.xsdst ! RUN = 40074 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v99e/91.25/kk2f4144_qqar_91.25_40075.xsdst ! RUN = 40075 ! NEVT = 3000
