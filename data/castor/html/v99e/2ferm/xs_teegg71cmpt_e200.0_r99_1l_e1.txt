*
*   Nickname     : xs_teegg71cmpt_e200.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E200.0/RAL/SUMT
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=200.0 , RAL
*---
*   Comments     :  time stamp: Tue Jul 31 09:13:40 2001
*---
*
*
*
*
*         * Compton, e e --> e + e + gamma
*            theta_e > 10 degrees, theta_veto_e < 2 degrees,
*            theta_gamma > 10 degrees, E_e>1 GeV, E_gam > 1 GeV
*            x-sec = 47.66+-0.13pb
*
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.1.xsdst ! RUN =  -44932 -44933 -44934 -44935 -44936 -44937 -44938 -44939 -44940 -44941! NEVT =    6358
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.2.xsdst ! RUN =  -44942 -44943 -44944 -44945 -44946 -44947 -44948 -44949 -44950 -44951! NEVT =    6372
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.3.xsdst ! RUN =  -44952 -44953 -44954 -44955 -44956 -44957 -44958 -44959 -44960 -44961! NEVT =    6464
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.4.xsdst ! RUN =  -44962 -44963 -44964 -44965 -44966 -44967 -44968 -44969 -44970 -44971! NEVT =    6088
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.5.xsdst ! RUN =  -44976 -44977 -44978 -44979 -44980 -44981 -44982 -44983 -44984 -44985! NEVT =    6398
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.6.xsdst ! RUN =  -44986 -44987 -44988 -44989 -44990 -44991 -44992 -44993 -44994 -44995! NEVT =    6453
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.7.xsdst ! RUN =  -44996 -44997 -44998 -44999 -45000 -45001 -45002 -45003 -45004 -45005! NEVT =    6465
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.8.xsdst ! RUN =  -45006 -45007 -45008 -45009 -45010 -45011 -45012 -45013 -45014 -45015! NEVT =    6509
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.9.xsdst ! RUN =  -45016 -45017 -45018 -45019 -45020 -45021 -45022 -45023 -45024 -45025! NEVT =    6460
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.10.xsdst ! RUN =  -45026 -45027 -45028 -45029 -45030 -45031 -45032 -45033 -45034 -45035! NEVT =    6440
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.11.xsdst ! RUN =  -45036 -45037 -45038 -45039 -45040 -45041 -45042 -45043 -45044 -45045! NEVT =    6325
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.12.xsdst ! RUN =  -45046 -45047 -45048 -45049 -45050 -45051 -45052 -45053 -45054 -45055! NEVT =    6442
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.13.xsdst ! RUN =  -45056 -45057 -45058 -45059 -45060 -45061 -45062 -45063 -45064 -45065! NEVT =    6405
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.14.xsdst ! RUN =  -45066 -45067 -45068 -45069 -45070 -45071 -45072 -45073 -45074 -45075! NEVT =    6430
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.15.xsdst ! RUN =  -45076 -45077 -45078 -45079 -45080 -45081 -45082 -45083 -45084 -45085! NEVT =    6425
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/200.0/EK5771.16.xsdst ! RUN =  -45086 -45087 -45088 -45089 -45090 -45091 -45092 -45093 -45094 -45095! NEVT =    6394
