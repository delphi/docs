*
*   Nickname     : xs_koralz402vvkz_e196.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KORALZ402VVKZ/E196.0/RAL/SUMT
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=196.0 , RAL
*---
*   Comments     :  time stamp: Tue Jul 31 09:32:21 2001
*---
*
*
*
*
*        * e+e- -> nu nubar + gammas KORALZ 4.02
*        * cross-section = 10.48+-0.06pb
*
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz402/vvkz/v99e/196.0/EK5771.50.xsdst ! RUN =  -43778 -43779 -43780 -43781 -43782 -43783 -43784 -43785 -43786 -43787! NEVT =   16787
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz402/vvkz/v99e/196.0/EK5771.51.xsdst ! RUN =  -43788 -43789 -43790 -43791 -43792 -43793 -43794 -43795 -43796 -43797! NEVT =   16827
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz402/vvkz/v99e/196.0/EK5771.52.xsdst ! RUN =  -43798 -43799 -43800 -43801 -43802 -43803 -43804 -43805 -43806 -43807! NEVT =   16984
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz402/vvkz/v99e/196.0/EK5771.53.xsdst ! RUN =  -43768 -43769 -43770 -43771 -43772 -43773 -43774 -43775 -43776 -43777! NEVT =   14051
