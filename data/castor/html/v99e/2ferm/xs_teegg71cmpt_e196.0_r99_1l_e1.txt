*
*   Nickname     : xs_teegg71cmpt_e196.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E196.0/RAL/SUMT
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=196.0 , RAL
*---
*   Comments     :  time stamp: Tue Jul 31 09:05:11 2001
*---
*
*
*
*
*         * Compton, e e --> e + e + gamma
*            theta_e > 10 degrees, theta_veto_e < 2 degrees,
*            theta_gamma > 10 degrees, E_e>1 GeV, E_gam > 1 GeV
*            x-sec = 49.6+-0.2pb
*
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.42.xsdst ! RUN =  -43917 -43918 -43919 -43920 -43921 -43922 -43923 -43924 -43925 -43926! NEVT =    5992
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.43.xsdst ! RUN =  -43927 -43928 -43929 -43930 -43931 -43932 -43933 -43934 -43935 -43936! NEVT =    6201
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.44.xsdst ! RUN =  -43937 -43938 -43939 -43940 -43941 -43942 -43943 -43944 -43945 -43946! NEVT =    6460
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.45.xsdst ! RUN =  -43947 -43948 -43949 -43950 -43951 -43952 -43953 -43954 -43955 -43956! NEVT =    6593
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.46.xsdst ! RUN =  -47222 -47223 -47224 -47225 -47226 -47227 -47228 -47229      0      0! NEVT =    5252
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.47.xsdst ! RUN =  -47230 -47231 -47232 -47233 -47234 -47235 -47236 -47237 -47238 -47239! NEVT =    6448
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.48.xsdst ! RUN =  -47240 -47241 -47242 -47243 -47244 -47245 -47246 -47247 -47248 -47249! NEVT =    6587
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.49.xsdst ! RUN =  -47250 -47251 -47252 -47253 -47254 -47255 -47256 -47257 -47258 -47259! NEVT =    6544
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.50.xsdst ! RUN =  -47260 -47261 -47262 -47263 -47264 -47265 -47266 -47267 -47268 -47269! NEVT =    6477
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.51.xsdst ! RUN =  -47270 -47271 -47272 -47273 -47274 -47275 -47276 -47277 -47278 -47279! NEVT =    6526
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.52.xsdst ! RUN =  -47280 -47281 -47282 -47283 -47284 -47285 -47286 -47287 -47288 -47289! NEVT =    6456
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.53.xsdst ! RUN =  -47290 -47291 -47292 -47293 -47294 -47295 -47296 -47297 -47298 -47299! NEVT =    6496
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.54.xsdst ! RUN =  -47300 -47301 -47302 -47303 -47304 -47305 -47306 -47307 -47308 -47309! NEVT =    6515
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.55.xsdst ! RUN =  -47310 -47311 -47312 -47313 -47314 -47315 -47316 -47317 -47318 -47319! NEVT =    6628
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.56.xsdst ! RUN =  -47320 -47321 -47322 -47323 -47324 -47325 -47326 -47327 -47328 -47329! NEVT =    6429
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v99e/196.0/EK5770.57.xsdst ! RUN =  -47330 -47331 -47332 -47333 -47334 -47335 -47336 -47337 -47338 -47339! NEVT =    6569
