*   Nickname :     xs_teegg71cmpt_e201.6_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E201.6/CERN/SUMT/C001-4
*   Description :   Extended Short DST simulation 99e  done at ecm=201.6 GeV , CERN
*   Comments :     in total 8054 events in 4 files, time stamp: Wed Nov 13 4:12:0 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/v99e/201.6/teegg71cmpt_201.6_38001.xsdst ! RUN = 38001 ! NEVT = 2004
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/v99e/201.6/teegg71cmpt_201.6_38002.xsdst ! RUN = 38002 ! NEVT = 2006
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/v99e/201.6/teegg71cmpt_201.6_38003.xsdst ! RUN = 38003 ! NEVT = 2047
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/v99e/201.6/teegg71cmpt_201.6_38004.xsdst ! RUN = 38004 ! NEVT = 1997
