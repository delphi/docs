*
*   Nickname     : xs_qedbk23eegg_e196.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/QEDBK23EEGG/E196.0/RAL/SUMT
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=196.0 , RAL
*---
*   Comments     :  time stamp: Tue Jul 31 09:38:28 2001
*---
*
*
*
*
*         * e+ e- --> gamma + gamma (+ gamma) QED
*           Total cross-section = 106pb
*           theta_1 > 15 degrees
*           visible cross-section = 9.72+-0.11pb
*
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v99e/196.0/EK5771.54.xsdst ! RUN =  -43616 -43617 -43618 -43619 -43620 -43621 -43622 -43623 -43624 -43625! NEVT =    6109
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v99e/196.0/EK5771.55.xsdst ! RUN =  -43626 -43627 -43628 -43629 -43630 -43631 -43632 -43633 -43634 -43635! NEVT =    5849
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v99e/196.0/EK5771.56.xsdst ! RUN =  -43636 -43637 -43638 -43639 -43640 -43641 -43642 -43643 -43644 -43645! NEVT =    6177
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v99e/196.0/EK5771.57.xsdst ! RUN =  -43646 -43647 -43648 -43649 -43650 -43651 -43652 -43653 -43654 -43655! NEVT =    5428
