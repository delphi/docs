*
*   Nickname     : xs_hzha03pyth6156hinvmu_e195.5_m97.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 17 16:08:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_97.5_40390.xsdst ! RUN = 40390 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_97.5_40391.xsdst ! RUN = 40391 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_97.5_40392.xsdst ! RUN = 40392 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_97.5_40393.xsdst ! RUN = 40393 ! NEVT = 500
