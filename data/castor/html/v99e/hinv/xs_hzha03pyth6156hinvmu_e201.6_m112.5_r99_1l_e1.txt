*
*   Nickname     : xs_hzha03pyth6156hinvmu_e201.6_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 19 14:42:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_112.5_41990.xsdst ! RUN = 41990 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_112.5_41991.xsdst ! RUN = 41991 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_112.5_41992.xsdst ! RUN = 41992 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_112.5_41993.xsdst ! RUN = 41993 ! NEVT = 500
