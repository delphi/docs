*
*   Nickname     : xs_hzha03pyth6156hinvqq_e199.5_m50_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E199.5/CERN/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 5500 events in 11 files time stamp: Wed Oct 10 07:43:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35700.xsdst ! RUN = 35700 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35701.xsdst ! RUN = 35701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35702.xsdst ! RUN = 35702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35703.xsdst ! RUN = 35703 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35704.xsdst ! RUN = 35704 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35705.xsdst ! RUN = 35705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35706.xsdst ! RUN = 35706 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35707.xsdst ! RUN = 35707 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35708.xsdst ! RUN = 35708 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35709.xsdst ! RUN = 35709 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_50_35710.xsdst ! RUN = 35710 ! NEVT = 500
