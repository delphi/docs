*
*   Nickname     : xs_hzha03pyth6156hinvqq_e191.6_m50_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E191.6/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Thu Sep 13 19:50:30 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35500.xsdst ! RUN = 35500 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35501.xsdst ! RUN = 35501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35502.xsdst ! RUN = 35502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35503.xsdst ! RUN = 35503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35504.xsdst ! RUN = 35504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35505.xsdst ! RUN = 35505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35506.xsdst ! RUN = 35506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35507.xsdst ! RUN = 35507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35508.xsdst ! RUN = 35508 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_50_35509.xsdst ! RUN = 35509 ! NEVT = 500
