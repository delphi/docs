*
*   Nickname     : xs_hzha03pyth6156hinvqq_e191.6_m102.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E191.6/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Fri Sep 14 21:31:55 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40750.xsdst ! RUN = 40750 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40751.xsdst ! RUN = 40751 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40752.xsdst ! RUN = 40752 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40753.xsdst ! RUN = 40753 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40754.xsdst ! RUN = 40754 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40755.xsdst ! RUN = 40755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40756.xsdst ! RUN = 40756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40757.xsdst ! RUN = 40757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40758.xsdst ! RUN = 40758 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvqq_191.6_102.5_40759.xsdst ! RUN = 40759 ! NEVT = 500
