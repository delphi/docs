*
*   Nickname     : xs_hzha03pyth6156hinvqq_e201.6_m45_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E201.6/CERN/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 5497 events in 11 files time stamp: Wed Oct 10 03:40:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35300.xsdst ! RUN = 35300 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35301.xsdst ! RUN = 35301 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35302.xsdst ! RUN = 35302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35303.xsdst ! RUN = 35303 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35304.xsdst ! RUN = 35304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35305.xsdst ! RUN = 35305 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35306.xsdst ! RUN = 35306 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35307.xsdst ! RUN = 35307 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35308.xsdst ! RUN = 35308 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35309.xsdst ! RUN = 35309 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_45_35310.xsdst ! RUN = 35310 ! NEVT = 500
