*
*   Nickname     : xs_hzha03pyth6156hinvmu_e195.5_m60_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 17 16:07:20 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_60_36640.xsdst ! RUN = 36640 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_60_36641.xsdst ! RUN = 36641 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_60_36642.xsdst ! RUN = 36642 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_60_36643.xsdst ! RUN = 36643 ! NEVT = 500
