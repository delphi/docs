*
*   Nickname     : xs_hzha03pyth6156hinvee_e191.6_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E191.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sat Sep 15 00:52:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvee_191.6_112.5_41770.xsdst ! RUN = 41770 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvee_191.6_112.5_41771.xsdst ! RUN = 41771 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvee_191.6_112.5_41772.xsdst ! RUN = 41772 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvee_191.6_112.5_41773.xsdst ! RUN = 41773 ! NEVT = 500
