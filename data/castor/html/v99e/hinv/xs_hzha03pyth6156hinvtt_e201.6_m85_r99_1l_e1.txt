*
*   Nickname     : xs_hzha03pyth6156hinvtt_e201.6_m85_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E201.6/RAL/SUMT/C001-2
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1000 events in 2 files time stamp: Wed Sep 26 19:03:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvtt_201.6_85_39261.xsdst ! RUN = 39261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvtt_201.6_85_39262.xsdst ! RUN = 39262 ! NEVT = 500
