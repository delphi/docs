*
*   Nickname     : xs_hzha03pyth6156hinvqq_e201.6_m85_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E201.6/CERN/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 5345 events in 11 files time stamp: Wed Oct 10 22:13:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39300.xsdst ! RUN = 39300 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39301.xsdst ! RUN = 39301 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39302.xsdst ! RUN = 39302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39303.xsdst ! RUN = 39303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39304.xsdst ! RUN = 39304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39305.xsdst ! RUN = 39305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39306.xsdst ! RUN = 39306 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39307.xsdst ! RUN = 39307 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39308.xsdst ! RUN = 39308 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39309.xsdst ! RUN = 39309 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_85_39310.xsdst ! RUN = 39310 ! NEVT = 345
