*
*   Nickname     : xs_hzha03pyth6156hinvqq_e201.6_m75_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E201.6/CERN/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , CERN
*---
*   Comments     : in total 5498 events in 11 files time stamp: Wed Oct 10 22:04:29 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38300.xsdst ! RUN = 38300 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38301.xsdst ! RUN = 38301 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38302.xsdst ! RUN = 38302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38303.xsdst ! RUN = 38303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38304.xsdst ! RUN = 38304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38305.xsdst ! RUN = 38305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38306.xsdst ! RUN = 38306 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38307.xsdst ! RUN = 38307 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38308.xsdst ! RUN = 38308 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38309.xsdst ! RUN = 38309 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvqq_201.6_75_38310.xsdst ! RUN = 38310 ! NEVT = 500
