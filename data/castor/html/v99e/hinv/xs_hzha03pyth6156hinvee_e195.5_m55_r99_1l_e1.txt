*
*   Nickname     : xs_hzha03pyth6156hinvee_e195.5_m55_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Sun Sep 16 02:46:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_55_36120.xsdst ! RUN = 36120 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_55_36121.xsdst ! RUN = 36121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_55_36122.xsdst ! RUN = 36122 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_55_36123.xsdst ! RUN = 36123 ! NEVT = 499
