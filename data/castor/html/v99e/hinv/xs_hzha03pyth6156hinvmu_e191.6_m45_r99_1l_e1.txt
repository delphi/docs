*
*   Nickname     : xs_hzha03pyth6156hinvmu_e191.6_m45_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E191.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Sep 15 00:52:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_45_35040.xsdst ! RUN = 35040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_45_35041.xsdst ! RUN = 35041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_45_35042.xsdst ! RUN = 35042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_45_35043.xsdst ! RUN = 35043 ! NEVT = 500
