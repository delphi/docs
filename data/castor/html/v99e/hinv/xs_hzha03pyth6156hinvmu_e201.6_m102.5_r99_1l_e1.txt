*
*   Nickname     : xs_hzha03pyth6156hinvmu_e201.6_m102.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 19 14:43:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_102.5_40990.xsdst ! RUN = 40990 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_102.5_40991.xsdst ! RUN = 40991 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_102.5_40992.xsdst ! RUN = 40992 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvmu_201.6_102.5_40993.xsdst ! RUN = 40993 ! NEVT = 500
