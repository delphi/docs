*
*   Nickname     : xs_hzha03pyth6156hinvee_e201.6_m97.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Sep 26 17:09:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_97.5_40470.xsdst ! RUN = 40470 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_97.5_40471.xsdst ! RUN = 40471 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_97.5_40472.xsdst ! RUN = 40472 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_97.5_40473.xsdst ! RUN = 40473 ! NEVT = 500
