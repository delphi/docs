*
*   Nickname     : xs_hzha03pyth6156hinvee_e195.5_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 1996 events in 4 files time stamp: Mon Sep 17 16:07:25 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_112.5_41870.xsdst ! RUN = 41870 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_112.5_41871.xsdst ! RUN = 41871 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_112.5_41872.xsdst ! RUN = 41872 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvee_195.5_112.5_41873.xsdst ! RUN = 41873 ! NEVT = 499
