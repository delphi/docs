*
*   Nickname     : xs_hzha03pyth6156hinvqq_e199.5_m60_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E199.5/CERN/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 5500 events in 11 files time stamp: Wed Oct 10 07:40:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36700.xsdst ! RUN = 36700 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36701.xsdst ! RUN = 36701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36702.xsdst ! RUN = 36702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36703.xsdst ! RUN = 36703 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36704.xsdst ! RUN = 36704 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36705.xsdst ! RUN = 36705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36706.xsdst ! RUN = 36706 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36707.xsdst ! RUN = 36707 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36708.xsdst ! RUN = 36708 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36709.xsdst ! RUN = 36709 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_60_36710.xsdst ! RUN = 36710 ! NEVT = 500
