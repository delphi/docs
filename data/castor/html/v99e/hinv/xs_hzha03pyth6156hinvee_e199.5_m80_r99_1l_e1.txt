*
*   Nickname     : xs_hzha03pyth6156hinvee_e199.5_m80_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1995 events in 4 files time stamp: Tue Sep 18 09:58:14 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_80_38620.xsdst ! RUN = 38620 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_80_38621.xsdst ! RUN = 38621 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_80_38622.xsdst ! RUN = 38622 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_80_38623.xsdst ! RUN = 38623 ! NEVT = 498
