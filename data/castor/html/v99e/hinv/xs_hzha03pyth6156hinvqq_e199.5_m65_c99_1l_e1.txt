*
*   Nickname     : xs_hzha03pyth6156hinvqq_e199.5_m65_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E199.5/CERN/SUMT/C001-11
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 5499 events in 11 files time stamp: Wed Oct 10 07:43:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37200.xsdst ! RUN = 37200 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37201.xsdst ! RUN = 37201 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37202.xsdst ! RUN = 37202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37203.xsdst ! RUN = 37203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37204.xsdst ! RUN = 37204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37205.xsdst ! RUN = 37205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37206.xsdst ! RUN = 37206 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37207.xsdst ! RUN = 37207 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37208.xsdst ! RUN = 37208 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37209.xsdst ! RUN = 37209 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvqq_199.5_65_37210.xsdst ! RUN = 37210 ! NEVT = 500
