*
*   Nickname     : xs_hzha03pyth6156hinvmu_e199.5_m65_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Wed Sep 26 14:58:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_65_37140.xsdst ! RUN = 37140 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_65_37141.xsdst ! RUN = 37141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_65_37142.xsdst ! RUN = 37142 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_65_37143.xsdst ! RUN = 37143 ! NEVT = 500
