*
*   Nickname     : xs_hzha03pyth6156hinvtt_e195.5_m65_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 17 20:07:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_65_37160.xsdst ! RUN = 37160 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_65_37161.xsdst ! RUN = 37161 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_65_37162.xsdst ! RUN = 37162 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_65_37163.xsdst ! RUN = 37163 ! NEVT = 500
