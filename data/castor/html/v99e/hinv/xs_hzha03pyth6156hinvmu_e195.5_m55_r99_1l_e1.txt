*
*   Nickname     : xs_hzha03pyth6156hinvmu_e195.5_m55_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 17 16:06:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_55_36140.xsdst ! RUN = 36140 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_55_36141.xsdst ! RUN = 36141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_55_36142.xsdst ! RUN = 36142 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvmu_195.5_55_36143.xsdst ! RUN = 36143 ! NEVT = 500
