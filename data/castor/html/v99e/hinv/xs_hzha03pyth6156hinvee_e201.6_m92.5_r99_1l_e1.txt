*
*   Nickname     : xs_hzha03pyth6156hinvee_e201.6_m92.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E201.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=201.6 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Sep 19 03:38:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_92.5_39970.xsdst ! RUN = 39970 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_92.5_39971.xsdst ! RUN = 39971 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_92.5_39972.xsdst ! RUN = 39972 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/201.6/hzha03pyth6156_hinvee_201.6_92.5_39973.xsdst ! RUN = 39973 ! NEVT = 499
