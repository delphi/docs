*
*   Nickname     : xs_hzha03pyth6156hinvtt_e195.5_m60.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Dec  6 13:20:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_60.0_36661.xsdst ! RUN = 36661 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_60.0_36662.xsdst ! RUN = 36662 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_60.0_36663.xsdst ! RUN = 36663 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_60.0_36664.xsdst ! RUN = 36664 ! NEVT = 500
