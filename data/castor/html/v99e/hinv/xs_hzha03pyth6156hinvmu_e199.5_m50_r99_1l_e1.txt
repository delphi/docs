*
*   Nickname     : xs_hzha03pyth6156hinvmu_e199.5_m50_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 26 14:58:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_50_35640.xsdst ! RUN = 35640 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_50_35641.xsdst ! RUN = 35641 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_50_35642.xsdst ! RUN = 35642 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvmu_199.5_50_35643.xsdst ! RUN = 35643 ! NEVT = 500
