*
*   Nickname     : xs_hzha03pyth6156hinvee_e199.5_m120.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E199.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=199.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Tue Sep 18 13:32:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_120.0_42620.xsdst ! RUN = 42620 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_120.0_42621.xsdst ! RUN = 42621 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_120.0_42622.xsdst ! RUN = 42622 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/199.5/hzha03pyth6156_hinvee_199.5_120.0_42623.xsdst ! RUN = 42623 ! NEVT = 500
