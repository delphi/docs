*
*   Nickname     : xs_hzha03pyth6156hinvmu_e191.6_m97.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E191.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=191.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Sep 15 15:47:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_97.5_40290.xsdst ! RUN = 40290 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_97.5_40291.xsdst ! RUN = 40291 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_97.5_40292.xsdst ! RUN = 40292 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/191.6/hzha03pyth6156_hinvmu_191.6_97.5_40293.xsdst ! RUN = 40293 ! NEVT = 500
