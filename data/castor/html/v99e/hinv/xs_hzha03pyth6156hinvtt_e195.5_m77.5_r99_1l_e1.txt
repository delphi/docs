*
*   Nickname     : xs_hzha03pyth6156hinvtt_e195.5_m77.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Thu Dec  6 13:20:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_77.5_38411.xsdst ! RUN = 38411 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_77.5_38412.xsdst ! RUN = 38412 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_77.5_38413.xsdst ! RUN = 38413 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_77.5_38414.xsdst ! RUN = 38414 ! NEVT = 500
