*
*   Nickname     : xs_hzha03pyth6156hinvqq_e195.5_m112.5_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E195.5/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Sun Sep 16 01:51:07 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41850.xsdst ! RUN = 41850 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41851.xsdst ! RUN = 41851 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41852.xsdst ! RUN = 41852 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41853.xsdst ! RUN = 41853 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41854.xsdst ! RUN = 41854 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41855.xsdst ! RUN = 41855 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41856.xsdst ! RUN = 41856 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41857.xsdst ! RUN = 41857 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41858.xsdst ! RUN = 41858 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvqq_195.5_112.5_41859.xsdst ! RUN = 41859 ! NEVT = 500
