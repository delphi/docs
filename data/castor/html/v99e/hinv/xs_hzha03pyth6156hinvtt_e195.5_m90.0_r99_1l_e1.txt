*
*   Nickname     : xs_hzha03pyth6156hinvtt_e195.5_m90.0_r99_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E195.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 99_e1 done at ecms=195.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Fri Nov 30 15:29:55 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_90.0_39661.xsdst ! RUN = 39661 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_90.0_39662.xsdst ! RUN = 39662 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_90.0_39663.xsdst ! RUN = 39663 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v99e/195.5/hzha03pyth6156_hinvtt_195.5_90.0_39664.xsdst ! RUN = 39664 ! NEVT = 500
