*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m90_m100_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Mon Nov 19 09:42:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10571.xsdst ! RUN = 10571 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10572.xsdst ! RUN = 10572 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10573.xsdst ! RUN = 10573 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10574.xsdst ! RUN = 10574 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10575.xsdst ! RUN = 10575 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10576.xsdst ! RUN = 10576 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10577.xsdst ! RUN = 10577 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10578.xsdst ! RUN = 10578 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10579.xsdst ! RUN = 10579 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_100_10580.xsdst ! RUN = 10580 ! NEVT = 499
