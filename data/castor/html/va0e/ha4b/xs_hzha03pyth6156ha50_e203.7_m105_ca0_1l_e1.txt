*
*   Nickname     : xs_hzha03pyth6156ha50_e203.7_m105_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Aug 26 05:23:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30801.xsdst ! RUN = 30801 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30802.xsdst ! RUN = 30802 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30803.xsdst ! RUN = 30803 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30804.xsdst ! RUN = 30804 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30805.xsdst ! RUN = 30805 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30806.xsdst ! RUN = 30806 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30807.xsdst ! RUN = 30807 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30808.xsdst ! RUN = 30808 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30809.xsdst ! RUN = 30809 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50_203.7_105_30810.xsdst ! RUN = 30810 ! NEVT = 500
