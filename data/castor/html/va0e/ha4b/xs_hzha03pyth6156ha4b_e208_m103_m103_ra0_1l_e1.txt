*
*   Nickname     : xs_hzha03pyth6156ha4b_e208_m103_m103_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Mon Nov 19 09:40:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30621.xsdst ! RUN = 30621 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30622.xsdst ! RUN = 30622 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30623.xsdst ! RUN = 30623 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30624.xsdst ! RUN = 30624 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30625.xsdst ! RUN = 30625 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30626.xsdst ! RUN = 30626 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30627.xsdst ! RUN = 30627 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30628.xsdst ! RUN = 30628 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30629.xsdst ! RUN = 30629 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_103_103_30630.xsdst ! RUN = 30630 ! NEVT = 498
