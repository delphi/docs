*
*   Nickname     : xs_hzha03pyth6156ha50_e208_m100_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 4880 events in 10 files time stamp: Wed Aug 22 11:17:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30101.xsdst ! RUN = 30101 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30102.xsdst ! RUN = 30102 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30103.xsdst ! RUN = 30103 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30104.xsdst ! RUN = 30104 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30105.xsdst ! RUN = 30105 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30106.xsdst ! RUN = 30106 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30107.xsdst ! RUN = 30107 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30108.xsdst ! RUN = 30108 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30109.xsdst ! RUN = 30109 ! NEVT = 384
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_100_30110.xsdst ! RUN = 30110 ! NEVT = 498
