*
*   Nickname     : xs_hzha03pyth6156ha4b_e205_m18_m18_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sun Sep 30 12:13:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40101.xsdst ! RUN = 40101 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40102.xsdst ! RUN = 40102 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40103.xsdst ! RUN = 40103 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40104.xsdst ! RUN = 40104 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40105.xsdst ! RUN = 40105 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40106.xsdst ! RUN = 40106 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40107.xsdst ! RUN = 40107 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40108.xsdst ! RUN = 40108 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40109.xsdst ! RUN = 40109 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_18_18_40110.xsdst ! RUN = 40110 ! NEVT = 500
