*
*   Nickname     : xs_hzha03pyth6156ha4b_e205_m30_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sun Sep 30 13:20:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40151.xsdst ! RUN = 40151 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40152.xsdst ! RUN = 40152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40153.xsdst ! RUN = 40153 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40154.xsdst ! RUN = 40154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40155.xsdst ! RUN = 40155 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40156.xsdst ! RUN = 40156 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40157.xsdst ! RUN = 40157 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40158.xsdst ! RUN = 40158 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40159.xsdst ! RUN = 40159 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_40_40160.xsdst ! RUN = 40160 ! NEVT = 500
