*
*   Nickname     : xs_hzha03pyth6156ha50_e206.5_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Tue Aug 21 08:16:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29001.xsdst ! RUN = 29001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29002.xsdst ! RUN = 29002 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29003.xsdst ! RUN = 29003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29004.xsdst ! RUN = 29004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29005.xsdst ! RUN = 29005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29006.xsdst ! RUN = 29006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29007.xsdst ! RUN = 29007 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29008.xsdst ! RUN = 29008 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29009.xsdst ! RUN = 29009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_90_29010.xsdst ! RUN = 29010 ! NEVT = 499
