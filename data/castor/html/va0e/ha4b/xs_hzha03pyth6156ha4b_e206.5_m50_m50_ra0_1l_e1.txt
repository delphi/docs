*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m50_m50_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4920 events in 10 files time stamp: Mon Nov 19 09:42:32 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10271.xsdst ! RUN = 10271 ! NEVT = 426
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10272.xsdst ! RUN = 10272 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10273.xsdst ! RUN = 10273 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10274.xsdst ! RUN = 10274 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10275.xsdst ! RUN = 10275 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10276.xsdst ! RUN = 10276 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10277.xsdst ! RUN = 10277 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10278.xsdst ! RUN = 10278 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10279.xsdst ! RUN = 10279 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_50_10280.xsdst ! RUN = 10280 ! NEVT = 500
