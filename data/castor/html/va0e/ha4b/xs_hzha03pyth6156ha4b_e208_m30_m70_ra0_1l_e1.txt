*
*   Nickname     : xs_hzha03pyth6156ha4b_e208_m30_m70_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4995 events in 10 files time stamp: Mon Nov 19 09:40:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30181.xsdst ! RUN = 30181 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30182.xsdst ! RUN = 30182 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30183.xsdst ! RUN = 30183 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30184.xsdst ! RUN = 30184 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30185.xsdst ! RUN = 30185 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30186.xsdst ! RUN = 30186 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30187.xsdst ! RUN = 30187 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30188.xsdst ! RUN = 30188 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30189.xsdst ! RUN = 30189 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_70_30190.xsdst ! RUN = 30190 ! NEVT = 499
