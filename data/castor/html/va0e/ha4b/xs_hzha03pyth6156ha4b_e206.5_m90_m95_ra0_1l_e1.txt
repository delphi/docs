*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m90_m95_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Mon Nov 19 09:50:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10561.xsdst ! RUN = 10561 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10562.xsdst ! RUN = 10562 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10563.xsdst ! RUN = 10563 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10564.xsdst ! RUN = 10564 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10565.xsdst ! RUN = 10565 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10566.xsdst ! RUN = 10566 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10567.xsdst ! RUN = 10567 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10568.xsdst ! RUN = 10568 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10569.xsdst ! RUN = 10569 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_90_95_10570.xsdst ! RUN = 10570 ! NEVT = 500
