*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m50_m150_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4995 events in 10 files time stamp: Mon Nov 19 09:46:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10331.xsdst ! RUN = 10331 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10332.xsdst ! RUN = 10332 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10333.xsdst ! RUN = 10333 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10334.xsdst ! RUN = 10334 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10335.xsdst ! RUN = 10335 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10336.xsdst ! RUN = 10336 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10337.xsdst ! RUN = 10337 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10338.xsdst ! RUN = 10338 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10339.xsdst ! RUN = 10339 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_50_150_10340.xsdst ! RUN = 10340 ! NEVT = 500
