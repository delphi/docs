*
*   Nickname     : xs_hzha03pyth6156ha4b_e205_m12_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Sep 30 09:15:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40021.xsdst ! RUN = 40021 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40022.xsdst ! RUN = 40022 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40023.xsdst ! RUN = 40023 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40024.xsdst ! RUN = 40024 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40025.xsdst ! RUN = 40025 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40026.xsdst ! RUN = 40026 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40027.xsdst ! RUN = 40027 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40028.xsdst ! RUN = 40028 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40029.xsdst ! RUN = 40029 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_50_40030.xsdst ! RUN = 40030 ! NEVT = 500
