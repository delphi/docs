*
*   Nickname     : xs_hzha03pyth6156ha4b_e205_m12_m130_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4994 events in 10 files time stamp: Sun Sep 30 12:16:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40061.xsdst ! RUN = 40061 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40062.xsdst ! RUN = 40062 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40063.xsdst ! RUN = 40063 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40064.xsdst ! RUN = 40064 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40065.xsdst ! RUN = 40065 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40066.xsdst ! RUN = 40066 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40067.xsdst ! RUN = 40067 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40068.xsdst ! RUN = 40068 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40069.xsdst ! RUN = 40069 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_12_130_40070.xsdst ! RUN = 40070 ! NEVT = 498
