*
*   Nickname     : xs_hzha03pyth6156ha4b_e208_m24_m24_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4998 events in 10 files time stamp: Tue Oct  2 01:25:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30131.xsdst ! RUN = 30131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30132.xsdst ! RUN = 30132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30133.xsdst ! RUN = 30133 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30134.xsdst ! RUN = 30134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30135.xsdst ! RUN = 30135 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30136.xsdst ! RUN = 30136 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30137.xsdst ! RUN = 30137 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30138.xsdst ! RUN = 30138 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30139.xsdst ! RUN = 30139 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_24_24_30140.xsdst ! RUN = 30140 ! NEVT = 500
