*
*   Nickname     : xs_hzha03pyth6156ha50_e208_m60_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 4991 events in 10 files time stamp: Fri Aug 24 15:22:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26101.xsdst ! RUN = 26101 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26102.xsdst ! RUN = 26102 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26103.xsdst ! RUN = 26103 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26104.xsdst ! RUN = 26104 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26105.xsdst ! RUN = 26105 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26106.xsdst ! RUN = 26106 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26107.xsdst ! RUN = 26107 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26108.xsdst ! RUN = 26108 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26109.xsdst ! RUN = 26109 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50_208_60_26110.xsdst ! RUN = 26110 ! NEVT = 499
