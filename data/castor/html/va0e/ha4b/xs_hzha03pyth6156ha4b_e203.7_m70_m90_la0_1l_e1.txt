*
*   Nickname     : xs_hzha03pyth6156ha4b_e203.7_m70_m90_la0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E203.7/LYON/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=203.7 , Lyon
*---
*   Comments     : in total 4996 events in 10 files time stamp: Wed Nov 14 18:28:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50400.xsdst ! RUN = 50400 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50402.xsdst ! RUN = 50402 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50403.xsdst ! RUN = 50403 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50404.xsdst ! RUN = 50404 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50405.xsdst ! RUN = 50405 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50406.xsdst ! RUN = 50406 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50407.xsdst ! RUN = 50407 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50408.xsdst ! RUN = 50408 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50409.xsdst ! RUN = 50409 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_70_90_50410.xsdst ! RUN = 50410 ! NEVT = 498
