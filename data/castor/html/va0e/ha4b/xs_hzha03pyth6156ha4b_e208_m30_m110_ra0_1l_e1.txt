*
*   Nickname     : xs_hzha03pyth6156ha4b_e208_m30_m110_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4924 events in 10 files time stamp: Mon Nov 19 09:39:18 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30201.xsdst ! RUN = 30201 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30202.xsdst ! RUN = 30202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30203.xsdst ! RUN = 30203 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30204.xsdst ! RUN = 30204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30205.xsdst ! RUN = 30205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30206.xsdst ! RUN = 30206 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30207.xsdst ! RUN = 30207 ! NEVT = 428
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30208.xsdst ! RUN = 30208 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30209.xsdst ! RUN = 30209 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_30_110_30210.xsdst ! RUN = 30210 ! NEVT = 500
