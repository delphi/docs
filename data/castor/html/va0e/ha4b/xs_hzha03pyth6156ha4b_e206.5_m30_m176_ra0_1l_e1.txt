*
*   Nickname     : xs_hzha03pyth6156ha4b_e206.5_m30_m176_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E206.5/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4992 events in 10 files time stamp: Wed Oct  3 00:41:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10241.xsdst ! RUN = 10241 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10242.xsdst ! RUN = 10242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10243.xsdst ! RUN = 10243 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10244.xsdst ! RUN = 10244 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10245.xsdst ! RUN = 10245 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10246.xsdst ! RUN = 10246 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10247.xsdst ! RUN = 10247 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10248.xsdst ! RUN = 10248 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10249.xsdst ! RUN = 10249 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha4b_206.5_30_176_10250.xsdst ! RUN = 10250 ! NEVT = 499
