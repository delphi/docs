*
*   Nickname     : xs_hzha03pyth6156ha4b_e203.7_m12_m170_la0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E203.7/LYON/SUMT/C001-11
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=203.7 , Lyon
*---
*   Comments     : in total 5491 events in 11 files time stamp: Wed Oct  3 01:14:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50080.xsdst ! RUN = 50080 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50081.xsdst ! RUN = 50081 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50082.xsdst ! RUN = 50082 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50083.xsdst ! RUN = 50083 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50084.xsdst ! RUN = 50084 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50085.xsdst ! RUN = 50085 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50086.xsdst ! RUN = 50086 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50087.xsdst ! RUN = 50087 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50088.xsdst ! RUN = 50088 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50089.xsdst ! RUN = 50089 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_12_170_50090.xsdst ! RUN = 50090 ! NEVT = 500
