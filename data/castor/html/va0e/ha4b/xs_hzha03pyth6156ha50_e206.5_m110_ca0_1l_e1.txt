*
*   Nickname     : xs_hzha03pyth6156ha50_e206.5_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Tue Aug 21 10:18:57 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31001.xsdst ! RUN = 31001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31002.xsdst ! RUN = 31002 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31003.xsdst ! RUN = 31003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31004.xsdst ! RUN = 31004 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31005.xsdst ! RUN = 31005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31006.xsdst ! RUN = 31006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31007.xsdst ! RUN = 31007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31008.xsdst ! RUN = 31008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31009.xsdst ! RUN = 31009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_110_31010.xsdst ! RUN = 31010 ! NEVT = 500
