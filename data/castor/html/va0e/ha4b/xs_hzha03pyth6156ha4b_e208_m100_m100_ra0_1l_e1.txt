*
*   Nickname     : xs_hzha03pyth6156ha4b_e208_m100_m100_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Nov 19 09:41:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30611.xsdst ! RUN = 30611 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30612.xsdst ! RUN = 30612 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30613.xsdst ! RUN = 30613 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30614.xsdst ! RUN = 30614 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30615.xsdst ! RUN = 30615 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30616.xsdst ! RUN = 30616 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30617.xsdst ! RUN = 30617 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30618.xsdst ! RUN = 30618 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30619.xsdst ! RUN = 30619 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha4b_208_100_100_30620.xsdst ! RUN = 30620 ! NEVT = 500
