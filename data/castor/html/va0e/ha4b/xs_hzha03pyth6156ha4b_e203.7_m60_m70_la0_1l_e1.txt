*
*   Nickname     : xs_hzha03pyth6156ha4b_e203.7_m60_m70_la0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E203.7/LYON/SUMT/C001-11
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=203.7 , Lyon
*---
*   Comments     : in total 5494 events in 11 files time stamp: Wed Nov 14 18:27:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50330.xsdst ! RUN = 50330 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50331.xsdst ! RUN = 50331 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50332.xsdst ! RUN = 50332 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50333.xsdst ! RUN = 50333 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50334.xsdst ! RUN = 50334 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50335.xsdst ! RUN = 50335 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50336.xsdst ! RUN = 50336 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50337.xsdst ! RUN = 50337 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50338.xsdst ! RUN = 50338 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50339.xsdst ! RUN = 50339 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_60_70_50340.xsdst ! RUN = 50340 ! NEVT = 500
