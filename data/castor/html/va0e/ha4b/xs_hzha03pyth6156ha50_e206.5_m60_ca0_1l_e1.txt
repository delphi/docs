*
*   Nickname     : xs_hzha03pyth6156ha50_e206.5_m60_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Fri Aug 24 12:36:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26001.xsdst ! RUN = 26001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26002.xsdst ! RUN = 26002 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26003.xsdst ! RUN = 26003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26004.xsdst ! RUN = 26004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26005.xsdst ! RUN = 26005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26006.xsdst ! RUN = 26006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26007.xsdst ! RUN = 26007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26008.xsdst ! RUN = 26008 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26009.xsdst ! RUN = 26009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50_206.5_60_26010.xsdst ! RUN = 26010 ! NEVT = 500
