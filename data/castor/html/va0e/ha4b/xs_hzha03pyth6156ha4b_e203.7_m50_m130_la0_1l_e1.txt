*
*   Nickname     : xs_hzha03pyth6156ha4b_e203.7_m50_m130_la0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E203.7/LYON/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=203.7 , Lyon
*---
*   Comments     : in total 4992 events in 10 files time stamp: Wed Nov 14 20:21:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50301.xsdst ! RUN = 50301 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50302.xsdst ! RUN = 50302 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50303.xsdst ! RUN = 50303 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50304.xsdst ! RUN = 50304 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50305.xsdst ! RUN = 50305 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50306.xsdst ! RUN = 50306 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50307.xsdst ! RUN = 50307 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50308.xsdst ! RUN = 50308 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50309.xsdst ! RUN = 50309 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_50_130_50310.xsdst ! RUN = 50310 ! NEVT = 500
