*
*   Nickname     : xs_hzha03pyth6156ha4b_e203.7_m95_m95_la0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E203.7/LYON/SUMT/C001-8
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 done at ecms=203.7 , Lyon
*---
*   Comments     : in total 3998 events in 8 files time stamp: Wed Nov 14 18:27:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50550.xsdst ! RUN = 50550 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50551.xsdst ! RUN = 50551 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50552.xsdst ! RUN = 50552 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50553.xsdst ! RUN = 50553 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50554.xsdst ! RUN = 50554 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50556.xsdst ! RUN = 50556 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50557.xsdst ! RUN = 50557 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha4b_203.7_95_95_50559.xsdst ! RUN = 50559 ! NEVT = 500
