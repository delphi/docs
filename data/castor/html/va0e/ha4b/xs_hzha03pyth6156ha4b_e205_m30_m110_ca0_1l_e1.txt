*
*   Nickname     : xs_hzha03pyth6156ha4b_e205_m30_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4993 events in 10 files time stamp: Sun Sep 30 16:14:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40191.xsdst ! RUN = 40191 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40192.xsdst ! RUN = 40192 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40193.xsdst ! RUN = 40193 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40194.xsdst ! RUN = 40194 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40195.xsdst ! RUN = 40195 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40196.xsdst ! RUN = 40196 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40197.xsdst ! RUN = 40197 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40198.xsdst ! RUN = 40198 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40199.xsdst ! RUN = 40199 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ha4b_205_30_110_40200.xsdst ! RUN = 40200 ! NEVT = 500
