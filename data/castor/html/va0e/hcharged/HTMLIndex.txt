# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
xs_pyth6156hpphmml_e205.0_mxxx_ca0_1l_e1        2000      va0e1      HIGGS          4            3998      205.0          -          -       cern
xs_pyth6156hpphmml_e206.7_mxxx_ca0_1l_e1        2000      va0e1      HIGGS          8            7999      206.7          -          -       cern
xs_pyth6156hpphmml_e208.0_mxxx_ca0_1l_e1        2000      va0e1      HIGGS          4            3997      208.0        102          -       cern
xs_pyth6156hpphmmr_e205.0_mxxx_ca0_1l_e1        2000      va0e1      HIGGS          4            3998      205.0          -          -       cern
xs_pyth6156hpphmmr_e206.7_mxxx_ca0_1l_e1        2000      va0e1      HIGGS          8            7994      206.7          -          -       cern
xs_pyth6156hpphmmr_e208.0_mxxx_ca0_1l_e1        2000      va0e1      HIGGS          4            3996      208.0          -          -       cern
