*
*   Nickname     : xs_kk2f4143mmnoifi_e206.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4143MMNOIFI/E206.5/RAL/SUMT/C001-11
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 109995 events in 11 files time stamp: Wed Jan 16 22:10:50 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17000.xsdst ! RUN = 17000 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17001.xsdst ! RUN = 17001 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17002.xsdst ! RUN = 17002 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17003.xsdst ! RUN = 17003 ! NEVT = 9998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17004.xsdst ! RUN = 17004 ! NEVT = 9999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17005.xsdst ! RUN = 17005 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17006.xsdst ! RUN = 17006 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17007.xsdst ! RUN = 17007 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17008.xsdst ! RUN = 17008 ! NEVT = 9999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17009.xsdst ! RUN = 17009 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4143/va0e/206.5/kk2f4143_mmnoifi_206.5_17010.xsdst ! RUN = 17010 ! NEVT = 9999
