*
*   Nickname     : xs_teegg71cmpt_e208.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E208.0/RAL/SUMT
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=208.0 , RAL
*---
*   Comments     :  time stamp: Fri Jul 27 11:42:51 2001
*---
*
*
*
*
*         * Compton, e e --> e + e + gamma
*            theta_e > 10 degrees, theta_veto_e < 2 degrees,
*            theta_gamma > 10 degrees, E_e>1 GeV, E_gam > 1 GeV
*            x-sec = (44.69+-0.13)pb
*
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.1.xsdst ! RUN =  -47442 -47443 -47444 -47445 -47446 -47447 -47448 -47449 -47450 -47451! NEVT =    6178
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.2.xsdst ! RUN =  -47452 -47453 -47454 -47455 -47456 -47457 -47458 -47459 -47460 -47461! NEVT =    6345
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.3.xsdst ! RUN =  -47462 -47463 -47464 -47465 -47466 -47467 -47468 -47469 -47470 -47471! NEVT =    6379
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.4.xsdst ! RUN =  -47472 -47473 -47474 -47475 -47476 -47477 -47478 -47479 -47480 -47481! NEVT =    6283
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.5.xsdst ! RUN =  -47629 -47630 -47631 -47632 -47633 -47634 -47635 -47636 -47637 -47638! NEVT =    6340
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.6.xsdst ! RUN =  -47639 -47640 -47641 -47642 -47643 -47644 -47645 -47646 -47647 -47648! NEVT =    6384
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.7.xsdst ! RUN =  -47649 -47650 -47651 -47652 -47653 -47654 -47655 -47656 -47657 -47658! NEVT =    6313
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/208.0/EK5770.8.xsdst ! RUN =  -47659 -47660 -47661 -47662 -47663 -47664 -47665 -47666 -47667 -47668! NEVT =    6276
