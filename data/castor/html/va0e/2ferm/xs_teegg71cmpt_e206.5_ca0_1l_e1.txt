*
*   Nickname     : xs_teegg71cmpt_e206.5_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E206.5/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 8140 events in 4 files time stamp: Fri May 10 06:10:36 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/va0e/206.5/teegg71cmpt_206.5_38001.xsdst ! RUN = 38001 ! NEVT = 2042
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/va0e/206.5/teegg71cmpt_206.5_38002.xsdst ! RUN = 38002 ! NEVT = 2073
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/va0e/206.5/teegg71cmpt_206.5_38003.xsdst ! RUN = 38003 ! NEVT = 1984
FILE = /castor/cern.ch/delphi/MCprod/cern/teegg71cmpt/va0e/206.5/teegg71cmpt_206.5_38004.xsdst ! RUN = 38004 ! NEVT = 2041
