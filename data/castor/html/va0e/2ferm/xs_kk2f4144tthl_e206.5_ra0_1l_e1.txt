*
*   Nickname     : xs_kk2f4144tthl_e206.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4144TTHL/E206.5/RAL/SUMT/C001-26
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 129963 events in 26 files time stamp: Wed Jan 16 15:10:41 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15002.xsdst ! RUN = 15002 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15003.xsdst ! RUN = 15003 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15005.xsdst ! RUN = 15005 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15006.xsdst ! RUN = 15006 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15007.xsdst ! RUN = 15007 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15008.xsdst ! RUN = 15008 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15010.xsdst ! RUN = 15010 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15011.xsdst ! RUN = 15011 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15013.xsdst ! RUN = 15013 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15014.xsdst ! RUN = 15014 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15015.xsdst ! RUN = 15015 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15016.xsdst ! RUN = 15016 ! NEVT = 4997
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15017.xsdst ! RUN = 15017 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15018.xsdst ! RUN = 15018 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15019.xsdst ! RUN = 15019 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15020.xsdst ! RUN = 15020 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15025.xsdst ! RUN = 15025 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15029.xsdst ! RUN = 15029 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15030.xsdst ! RUN = 15030 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15031.xsdst ! RUN = 15031 ! NEVT = 4998
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15032.xsdst ! RUN = 15032 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15033.xsdst ! RUN = 15033 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15034.xsdst ! RUN = 15034 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15035.xsdst ! RUN = 15035 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15036.xsdst ! RUN = 15036 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4144/va0e/206.5/kk2f4144_tthl_206.5_15039.xsdst ! RUN = 15039 ! NEVT = 4998
