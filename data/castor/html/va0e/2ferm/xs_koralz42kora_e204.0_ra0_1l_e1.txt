*
*   Nickname     : xs_koralz42kora_e204.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KORALZ42KORA/E204.0/RAL/SUMT
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=204.0 , RAL
*---
*   Comments     :  time stamp: Mon Dec 17 12:04:22 2001
*---
*
*
*
*
*        * e+e- -> Z0 + gammas, Z0 -> tau+tau- KORALZ 4.2
*        * cross-section = (6.94+-0.02)pb
*          KORALZ 4.2: S, Jadach, B. Ward, Z. Was,
*          Comp Phys Com (94)
*
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz42/kora/va0e/204.0/TAP885538.xsdst ! RUN =  -45517 -45518 -45519 -45520 -45521 -45522 -45523 ! NEVT = 6558
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz42/kora/va0e/204.0/TAP885539.xsdst ! RUN =  -45524 -45525 -45526 -45527 -45528 -45529 -45530 ! NEVT = 6569
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz42/kora/va0e/204.0/TAP885540.xsdst ! RUN =  -45531 -45532 -45533 -45534 -45535 -45536 -45537 ! NEVT = 6271
