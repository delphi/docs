*
*   Nickname     : xs_teegg71cmpt_e206.7_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E206.7/RAL/SUMT
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.7 , RAL
*---
*   Comments     :  time stamp: Fri Oct 12 12:48:44 2001
*---
*
*
*
*
*          * Compton, e e --> e + e + gamma
*             theta_e > 10 degrees, theta_veto_e < 2 degrees,
*             theta_gamma > 10 degrees, E_e>1 GeV, E_gam > 1 GeV
*             x-sec = (44.72+-0.11)pb
*             TEEGG7.1: Berends & Kleiss with m term
*             Nucl.Phys.B264(1986)265
*  
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5769.24.xsdst ! RUN =  -48491 -48492 -48493 -48494 -48495 -48496 -48497 -48498 -48499 -48500! NEVT =    6310
*
*         * Compton, e e --> e + e + gamma special request sample
*            Special sample with high pt electron
*            theta_e > 15 degrees, theta_veto_e < 14.5 degrees,
*            E_e>15 GeV, pt_e > 18 GeV
*            x-sec = (44.29+-0.13)pb
*            TEEGG7.1: Berends & Kleiss with m term
*            Nucl.Phys.B264(1986)265
*
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.1.xsdst ! RUN =  -49724 -49725 -49726 -49727 -49728 -49729 -49730 -49731 -49732 -49733! NEVT =    5372
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.2.xsdst ! RUN =  -49734 -49735 -49736 -49737 -49738 -49739 -49740 -49741 -49742 -49743! NEVT =    5303
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.3.xsdst ! RUN =  -49744 -49745 -49746 -49747 -49748 -49749 -49750 -49751 -49752 -49753! NEVT =    5337
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.4.xsdst ! RUN =  -49754 -49755 -49756 -49757 -49758 -49759 -49760 -49761 -49762 -49763! NEVT =    5420
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.5.xsdst ! RUN =  -49764 -49765 -49766 -49767 -49768 -49769 -49770 -49771 -49772 -49773! NEVT =    5369
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.6.xsdst ! RUN =  -49774 -49775 -49776 -49777 -49778 -49779 -49780 -49781 -49782 -49783! NEVT =    5440
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.7.xsdst ! RUN =  -49784 -49785 -49786 -49787 -49788 -49789 -49790 -49791 -49792 -49793! NEVT =    5370
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.8.xsdst ! RUN =  -49794 -49795 -49796 -49797 -49798 -49799 -49800 -49801 -49802 -49803! NEVT =    4988
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.9.xsdst ! RUN =  -49904 -49905 -49906 -49907 -49908 -49909 -49910 -49911 -49912 -49913! NEVT =    5299
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.10.xsdst ! RUN =  -49914 -49915 -49916 -49917 -49918 -49919 -49920 -49921 -49922 -49923! NEVT =    5326
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.11.xsdst ! RUN =  -49924 -49925 -49926 -49927 -49928 -49929 -49930 -49931 -49932 -49933! NEVT =    5362
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.12.xsdst ! RUN =  -49934 -49935 -49936 -49937 -49938 -49939 -49940 -49941 -49942 -49943! NEVT =    5347
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.13.xsdst ! RUN =  -49944 -49945 -49946 -49947 -49948 -49949 -49950 -49951 -49952 -49953! NEVT =    5321
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.14.xsdst ! RUN =  -49954 -49955 -49956 -49957 -49958 -49959 -49960 -49961 -49962 -49963! NEVT =    5301
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.15.xsdst ! RUN =  -49964 -49965 -49966 -49967 -49968 -49969 -49970 -49971 -49972 -49973! NEVT =    5327
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.16.xsdst ! RUN =  -49974 -49975 -49976 -49977 -49978 -49979 -49980 -49981 -49982 -49983! NEVT =    5375
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.17.xsdst ! RUN =  -49984 -49985 -49986 -49987 -49988 -49989 -49990 -49991 -49992 -49993! NEVT =    5355
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.18.xsdst ! RUN =  -49994 -49995 -49996 -49997 -49998 -49999    -41    -42    -43      0! NEVT =    4810
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.19.xsdst ! RUN =     -44    -45    -46    -47    -48    -49   -410   -411   -412   -413! NEVT =    5409
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.20.xsdst ! RUN =    -414   -415   -416   -417   -418   -419   -420   -421   -422   -423! NEVT =    5347
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/va0e/206.7/EK5772.21.xsdst ! RUN =    -424   -425   -426   -427   -428   -429   -430   -431   -432   -433! NEVT =    5356

