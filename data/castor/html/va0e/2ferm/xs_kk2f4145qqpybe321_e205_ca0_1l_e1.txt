*
*   Nickname     : xs_kk2f4145qqpybe321_e205_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4145QQPYBE321/CERN/SUMT/C001-50
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 49961 events in 50 files time stamp: Wed Mar  6 22:11:39 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120001.xsdst ! RUN = 120001 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120002.xsdst ! RUN = 120002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120003.xsdst ! RUN = 120003 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120004.xsdst ! RUN = 120004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120005.xsdst ! RUN = 120005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120006.xsdst ! RUN = 120006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120007.xsdst ! RUN = 120007 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120008.xsdst ! RUN = 120008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120009.xsdst ! RUN = 120009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120010.xsdst ! RUN = 120010 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120011.xsdst ! RUN = 120011 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120012.xsdst ! RUN = 120012 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120013.xsdst ! RUN = 120013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120014.xsdst ! RUN = 120014 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120015.xsdst ! RUN = 120015 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120016.xsdst ! RUN = 120016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120017.xsdst ! RUN = 120017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120018.xsdst ! RUN = 120018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120019.xsdst ! RUN = 120019 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120020.xsdst ! RUN = 120020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120021.xsdst ! RUN = 120021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120022.xsdst ! RUN = 120022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120023.xsdst ! RUN = 120023 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120024.xsdst ! RUN = 120024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120025.xsdst ! RUN = 120025 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120026.xsdst ! RUN = 120026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120027.xsdst ! RUN = 120027 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120028.xsdst ! RUN = 120028 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120029.xsdst ! RUN = 120029 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120030.xsdst ! RUN = 120030 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120031.xsdst ! RUN = 120031 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120032.xsdst ! RUN = 120032 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120033.xsdst ! RUN = 120033 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120034.xsdst ! RUN = 120034 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120035.xsdst ! RUN = 120035 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120036.xsdst ! RUN = 120036 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120037.xsdst ! RUN = 120037 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120038.xsdst ! RUN = 120038 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120039.xsdst ! RUN = 120039 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120040.xsdst ! RUN = 120040 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120041.xsdst ! RUN = 120041 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120042.xsdst ! RUN = 120042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120043.xsdst ! RUN = 120043 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120044.xsdst ! RUN = 120044 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120045.xsdst ! RUN = 120045 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120046.xsdst ! RUN = 120046 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120047.xsdst ! RUN = 120047 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120048.xsdst ! RUN = 120048 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120049.xsdst ! RUN = 120049 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/va0e/205/kk2f4145_qqpybe321_205_120050.xsdst ! RUN = 120050 ! NEVT = 1000
