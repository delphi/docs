*
*   Nickname     : xs_hzha03pyth6156hsqq_e205_m45.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/RAL/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 1500 events in 3 files time stamp: Sat Nov 24 12:30:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_45.0_74501.xsdst ! RUN = 74501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_45.0_74503.xsdst ! RUN = 74503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_45.0_74504.xsdst ! RUN = 74504 ! NEVT = 500
