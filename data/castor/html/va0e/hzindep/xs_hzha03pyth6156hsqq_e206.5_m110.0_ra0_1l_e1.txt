*
*   Nickname     : xs_hzha03pyth6156hsqq_e206.5_m110.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E206.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Tue Nov 20 16:29:18 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsqq_206.5_110.0_71001.xsdst ! RUN = 71001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsqq_206.5_110.0_71002.xsdst ! RUN = 71002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsqq_206.5_110.0_71003.xsdst ! RUN = 71003 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsqq_206.5_110.0_71004.xsdst ! RUN = 71004 ! NEVT = 500
