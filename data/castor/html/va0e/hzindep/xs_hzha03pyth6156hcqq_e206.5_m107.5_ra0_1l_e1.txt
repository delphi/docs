*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m107.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Nov 21 14:44:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_107.5_70755.xsdst ! RUN = 70755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_107.5_70756.xsdst ! RUN = 70756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_107.5_70757.xsdst ! RUN = 70757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_107.5_70758.xsdst ! RUN = 70758 ! NEVT = 500
