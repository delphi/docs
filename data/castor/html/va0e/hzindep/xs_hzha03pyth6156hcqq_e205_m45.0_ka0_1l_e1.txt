*
*   Nickname     : xs_hzha03pyth6156hcqq_e205_m45.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Feb  7 12:12:37 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_45.0_11171.xsdst ! RUN = 11171 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_45.0_11172.xsdst ! RUN = 11172 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_45.0_11173.xsdst ! RUN = 11173 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_45.0_11174.xsdst ! RUN = 11174 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_45.0_11175.xsdst ! RUN = 11175 ! NEVT = 400
