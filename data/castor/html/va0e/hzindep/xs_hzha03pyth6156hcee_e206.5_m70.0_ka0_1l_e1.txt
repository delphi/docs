*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m70.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1997 events in 5 files time stamp: Wed Nov 21 12:26:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_70.0_1896.xsdst ! RUN = 1896 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_70.0_1897.xsdst ! RUN = 1897 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_70.0_1898.xsdst ! RUN = 1898 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_70.0_1899.xsdst ! RUN = 1899 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_70.0_1900.xsdst ! RUN = 1900 ! NEVT = 399
