*
*   Nickname     : xs_hzha03pyth6156hsmm_e206.5_m107.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Nov 29 16:43:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsmm_206.5_107.5_2611.xsdst ! RUN = 2611 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsmm_206.5_107.5_2612.xsdst ! RUN = 2612 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsmm_206.5_107.5_2613.xsdst ! RUN = 2613 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsmm_206.5_107.5_2614.xsdst ! RUN = 2614 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsmm_206.5_107.5_2615.xsdst ! RUN = 2615 ! NEVT = 400
