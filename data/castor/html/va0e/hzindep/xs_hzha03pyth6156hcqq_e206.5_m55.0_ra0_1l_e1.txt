*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m55.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Nov 21 02:29:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_55.0_65505.xsdst ! RUN = 65505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_55.0_65506.xsdst ! RUN = 65506 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_55.0_65507.xsdst ! RUN = 65507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_55.0_65508.xsdst ! RUN = 65508 ! NEVT = 500
