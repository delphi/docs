*
*   Nickname     : xs_hzha03pyth6156hsee_e205_m65.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1997 events in 5 files time stamp: Thu Nov 15 10:18:49 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsee_205_65.0_1401.xsdst ! RUN = 1401 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsee_205_65.0_1402.xsdst ! RUN = 1402 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsee_205_65.0_1403.xsdst ! RUN = 1403 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsee_205_65.0_1404.xsdst ! RUN = 1404 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsee_205_65.0_1405.xsdst ! RUN = 1405 ! NEVT = 400
