*
*   Nickname     : xs_hzha03pyth6156hcnn_e206.5_m40.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Sun Jan  6 13:53:16 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcnn_206.5_40.0_6816.xsdst ! RUN = 6816 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcnn_206.5_40.0_6817.xsdst ! RUN = 6817 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcnn_206.5_40.0_6818.xsdst ! RUN = 6818 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcnn_206.5_40.0_6819.xsdst ! RUN = 6819 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcnn_206.5_40.0_6820.xsdst ! RUN = 6820 ! NEVT = 399
