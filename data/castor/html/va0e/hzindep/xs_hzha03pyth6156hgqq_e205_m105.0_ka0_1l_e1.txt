*
*   Nickname     : xs_hzha03pyth6156hgqq_e205_m105.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1998 events in 5 files time stamp: Sun Jan 27 15:11:11 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_105.0_8961.xsdst ! RUN = 8961 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_105.0_8962.xsdst ! RUN = 8962 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_105.0_8963.xsdst ! RUN = 8963 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_105.0_8964.xsdst ! RUN = 8964 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_105.0_8965.xsdst ! RUN = 8965 ! NEVT = 399
