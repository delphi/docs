*
*   Nickname     : xs_hzha03pyth6156hgqq_e205_m47.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1994 events in 5 files time stamp: Sun Jan 27 15:11:10 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_47.5_8846.xsdst ! RUN = 8846 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_47.5_8847.xsdst ! RUN = 8847 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_47.5_8848.xsdst ! RUN = 8848 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_47.5_8849.xsdst ! RUN = 8849 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgqq_205_47.5_8850.xsdst ! RUN = 8850 ! NEVT = 400
