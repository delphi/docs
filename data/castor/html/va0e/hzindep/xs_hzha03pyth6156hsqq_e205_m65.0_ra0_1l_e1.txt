*
*   Nickname     : xs_hzha03pyth6156hsqq_e205_m65.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Sat Nov 24 12:23:31 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_65.0_76501.xsdst ! RUN = 76501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_65.0_76502.xsdst ! RUN = 76502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_65.0_76503.xsdst ! RUN = 76503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hsqq_205_65.0_76504.xsdst ! RUN = 76504 ! NEVT = 499
