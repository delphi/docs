*
*   Nickname     : xs_hzha03pyth6156hsnn_e205_m110.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSNN/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Jan  3 07:46:05 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_110.0_5871.xsdst ! RUN = 5871 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_110.0_5872.xsdst ! RUN = 5872 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_110.0_5873.xsdst ! RUN = 5873 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_110.0_5874.xsdst ! RUN = 5874 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_110.0_5875.xsdst ! RUN = 5875 ! NEVT = 400
