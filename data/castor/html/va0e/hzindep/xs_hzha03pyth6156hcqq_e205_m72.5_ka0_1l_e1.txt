*
*   Nickname     : xs_hzha03pyth6156hcqq_e205_m72.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Feb  7 12:12:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_72.5_11226.xsdst ! RUN = 11226 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_72.5_11227.xsdst ! RUN = 11227 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_72.5_11228.xsdst ! RUN = 11228 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_72.5_11229.xsdst ! RUN = 11229 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcqq_205_72.5_11230.xsdst ! RUN = 11230 ! NEVT = 400
