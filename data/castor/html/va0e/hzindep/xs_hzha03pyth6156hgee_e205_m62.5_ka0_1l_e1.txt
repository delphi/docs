*
*   Nickname     : xs_hzha03pyth6156hgee_e205_m62.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1998 events in 5 files time stamp: Wed Nov  7 14:49:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_62.5_1046.xsdst ! RUN = 1046 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_62.5_1047.xsdst ! RUN = 1047 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_62.5_1048.xsdst ! RUN = 1048 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_62.5_1049.xsdst ! RUN = 1049 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_62.5_1050.xsdst ! RUN = 1050 ! NEVT = 400
