*
*   Nickname     : xs_hzha03pyth6156hsnn_e206.5_m42.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Jan  3 16:51:59 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsnn_206.5_42.5_5891.xsdst ! RUN = 5891 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsnn_206.5_42.5_5892.xsdst ! RUN = 5892 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsnn_206.5_42.5_5893.xsdst ! RUN = 5893 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsnn_206.5_42.5_5894.xsdst ! RUN = 5894 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hsnn_206.5_42.5_5895.xsdst ! RUN = 5895 ! NEVT = 400
