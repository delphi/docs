*
*   Nickname     : xs_hzha03pyth6156hcnn_e205_m107.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Sat Jan  5 17:01:58 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_107.5_6796.xsdst ! RUN = 6796 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_107.5_6797.xsdst ! RUN = 6797 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_107.5_6798.xsdst ! RUN = 6798 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_107.5_6799.xsdst ! RUN = 6799 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_107.5_6800.xsdst ! RUN = 6800 ! NEVT = 400
