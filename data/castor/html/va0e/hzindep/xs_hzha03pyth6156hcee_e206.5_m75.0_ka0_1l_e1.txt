*
*   Nickname     : xs_hzha03pyth6156hcee_e206.5_m75.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1951 events in 5 files time stamp: Wed Nov 21 12:25:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_75.0_1906.xsdst ! RUN = 1906 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_75.0_1907.xsdst ! RUN = 1907 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_75.0_1908.xsdst ! RUN = 1908 ! NEVT = 358
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_75.0_1909.xsdst ! RUN = 1909 ! NEVT = 395
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcee_206.5_75.0_1910.xsdst ! RUN = 1910 ! NEVT = 400
