*
*   Nickname     : xs_hzha03pyth6156hgmm_e206.5_m102.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Mon Jan  7 16:47:21 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgmm_206.5_102.5_2291.xsdst ! RUN = 2291 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgmm_206.5_102.5_2292.xsdst ! RUN = 2292 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgmm_206.5_102.5_2293.xsdst ! RUN = 2293 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgmm_206.5_102.5_2294.xsdst ! RUN = 2294 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgmm_206.5_102.5_2295.xsdst ! RUN = 2295 ! NEVT = 400
