*
*   Nickname     : xs_hzha03pyth6156hgnn_e205_m67.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1937 events in 5 files time stamp: Mon Dec 31 14:36:19 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgnn_205_67.5_4856.xsdst ! RUN = 4856 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgnn_205_67.5_4857.xsdst ! RUN = 4857 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgnn_205_67.5_4858.xsdst ! RUN = 4858 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgnn_205_67.5_4859.xsdst ! RUN = 4859 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgnn_205_67.5_4860.xsdst ! RUN = 4860 ! NEVT = 337
