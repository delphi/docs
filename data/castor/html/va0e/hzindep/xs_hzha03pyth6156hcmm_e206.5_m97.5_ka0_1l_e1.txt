*
*   Nickname     : xs_hzha03pyth6156hcmm_e206.5_m97.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Mon Dec  3 22:25:11 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcmm_206.5_97.5_2901.xsdst ! RUN = 2901 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcmm_206.5_97.5_2902.xsdst ! RUN = 2902 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcmm_206.5_97.5_2903.xsdst ! RUN = 2903 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcmm_206.5_97.5_2904.xsdst ! RUN = 2904 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcmm_206.5_97.5_2905.xsdst ! RUN = 2905 ! NEVT = 400
