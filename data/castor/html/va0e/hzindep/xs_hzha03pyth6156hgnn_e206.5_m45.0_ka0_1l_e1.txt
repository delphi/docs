*
*   Nickname     : xs_hzha03pyth6156hgnn_e206.5_m45.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Jan  3 02:34:16 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgnn_206.5_45.0_4966.xsdst ! RUN = 4966 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgnn_206.5_45.0_4967.xsdst ! RUN = 4967 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgnn_206.5_45.0_4968.xsdst ! RUN = 4968 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgnn_206.5_45.0_4969.xsdst ! RUN = 4969 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgnn_206.5_45.0_4970.xsdst ! RUN = 4970 ! NEVT = 400
