*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m70.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Wed Nov 21 02:28:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_70.0_67005.xsdst ! RUN = 67005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_70.0_67006.xsdst ! RUN = 67006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_70.0_67007.xsdst ! RUN = 67007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_70.0_67008.xsdst ! RUN = 67008 ! NEVT = 500
