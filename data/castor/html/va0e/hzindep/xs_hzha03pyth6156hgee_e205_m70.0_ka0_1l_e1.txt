*
*   Nickname     : xs_hzha03pyth6156hgee_e205_m70.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1997 events in 5 files time stamp: Wed Nov  7 14:49:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_70.0_1061.xsdst ! RUN = 1061 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_70.0_1062.xsdst ! RUN = 1062 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_70.0_1063.xsdst ! RUN = 1063 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_70.0_1064.xsdst ! RUN = 1064 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_70.0_1065.xsdst ! RUN = 1065 ! NEVT = 400
