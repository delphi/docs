*
*   Nickname     : xs_hzha03pyth6156hgee_e205_m77.5_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1882 events in 5 files time stamp: Thu Nov  8 16:48:27 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_77.5_1076.xsdst ! RUN = 1076 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_77.5_1077.xsdst ! RUN = 1077 ! NEVT = 285
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_77.5_1078.xsdst ! RUN = 1078 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_77.5_1079.xsdst ! RUN = 1079 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_77.5_1080.xsdst ! RUN = 1080 ! NEVT = 400
