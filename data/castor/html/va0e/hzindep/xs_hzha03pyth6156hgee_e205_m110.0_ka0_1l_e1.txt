*
*   Nickname     : xs_hzha03pyth6156hgee_e205_m110.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1996 events in 5 files time stamp: Fri Nov  9 17:17:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_110.0_1161.xsdst ! RUN = 1161 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_110.0_1162.xsdst ! RUN = 1162 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_110.0_1163.xsdst ! RUN = 1163 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_110.0_1164.xsdst ! RUN = 1164 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hgee_205_110.0_1165.xsdst ! RUN = 1165 ! NEVT = 398
