*
*   Nickname     : xs_hzha03pyth6156hsnn_e205_m80.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSNN/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Jan  3 07:45:59 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_80.0_5811.xsdst ! RUN = 5811 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_80.0_5812.xsdst ! RUN = 5812 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_80.0_5813.xsdst ! RUN = 5813 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_80.0_5814.xsdst ! RUN = 5814 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hsnn_205_80.0_5815.xsdst ! RUN = 5815 ! NEVT = 400
