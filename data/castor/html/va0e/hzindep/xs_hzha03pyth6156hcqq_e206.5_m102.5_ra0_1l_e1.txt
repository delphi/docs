*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m102.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Nov 21 12:22:29 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_102.5_70255.xsdst ! RUN = 70255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_102.5_70256.xsdst ! RUN = 70256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_102.5_70257.xsdst ! RUN = 70257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_102.5_70258.xsdst ! RUN = 70258 ! NEVT = 500
