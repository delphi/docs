*
*   Nickname     : xs_hzha03pyth6156hgee_e206.5_m75.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E206.5/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation a0_e1 done at ecms=206.5 , Karlsruhe
*---
*   Comments     : in total 1997 events in 5 files time stamp: Mon Jan  7 16:44:48 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgee_206.5_75.0_1246.xsdst ! RUN = 1246 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgee_206.5_75.0_1247.xsdst ! RUN = 1247 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgee_206.5_75.0_1248.xsdst ! RUN = 1248 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgee_206.5_75.0_1249.xsdst ! RUN = 1249 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgee_206.5_75.0_1250.xsdst ! RUN = 1250 ! NEVT = 399
