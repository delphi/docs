*
*   Nickname     : xs_hzha03pyth6156hcmm_e205_m50.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Fri Nov 30 12:34:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcmm_205_50.0_2651.xsdst ! RUN = 2651 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcmm_205_50.0_2652.xsdst ! RUN = 2652 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcmm_205_50.0_2653.xsdst ! RUN = 2653 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcmm_205_50.0_2654.xsdst ! RUN = 2654 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcmm_205_50.0_2655.xsdst ! RUN = 2655 ! NEVT = 400
