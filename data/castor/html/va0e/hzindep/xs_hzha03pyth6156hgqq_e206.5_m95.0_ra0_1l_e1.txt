*
*   Nickname     : xs_hzha03pyth6156hgqq_e206.5_m95.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E206.5/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 23 12:33:20 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgqq_206.5_95.0_69509.xsdst ! RUN = 69509 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgqq_206.5_95.0_69510.xsdst ! RUN = 69510 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgqq_206.5_95.0_69511.xsdst ! RUN = 69511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hgqq_206.5_95.0_69512.xsdst ! RUN = 69512 ! NEVT = 500
