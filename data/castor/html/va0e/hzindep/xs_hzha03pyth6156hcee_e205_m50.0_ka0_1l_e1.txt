*
*   Nickname     : xs_hzha03pyth6156hcee_e205_m50.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1995 events in 5 files time stamp: Tue Nov 20 14:49:25 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcee_205_50.0_1681.xsdst ! RUN = 1681 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcee_205_50.0_1682.xsdst ! RUN = 1682 ! NEVT = 397
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcee_205_50.0_1683.xsdst ! RUN = 1683 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcee_205_50.0_1684.xsdst ! RUN = 1684 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcee_205_50.0_1685.xsdst ! RUN = 1685 ! NEVT = 400
