*
*   Nickname     : xs_hzha03pyth6156hcnn_e205_m55.0_ka0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Sat Jan  5 17:02:27 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_55.0_6691.xsdst ! RUN = 6691 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_55.0_6692.xsdst ! RUN = 6692 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_55.0_6693.xsdst ! RUN = 6693 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_55.0_6694.xsdst ! RUN = 6694 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/va0e/205/hzha03pyth6156_hcnn_205_55.0_6695.xsdst ! RUN = 6695 ! NEVT = 400
