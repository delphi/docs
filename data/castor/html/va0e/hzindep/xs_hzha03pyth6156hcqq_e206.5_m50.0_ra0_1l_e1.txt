*
*   Nickname     : xs_hzha03pyth6156hcqq_e206.5_m50.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1996 events in 4 files time stamp: Wed Nov 21 02:30:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_50.0_65005.xsdst ! RUN = 65005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_50.0_65006.xsdst ! RUN = 65006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_50.0_65007.xsdst ! RUN = 65007 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hcqq_206.5_50.0_65008.xsdst ! RUN = 65008 ! NEVT = 499
