*
*   Nickname     : xs_hzha03pyth6156hwqqlnqq_e206.5_m110.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQLNQQ/E206.5/RAL/SUMT/C001-13
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 6496 events in 13 files time stamp: Sun Aug  4 06:41:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103001.xsdst ! RUN = 103001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103002.xsdst ! RUN = 103002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103003.xsdst ! RUN = 103003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103004.xsdst ! RUN = 103004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103005.xsdst ! RUN = 103005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103006.xsdst ! RUN = 103006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103007.xsdst ! RUN = 103007 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103008.xsdst ! RUN = 103008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103009.xsdst ! RUN = 103009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103010.xsdst ! RUN = 103010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103011.xsdst ! RUN = 103011 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103012.xsdst ! RUN = 103012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqlnqq_206.5_110.0_103013.xsdst ! RUN = 103013 ! NEVT = 499
