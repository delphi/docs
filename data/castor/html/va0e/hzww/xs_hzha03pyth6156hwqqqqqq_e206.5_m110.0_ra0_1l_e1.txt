*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e206.5_m110.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E206.5/RAL/SUMT/C001-11
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 5497 events in 11 files time stamp: Sat Aug  3 18:23:05 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101001.xsdst ! RUN = 101001 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101002.xsdst ! RUN = 101002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101003.xsdst ! RUN = 101003 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101004.xsdst ! RUN = 101004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101006.xsdst ! RUN = 101006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101007.xsdst ! RUN = 101007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101008.xsdst ! RUN = 101008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101009.xsdst ! RUN = 101009 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101010.xsdst ! RUN = 101010 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101014.xsdst ! RUN = 101014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_110.0_101015.xsdst ! RUN = 101015 ! NEVT = 500
