*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e205_m95.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/RAL/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0e1 205 , RAL
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sun Aug  4 11:12:55 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hwqqqqqq_205_95.0_102507.xsdst ! RUN = 102507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hwqqqqqq_205_95.0_102508.xsdst ! RUN = 102508 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hwqqqqqq_205_95.0_102511.xsdst ! RUN = 102511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hwqqqqqq_205_95.0_102512.xsdst ! RUN = 102512 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hwqqqqqq_205_95.0_102515.xsdst ! RUN = 102515 ! NEVT = 500
