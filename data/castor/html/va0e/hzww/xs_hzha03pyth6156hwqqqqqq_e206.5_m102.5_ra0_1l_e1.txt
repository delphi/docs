*
*   Nickname     : xs_hzha03pyth6156hwqqqqqq_e206.5_m102.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HWQQQQQQ/E206.5/RAL/SUMT/C001-9
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 4496 events in 9 files time stamp: Sat Aug  3 22:12:51 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100251.xsdst ! RUN = 100251 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100253.xsdst ! RUN = 100253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100254.xsdst ! RUN = 100254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100255.xsdst ! RUN = 100255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100256.xsdst ! RUN = 100256 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100257.xsdst ! RUN = 100257 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100258.xsdst ! RUN = 100258 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100260.xsdst ! RUN = 100260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hwqqqqqq_206.5_102.5_100263.xsdst ! RUN = 100263 ! NEVT = 500
