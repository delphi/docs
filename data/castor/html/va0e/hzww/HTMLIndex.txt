# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
xs_hzha03pyth6156hwqqlnqq_e205_mxxx_ra0_1l_e1        2000      va0e1      HIGGS        118           58720        205      100.0          -       ruth
xs_hzha03pyth6156hwqqlnqq_e206.5_mxxx_ra0_1l_e1        2000      va0e1      HIGGS        116           57963      206.5      107.5          -       ruth
xs_hzha03pyth6156hwqqqqnn_e205_mxxx_ra0_1l_e1        2000      va0e1      HIGGS         96           47989        205      112.5          -       ruth
xs_hzha03pyth6156hwqqqqnn_e206.5_mxxx_ra0_1l_e1        2000      va0e1      HIGGS         89           43993      206.5      120.0          -       ruth
xs_hzha03pyth6156hwqqqqqq_e205_mxxx_ra0_1l_e1        2000      va0e1      HIGGS         94           46967        205      112.5          -       ruth
xs_hzha03pyth6156hwqqqqqq_e206.5_mxxx_ra0_1l_e1        2000      va0e1      HIGGS         91           45461      206.5      107.5          -       ruth
