*
*   Nickname     : xs_bdk02eeee_e206.5_la0_1l_e2
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/BDK02EEEE/E206.5/LYON/SUMT/C001-65
*   Description  :  Extended Short DST simulation a0e2 done at ecms=206.5 , Lyon
*---
*   Comments     : in total 3217934 events in 65 files time stamp: Fri Jul  5 14:09:39 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10501:10510.xsdst ! RUN =  -10501 -10502 -10503 -10504 -10505 -10506 -10507 -10508 -10509 -10510  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10511:10520.xsdst ! RUN =  -10511 -10512 -10513 -10514 -10515 -10516 -10517 -10518 -10519 -10520  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10521:10530.xsdst ! RUN =  -10521 -10522 -10523 -10524 -10525 -10526 -10527 -10528 -10529 -10530  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10531:10540.xsdst ! RUN =  -10531 -10532 -10533 -10534 -10535 -10536 -10537 -10538 -10539 -10540  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10541:10550.xsdst ! RUN =  -10541 -10542 -10543 -10544 -10545 -10546 -10547 -10548 -10549 -10550  ! NEVT =   46261
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10551:10560.xsdst ! RUN =  -10551 -10552 -10553 -10554 -10555 -10556 -10557 -10558 -10559 -10560  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10561:10570.xsdst ! RUN =  -10561 -10562 -10563 -10564 -10565 -10566 -10567 -10568 -10569 -10570  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10571:10580.xsdst ! RUN =  -10571 -10572 -10573 -10574 -10575 -10576 -10577 -10578 -10579 -10580  ! NEVT =   43293
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10581:10590.xsdst ! RUN =  -10581 -10582 -10583 -10584 -10585 -10586 -10587 -10588 -10589 -10590  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10591:10600.xsdst ! RUN =  -10591 -10592 -10593 -10594 -10595 -10596 -10597 -10598 -10599 -10600  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10601:10610.xsdst ! RUN =  -10601 -10602 -10603 -10604 -10605 -10606 -10607 -10608 -10609 -10610  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10611:10620.xsdst ! RUN =  -10611 -10612 -10613 -10614 -10615 -10616 -10617 -10618 -10619 -10620  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10621:10630.xsdst ! RUN =  -10621 -10622 -10623 -10624 -10625 -10626 -10627 -10628 -10629 -10630  ! NEVT =   46439
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10631:10640.xsdst ! RUN =  -10631 -10632 -10633 -10634 -10635 -10636 -10637 -10638 -10639 -10640  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10641:10650.xsdst ! RUN =  -10641 -10642 -10643 -10644 -10645 -10646 -10647 -10648 -10649 -10650  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10651:10660.xsdst ! RUN =  -10651 -10652 -10653 -10654 -10655 -10656 -10657 -10658 -10659 -10660  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10661:10670.xsdst ! RUN =  -10661 -10662 -10663 -10664 -10665 -10666 -10667 -10668 -10669 -10670  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10671:10680.xsdst ! RUN =  -10671 -10672 -10673 -10674 -10675 -10676 -10677 -10678 -10679 -10680  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10681:10690.xsdst ! RUN =  -10681 -10682 -10683 -10684 -10685 -10686 -10687 -10688 -10689 -10690  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10691:10700.xsdst ! RUN =  -10691 -10692 -10693 -10694 -10695 -10696 -10697 -10698 -10699 -10700  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10701:10710.xsdst ! RUN =  -10701 -10702 -10703 -10704 -10705 -10706 -10707 -10708 -10709 -10710  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10711:10720.xsdst ! RUN =  -10711 -10712 -10713 -10714 -10715 -10716 -10717 -10718 -10719 -10720  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10721:10730.xsdst ! RUN =  -10721 -10722 -10723 -10724 -10725 -10726 -10727 -10728 -10729 -10730  ! NEVT =   45349
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10731:10740.xsdst ! RUN =  -10731 -10732 -10733 -10734 -10735 -10736 -10737 -10738 -10739 -10740  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10741:10750.xsdst ! RUN =  -10741 -10742 -10743 -10744 -10745 -10746 -10747 -10748 -10749 -10750  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10751:10760.xsdst ! RUN =  -10751 -10752 -10753 -10754 -10755 -10756 -10757 -10758 -10759 -10760  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10761:10770.xsdst ! RUN =  -10761 -10762 -10763 -10764 -10765 -10766 -10767 -10768 -10769 -10770  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10771:10780.xsdst ! RUN =  -10771 -10772 -10773 -10774 -10775 -10776 -10777 -10778 -10779 -10780  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10781:10790.xsdst ! RUN =  -10781 -10782 -10783 -10784 -10785 -10786 -10787 -10788 -10789 -10790  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10791:10800.xsdst ! RUN =  -10791 -10792 -10793 -10794 -10795 -10796 -10797 -10798 -10799 -10800  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10801:10811.xsdst ! RUN =  -10801 -10802 -10803 -10804 -10806 -10807 -10808 -10809 -10810 -10811  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10812:10822.xsdst ! RUN =  -10812 -10813 -10815 -10816 -10817 -10818 -10819 -10820 -10821 -10822  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10823:10833.xsdst ! RUN =  -10823 -10824 -10825 -10827 -10828 -10829 -10830 -10831 -10832 -10833  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10834:10843.xsdst ! RUN =  -10834 -10835 -10836 -10837 -10838 -10839 -10840 -10841 -10842 -10843  ! NEVT =   48990
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10844:10853.xsdst ! RUN =  -10844 -10845 -10846 -10847 -10848 -10849 -10850 -10851 -10852 -10853  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10854:10863.xsdst ! RUN =  -10854 -10855 -10856 -10857 -10858 -10859 -10860 -10861 -10862 -10863  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10864:10873.xsdst ! RUN =  -10864 -10865 -10866 -10867 -10868 -10869 -10870 -10871 -10872 -10873  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10874:10883.xsdst ! RUN =  -10874 -10875 -10876 -10877 -10878 -10879 -10880 -10881 -10882 -10883  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10884:10893.xsdst ! RUN =  -10884 -10885 -10886 -10887 -10888 -10889 -10890 -10891 -10892 -10893  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10894:10903.xsdst ! RUN =  -10894 -10895 -10896 -10897 -10898 -10899 -10900 -10901 -10902 -10903  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10904:10913.xsdst ! RUN =  -10904 -10905 -10906 -10907 -10908 -10909 -10910 -10911 -10912 -10913  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10914:10923.xsdst ! RUN =  -10914 -10915 -10916 -10917 -10918 -10919 -10920 -10921 -10922 -10923  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10924:10933.xsdst ! RUN =  -10924 -10925 -10926 -10927 -10928 -10929 -10930 -10931 -10932 -10933  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10934:10943.xsdst ! RUN =  -10934 -10935 -10936 -10937 -10938 -10939 -10940 -10941 -10942 -10943  ! NEVT =   49270
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10944:10953.xsdst ! RUN =  -10944 -10945 -10946 -10947 -10948 -10949 -10950 -10951 -10952 -10953  ! NEVT =   47245
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10954:10963.xsdst ! RUN =  -10954 -10955 -10956 -10957 -10958 -10959 -10960 -10961 -10962 -10963  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10964:10973.xsdst ! RUN =  -10964 -10965 -10966 -10967 -10968 -10969 -10970 -10971 -10972 -10973  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10974:10983.xsdst ! RUN =  -10974 -10975 -10976 -10977 -10978 -10979 -10980 -10981 -10982 -10983  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10984:10993.xsdst ! RUN =  -10984 -10985 -10986 -10987 -10988 -10989 -10990 -10991 -10992 -10993  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_10994:11003.xsdst ! RUN =  -10994 -10995 -10996 -10997 -10998 -10999 -11000 -11001 -11002 -11003  ! NEVT =   48140
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11004:11013.xsdst ! RUN =  -11004 -11005 -11006 -11007 -11008 -11009 -11010 -11011 -11012 -11013  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11014:11023.xsdst ! RUN =  -11014 -11015 -11016 -11017 -11018 -11019 -11020 -11021 -11022 -11023  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11024:11033.xsdst ! RUN =  -11024 -11025 -11026 -11027 -11028 -11029 -11030 -11031 -11032 -11033  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11034:11043.xsdst ! RUN =  -11034 -11035 -11036 -11037 -11038 -11039 -11040 -11041 -11042 -11043  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11044:11054.xsdst ! RUN =  -11044 -11045 -11046 -11047 -11048 -11049 -11051 -11052 -11053 -11054  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11055:11064.xsdst ! RUN =  -11055 -11056 -11057 -11058 -11059 -11060 -11061 -11062 -11063 -11064  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11065:11076.xsdst ! RUN =  -11065 -11066 -11067 -11068 -11069 -11070 -11071 -11072 -11073 -11076  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11077:11086.xsdst ! RUN =  -11077 -11078 -11079 -11080 -11081 -11082 -11083 -11084 -11085 -11086  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11090:11101.xsdst ! RUN =  -11090 -11091 -11092 -11093 -11094 -11095 -11096 -11098 -11100 -11101  ! NEVT =   46308
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11102:11111.xsdst ! RUN =  -11102 -11103 -11104 -11105 -11106 -11107 -11108 -11109 -11110 -11111  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11112:11121.xsdst ! RUN =  -11112 -11113 -11114 -11115 -11116 -11117 -11118 -11119 -11120 -11121  ! NEVT =   46650
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11122:11131.xsdst ! RUN =  -11122 -11123 -11124 -11125 -11126 -11127 -11128 -11129 -11130 -11131  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11132:11141.xsdst ! RUN =  -11132 -11133 -11134 -11135 -11136 -11137 -11138 -11139 -11140 -11141  ! NEVT =   50000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11142:11151.xsdst ! RUN =  -11142 -11143 -11144 -11145 -11146 -11147 -11148 -11149 -11150 -11151  ! NEVT =   49999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bdk02/va0e/206.5/x2/bdk02_eeee_206.5_11152:11161.xsdst ! RUN =  -11152 -11153 -11154 -11155 -11156 -11157 -11158 -11159 -11160 -11161  ! NEVT =   50000
