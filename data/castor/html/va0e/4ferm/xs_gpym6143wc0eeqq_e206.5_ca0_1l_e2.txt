*
*   Nickname     : xs_gpym6143wc0eeqq_e206.5_ca0_1l_e2
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/GPYM6143WC0EEQQ/E206.5/CERN/SUMT/C001-92
*   Description  :  Extended Short DST simulation a0_e2 done at ecms=206.5 , CERN
*---
*   Comments     : in total 7049740 events in 92 files time stamp: Mon Jul  8 22:11:33 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11001:11011.xsdst ! RUN =-11001 -11002 -11004 -11005 -11006 -11007 -11008 -11009 -11010 -11011 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11012:11028.xsdst ! RUN =-11012 -11013 -11014 -11015 -11016 -11018 -11019 -11020 -11025 -11028 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11030:11039.xsdst ! RUN =-11030 -11031 -11032 -11033 -11034 -11035 -11036 -11037 -11038 -11039 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11040:11049.xsdst ! RUN =-11040 -11041 -11042 -11043 -11044 -11045 -11046 -11047 -11048 -11049 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11050:11059.xsdst ! RUN =-11050 -11051 -11052 -11053 -11054 -11055 -11056 -11057 -11058 -11059 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11060:11069.xsdst ! RUN =-11060 -11061 -11062 -11063 -11064 -11065 -11066 -11067 -11068 -11069 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11070:11079.xsdst ! RUN =-11070 -11071 -11072 -11073 -11074 -11075 -11076 -11077 -11078 -11079 ! NEVT = 69220
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11080:11089.xsdst ! RUN =-11080 -11081 -11082 -11083 -11084 -11085 -11086 -11087 -11088 -11089 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11090:11099.xsdst ! RUN =-11090 -11091 -11092 -11093 -11094 -11095 -11096 -11097 -11098 -11099 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11100:11109.xsdst ! RUN =-11100 -11101 -11102 -11103 -11104 -11105 -11106 -11107 -11108 -11109 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11110:11120.xsdst ! RUN =-11110 -11112 -11113 -11114 -11115 -11116 -11117 -1111800 ! NEVT = 52996
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11121:11130.xsdst ! RUN =-11121 -11122 -11123 -11124 -11125 -11126 -11127 -11128 -11129 -11130 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11131:11140.xsdst ! RUN =-11131 -11132 -11133 -11134 -11135 -11136 -11137 -11138 -11139 -11140 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11141:11151.xsdst ! RUN =-11141 -11142 -11143 -11144 -11145 -11146 -11148 -11149 -11150 -11151 ! NEVT = 75500
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11152:11161.xsdst ! RUN =-11152 -11153 -11154 -11155 -11156 -11157 -11158 -11159 -11160 -11161 ! NEVT = 77422
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11162:11171.xsdst ! RUN =-11162 -11163 -11164 -11165 -11166 -11167 -11168 -11169 -11170 -11171 ! NEVT = 77514
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11172:11181.xsdst ! RUN =-11172 -11173 -11174 -11175 -11176 -11177 -11178 -11179 -11180 -11181 ! NEVT = 77774
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11182:11191.xsdst ! RUN =-11182 -11183 -11184 -11185 -11186 -11187 -11188 -11189 -11190 -11191 ! NEVT = 77583
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11192:11201.xsdst ! RUN =-11192 -11193 -11194 -11195 -11196 -11197 -11198 -11199 -11200 -11201 ! NEVT = 77676
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11202:11211.xsdst ! RUN =-11202 -11203 -11204 -11205 -11206 -11207 -11208 -11209 -11210 -11211 ! NEVT = 77543
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11212:11221.xsdst ! RUN =-11212 -11213 -11214 -11215 -11216 -11217 -11218 -11219 -11220 -11221 ! NEVT = 77619
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11222:11231.xsdst ! RUN =-11222 -11223 -11224 -11225 -11226 -11227 -11228 -11229 -11230 -11231 ! NEVT = 77495
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11232:11241.xsdst ! RUN =-11232 -11233 -11234 -11235 -11236 -11237 -11238 -11239 -11240 -11241 ! NEVT = 77533
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11242:11251.xsdst ! RUN =-11242 -11243 -11244 -11245 -11246 -11247 -11248 -11249 -11250 -11251 ! NEVT = 77328
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11252:11261.xsdst ! RUN =-11252 -11253 -11254 -11255 -11256 -11257 -11258 -11259 -11260 -11261 ! NEVT = 77481
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11262:11271.xsdst ! RUN =-11262 -11263 -11264 -11265 -11266 -11267 -11268 -11269 -11270 -11271 ! NEVT = 77432
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11272:11281.xsdst ! RUN =-11272 -11273 -11274 -11275 -11276 -11277 -11278 -11279 -11280 -11281 ! NEVT = 77398
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11282:11291.xsdst ! RUN =-11282 -11283 -11284 -11285 -11286 -11287 -11288 -11289 -11290 -11291 ! NEVT = 77601
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11292:11301.xsdst ! RUN =-11292 -11293 -11294 -11295 -11296 -11297 -11298 -11299 -11300 -11301 ! NEVT = 77662
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11302:11311.xsdst ! RUN =-11302 -11303 -11304 -11305 -11306 -11307 -11308 -11309 -11310 -11311 ! NEVT = 77430
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11312:11323.xsdst ! RUN =-11312 -11314 -11315 -11316 -11317 -11318 -11319 -11321 -11322 -11323 ! NEVT = 77446
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11324:11333.xsdst ! RUN =-11324 -11325 -11326 -11327 -11328 -11329 -11330 -11331 -11332 -11333 ! NEVT = 77524
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11334:11343.xsdst ! RUN =-11334 -11335 -11336 -11337 -11338 -11339 -11340 -11341 -11342 -11343 ! NEVT = 77473
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11344:11353.xsdst ! RUN =-11344 -11345 -11346 -11347 -11348 -11349 -11350 -11351 -11352 -11353 ! NEVT = 77820
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11354:11363.xsdst ! RUN =-11354 -11355 -11356 -11357 -11358 -11359 -11360 -11361 -11362 -11363 ! NEVT = 77587
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11364:11373.xsdst ! RUN =-11364 -11365 -11366 -11367 -11368 -11369 -11370 -11371 -11372 -11373 ! NEVT = 72374
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11374:11383.xsdst ! RUN =-11374 -11375 -11376 -11377 -11378 -11379 -11380 -11381 -11382 -11383 ! NEVT = 77646
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11384:11393.xsdst ! RUN =-11384 -11385 -11386 -11387 -11388 -11389 -11390 -11391 -11392 -11393 ! NEVT = 77743
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11394:11403.xsdst ! RUN =-11394 -11395 -11396 -11397 -11398 -11399 -11400 -11401 -11402 -11403 ! NEVT = 77519
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11404:11413.xsdst ! RUN =-11404 -11405 -11406 -11407 -11408 -11409 -11410 -11411 -11412 -11413 ! NEVT = 77804
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11414:11423.xsdst ! RUN =-11414 -11415 -11416 -11417 -11418 -11419 -11420 -11421 -11422 -11423 ! NEVT = 77834
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11424:11433.xsdst ! RUN =-11424 -11425 -11426 -11427 -11428 -11429 -11430 -11431 -11432 -11433 ! NEVT = 77720
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11434:11443.xsdst ! RUN =-11434 -11435 -11436 -11437 -11438 -11439 -11440 -11441 -11442 -11443 ! NEVT = 77596
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11444:11454.xsdst ! RUN =-11444 -11445 -11446 -11447 -11448 -11449 -11451 -11452 -11453 -11454 ! NEVT = 77680
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11455:11464.xsdst ! RUN =-11455 -11456 -11457 -11458 -11459 -11460 -11461 -11462 -11463 -11464 ! NEVT = 77828
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11465:11475.xsdst ! RUN =-11465 -11466 -11467 -11468 -11469 -11470 -11472 -11473 -11474 -11475 ! NEVT = 76870
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11476:11485.xsdst ! RUN =-11476 -11477 -11478 -11479 -11480 -11481 -11482 -11483 -11484 -11485 ! NEVT = 75762
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11486:11495.xsdst ! RUN =-11486 -11487 -11488 -11489 -11490 -11491 -11492 -11493 -11494 -11495 ! NEVT = 77712
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11496:11505.xsdst ! RUN =-11496 -11497 -11498 -11499 -11500 -11501 -11502 -11503 -11504 -11505 ! NEVT = 77753
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11506:11515.xsdst ! RUN =-11506 -11507 -11508 -11509 -11510 -11511 -11512 -11513 -11514 -11515 ! NEVT = 77891
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11516:11525.xsdst ! RUN =-11516 -11517 -11518 -11519 -11520 -11521 -11522 -11523 -11524 -11525 ! NEVT = 75692
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11526:11535.xsdst ! RUN =-11526 -11527 -11528 -11529 -11530 -11531 -11532 -11533 -11534 -11535 ! NEVT = 77542
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11536:11545.xsdst ! RUN =-11536 -11537 -11538 -11539 -11540 -11541 -11542 -11543 -11544 -11545 ! NEVT = 77806
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11546:11555.xsdst ! RUN =-11546 -11547 -11548 -11549 -11550 -11551 -11552 -11553 -11554 -11555 ! NEVT = 77783
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11556:11565.xsdst ! RUN =-11556 -11557 -11558 -11559 -11560 -11561 -11562 -11563 -11564 -11565 ! NEVT = 77685
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11566:11575.xsdst ! RUN =-11566 -11567 -11568 -11569 -11570 -11571 -11572 -11573 -11574 -11575 ! NEVT = 77720
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11576:11585.xsdst ! RUN =-11576 -11577 -11578 -11579 -11580 -11581 -11582 -11583 -11584 -11585 ! NEVT = 77534
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11586:11595.xsdst ! RUN =-11586 -11587 -11588 -11589 -11590 -11591 -11592 -11593 -11594 -11595 ! NEVT = 77831
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11596:11605.xsdst ! RUN =-11596 -11597 -11598 -11599 -11600 -11601 -11602 -11603 -11604 -11605 ! NEVT = 77696
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11606:11615.xsdst ! RUN =-11606 -11607 -11608 -11609 -11610 -11611 -11612 -11613 -11614 -11615 ! NEVT = 77687
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11616:11625.xsdst ! RUN =-11616 -11617 -11618 -11619 -11620 -11621 -11622 -11623 -11624 -11625 ! NEVT = 77779
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11626:11635.xsdst ! RUN =-11626 -11627 -11628 -11629 -11630 -11631 -11632 -11633 -11634 -11635 ! NEVT = 77719
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11636:11645.xsdst ! RUN =-11636 -11637 -11638 -11639 -11640 -11641 -11642 -11643 -11644 -11645 ! NEVT = 77451
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11646:11655.xsdst ! RUN =-11646 -11647 -11648 -11649 -11650 -11651 -11652 -11653 -11654 -11655 ! NEVT = 77664
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11656:11665.xsdst ! RUN =-11656 -11657 -11658 -11659 -11660 -11661 -11662 -11663 -11664 -11665 ! NEVT = 77737
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11666:11675.xsdst ! RUN =-11666 -11667 -11668 -11669 -11670 -11671 -11672 -11673 -11674 -11675 ! NEVT = 78001
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11676:11685.xsdst ! RUN =-11676 -11677 -11678 -11679 -11680 -11681 -11682 -11683 -11684 -11685 ! NEVT = 77864
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11686:11696.xsdst ! RUN =-11686 -11687 -11688 -11689 -11690 -11691 -11693 -11694 -11695 -11696 ! NEVT = 77766
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11697:11706.xsdst ! RUN =-11697 -11698 -11699 -11700 -11701 -11702 -11703 -11704 -11705 -11706 ! NEVT = 77641
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11707:11716.xsdst ! RUN =-11707 -11708 -11709 -11710 -11711 -11712 -11713 -11714 -11715 -11716 ! NEVT = 77630
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11718:11727.xsdst ! RUN =-11718 -11719 -11720 -11721 -11722 -11723 -11724 -11725 -11726 -11727 ! NEVT = 77760
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11728:11737.xsdst ! RUN =-11728 -11729 -11730 -11731 -11732 -11733 -11734 -11735 -11736 -11737 ! NEVT = 76812
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11738:11747.xsdst ! RUN =-11738 -11739 -11740 -11741 -11742 -11743 -11744 -11745 -11746 -11747 ! NEVT = 76816
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11748:11757.xsdst ! RUN =-11748 -11749 -11750 -11751 -11752 -11753 -11754 -11755 -11756 -11757 ! NEVT = 77867
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11758:11769.xsdst ! RUN =-11758 -11761 -11762 -11763 -11764 -11765 -11766 -11767 -11768 -11769 ! NEVT = 77688
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11770:11780.xsdst ! RUN =-11770 -11771 -11772 -11773 -11774 -11775 -11777 -11778 -11779 -11780 ! NEVT = 77699
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11781:11790.xsdst ! RUN =-11781 -11782 -11783 -11784 -11785 -11786 -11787 -11788 -11789 -11790 ! NEVT = 77620
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11791:11803.xsdst ! RUN =-11791 -11792 -11793 -11796 -11797 -11798 -11799 -11800 -11801 -11803 ! NEVT = 77712
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11804:11815.xsdst ! RUN =-11804 -11805 -11806 -11807 -11808 -11810 -11811 -11812 -11814 -11815 ! NEVT = 77742
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11816:11825.xsdst ! RUN =-11816 -11817 -11818 -11819 -11820 -11821 -11822 -11823 -11824 -11825 ! NEVT = 77743
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11826:11837.xsdst ! RUN =-11826 -11827 -11828 -11829 -11830 -11831 -11833 -11835 -11836 -11837 ! NEVT = 77743
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11838:11848.xsdst ! RUN =-11838 -11839 -11840 -11842 -11843 -11844 -11845 -11846 -11847 -11848 ! NEVT = 77996
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11849:11858.xsdst ! RUN =-11849 -11850 -11851 -11852 -11853 -11854 -11855 -11856 -11857 -11858 ! NEVT = 77899
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11859:11868.xsdst ! RUN =-11859 -11860 -11861 -11862 -11863 -11864 -11865 -11866 -11867 -11868 ! NEVT = 77642
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11869:11878.xsdst ! RUN =-11869 -11870 -11871 -11872 -11873 -11874 -11875 -11876 -11877 -11878 ! NEVT = 77454
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11880:11889.xsdst ! RUN =-11880 -11881 -11882 -11883 -11884 -11885 -11886 -11887 -11888 -11889 ! NEVT = 77695
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11891:11900.xsdst ! RUN =-11891 -11892 -11893 -11894 -11895 -11896 -11897 -11898 -11899 -11900 ! NEVT = 77797
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11901:11911.xsdst ! RUN = 11901 11911 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11912:11922.xsdst ! RUN = 11912 11922 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11923:11932.xsdst ! RUN = 11923 11932 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11933:11943.xsdst ! RUN = 11933 11943 ! NEVT = 75000
FILE = /castor/cern.ch/delphi/MCprod/cern/gpym6143wc0/va0e/206.5/x2/gpym6143wc0_eeqq_206.5_11944:11955.xsdst ! RUN = 11944 11955 ! NEVT = 69038
