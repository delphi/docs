*
*   Nickname     : xs_wphact21tgccqqqq_e206.5_m9_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQQQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9997 events in 10 files time stamp: Sat Feb  2 01:15:05 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32001.xsdst ! RUN = 32001 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32002.xsdst ! RUN = 32002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32003.xsdst ! RUN = 32003 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32004.xsdst ! RUN = 32004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32005.xsdst ! RUN = 32005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32006.xsdst ! RUN = 32006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32007.xsdst ! RUN = 32007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32008.xsdst ! RUN = 32008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32009.xsdst ! RUN = 32009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_9_32010.xsdst ! RUN = 32010 ! NEVT = 1000
