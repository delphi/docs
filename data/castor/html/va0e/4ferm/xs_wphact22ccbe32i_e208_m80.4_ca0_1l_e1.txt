*
*   Nickname     : xs_wphact22ccbe32i_e208_m80.4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32I/CERN/SUMT/C001-25
*   Description  :  Extended Short DST simulation a0e1 208 , CERN
*---
*   Comments     : in total 49977 events in 25 files time stamp: Wed Mar  6 12:16:13 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100201.xsdst ! RUN = 100201 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100202.xsdst ! RUN = 100202 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100203.xsdst ! RUN = 100203 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100204.xsdst ! RUN = 100204 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100205.xsdst ! RUN = 100205 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100206.xsdst ! RUN = 100206 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100207.xsdst ! RUN = 100207 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100208.xsdst ! RUN = 100208 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100209.xsdst ! RUN = 100209 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100210.xsdst ! RUN = 100210 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100211.xsdst ! RUN = 100211 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100212.xsdst ! RUN = 100212 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100213.xsdst ! RUN = 100213 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100214.xsdst ! RUN = 100214 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100215.xsdst ! RUN = 100215 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100216.xsdst ! RUN = 100216 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100217.xsdst ! RUN = 100217 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100218.xsdst ! RUN = 100218 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100219.xsdst ! RUN = 100219 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100220.xsdst ! RUN = 100220 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100221.xsdst ! RUN = 100221 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100222.xsdst ! RUN = 100222 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100223.xsdst ! RUN = 100223 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100224.xsdst ! RUN = 100224 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32i_208_80.4_100225.xsdst ! RUN = 100225 ! NEVT = 1998
