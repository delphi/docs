*
*   Nickname     : xs_wphact21tgccqqev_e206.5_m9_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQEV/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9991 events in 10 files time stamp: Sat Feb  2 01:15:07 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32011.xsdst ! RUN = 32011 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32012.xsdst ! RUN = 32012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32013.xsdst ! RUN = 32013 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32014.xsdst ! RUN = 32014 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32015.xsdst ! RUN = 32015 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32016.xsdst ! RUN = 32016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32017.xsdst ! RUN = 32017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32018.xsdst ! RUN = 32018 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32019.xsdst ! RUN = 32019 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_9_32020.xsdst ! RUN = 32020 ! NEVT = 998
