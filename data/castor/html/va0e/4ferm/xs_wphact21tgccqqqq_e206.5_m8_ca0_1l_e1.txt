*
*   Nickname     : xs_wphact21tgccqqqq_e206.5_m8_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQQQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9996 events in 10 files time stamp: Sat Feb  2 01:15:05 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31001.xsdst ! RUN = 31001 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31002.xsdst ! RUN = 31002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31003.xsdst ! RUN = 31003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31004.xsdst ! RUN = 31004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31005.xsdst ! RUN = 31005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31006.xsdst ! RUN = 31006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31007.xsdst ! RUN = 31007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31008.xsdst ! RUN = 31008 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31009.xsdst ! RUN = 31009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_8_31010.xsdst ! RUN = 31010 ! NEVT = 999
