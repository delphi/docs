*
*   Nickname     : xs_wphact21tgccqqev_e206.5_m7_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQEV/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9992 events in 10 files time stamp: Sun Feb 24 15:14:36 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30011.xsdst ! RUN = 30011 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30012.xsdst ! RUN = 30012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30013.xsdst ! RUN = 30013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30014.xsdst ! RUN = 30014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30015.xsdst ! RUN = 30015 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30016.xsdst ! RUN = 30016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30017.xsdst ! RUN = 30017 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30018.xsdst ! RUN = 30018 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30019.xsdst ! RUN = 30019 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqev_206.5_7_30020.xsdst ! RUN = 30020 ! NEVT = 1000
