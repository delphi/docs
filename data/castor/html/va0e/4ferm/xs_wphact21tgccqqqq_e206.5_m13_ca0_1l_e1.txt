*
*   Nickname     : xs_wphact21tgccqqqq_e206.5_m13_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQQQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9997 events in 10 files time stamp: Sun Feb  3 06:28:39 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36001.xsdst ! RUN = 36001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36002.xsdst ! RUN = 36002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36003.xsdst ! RUN = 36003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36004.xsdst ! RUN = 36004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36005.xsdst ! RUN = 36005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36006.xsdst ! RUN = 36006 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36007.xsdst ! RUN = 36007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36008.xsdst ! RUN = 36008 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36009.xsdst ! RUN = 36009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_13_36010.xsdst ! RUN = 36010 ! NEVT = 1000
