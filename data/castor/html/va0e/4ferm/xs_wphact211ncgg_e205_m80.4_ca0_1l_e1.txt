*
*   Nickname     : xs_wphact211ncgg_e205_m80.4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT211NCGG/CERN/SUMT/C001-123
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 1009977 events in 123 files time stamp: Mon Jan  7 11:16:07 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11001.xsdst ! RUN = 11001 ! NEVT = 8364
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11002.xsdst ! RUN = 11002 ! NEVT = 8248
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11003.xsdst ! RUN = 11003 ! NEVT = 8424
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11004.xsdst ! RUN = 11004 ! NEVT = 4429
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11006.xsdst ! RUN = 11006 ! NEVT = 8171
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11007.xsdst ! RUN = 11007 ! NEVT = 8407
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11009.xsdst ! RUN = 11009 ! NEVT = 8324
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11010.xsdst ! RUN = 11010 ! NEVT = 8398
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11011.xsdst ! RUN = 11011 ! NEVT = 8376
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11012.xsdst ! RUN = 11012 ! NEVT = 8335
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11013.xsdst ! RUN = 11013 ! NEVT = 8225
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11014.xsdst ! RUN = 11014 ! NEVT = 8299
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11015.xsdst ! RUN = 11015 ! NEVT = 8335
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11016.xsdst ! RUN = 11016 ! NEVT = 8411
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11017.xsdst ! RUN = 11017 ! NEVT = 8352
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11018.xsdst ! RUN = 11018 ! NEVT = 8385
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11019.xsdst ! RUN = 11019 ! NEVT = 8431
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11020.xsdst ! RUN = 11020 ! NEVT = 8336
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11021.xsdst ! RUN = 11021 ! NEVT = 8315
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11022.xsdst ! RUN = 11022 ! NEVT = 8221
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11023.xsdst ! RUN = 11023 ! NEVT = 8417
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11024.xsdst ! RUN = 11024 ! NEVT = 8225
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11025.xsdst ! RUN = 11025 ! NEVT = 8354
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11026.xsdst ! RUN = 11026 ! NEVT = 2492
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11027.xsdst ! RUN = 11027 ! NEVT = 8465
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11028.xsdst ! RUN = 11028 ! NEVT = 8278
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11029.xsdst ! RUN = 11029 ! NEVT = 8293
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11030.xsdst ! RUN = 11030 ! NEVT = 8305
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11031.xsdst ! RUN = 11031 ! NEVT = 8359
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11032.xsdst ! RUN = 11032 ! NEVT = 2561
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11033.xsdst ! RUN = 11033 ! NEVT = 8337
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11034.xsdst ! RUN = 11034 ! NEVT = 8328
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11035.xsdst ! RUN = 11035 ! NEVT = 8376
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11036.xsdst ! RUN = 11036 ! NEVT = 8395
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11037.xsdst ! RUN = 11037 ! NEVT = 8298
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11038.xsdst ! RUN = 11038 ! NEVT = 8367
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11039.xsdst ! RUN = 11039 ! NEVT = 8258
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11041.xsdst ! RUN = 11041 ! NEVT = 8263
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11042.xsdst ! RUN = 11042 ! NEVT = 8264
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11043.xsdst ! RUN = 11043 ! NEVT = 8372
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11044.xsdst ! RUN = 11044 ! NEVT = 8354
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11045.xsdst ! RUN = 11045 ! NEVT = 8282
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11046.xsdst ! RUN = 11046 ! NEVT = 8184
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11047.xsdst ! RUN = 11047 ! NEVT = 8416
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11048.xsdst ! RUN = 11048 ! NEVT = 8399
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11049.xsdst ! RUN = 11049 ! NEVT = 8382
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11050.xsdst ! RUN = 11050 ! NEVT = 8250
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11051.xsdst ! RUN = 11051 ! NEVT = 8183
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11052.xsdst ! RUN = 11052 ! NEVT = 8400
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11053.xsdst ! RUN = 11053 ! NEVT = 8286
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11054.xsdst ! RUN = 11054 ! NEVT = 8448
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11055.xsdst ! RUN = 11055 ! NEVT = 8244
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11056.xsdst ! RUN = 11056 ! NEVT = 8334
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11057.xsdst ! RUN = 11057 ! NEVT = 8434
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11058.xsdst ! RUN = 11058 ! NEVT = 8324
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11059.xsdst ! RUN = 11059 ! NEVT = 8350
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11060.xsdst ! RUN = 11060 ! NEVT = 8336
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11061.xsdst ! RUN = 11061 ! NEVT = 8392
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11062.xsdst ! RUN = 11062 ! NEVT = 8426
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11063.xsdst ! RUN = 11063 ! NEVT = 8367
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11064.xsdst ! RUN = 11064 ! NEVT = 8262
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11066.xsdst ! RUN = 11066 ! NEVT = 8448
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11067.xsdst ! RUN = 11067 ! NEVT = 8293
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11068.xsdst ! RUN = 11068 ! NEVT = 8318
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11069.xsdst ! RUN = 11069 ! NEVT = 8365
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11070.xsdst ! RUN = 11070 ! NEVT = 8371
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11071.xsdst ! RUN = 11071 ! NEVT = 8419
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11072.xsdst ! RUN = 11072 ! NEVT = 8297
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11073.xsdst ! RUN = 11073 ! NEVT = 8334
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11074.xsdst ! RUN = 11074 ! NEVT = 8296
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11075.xsdst ! RUN = 11075 ! NEVT = 8419
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11076.xsdst ! RUN = 11076 ! NEVT = 8223
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11077.xsdst ! RUN = 11077 ! NEVT = 8327
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11078.xsdst ! RUN = 11078 ! NEVT = 8314
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11079.xsdst ! RUN = 11079 ! NEVT = 8297
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11080.xsdst ! RUN = 11080 ! NEVT = 8224
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11081.xsdst ! RUN = 11081 ! NEVT = 8275
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11082.xsdst ! RUN = 11082 ! NEVT = 8397
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11083.xsdst ! RUN = 11083 ! NEVT = 8358
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11084.xsdst ! RUN = 11084 ! NEVT = 8349
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11085.xsdst ! RUN = 11085 ! NEVT = 8325
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11086.xsdst ! RUN = 11086 ! NEVT = 8314
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11088.xsdst ! RUN = 11088 ! NEVT = 8387
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11089.xsdst ! RUN = 11089 ! NEVT = 8410
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11090.xsdst ! RUN = 11090 ! NEVT = 8207
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11092.xsdst ! RUN = 11092 ! NEVT = 8404
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11093.xsdst ! RUN = 11093 ! NEVT = 8378
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11094.xsdst ! RUN = 11094 ! NEVT = 8288
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11095.xsdst ! RUN = 11095 ! NEVT = 8380
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11096.xsdst ! RUN = 11096 ! NEVT = 8338
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11097.xsdst ! RUN = 11097 ! NEVT = 8316
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11098.xsdst ! RUN = 11098 ! NEVT = 8406
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11099.xsdst ! RUN = 11099 ! NEVT = 8396
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11100.xsdst ! RUN = 11100 ! NEVT = 8400
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11101.xsdst ! RUN = 11101 ! NEVT = 8296
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11102.xsdst ! RUN = 11102 ! NEVT = 8281
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11103.xsdst ! RUN = 11103 ! NEVT = 8388
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11104.xsdst ! RUN = 11104 ! NEVT = 8372
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11105.xsdst ! RUN = 11105 ! NEVT = 8268
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11106.xsdst ! RUN = 11106 ! NEVT = 8406
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11107.xsdst ! RUN = 11107 ! NEVT = 8468
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11108.xsdst ! RUN = 11108 ! NEVT = 8264
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11109.xsdst ! RUN = 11109 ! NEVT = 8381
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11110.xsdst ! RUN = 11110 ! NEVT = 8375
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11111.xsdst ! RUN = 11111 ! NEVT = 8306
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11112.xsdst ! RUN = 11112 ! NEVT = 8365
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11113.xsdst ! RUN = 11113 ! NEVT = 8334
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11114.xsdst ! RUN = 11114 ! NEVT = 8354
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11115.xsdst ! RUN = 11115 ! NEVT = 8305
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11116.xsdst ! RUN = 11116 ! NEVT = 8269
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11117.xsdst ! RUN = 11117 ! NEVT = 8352
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11118.xsdst ! RUN = 11118 ! NEVT = 8351
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11119.xsdst ! RUN = 11119 ! NEVT = 8306
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11120.xsdst ! RUN = 11120 ! NEVT = 8325
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11121.xsdst ! RUN = 11121 ! NEVT = 8250
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11122.xsdst ! RUN = 11122 ! NEVT = 8319
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11123.xsdst ! RUN = 11123 ! NEVT = 8293
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11124.xsdst ! RUN = 11124 ! NEVT = 8395
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11125.xsdst ! RUN = 11125 ! NEVT = 8330
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11126.xsdst ! RUN = 11126 ! NEVT = 8380
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11127.xsdst ! RUN = 11127 ! NEVT = 8390
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11128.xsdst ! RUN = 11128 ! NEVT = 8438
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/205/wphact211_ncgg_205_80.4_11129.xsdst ! RUN = 11129 ! NEVT = 8297
