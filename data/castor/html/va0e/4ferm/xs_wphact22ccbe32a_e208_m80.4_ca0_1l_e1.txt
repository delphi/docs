*
*   Nickname     : xs_wphact22ccbe32a_e208_m80.4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32A/CERN/SUMT/C001-50
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 99282 events in 50 files time stamp: Mon Mar  4 15:13:45 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100001.xsdst ! RUN = 100001 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100002.xsdst ! RUN = 100002 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100003.xsdst ! RUN = 100003 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100004.xsdst ! RUN = 100004 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100005.xsdst ! RUN = 100005 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100006.xsdst ! RUN = 100006 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100007.xsdst ! RUN = 100007 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100008.xsdst ! RUN = 100008 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100009.xsdst ! RUN = 100009 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100010.xsdst ! RUN = 100010 ! NEVT = 1996
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100011.xsdst ! RUN = 100011 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100012.xsdst ! RUN = 100012 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100013.xsdst ! RUN = 100013 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100014.xsdst ! RUN = 100014 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100015.xsdst ! RUN = 100015 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100016.xsdst ! RUN = 100016 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100017.xsdst ! RUN = 100017 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100018.xsdst ! RUN = 100018 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100019.xsdst ! RUN = 100019 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100020.xsdst ! RUN = 100020 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100021.xsdst ! RUN = 100021 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100022.xsdst ! RUN = 100022 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100023.xsdst ! RUN = 100023 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100024.xsdst ! RUN = 100024 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100025.xsdst ! RUN = 100025 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100101.xsdst ! RUN = 100101 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100102.xsdst ! RUN = 100102 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100103.xsdst ! RUN = 100103 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100104.xsdst ! RUN = 100104 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100105.xsdst ! RUN = 100105 ! NEVT = 1327
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100106.xsdst ! RUN = 100106 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100107.xsdst ! RUN = 100107 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100108.xsdst ! RUN = 100108 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100109.xsdst ! RUN = 100109 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100110.xsdst ! RUN = 100110 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100111.xsdst ! RUN = 100111 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100112.xsdst ! RUN = 100112 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100113.xsdst ! RUN = 100113 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100114.xsdst ! RUN = 100114 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100115.xsdst ! RUN = 100115 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100116.xsdst ! RUN = 100116 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100117.xsdst ! RUN = 100117 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100118.xsdst ! RUN = 100118 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100119.xsdst ! RUN = 100119 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100120.xsdst ! RUN = 100120 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100121.xsdst ! RUN = 100121 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100122.xsdst ! RUN = 100122 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100123.xsdst ! RUN = 100123 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100124.xsdst ! RUN = 100124 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/208/wphact22_ccbe32a_208_80.4_100125.xsdst ! RUN = 100125 ! NEVT = 2000
