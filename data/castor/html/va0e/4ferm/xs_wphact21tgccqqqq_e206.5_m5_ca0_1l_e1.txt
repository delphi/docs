*
*   Nickname     : xs_wphact21tgccqqqq_e206.5_m5_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQQQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9996 events in 10 files time stamp: Wed Jan 30 10:17:33 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28001.xsdst ! RUN = 28001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28002.xsdst ! RUN = 28002 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28003.xsdst ! RUN = 28003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28004.xsdst ! RUN = 28004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28005.xsdst ! RUN = 28005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28006.xsdst ! RUN = 28006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28007.xsdst ! RUN = 28007 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28008.xsdst ! RUN = 28008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28009.xsdst ! RUN = 28009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqqq_206.5_5_28010.xsdst ! RUN = 28010 ! NEVT = 998
