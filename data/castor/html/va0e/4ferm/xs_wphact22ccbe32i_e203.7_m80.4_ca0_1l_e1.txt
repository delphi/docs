*
*   Nickname     : xs_wphact22ccbe32i_e203.7_m80.4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32I/E203.7/CERN/SUMT/C001-24
*   Description  :  Extended Short DST simulation a0e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 47973 events in 24 files time stamp: Wed Mar  6 22:15:19 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130201.xsdst ! RUN = 130201 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130202.xsdst ! RUN = 130202 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130203.xsdst ! RUN = 130203 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130204.xsdst ! RUN = 130204 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130205.xsdst ! RUN = 130205 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130206.xsdst ! RUN = 130206 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130207.xsdst ! RUN = 130207 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130208.xsdst ! RUN = 130208 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130210.xsdst ! RUN = 130210 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130211.xsdst ! RUN = 130211 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130212.xsdst ! RUN = 130212 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130213.xsdst ! RUN = 130213 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130214.xsdst ! RUN = 130214 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130215.xsdst ! RUN = 130215 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130216.xsdst ! RUN = 130216 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130217.xsdst ! RUN = 130217 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130218.xsdst ! RUN = 130218 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130219.xsdst ! RUN = 130219 ! NEVT = 1997
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130220.xsdst ! RUN = 130220 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130221.xsdst ! RUN = 130221 ! NEVT = 1996
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130222.xsdst ! RUN = 130222 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130223.xsdst ! RUN = 130223 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130224.xsdst ! RUN = 130224 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/va0e/203.7/wphact22_ccbe32i_203.7_80.4_130225.xsdst ! RUN = 130225 ! NEVT = 1998
