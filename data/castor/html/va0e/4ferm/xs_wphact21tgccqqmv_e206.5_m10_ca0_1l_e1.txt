*
*   Nickname     : xs_wphact21tgccqqmv_e206.5_m10_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT21TGCCQQMV/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9998 events in 10 files time stamp: Sat Feb  2 01:15:06 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33021.xsdst ! RUN = 33021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33022.xsdst ! RUN = 33022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33023.xsdst ! RUN = 33023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33024.xsdst ! RUN = 33024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33025.xsdst ! RUN = 33025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33026.xsdst ! RUN = 33026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33027.xsdst ! RUN = 33027 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33028.xsdst ! RUN = 33028 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33029.xsdst ! RUN = 33029 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact21tgcc/va0e/206.5/wphact21tgcc_qqmv_206.5_10_33030.xsdst ! RUN = 33030 ! NEVT = 1000
