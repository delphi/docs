*
*   Nickname     : xs_hzha03pyth6156hinvmu_e203.7_m90_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Sep 13 04:24:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_90_39440.xsdst ! RUN = 39440 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_90_39441.xsdst ! RUN = 39441 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_90_39442.xsdst ! RUN = 39442 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_90_39443.xsdst ! RUN = 39443 ! NEVT = 500
