*
*   Nickname     : xs_hzha03pyth6156hinvtt_e208_m45_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sun Sep  9 13:24:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_45_34760.xsdst ! RUN = 34760 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_45_34761.xsdst ! RUN = 34761 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_45_34762.xsdst ! RUN = 34762 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_45_34763.xsdst ! RUN = 34763 ! NEVT = 499
