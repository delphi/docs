*
*   Nickname     : xs_hzha03pyth6156hinvtt_e205_m112.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Mon Sep 10 22:23:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_112.5_41610.xsdst ! RUN = 41610 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_112.5_41611.xsdst ! RUN = 41611 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_112.5_41612.xsdst ! RUN = 41612 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_112.5_41613.xsdst ! RUN = 41613 ! NEVT = 500
