*
*   Nickname     : xs_hzha03pyth6156hinvqq_e208_m45_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4998 events in 10 files time stamp: Fri Sep  7 23:30:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34700.xsdst ! RUN = 34700 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34701.xsdst ! RUN = 34701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34702.xsdst ! RUN = 34702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34703.xsdst ! RUN = 34703 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34704.xsdst ! RUN = 34704 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34705.xsdst ! RUN = 34705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34706.xsdst ! RUN = 34706 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34707.xsdst ! RUN = 34707 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34708.xsdst ! RUN = 34708 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_45_34709.xsdst ! RUN = 34709 ! NEVT = 500
