*
*   Nickname     : xs_hzha03pyth6156hinvmu_e203.7_m107.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Sep 13 05:25:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_107.5_41190.xsdst ! RUN = 41190 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_107.5_41191.xsdst ! RUN = 41191 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_107.5_41192.xsdst ! RUN = 41192 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_107.5_41193.xsdst ! RUN = 41193 ! NEVT = 500
