*
*   Nickname     : xs_hzha03pyth6156hinvmu_e205_m100_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Wed Sep 12 15:25:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_100_40340.xsdst ! RUN = 40340 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_100_40341.xsdst ! RUN = 40341 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_100_40342.xsdst ! RUN = 40342 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_100_40343.xsdst ! RUN = 40343 ! NEVT = 500
