*
*   Nickname     : xs_hzha03pyth6156hinvee_e208_m112.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Sun Sep  9 07:26:27 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_112.5_41470.xsdst ! RUN = 41470 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_112.5_41471.xsdst ! RUN = 41471 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_112.5_41472.xsdst ! RUN = 41472 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_112.5_41473.xsdst ! RUN = 41473 ! NEVT = 499
