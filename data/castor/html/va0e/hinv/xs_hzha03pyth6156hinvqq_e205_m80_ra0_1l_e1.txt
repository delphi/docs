*
*   Nickname     : xs_hzha03pyth6156hinvqq_e205_m80_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Sep 10 09:27:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38300.xsdst ! RUN = 38300 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38301.xsdst ! RUN = 38301 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38302.xsdst ! RUN = 38302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38303.xsdst ! RUN = 38303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38304.xsdst ! RUN = 38304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38305.xsdst ! RUN = 38305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38306.xsdst ! RUN = 38306 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38307.xsdst ! RUN = 38307 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38308.xsdst ! RUN = 38308 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_80_38309.xsdst ! RUN = 38309 ! NEVT = 499
