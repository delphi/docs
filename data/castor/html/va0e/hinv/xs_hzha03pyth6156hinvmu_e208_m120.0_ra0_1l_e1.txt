*
*   Nickname     : xs_hzha03pyth6156hinvmu_e208_m120.0_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sun Sep  9 08:27:33 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvmu_208_120.0_42240.xsdst ! RUN = 42240 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvmu_208_120.0_42241.xsdst ! RUN = 42241 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvmu_208_120.0_42242.xsdst ! RUN = 42242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvmu_208_120.0_42243.xsdst ! RUN = 42243 ! NEVT = 500
