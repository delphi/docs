*
*   Nickname     : xs_hzha03pyth6156hinvqq_e203.7_m90_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E203.7/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 4828 events in 10 files time stamp: Thu Sep 13 00:28:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39400.xsdst ! RUN = 39400 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39401.xsdst ! RUN = 39401 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39402.xsdst ! RUN = 39402 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39403.xsdst ! RUN = 39403 ! NEVT = 330
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39404.xsdst ! RUN = 39404 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39405.xsdst ! RUN = 39405 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39406.xsdst ! RUN = 39406 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39407.xsdst ! RUN = 39407 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39408.xsdst ! RUN = 39408 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_90_39409.xsdst ! RUN = 39409 ! NEVT = 500
