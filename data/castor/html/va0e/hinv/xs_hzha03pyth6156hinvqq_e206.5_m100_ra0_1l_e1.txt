*
*   Nickname     : xs_hzha03pyth6156hinvqq_e206.5_m100_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E206.5/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Wed Sep  5 03:30:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40000.xsdst ! RUN = 40000 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40001.xsdst ! RUN = 40001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40002.xsdst ! RUN = 40002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40003.xsdst ! RUN = 40003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40004.xsdst ! RUN = 40004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40005.xsdst ! RUN = 40005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40006.xsdst ! RUN = 40006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40007.xsdst ! RUN = 40007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40008.xsdst ! RUN = 40008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvqq_206.5_100_40009.xsdst ! RUN = 40009 ! NEVT = 500
