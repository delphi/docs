*
*   Nickname     : xs_hzha03pyth6156hinvee_e206.5_m100_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E206.5/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Sep  5 09:30:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvee_206.5_100_40020.xsdst ! RUN = 40020 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvee_206.5_100_40021.xsdst ! RUN = 40021 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvee_206.5_100_40022.xsdst ! RUN = 40022 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hinvee_206.5_100_40023.xsdst ! RUN = 40023 ! NEVT = 499
