*
*   Nickname     : xs_hzha03pyth6156hinvqq_e205_m75_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Sep 10 11:23:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37800.xsdst ! RUN = 37800 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37801.xsdst ! RUN = 37801 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37802.xsdst ! RUN = 37802 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37803.xsdst ! RUN = 37803 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37804.xsdst ! RUN = 37804 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37805.xsdst ! RUN = 37805 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37806.xsdst ! RUN = 37806 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37807.xsdst ! RUN = 37807 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37808.xsdst ! RUN = 37808 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_75_37809.xsdst ! RUN = 37809 ! NEVT = 499
