*
*   Nickname     : xs_hzha03pyth6156hinvtt_e205_m107.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 10 22:23:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_107.5_41110.xsdst ! RUN = 41110 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_107.5_41111.xsdst ! RUN = 41111 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_107.5_41112.xsdst ! RUN = 41112 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvtt_205_107.5_41113.xsdst ! RUN = 41113 ! NEVT = 500
