*
*   Nickname     : xs_hzha03pyth6156hinvqq_e203.7_m110_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E203.7/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Thu Sep 13 02:24:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41400.xsdst ! RUN = 41400 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41401.xsdst ! RUN = 41401 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41402.xsdst ! RUN = 41402 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41403.xsdst ! RUN = 41403 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41404.xsdst ! RUN = 41404 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41405.xsdst ! RUN = 41405 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41406.xsdst ! RUN = 41406 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41407.xsdst ! RUN = 41407 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41408.xsdst ! RUN = 41408 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvqq_203.7_110_41409.xsdst ! RUN = 41409 ! NEVT = 500
