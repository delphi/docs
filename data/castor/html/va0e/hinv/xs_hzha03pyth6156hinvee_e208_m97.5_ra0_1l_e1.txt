*
*   Nickname     : xs_hzha03pyth6156hinvee_e208_m97.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 1996 events in 4 files time stamp: Sun Sep  9 06:27:14 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_97.5_39970.xsdst ! RUN = 39970 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_97.5_39971.xsdst ! RUN = 39971 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_97.5_39972.xsdst ! RUN = 39972 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_97.5_39973.xsdst ! RUN = 39973 ! NEVT = 500
