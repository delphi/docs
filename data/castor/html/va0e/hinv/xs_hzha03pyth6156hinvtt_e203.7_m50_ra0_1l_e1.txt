*
*   Nickname     : xs_hzha03pyth6156hinvtt_e203.7_m50_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Sep 13 05:24:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_50_35460.xsdst ! RUN = 35460 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_50_35461.xsdst ! RUN = 35461 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_50_35462.xsdst ! RUN = 35462 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_50_35463.xsdst ! RUN = 35463 ! NEVT = 500
