*
*   Nickname     : xs_hzha03pyth6156hinvee_e208_m75_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 1995 events in 4 files time stamp: Sun Sep  9 06:27:54 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_75_37720.xsdst ! RUN = 37720 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_75_37721.xsdst ! RUN = 37721 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_75_37722.xsdst ! RUN = 37722 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvee_208_75_37723.xsdst ! RUN = 37723 ! NEVT = 498
