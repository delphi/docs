*
*   Nickname     : xs_hzha03pyth6156hinvqq_e205_m45_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Mon Sep 10 08:25:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34800.xsdst ! RUN = 34800 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34801.xsdst ! RUN = 34801 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34802.xsdst ! RUN = 34802 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34803.xsdst ! RUN = 34803 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34804.xsdst ! RUN = 34804 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34805.xsdst ! RUN = 34805 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34806.xsdst ! RUN = 34806 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34807.xsdst ! RUN = 34807 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34808.xsdst ! RUN = 34808 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvqq_205_45_34809.xsdst ! RUN = 34809 ! NEVT = 500
