*
*   Nickname     : xs_hzha03pyth6156hinvmu_e203.7_m65_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Sep 13 04:22:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_65_36940.xsdst ! RUN = 36940 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_65_36941.xsdst ! RUN = 36941 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_65_36942.xsdst ! RUN = 36942 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_65_36943.xsdst ! RUN = 36943 ! NEVT = 500
