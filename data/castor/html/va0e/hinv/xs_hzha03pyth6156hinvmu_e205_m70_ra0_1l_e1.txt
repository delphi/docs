*
*   Nickname     : xs_hzha03pyth6156hinvmu_e205_m70_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 10 19:22:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_70_37340.xsdst ! RUN = 37340 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_70_37341.xsdst ! RUN = 37341 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_70_37342.xsdst ! RUN = 37342 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_70_37343.xsdst ! RUN = 37343 ! NEVT = 500
