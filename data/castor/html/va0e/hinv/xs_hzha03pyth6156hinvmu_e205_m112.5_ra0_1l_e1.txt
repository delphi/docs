*
*   Nickname     : xs_hzha03pyth6156hinvmu_e205_m112.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Mon Sep 10 20:29:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_112.5_41590.xsdst ! RUN = 41590 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_112.5_41591.xsdst ! RUN = 41591 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_112.5_41592.xsdst ! RUN = 41592 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_hinvmu_205_112.5_41593.xsdst ! RUN = 41593 ! NEVT = 500
