*
*   Nickname     : xs_hzha03pyth6156hinvqq_e208_m60_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4575 events in 10 files time stamp: Sat Sep  8 04:28:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36200.xsdst ! RUN = 36200 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36201.xsdst ! RUN = 36201 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36202.xsdst ! RUN = 36202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36203.xsdst ! RUN = 36203 ! NEVT = 76
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36204.xsdst ! RUN = 36204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36205.xsdst ! RUN = 36205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36206.xsdst ! RUN = 36206 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36207.xsdst ! RUN = 36207 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36208.xsdst ! RUN = 36208 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvqq_208_60_36209.xsdst ! RUN = 36209 ! NEVT = 500
