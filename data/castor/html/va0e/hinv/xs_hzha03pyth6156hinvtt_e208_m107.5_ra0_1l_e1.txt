*
*   Nickname     : xs_hzha03pyth6156hinvtt_e208_m107.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sun Sep  9 10:29:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_107.5_41010.xsdst ! RUN = 41010 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_107.5_41011.xsdst ! RUN = 41011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_107.5_41012.xsdst ! RUN = 41012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_107.5_41013.xsdst ! RUN = 41013 ! NEVT = 500
