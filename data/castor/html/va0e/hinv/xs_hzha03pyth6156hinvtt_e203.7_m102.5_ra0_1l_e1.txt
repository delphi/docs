*
*   Nickname     : xs_hzha03pyth6156hinvtt_e203.7_m102.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Thu Sep 13 06:23:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_102.5_40710.xsdst ! RUN = 40710 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_102.5_40711.xsdst ! RUN = 40711 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_102.5_40712.xsdst ! RUN = 40712 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvtt_203.7_102.5_40713.xsdst ! RUN = 40713 ! NEVT = 500
