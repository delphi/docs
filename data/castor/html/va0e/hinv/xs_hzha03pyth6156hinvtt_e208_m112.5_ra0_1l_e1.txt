*
*   Nickname     : xs_hzha03pyth6156hinvtt_e208_m112.5_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Fri Sep  7 15:30:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_112.5_41510.xsdst ! RUN = 41510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_112.5_41511.xsdst ! RUN = 41511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_112.5_41512.xsdst ! RUN = 41512 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_hinvtt_208_112.5_41513.xsdst ! RUN = 41513 ! NEVT = 500
