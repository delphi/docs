*
*   Nickname     : xs_hzha03pyth6156hinvmu_e203.7_m110_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Thu Sep 13 05:25:27 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_110_41440.xsdst ! RUN = 41440 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_110_41441.xsdst ! RUN = 41441 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_110_41442.xsdst ! RUN = 41442 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hinvmu_203.7_110_41443.xsdst ! RUN = 41443 ! NEVT = 500
