*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m30_m30_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sat Oct  6 03:26:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_30_11151.xsdst ! RUN = 11151 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_30_11152.xsdst ! RUN = 11152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_30_11153.xsdst ! RUN = 11153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_30_11154.xsdst ! RUN = 11154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_30_11155.xsdst ! RUN = 11155 ! NEVT = 500
