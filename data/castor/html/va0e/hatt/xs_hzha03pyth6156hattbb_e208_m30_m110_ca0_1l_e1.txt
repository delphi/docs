*
*   Nickname     : xs_hzha03pyth6156hattbb_e208_m30_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Wed Nov  7 00:50:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_110_14201.xsdst ! RUN = 14201 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_110_14202.xsdst ! RUN = 14202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_110_14203.xsdst ! RUN = 14203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_110_14204.xsdst ! RUN = 14204 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_110_14205.xsdst ! RUN = 14205 ! NEVT = 500
