*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m100_m100_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Sun Oct  7 01:34:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_100_100_12611.xsdst ! RUN = 12611 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_100_100_12612.xsdst ! RUN = 12612 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_100_100_12613.xsdst ! RUN = 12613 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_100_100_12614.xsdst ! RUN = 12614 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_100_100_12615.xsdst ! RUN = 12615 ! NEVT = 499
