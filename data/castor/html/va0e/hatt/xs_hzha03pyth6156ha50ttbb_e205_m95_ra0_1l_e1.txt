*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e205_m95_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Tue Oct 30 18:25:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59530.xsdst ! RUN = 59530 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59531.xsdst ! RUN = 59531 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59532.xsdst ! RUN = 59532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59533.xsdst ! RUN = 59533 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59534.xsdst ! RUN = 59534 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59535.xsdst ! RUN = 59535 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59536.xsdst ! RUN = 59536 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59537.xsdst ! RUN = 59537 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59538.xsdst ! RUN = 59538 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_95_59539.xsdst ! RUN = 59539 ! NEVT = 499
