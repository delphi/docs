*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m30_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Oct  6 06:34:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_30_90_12191.xsdst ! RUN = 12191 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_30_90_12192.xsdst ! RUN = 12192 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_30_90_12193.xsdst ! RUN = 12193 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_30_90_12194.xsdst ! RUN = 12194 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_30_90_12195.xsdst ! RUN = 12195 ! NEVT = 500
