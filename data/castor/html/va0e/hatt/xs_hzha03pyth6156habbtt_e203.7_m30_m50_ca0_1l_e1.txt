*
*   Nickname     : xs_hzha03pyth6156habbtt_e203.7_m30_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Wed Nov  7 20:43:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_30_50_17161.xsdst ! RUN = 17161 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_30_50_17162.xsdst ! RUN = 17162 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_30_50_17163.xsdst ! RUN = 17163 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_30_50_17164.xsdst ! RUN = 17164 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_30_50_17165.xsdst ! RUN = 17165 ! NEVT = 500
