*
*   Nickname     : xs_hzha03pyth6156habbtt_e205_m50_m70_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Wed Nov  7 06:51:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_70_15271.xsdst ! RUN = 15271 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_70_15272.xsdst ! RUN = 15272 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_70_15273.xsdst ! RUN = 15273 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_70_15274.xsdst ! RUN = 15274 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_70_15275.xsdst ! RUN = 15275 ! NEVT = 500
