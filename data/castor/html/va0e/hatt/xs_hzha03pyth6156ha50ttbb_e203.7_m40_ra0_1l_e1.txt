*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e203.7_m40_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E203.7/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Wed Oct 31 04:26:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64030.xsdst ! RUN = 64030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64031.xsdst ! RUN = 64031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64032.xsdst ! RUN = 64032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64033.xsdst ! RUN = 64033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64034.xsdst ! RUN = 64034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64035.xsdst ! RUN = 64035 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64036.xsdst ! RUN = 64036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64037.xsdst ! RUN = 64037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64038.xsdst ! RUN = 64038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_40_64039.xsdst ! RUN = 64039 ! NEVT = 500
