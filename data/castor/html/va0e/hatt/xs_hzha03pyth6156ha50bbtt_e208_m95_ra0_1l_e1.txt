*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e208_m95_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Sat Nov 24 20:26:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49540.xsdst ! RUN = 49540 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49541.xsdst ! RUN = 49541 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49542.xsdst ! RUN = 49542 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49543.xsdst ! RUN = 49543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49544.xsdst ! RUN = 49544 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49545.xsdst ! RUN = 49545 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49546.xsdst ! RUN = 49546 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49547.xsdst ! RUN = 49547 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49548.xsdst ! RUN = 49548 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_95_49549.xsdst ! RUN = 49549 ! NEVT = 500
