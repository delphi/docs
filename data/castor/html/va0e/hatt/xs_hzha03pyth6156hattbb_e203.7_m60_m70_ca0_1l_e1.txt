*
*   Nickname     : xs_hzha03pyth6156hattbb_e203.7_m60_m70_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Thu Nov  8 04:05:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_70_18331.xsdst ! RUN = 18331 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_70_18332.xsdst ! RUN = 18332 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_70_18333.xsdst ! RUN = 18333 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_70_18334.xsdst ! RUN = 18334 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_70_18335.xsdst ! RUN = 18335 ! NEVT = 500
