*
*   Nickname     : xs_hzha03pyth6156hattbb_e203.7_m24_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Thu Nov  8 04:03:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_24_40_18131.xsdst ! RUN = 18131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_24_40_18132.xsdst ! RUN = 18132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_24_40_18133.xsdst ! RUN = 18133 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_24_40_18134.xsdst ! RUN = 18134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_24_40_18135.xsdst ! RUN = 18135 ! NEVT = 500
