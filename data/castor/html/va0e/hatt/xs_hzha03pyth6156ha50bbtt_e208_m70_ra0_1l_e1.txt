*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e208_m70_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4994 events in 10 files time stamp: Sat Nov 24 20:39:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47040.xsdst ! RUN = 47040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47041.xsdst ! RUN = 47041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47042.xsdst ! RUN = 47042 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47043.xsdst ! RUN = 47043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47044.xsdst ! RUN = 47044 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47045.xsdst ! RUN = 47045 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47046.xsdst ! RUN = 47046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47047.xsdst ! RUN = 47047 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47048.xsdst ! RUN = 47048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50bbtt_208_70_47049.xsdst ! RUN = 47049 ! NEVT = 497
