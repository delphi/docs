*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e206.5_m24_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Oct  8 22:33:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22431.xsdst ! RUN = 22431 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22432.xsdst ! RUN = 22432 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22433.xsdst ! RUN = 22433 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22434.xsdst ! RUN = 22434 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22435.xsdst ! RUN = 22435 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22436.xsdst ! RUN = 22436 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22437.xsdst ! RUN = 22437 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22438.xsdst ! RUN = 22438 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22439.xsdst ! RUN = 22439 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_24_22440.xsdst ! RUN = 22440 ! NEVT = 500
