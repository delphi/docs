*
*   Nickname     : xs_hzha03pyth6156habbtt_e208_m30_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Tue Nov  6 07:19:13 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_30_40_13161.xsdst ! RUN = 13161 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_30_40_13162.xsdst ! RUN = 13162 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_30_40_13163.xsdst ! RUN = 13163 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_30_40_13164.xsdst ! RUN = 13164 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_30_40_13165.xsdst ! RUN = 13165 ! NEVT = 500
