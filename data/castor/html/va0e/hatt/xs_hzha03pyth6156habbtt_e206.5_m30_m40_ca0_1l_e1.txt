*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m30_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Oct  6 03:30:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_40_11161.xsdst ! RUN = 11161 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_40_11162.xsdst ! RUN = 11162 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_40_11163.xsdst ! RUN = 11163 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_40_11164.xsdst ! RUN = 11164 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_30_40_11165.xsdst ! RUN = 11165 ! NEVT = 499
