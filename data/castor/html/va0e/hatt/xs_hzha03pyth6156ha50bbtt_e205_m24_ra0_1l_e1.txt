*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e205_m24_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Sat Nov 24 15:09:57 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52440.xsdst ! RUN = 52440 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52441.xsdst ! RUN = 52441 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52442.xsdst ! RUN = 52442 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52443.xsdst ! RUN = 52443 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52444.xsdst ! RUN = 52444 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52445.xsdst ! RUN = 52445 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52446.xsdst ! RUN = 52446 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52447.xsdst ! RUN = 52447 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52448.xsdst ! RUN = 52448 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_24_52449.xsdst ! RUN = 52449 ! NEVT = 500
