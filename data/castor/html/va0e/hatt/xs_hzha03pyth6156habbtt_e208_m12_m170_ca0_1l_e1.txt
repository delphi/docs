*
*   Nickname     : xs_hzha03pyth6156habbtt_e208_m12_m170_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Tue Nov  6 02:02:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_12_170_13081.xsdst ! RUN = 13081 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_12_170_13082.xsdst ! RUN = 13082 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_12_170_13083.xsdst ! RUN = 13083 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_12_170_13084.xsdst ! RUN = 13084 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_12_170_13085.xsdst ! RUN = 13085 ! NEVT = 500
