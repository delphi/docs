*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m12_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Mon Nov  5 22:29:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_12_90_11041.xsdst ! RUN = 11041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_12_90_11042.xsdst ! RUN = 11042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_12_90_11043.xsdst ! RUN = 11043 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_12_90_11044.xsdst ! RUN = 11044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_12_90_11045.xsdst ! RUN = 11045 ! NEVT = 499
