*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e206.5_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Tue Oct  9 12:41:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29041.xsdst ! RUN = 29041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29042.xsdst ! RUN = 29042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29043.xsdst ! RUN = 29043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29044.xsdst ! RUN = 29044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29045.xsdst ! RUN = 29045 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29046.xsdst ! RUN = 29046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29047.xsdst ! RUN = 29047 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29048.xsdst ! RUN = 29048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29049.xsdst ! RUN = 29049 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_90_29050.xsdst ! RUN = 29050 ! NEVT = 500
