*
*   Nickname     : xs_hzha03pyth6156habbtt_e205_m12_m190_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Wed Nov  7 14:41:48 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_12_190_15091.xsdst ! RUN = 15091 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_12_190_15092.xsdst ! RUN = 15092 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_12_190_15093.xsdst ! RUN = 15093 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_12_190_15094.xsdst ! RUN = 15094 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_12_190_15095.xsdst ! RUN = 15095 ! NEVT = 500
