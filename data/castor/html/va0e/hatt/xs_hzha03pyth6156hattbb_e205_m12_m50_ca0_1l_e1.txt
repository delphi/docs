*
*   Nickname     : xs_hzha03pyth6156hattbb_e205_m12_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Tue Nov  6 20:40:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_50_16021.xsdst ! RUN = 16021 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_50_16022.xsdst ! RUN = 16022 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_50_16023.xsdst ! RUN = 16023 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_50_16024.xsdst ! RUN = 16024 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_50_16025.xsdst ! RUN = 16025 ! NEVT = 500
