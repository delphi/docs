*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e206.5_m24_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Mon Oct  8 22:39:56 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22441.xsdst ! RUN = 22441 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22442.xsdst ! RUN = 22442 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22443.xsdst ! RUN = 22443 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22444.xsdst ! RUN = 22444 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22445.xsdst ! RUN = 22445 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22446.xsdst ! RUN = 22446 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22447.xsdst ! RUN = 22447 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22448.xsdst ! RUN = 22448 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22449.xsdst ! RUN = 22449 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50bbtt_206.5_24_22450.xsdst ! RUN = 22450 ! NEVT = 499
