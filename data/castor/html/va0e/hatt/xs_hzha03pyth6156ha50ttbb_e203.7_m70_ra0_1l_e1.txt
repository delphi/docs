*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e203.7_m70_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E203.7/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Wed Oct 31 20:25:09 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67030.xsdst ! RUN = 67030 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67031.xsdst ! RUN = 67031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67032.xsdst ! RUN = 67032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67033.xsdst ! RUN = 67033 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67034.xsdst ! RUN = 67034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67035.xsdst ! RUN = 67035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67036.xsdst ! RUN = 67036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67037.xsdst ! RUN = 67037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67038.xsdst ! RUN = 67038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_70_67039.xsdst ! RUN = 67039 ! NEVT = 500
