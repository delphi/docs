*
*   Nickname     : xs_hzha03pyth6156habbtt_e205_m70_m70_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2496 events in 5 files time stamp: Wed Nov  7 14:48:56 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_70_15371.xsdst ! RUN = 15371 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_70_15372.xsdst ! RUN = 15372 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_70_15373.xsdst ! RUN = 15373 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_70_15374.xsdst ! RUN = 15374 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_70_15375.xsdst ! RUN = 15375 ! NEVT = 499
