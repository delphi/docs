*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m85_m85_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Oct  6 21:26:37 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_85_85_11521.xsdst ! RUN = 11521 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_85_85_11522.xsdst ! RUN = 11522 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_85_85_11523.xsdst ! RUN = 11523 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_85_85_11524.xsdst ! RUN = 11524 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_85_85_11525.xsdst ! RUN = 11525 ! NEVT = 499
