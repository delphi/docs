*
*   Nickname     : xs_hzha03pyth6156hattbb_e208_m60_m100_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2495 events in 5 files time stamp: Tue Nov  6 10:51:25 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_60_100_14391.xsdst ! RUN = 14391 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_60_100_14392.xsdst ! RUN = 14392 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_60_100_14393.xsdst ! RUN = 14393 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_60_100_14394.xsdst ! RUN = 14394 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_60_100_14395.xsdst ! RUN = 14395 ! NEVT = 497
