*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e203.7_m80_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E203.7/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Wed Oct 31 06:29:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68030.xsdst ! RUN = 68030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68031.xsdst ! RUN = 68031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68032.xsdst ! RUN = 68032 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68033.xsdst ! RUN = 68033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68034.xsdst ! RUN = 68034 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68035.xsdst ! RUN = 68035 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68036.xsdst ! RUN = 68036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68037.xsdst ! RUN = 68037 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68038.xsdst ! RUN = 68038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_80_68039.xsdst ! RUN = 68039 ! NEVT = 499
