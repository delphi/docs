*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e203.7_m80_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E203.7/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Oct 31 20:25:18 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_80_68040.xsdst ! RUN = 68040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_80_68041.xsdst ! RUN = 68041 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_80_68042.xsdst ! RUN = 68042 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_80_68043.xsdst ! RUN = 68043 ! NEVT = 500
