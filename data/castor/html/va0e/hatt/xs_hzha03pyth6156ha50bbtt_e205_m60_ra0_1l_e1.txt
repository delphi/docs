*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e205_m60_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 4994 events in 10 files time stamp: Wed Oct 31 20:24:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56040.xsdst ! RUN = 56040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56041.xsdst ! RUN = 56041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56042.xsdst ! RUN = 56042 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56043.xsdst ! RUN = 56043 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56044.xsdst ! RUN = 56044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56045.xsdst ! RUN = 56045 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56046.xsdst ! RUN = 56046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56047.xsdst ! RUN = 56047 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56048.xsdst ! RUN = 56048 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_60_56049.xsdst ! RUN = 56049 ! NEVT = 498
