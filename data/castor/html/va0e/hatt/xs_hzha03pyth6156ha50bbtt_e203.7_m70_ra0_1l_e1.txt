*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e203.7_m70_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/E203.7/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 4993 events in 10 files time stamp: Wed Oct 31 12:24:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67040.xsdst ! RUN = 67040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67041.xsdst ! RUN = 67041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67042.xsdst ! RUN = 67042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67043.xsdst ! RUN = 67043 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67044.xsdst ! RUN = 67044 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67045.xsdst ! RUN = 67045 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67046.xsdst ! RUN = 67046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67047.xsdst ! RUN = 67047 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67048.xsdst ! RUN = 67048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50bbtt_203.7_70_67049.xsdst ! RUN = 67049 ! NEVT = 498
