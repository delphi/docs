*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m24_m24_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Sat Oct  6 01:33:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_24_24_12131.xsdst ! RUN = 12131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_24_24_12132.xsdst ! RUN = 12132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_24_24_12133.xsdst ! RUN = 12133 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_24_24_12134.xsdst ! RUN = 12134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_24_24_12135.xsdst ! RUN = 12135 ! NEVT = 500
