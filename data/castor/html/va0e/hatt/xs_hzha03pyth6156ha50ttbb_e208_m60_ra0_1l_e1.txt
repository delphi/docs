*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e208_m60_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sat Nov 24 20:37:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46030.xsdst ! RUN = 46030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46031.xsdst ! RUN = 46031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46032.xsdst ! RUN = 46032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46033.xsdst ! RUN = 46033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46034.xsdst ! RUN = 46034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46035.xsdst ! RUN = 46035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46036.xsdst ! RUN = 46036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46037.xsdst ! RUN = 46037 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46038.xsdst ! RUN = 46038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_60_46039.xsdst ! RUN = 46039 ! NEVT = 500
