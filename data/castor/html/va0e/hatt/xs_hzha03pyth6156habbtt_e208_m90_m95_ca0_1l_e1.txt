*
*   Nickname     : xs_hzha03pyth6156habbtt_e208_m90_m95_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Tue Nov  6 12:33:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_90_95_13561.xsdst ! RUN = 13561 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_90_95_13562.xsdst ! RUN = 13562 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_90_95_13563.xsdst ! RUN = 13563 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_90_95_13564.xsdst ! RUN = 13564 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_90_95_13565.xsdst ! RUN = 13565 ! NEVT = 499
