*
*   Nickname     : xs_hzha03pyth6156hattbb_e203.7_m80_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Thu Nov  8 13:29:07 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_80_90_18461.xsdst ! RUN = 18461 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_80_90_18462.xsdst ! RUN = 18462 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_80_90_18463.xsdst ! RUN = 18463 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_80_90_18464.xsdst ! RUN = 18464 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_80_90_18465.xsdst ! RUN = 18465 ! NEVT = 500
