*
*   Nickname     : xs_hzha03pyth6156habbtt_e206.5_m18_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Oct  6 01:29:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_18_40_11121.xsdst ! RUN = 11121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_18_40_11122.xsdst ! RUN = 11122 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_18_40_11123.xsdst ! RUN = 11123 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_18_40_11124.xsdst ! RUN = 11124 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_habbtt_206.5_18_40_11125.xsdst ! RUN = 11125 ! NEVT = 499
