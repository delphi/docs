*
*   Nickname     : xs_hzha03pyth6156habbtt_e208_m85_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Tue Nov  6 10:57:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_85_90_13531.xsdst ! RUN = 13531 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_85_90_13532.xsdst ! RUN = 13532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_85_90_13533.xsdst ! RUN = 13533 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_85_90_13534.xsdst ! RUN = 13534 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_85_90_13535.xsdst ! RUN = 13535 ! NEVT = 500
