*
*   Nickname     : xs_hzha03pyth6156habbtt_e203.7_m60_m60_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Thu Nov  8 09:12:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_60_60_17321.xsdst ! RUN = 17321 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_60_60_17322.xsdst ! RUN = 17322 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_60_60_17323.xsdst ! RUN = 17323 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_60_60_17324.xsdst ! RUN = 17324 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_60_60_17325.xsdst ! RUN = 17325 ! NEVT = 500
