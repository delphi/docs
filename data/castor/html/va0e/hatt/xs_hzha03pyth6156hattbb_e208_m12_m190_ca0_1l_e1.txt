*
*   Nickname     : xs_hzha03pyth6156hattbb_e208_m12_m190_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Tue Nov  6 07:57:30 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_12_190_14091.xsdst ! RUN = 14091 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_12_190_14092.xsdst ! RUN = 14092 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_12_190_14093.xsdst ! RUN = 14093 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_12_190_14094.xsdst ! RUN = 14094 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_12_190_14095.xsdst ! RUN = 14095 ! NEVT = 500
