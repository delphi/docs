*
*   Nickname     : xs_hzha03pyth6156hattbb_e203.7_m60_m60_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Thu Nov  8 03:48:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_60_18321.xsdst ! RUN = 18321 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_60_18322.xsdst ! RUN = 18322 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_60_18323.xsdst ! RUN = 18323 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_60_18324.xsdst ! RUN = 18324 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_60_60_18325.xsdst ! RUN = 18325 ! NEVT = 500
