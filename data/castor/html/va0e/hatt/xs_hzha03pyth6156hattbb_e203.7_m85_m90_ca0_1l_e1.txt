*
*   Nickname     : xs_hzha03pyth6156hattbb_e203.7_m85_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Fri Nov 16 22:19:16 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_85_90_18491.xsdst ! RUN = 18491 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_85_90_18492.xsdst ! RUN = 18492 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_85_90_18493.xsdst ! RUN = 18493 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_85_90_18494.xsdst ! RUN = 18494 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hattbb_203.7_85_90_18495.xsdst ! RUN = 18495 ! NEVT = 499
