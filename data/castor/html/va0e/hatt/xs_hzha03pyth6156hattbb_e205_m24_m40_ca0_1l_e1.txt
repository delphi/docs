*
*   Nickname     : xs_hzha03pyth6156hattbb_e205_m24_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Wed Nov  7 20:45:49 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_40_16131.xsdst ! RUN = 16131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_40_16132.xsdst ! RUN = 16132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_40_16133.xsdst ! RUN = 16133 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_40_16134.xsdst ! RUN = 16134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_40_16135.xsdst ! RUN = 16135 ! NEVT = 500
