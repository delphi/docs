*
*   Nickname     : xs_hzha03pyth6156habbtt_e203.7_m24_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Thu Nov  8 00:40:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_24_40_17131.xsdst ! RUN = 17131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_24_40_17132.xsdst ! RUN = 17132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_24_40_17133.xsdst ! RUN = 17133 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_24_40_17134.xsdst ! RUN = 17134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_24_40_17135.xsdst ! RUN = 17135 ! NEVT = 500
