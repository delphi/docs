*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e203.7_m90_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E203.7/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=203.7 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Wed Oct 31 06:30:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69030.xsdst ! RUN = 69030 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69031.xsdst ! RUN = 69031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69032.xsdst ! RUN = 69032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69033.xsdst ! RUN = 69033 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69034.xsdst ! RUN = 69034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69035.xsdst ! RUN = 69035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69036.xsdst ! RUN = 69036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69037.xsdst ! RUN = 69037 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69038.xsdst ! RUN = 69038 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ha50ttbb_203.7_90_69039.xsdst ! RUN = 69039 ! NEVT = 500
