*
*   Nickname     : xs_hzha03pyth6156hattbb_e206.5_m50_m130_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E206.5/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Sat Oct  6 14:35:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_50_130_12321.xsdst ! RUN = 12321 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_50_130_12322.xsdst ! RUN = 12322 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_50_130_12323.xsdst ! RUN = 12323 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_50_130_12324.xsdst ! RUN = 12324 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hattbb_206.5_50_130_12325.xsdst ! RUN = 12325 ! NEVT = 500
