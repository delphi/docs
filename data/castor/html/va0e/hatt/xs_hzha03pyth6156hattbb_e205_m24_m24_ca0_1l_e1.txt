*
*   Nickname     : xs_hzha03pyth6156hattbb_e205_m24_m24_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-4
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 1998 events in 4 files time stamp: Wed Nov  7 12:41:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_24_16121.xsdst ! RUN = 16121 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_24_16123.xsdst ! RUN = 16123 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_24_16124.xsdst ! RUN = 16124 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_24_24_16125.xsdst ! RUN = 16125 ! NEVT = 500
