*
*   Nickname     : xs_hzha03pyth6156hattbb_e205_m40_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Wed Nov  7 04:54:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_40_40_16231.xsdst ! RUN = 16231 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_40_40_16232.xsdst ! RUN = 16232 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_40_40_16233.xsdst ! RUN = 16233 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_40_40_16234.xsdst ! RUN = 16234 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_40_40_16235.xsdst ! RUN = 16235 ! NEVT = 500
