*
*   Nickname     : xs_hzha03pyth6156ha50bbtt_e205_m40_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50BBTT/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Wed Oct 31 00:27:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54040.xsdst ! RUN = 54040 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54041.xsdst ! RUN = 54041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54042.xsdst ! RUN = 54042 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54043.xsdst ! RUN = 54043 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54044.xsdst ! RUN = 54044 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54045.xsdst ! RUN = 54045 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54046.xsdst ! RUN = 54046 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54047.xsdst ! RUN = 54047 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54048.xsdst ! RUN = 54048 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50bbtt_205_40_54049.xsdst ! RUN = 54049 ! NEVT = 500
