*
*   Nickname     : xs_hzha03pyth6156habbtt_e203.7_m90_m100_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2345 events in 5 files time stamp: Thu Nov  8 08:10:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_90_100_17531.xsdst ! RUN = 17531 ! NEVT = 345
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_90_100_17532.xsdst ! RUN = 17532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_90_100_17533.xsdst ! RUN = 17533 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_90_100_17534.xsdst ! RUN = 17534 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_90_100_17535.xsdst ! RUN = 17535 ! NEVT = 500
