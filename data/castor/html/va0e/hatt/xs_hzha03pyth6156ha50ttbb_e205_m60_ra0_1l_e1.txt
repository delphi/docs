*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e205_m60_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 205 , RAL
*---
*   Comments     : in total 4996 events in 10 files time stamp: Sat Nov 24 15:14:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56030.xsdst ! RUN = 56030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56031.xsdst ! RUN = 56031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56032.xsdst ! RUN = 56032 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56033.xsdst ! RUN = 56033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56034.xsdst ! RUN = 56034 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56035.xsdst ! RUN = 56035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56036.xsdst ! RUN = 56036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56037.xsdst ! RUN = 56037 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56038.xsdst ! RUN = 56038 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/205/hzha03pyth6156_ha50ttbb_205_60_56039.xsdst ! RUN = 56039 ! NEVT = 500
