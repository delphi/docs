*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e208_m95_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Tue Oct 30 06:24:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49530.xsdst ! RUN = 49530 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49531.xsdst ! RUN = 49531 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49532.xsdst ! RUN = 49532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49533.xsdst ! RUN = 49533 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49534.xsdst ! RUN = 49534 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49535.xsdst ! RUN = 49535 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49536.xsdst ! RUN = 49536 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49537.xsdst ! RUN = 49537 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49538.xsdst ! RUN = 49538 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_95_49539.xsdst ! RUN = 49539 ! NEVT = 500
