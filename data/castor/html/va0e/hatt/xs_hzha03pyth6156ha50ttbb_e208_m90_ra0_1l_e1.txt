*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e208_m90_ra0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 208 , RAL
*---
*   Comments     : in total 4997 events in 10 files time stamp: Sat Nov 24 20:38:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49030.xsdst ! RUN = 49030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49031.xsdst ! RUN = 49031 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49032.xsdst ! RUN = 49032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49033.xsdst ! RUN = 49033 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49034.xsdst ! RUN = 49034 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49035.xsdst ! RUN = 49035 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49036.xsdst ! RUN = 49036 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49037.xsdst ! RUN = 49037 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49038.xsdst ! RUN = 49038 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/va0e/208/hzha03pyth6156_ha50ttbb_208_90_49039.xsdst ! RUN = 49039 ! NEVT = 500
