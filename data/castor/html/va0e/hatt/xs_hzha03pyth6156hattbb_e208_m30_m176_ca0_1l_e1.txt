*
*   Nickname     : xs_hzha03pyth6156hattbb_e208_m30_m176_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Tue Nov  6 08:41:33 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_176_14241.xsdst ! RUN = 14241 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_176_14242.xsdst ! RUN = 14242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_176_14243.xsdst ! RUN = 14243 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_176_14244.xsdst ! RUN = 14244 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_30_176_14245.xsdst ! RUN = 14245 ! NEVT = 500
