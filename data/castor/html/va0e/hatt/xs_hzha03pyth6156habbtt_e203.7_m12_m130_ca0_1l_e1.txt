*
*   Nickname     : xs_hzha03pyth6156habbtt_e203.7_m12_m130_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E203.7/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Wed Nov  7 18:38:46 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_12_130_17061.xsdst ! RUN = 17061 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_12_130_17062.xsdst ! RUN = 17062 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_12_130_17063.xsdst ! RUN = 17063 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_12_130_17064.xsdst ! RUN = 17064 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_habbtt_203.7_12_130_17065.xsdst ! RUN = 17065 ! NEVT = 499
