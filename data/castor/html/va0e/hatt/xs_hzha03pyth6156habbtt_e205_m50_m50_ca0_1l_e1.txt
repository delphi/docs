*
*   Nickname     : xs_hzha03pyth6156habbtt_e205_m50_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Wed Nov  7 04:38:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_50_15251.xsdst ! RUN = 15251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_50_15252.xsdst ! RUN = 15252 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_50_15253.xsdst ! RUN = 15253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_50_15254.xsdst ! RUN = 15254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_50_50_15255.xsdst ! RUN = 15255 ! NEVT = 500
