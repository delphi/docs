*
*   Nickname     : xs_hzha03pyth6156hattbb_e205_m85_m95_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Wed Nov  7 14:35:25 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_85_95_16501.xsdst ! RUN = 16501 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_85_95_16502.xsdst ! RUN = 16502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_85_95_16503.xsdst ! RUN = 16503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_85_95_16504.xsdst ! RUN = 16504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_85_95_16505.xsdst ! RUN = 16505 ! NEVT = 500
