*
*   Nickname     : xs_hzha03pyth6156ha50ttbb_e206.5_m95_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50TTBB/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Tue Oct  9 16:42:15 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29531.xsdst ! RUN = 29531 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29532.xsdst ! RUN = 29532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29533.xsdst ! RUN = 29533 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29534.xsdst ! RUN = 29534 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29535.xsdst ! RUN = 29535 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29536.xsdst ! RUN = 29536 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29537.xsdst ! RUN = 29537 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29538.xsdst ! RUN = 29538 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29539.xsdst ! RUN = 29539 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ha50ttbb_206.5_95_29540.xsdst ! RUN = 29540 ! NEVT = 499
