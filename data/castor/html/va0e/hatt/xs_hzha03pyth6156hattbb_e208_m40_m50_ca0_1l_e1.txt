*
*   Nickname     : xs_hzha03pyth6156hattbb_e208_m40_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Tue Nov  6 04:38:10 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_40_50_14261.xsdst ! RUN = 14261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_40_50_14262.xsdst ! RUN = 14262 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_40_50_14263.xsdst ! RUN = 14263 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_40_50_14264.xsdst ! RUN = 14264 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hattbb_208_40_50_14265.xsdst ! RUN = 14265 ! NEVT = 499
