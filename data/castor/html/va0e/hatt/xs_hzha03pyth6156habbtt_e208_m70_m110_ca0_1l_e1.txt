*
*   Nickname     : xs_hzha03pyth6156habbtt_e208_m70_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Tue Nov  6 10:47:20 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_70_110_13451.xsdst ! RUN = 13451 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_70_110_13452.xsdst ! RUN = 13452 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_70_110_13453.xsdst ! RUN = 13453 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_70_110_13454.xsdst ! RUN = 13454 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_habbtt_208_70_110_13455.xsdst ! RUN = 13455 ! NEVT = 500
