*
*   Nickname     : xs_hzha03pyth6156hattbb_e205_m12_m30_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Fri Nov 16 22:19:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_30_16011.xsdst ! RUN = 16011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_30_16012.xsdst ! RUN = 16012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_30_16013.xsdst ! RUN = 16013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_30_16014.xsdst ! RUN = 16014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hattbb_205_12_30_16015.xsdst ! RUN = 16015 ! NEVT = 499
