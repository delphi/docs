*
*   Nickname     : xs_hzha03pyth6156habbtt_e205_m70_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 2498 events in 5 files time stamp: Wed Nov  7 14:36:20 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_110_15421.xsdst ! RUN = 15421 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_110_15422.xsdst ! RUN = 15422 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_110_15423.xsdst ! RUN = 15423 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_110_15424.xsdst ! RUN = 15424 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_habbtt_205_70_110_15425.xsdst ! RUN = 15425 ! NEVT = 500
