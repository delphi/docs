*
*   Nickname     : xs_hzha03pyth6156ttz_e203.7_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Thu Sep  6 19:21:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21350.xsdst ! RUN = 21350 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21351.xsdst ! RUN = 21351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21352.xsdst ! RUN = 21352 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21353.xsdst ! RUN = 21353 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21354.xsdst ! RUN = 21354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21355.xsdst ! RUN = 21355 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21356.xsdst ! RUN = 21356 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21357.xsdst ! RUN = 21357 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21358.xsdst ! RUN = 21358 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_110_21359.xsdst ! RUN = 21359 ! NEVT = 498
