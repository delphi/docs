*
*   Nickname     : xs_hzha03pyth6156hznu_e208_m95_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZNU/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar)(HZ+interference+fusion) Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Fri Aug 31 20:24:55 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19611.xsdst ! RUN = 19611 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19612.xsdst ! RUN = 19612 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19613.xsdst ! RUN = 19613 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19614.xsdst ! RUN = 19614 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19615.xsdst ! RUN = 19615 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19616.xsdst ! RUN = 19616 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19617.xsdst ! RUN = 19617 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19618.xsdst ! RUN = 19618 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19619.xsdst ! RUN = 19619 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hznu_208_95_19620.xsdst ! RUN = 19620 ! NEVT = 500
