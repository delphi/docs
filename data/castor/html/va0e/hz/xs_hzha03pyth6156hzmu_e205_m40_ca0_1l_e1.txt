*
*   Nickname     : xs_hzha03pyth6156hzmu_e205_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Sat Sep  1 07:23:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14231.xsdst ! RUN = 14231 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14232.xsdst ! RUN = 14232 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14233.xsdst ! RUN = 14233 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14234.xsdst ! RUN = 14234 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14235.xsdst ! RUN = 14235 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14236.xsdst ! RUN = 14236 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14237.xsdst ! RUN = 14237 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14238.xsdst ! RUN = 14238 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14239.xsdst ! RUN = 14239 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_40_14240.xsdst ! RUN = 14240 ! NEVT = 500
