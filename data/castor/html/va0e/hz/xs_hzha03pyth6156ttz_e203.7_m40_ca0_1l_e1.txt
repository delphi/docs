*
*   Nickname     : xs_hzha03pyth6156ttz_e203.7_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 4993 events in 10 files time stamp: Sat Aug 25 04:23:33 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14351.xsdst ! RUN = 14351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14352.xsdst ! RUN = 14352 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14353.xsdst ! RUN = 14353 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14354.xsdst ! RUN = 14354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14355.xsdst ! RUN = 14355 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14356.xsdst ! RUN = 14356 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14357.xsdst ! RUN = 14357 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14358.xsdst ! RUN = 14358 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14359.xsdst ! RUN = 14359 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_40_14360.xsdst ! RUN = 14360 ! NEVT = 500
