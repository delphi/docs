*
*   Nickname     : xs_hzha03pyth6156hzqq_e208_m114_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/CERN/SUMT/C001-11
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 5496 events in 11 files time stamp: Tue Sep  4 17:28:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21501.xsdst ! RUN = 21501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21502.xsdst ! RUN = 21502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21503.xsdst ! RUN = 21503 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21504.xsdst ! RUN = 21504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21505.xsdst ! RUN = 21505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21506.xsdst ! RUN = 21506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21507.xsdst ! RUN = 21507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21508.xsdst ! RUN = 21508 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21509.xsdst ! RUN = 21509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21510.xsdst ! RUN = 21510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_114_21709.xsdst ! RUN = 21709 ! NEVT = 500
