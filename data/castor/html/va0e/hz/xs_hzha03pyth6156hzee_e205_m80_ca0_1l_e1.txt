*
*   Nickname     : xs_hzha03pyth6156hzee_e205_m80_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZEE/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-)(HZ+interference+fusion) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4993 events in 10 files time stamp: Thu Aug 23 14:46:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18221.xsdst ! RUN = 18221 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18222.xsdst ! RUN = 18222 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18223.xsdst ! RUN = 18223 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18224.xsdst ! RUN = 18224 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18225.xsdst ! RUN = 18225 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18226.xsdst ! RUN = 18226 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18227.xsdst ! RUN = 18227 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18228.xsdst ! RUN = 18228 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18229.xsdst ! RUN = 18229 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzee_205_80_18230.xsdst ! RUN = 18230 ! NEVT = 499
