*
*   Nickname     : xs_hzha03pyth6156hzqq_e205_m115_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4997 events in 10 files time stamp: Fri Aug 24 00:24:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21701.xsdst ! RUN = 21701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21702.xsdst ! RUN = 21702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21703.xsdst ! RUN = 21703 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21704.xsdst ! RUN = 21704 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21705.xsdst ! RUN = 21705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21706.xsdst ! RUN = 21706 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21707.xsdst ! RUN = 21707 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21708.xsdst ! RUN = 21708 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21709.xsdst ! RUN = 21709 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzqq_205_115_21710.xsdst ! RUN = 21710 ! NEVT = 499
