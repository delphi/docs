*
*   Nickname     : xs_hzha03pyth6156ttz_e205_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4995 events in 10 files time stamp: Thu Aug 23 04:20:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19251.xsdst ! RUN = 19251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19252.xsdst ! RUN = 19252 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19253.xsdst ! RUN = 19253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19254.xsdst ! RUN = 19254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19255.xsdst ! RUN = 19255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19256.xsdst ! RUN = 19256 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19257.xsdst ! RUN = 19257 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19258.xsdst ! RUN = 19258 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19259.xsdst ! RUN = 19259 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_90_19260.xsdst ! RUN = 19260 ! NEVT = 499
