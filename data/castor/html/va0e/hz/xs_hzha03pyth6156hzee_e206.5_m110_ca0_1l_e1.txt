*
*   Nickname     : xs_hzha03pyth6156hzee_e206.5_m110_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZEE/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-)(HZ+interference+fusion) Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4992 events in 10 files time stamp: Thu Aug 23 14:47:04 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21021.xsdst ! RUN = 21021 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21022.xsdst ! RUN = 21022 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21023.xsdst ! RUN = 21023 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21024.xsdst ! RUN = 21024 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21025.xsdst ! RUN = 21025 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21026.xsdst ! RUN = 21026 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21027.xsdst ! RUN = 21027 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21028.xsdst ! RUN = 21028 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21029.xsdst ! RUN = 21029 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzee_206.5_110_21030.xsdst ! RUN = 21030 ! NEVT = 500
