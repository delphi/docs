*
*   Nickname     : xs_hzha03pyth6156ttz_e203.7_m120_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Fri Apr  5 18:36:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22351.xsdst ! RUN = 22351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22352.xsdst ! RUN = 22352 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22353.xsdst ! RUN = 22353 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22354.xsdst ! RUN = 22354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22355.xsdst ! RUN = 22355 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22356.xsdst ! RUN = 22356 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22357.xsdst ! RUN = 22357 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22358.xsdst ! RUN = 22358 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22359.xsdst ! RUN = 22359 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_ttz_203.7_120_22360.xsdst ! RUN = 22360 ! NEVT = 500
