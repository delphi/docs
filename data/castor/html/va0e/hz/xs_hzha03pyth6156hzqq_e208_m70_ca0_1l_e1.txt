*
*   Nickname     : xs_hzha03pyth6156hzqq_e208_m70_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/CERN/SUMT/C001-20
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 9992 events in 20 files time stamp: Sat Aug 25 21:19:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17101.xsdst ! RUN = 17101 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17102.xsdst ! RUN = 17102 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17103.xsdst ! RUN = 17103 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17104.xsdst ! RUN = 17104 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17105.xsdst ! RUN = 17105 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17106.xsdst ! RUN = 17106 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17107.xsdst ! RUN = 17107 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17108.xsdst ! RUN = 17108 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17109.xsdst ! RUN = 17109 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17110.xsdst ! RUN = 17110 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17301.xsdst ! RUN = 17301 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17302.xsdst ! RUN = 17302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17303.xsdst ! RUN = 17303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17304.xsdst ! RUN = 17304 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17305.xsdst ! RUN = 17305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17306.xsdst ! RUN = 17306 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17307.xsdst ! RUN = 17307 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17308.xsdst ! RUN = 17308 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17309.xsdst ! RUN = 17309 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzqq_208_70_17310.xsdst ! RUN = 17310 ! NEVT = 500
