*
*   Nickname     : xs_hzha03pyth6156ttz_e205_m115_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Thu Aug 23 20:26:17 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21751.xsdst ! RUN = 21751 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21752.xsdst ! RUN = 21752 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21753.xsdst ! RUN = 21753 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21754.xsdst ! RUN = 21754 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21755.xsdst ! RUN = 21755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21756.xsdst ! RUN = 21756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21757.xsdst ! RUN = 21757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21758.xsdst ! RUN = 21758 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21759.xsdst ! RUN = 21759 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_115_21760.xsdst ! RUN = 21760 ! NEVT = 500
