*
*   Nickname     : xs_hzha03pyth6156hznu_e206.5_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZNU/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar)(HZ+interference+fusion) Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Mon Aug 27 10:29:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15011.xsdst ! RUN = 15011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15012.xsdst ! RUN = 15012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15013.xsdst ! RUN = 15013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15014.xsdst ! RUN = 15014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15015.xsdst ! RUN = 15015 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15016.xsdst ! RUN = 15016 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15017.xsdst ! RUN = 15017 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15018.xsdst ! RUN = 15018 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15019.xsdst ! RUN = 15019 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hznu_206.5_50_15020.xsdst ! RUN = 15020 ! NEVT = 500
