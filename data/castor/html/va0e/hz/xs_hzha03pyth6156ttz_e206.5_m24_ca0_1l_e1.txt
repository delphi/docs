*
*   Nickname     : xs_hzha03pyth6156ttz_e206.5_m24_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E206.5/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Mon Aug 27 08:19:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12451.xsdst ! RUN = 12451 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12452.xsdst ! RUN = 12452 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12453.xsdst ! RUN = 12453 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12454.xsdst ! RUN = 12454 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12455.xsdst ! RUN = 12455 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12456.xsdst ! RUN = 12456 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12457.xsdst ! RUN = 12457 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12458.xsdst ! RUN = 12458 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12459.xsdst ! RUN = 12459 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_ttz_206.5_24_12460.xsdst ! RUN = 12460 ! NEVT = 500
