*
*   Nickname     : xs_hzha03pyth6156hzmu_e203.7_m114_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Fri Nov 30 06:23:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21731.xsdst ! RUN = 21731 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21732.xsdst ! RUN = 21732 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21733.xsdst ! RUN = 21733 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21734.xsdst ! RUN = 21734 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21735.xsdst ! RUN = 21735 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21736.xsdst ! RUN = 21736 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21737.xsdst ! RUN = 21737 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21738.xsdst ! RUN = 21738 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21739.xsdst ! RUN = 21739 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzmu_203.7_114_21740.xsdst ! RUN = 21740 ! NEVT = 500
