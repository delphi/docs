*
*   Nickname     : xs_hzha03pyth6156hzqq_e203.7_m114_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZQQ/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Tue Sep  4 13:26:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21701.xsdst ! RUN = 21701 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21702.xsdst ! RUN = 21702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21703.xsdst ! RUN = 21703 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21704.xsdst ! RUN = 21704 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21705.xsdst ! RUN = 21705 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21706.xsdst ! RUN = 21706 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21707.xsdst ! RUN = 21707 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21708.xsdst ! RUN = 21708 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21709.xsdst ! RUN = 21709 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hzqq_203.7_114_21710.xsdst ! RUN = 21710 ! NEVT = 498
