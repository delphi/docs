*
*   Nickname     : xs_hzha03pyth6156hzee_e208_m115_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZEE/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-)(HZ+interference+fusion) Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 4647 events in 10 files time stamp: Thu Aug 23 14:46:13 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21621.xsdst ! RUN = 21621 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21622.xsdst ! RUN = 21622 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21623.xsdst ! RUN = 21623 ! NEVT = 159
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21624.xsdst ! RUN = 21624 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21625.xsdst ! RUN = 21625 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21626.xsdst ! RUN = 21626 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21627.xsdst ! RUN = 21627 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21628.xsdst ! RUN = 21628 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21629.xsdst ! RUN = 21629 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzee_208_115_21630.xsdst ! RUN = 21630 ! NEVT = 500
