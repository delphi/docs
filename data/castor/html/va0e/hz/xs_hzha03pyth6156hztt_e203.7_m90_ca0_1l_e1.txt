*
*   Nickname     : xs_hzha03pyth6156hztt_e203.7_m90_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZTT/E203.7/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation a0_e1 done at ecms=203.7 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Aug 26 05:23:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19341.xsdst ! RUN = 19341 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19342.xsdst ! RUN = 19342 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19343.xsdst ! RUN = 19343 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19344.xsdst ! RUN = 19344 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19345.xsdst ! RUN = 19345 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19346.xsdst ! RUN = 19346 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19347.xsdst ! RUN = 19347 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19348.xsdst ! RUN = 19348 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19349.xsdst ! RUN = 19349 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/203.7/hzha03pyth6156_hztt_203.7_90_19350.xsdst ! RUN = 19350 ! NEVT = 500
