*
*   Nickname     : xs_hzha03pyth6156hzmu_e205_m80_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Wed Aug 22 20:20:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18231.xsdst ! RUN = 18231 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18232.xsdst ! RUN = 18232 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18233.xsdst ! RUN = 18233 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18234.xsdst ! RUN = 18234 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18235.xsdst ! RUN = 18235 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18236.xsdst ! RUN = 18236 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18237.xsdst ! RUN = 18237 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18238.xsdst ! RUN = 18238 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18239.xsdst ! RUN = 18239 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hzmu_205_80_18240.xsdst ! RUN = 18240 ! NEVT = 500
