*
*   Nickname     : xs_hzha03pyth6156ttz_e205_m30_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 4994 events in 10 files time stamp: Sat Sep  1 10:29:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13251.xsdst ! RUN = 13251 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13252.xsdst ! RUN = 13252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13253.xsdst ! RUN = 13253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13254.xsdst ! RUN = 13254 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13255.xsdst ! RUN = 13255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13256.xsdst ! RUN = 13256 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13257.xsdst ! RUN = 13257 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13258.xsdst ! RUN = 13258 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13259.xsdst ! RUN = 13259 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_ttz_205_30_13260.xsdst ! RUN = 13260 ! NEVT = 499
