*
*   Nickname     : xs_hzha03pyth6156hznu_e205_m60_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZNU/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar)(HZ+interference+fusion) Extended Short DST simulation a0_e1 205 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Sat Sep  1 09:29:54 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16211.xsdst ! RUN = 16211 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16212.xsdst ! RUN = 16212 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16213.xsdst ! RUN = 16213 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16214.xsdst ! RUN = 16214 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16215.xsdst ! RUN = 16215 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16216.xsdst ! RUN = 16216 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16217.xsdst ! RUN = 16217 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16218.xsdst ! RUN = 16218 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16219.xsdst ! RUN = 16219 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205/hzha03pyth6156_hznu_205_60_16220.xsdst ! RUN = 16220 ! NEVT = 500
