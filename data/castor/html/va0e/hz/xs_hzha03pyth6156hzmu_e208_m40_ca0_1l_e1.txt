*
*   Nickname     : xs_hzha03pyth6156hzmu_e208_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Fri Aug 31 12:19:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14131.xsdst ! RUN = 14131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14132.xsdst ! RUN = 14132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14133.xsdst ! RUN = 14133 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14134.xsdst ! RUN = 14134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14135.xsdst ! RUN = 14135 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14136.xsdst ! RUN = 14136 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14137.xsdst ! RUN = 14137 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14138.xsdst ! RUN = 14138 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14139.xsdst ! RUN = 14139 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_40_14140.xsdst ! RUN = 14140 ! NEVT = 500
