*
*   Nickname     : xs_hzha03pyth6156hzmu_e208_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZMU/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation a0_e1 208 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Fri Aug 31 15:26:05 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15131.xsdst ! RUN = 15131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15132.xsdst ! RUN = 15132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15133.xsdst ! RUN = 15133 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15134.xsdst ! RUN = 15134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15135.xsdst ! RUN = 15135 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15136.xsdst ! RUN = 15136 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15137.xsdst ! RUN = 15137 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15138.xsdst ! RUN = 15138 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15139.xsdst ! RUN = 15139 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/208/hzha03pyth6156_hzmu_208_50_15140.xsdst ! RUN = 15140 ! NEVT = 500
