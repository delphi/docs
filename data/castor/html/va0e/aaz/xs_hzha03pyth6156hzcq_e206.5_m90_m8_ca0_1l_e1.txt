*   Nickname :     xs_hzha03pyth6156hzcq_e206.5_m90_m8_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E206.5/CERN/SUMT/C001-10
*   Description :   Extended Short DST simulation a0e  done at ecm=206.5 GeV , CERN
*   Comments :     in total 9899 events in 10 files, time stamp: Fri Mai 16 4:17:45 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9052.xsdst ! RUN = 9052 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9053.xsdst ! RUN = 9053 ! NEVT = 901
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9054.xsdst ! RUN = 9054 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9055.xsdst ! RUN = 9055 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9056.xsdst ! RUN = 9056 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9057.xsdst ! RUN = 9057 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9058.xsdst ! RUN = 9058 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9059.xsdst ! RUN = 9059 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9060.xsdst ! RUN = 9060 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_8_9061.xsdst ! RUN = 9061 ! NEVT = 1000
