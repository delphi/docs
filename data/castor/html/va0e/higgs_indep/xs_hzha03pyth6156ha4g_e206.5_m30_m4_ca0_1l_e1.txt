*   Nickname :     xs_hzha03pyth6156ha4g_e206.5_m30_m4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/E206.5/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation a0e  done at ecm=206.5 GeV , CERN
*   Comments :     in total 1999 events in 2 files, time stamp: Tue Sep 17 12:51:45 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hA4g_206.5_30_4_3041.xsdst ! RUN = 3041 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hA4g_206.5_30_4_3042.xsdst ! RUN = 3042 ! NEVT = 1000
