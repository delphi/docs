*   Nickname :     xs_hzha03pyth6156hzvc_e205.0_m10_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVC/E205.0/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu c c Extended Short DST simulation a0e  done at ecm=205.0 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Sat Aug 31 18:11:30 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205.0/hzha03pyth6156_hzvc_205.0_10_1303.xsdst ! RUN = 1303 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205.0/hzha03pyth6156_hzvc_205.0_10_1304.xsdst ! RUN = 1304 ! NEVT = 1000
