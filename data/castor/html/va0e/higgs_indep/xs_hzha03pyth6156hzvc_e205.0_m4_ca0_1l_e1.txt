*   Nickname :     xs_hzha03pyth6156hzvc_e205.0_m4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVC/E205.0/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu c c Extended Short DST simulation a0e  done at ecm=205.0 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Sat Aug 31 18:11:29 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205.0/hzha03pyth6156_hzvc_205.0_4_703.xsdst ! RUN = 703 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205.0/hzha03pyth6156_hzvc_205.0_4_704.xsdst ! RUN = 704 ! NEVT = 1000
