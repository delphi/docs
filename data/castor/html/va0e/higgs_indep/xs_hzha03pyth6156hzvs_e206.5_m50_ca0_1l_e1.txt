*   Nickname :     xs_hzha03pyth6156hzvs_e206.5_m50_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVS/E206.5/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu s s Extended Short DST simulation a0e  done at ecm=206.5 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Sat Aug 31 18:11:29 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzvs_206.5_50_5302.xsdst ! RUN = 5302 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzvs_206.5_50_5303.xsdst ! RUN = 5303 ! NEVT = 1000
