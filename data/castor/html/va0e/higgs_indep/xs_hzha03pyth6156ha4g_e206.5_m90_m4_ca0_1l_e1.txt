*   Nickname :     xs_hzha03pyth6156ha4g_e206.5_m90_m4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/E206.5/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation a0e  done at ecm=206.5 GeV , CERN
*   Comments :     in total 1999 events in 2 files, time stamp: Tue Sep 17 12:51:46 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hA4g_206.5_90_4_9041.xsdst ! RUN = 9041 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hA4g_206.5_90_4_9042.xsdst ! RUN = 9042 ! NEVT = 999
