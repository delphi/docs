*   Nickname :     xs_hzha03pyth6156hzvs_e205.0_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVS/E205.0/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu s s Extended Short DST simulation a0e  done at ecm=205.0 GeV , CERN
*---
*   Comments :     in total 1999 events in 2 files, time stamp: Sat Aug 31 18:11:29 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205.0/hzha03pyth6156_hzvs_205.0_40_4302.xsdst ! RUN = 4302 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/205.0/hzha03pyth6156_hzvs_205.0_40_4303.xsdst ! RUN = 4303 ! NEVT = 1000
