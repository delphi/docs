*
*   Nickname     : xs_hzha03pyth6156hzcq_e206.5_m50_m20_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9043 events in 10 files time stamp: Fri Apr 12 22:10:57 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5208.xsdst ! RUN = 5208 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5212.xsdst ! RUN = 5212 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5216.xsdst ! RUN = 5216 ! NEVT = 50
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5220.xsdst ! RUN = 5220 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5224.xsdst ! RUN = 5224 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5228.xsdst ! RUN = 5228 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5232.xsdst ! RUN = 5232 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5236.xsdst ! RUN = 5236 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5240.xsdst ! RUN = 5240 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_50_20_5244.xsdst ! RUN = 5244 ! NEVT = 999
