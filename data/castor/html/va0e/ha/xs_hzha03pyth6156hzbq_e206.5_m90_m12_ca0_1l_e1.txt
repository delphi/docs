*
*   Nickname     : xs_hzha03pyth6156hzbq_e206.5_m90_m12_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZBQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9993 events in 10 files time stamp: Thu Apr 18 15:11:03 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9127.xsdst ! RUN = 9127 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9131.xsdst ! RUN = 9131 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9135.xsdst ! RUN = 9135 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9139.xsdst ! RUN = 9139 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9143.xsdst ! RUN = 9143 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9147.xsdst ! RUN = 9147 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9151.xsdst ! RUN = 9151 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9155.xsdst ! RUN = 9155 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9159.xsdst ! RUN = 9159 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_12_9163.xsdst ! RUN = 9163 ! NEVT = 999
