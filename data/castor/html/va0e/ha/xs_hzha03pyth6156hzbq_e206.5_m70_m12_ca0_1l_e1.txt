*
*   Nickname     : xs_hzha03pyth6156hzbq_e206.5_m70_m12_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZBQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9039 events in 10 files time stamp: Wed Apr 17 18:10:54 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7127.xsdst ! RUN = 7127 ! NEVT = 43
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7131.xsdst ! RUN = 7131 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7135.xsdst ! RUN = 7135 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7139.xsdst ! RUN = 7139 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7143.xsdst ! RUN = 7143 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7147.xsdst ! RUN = 7147 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7151.xsdst ! RUN = 7151 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7155.xsdst ! RUN = 7155 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7159.xsdst ! RUN = 7159 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_70_12_7163.xsdst ! RUN = 7163 ! NEVT = 1000
