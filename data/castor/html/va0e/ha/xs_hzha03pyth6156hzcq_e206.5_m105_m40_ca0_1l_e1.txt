*
*   Nickname     : xs_hzha03pyth6156hzcq_e206.5_m105_m40_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9359 events in 10 files time stamp: Sat Apr 13 10:10:45 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10908.xsdst ! RUN = 10908 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10912.xsdst ! RUN = 10912 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10916.xsdst ! RUN = 10916 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10920.xsdst ! RUN = 10920 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10924.xsdst ! RUN = 10924 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10928.xsdst ! RUN = 10928 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10932.xsdst ! RUN = 10932 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10936.xsdst ! RUN = 10936 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10940.xsdst ! RUN = 10940 ! NEVT = 361
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_105_40_10944.xsdst ! RUN = 10944 ! NEVT = 999
