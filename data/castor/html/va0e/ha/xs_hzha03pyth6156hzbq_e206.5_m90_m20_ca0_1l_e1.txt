*
*   Nickname     : xs_hzha03pyth6156hzbq_e206.5_m90_m20_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZBQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9194 events in 10 files time stamp: Thu Apr 18 22:11:19 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9207.xsdst ! RUN = 9207 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9211.xsdst ! RUN = 9211 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9215.xsdst ! RUN = 9215 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9219.xsdst ! RUN = 9219 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9223.xsdst ! RUN = 9223 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9227.xsdst ! RUN = 9227 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9231.xsdst ! RUN = 9231 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9235.xsdst ! RUN = 9235 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9239.xsdst ! RUN = 9239 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzbq_206.5_90_20_9243.xsdst ! RUN = 9243 ! NEVT = 1000
