*
*   Nickname     : xs_hzha03pyth6156hzcq_e206.5_m90_m12_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZCQ/E206.5/CERN/SUMT/C001-10
*   Description  :  Extended Short DST simulation a0e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 9183 events in 10 files time stamp: Mon Apr 15 10:10:40 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9128.xsdst ! RUN = 9128 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9132.xsdst ! RUN = 9132 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9136.xsdst ! RUN = 9136 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9140.xsdst ! RUN = 9140 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9144.xsdst ! RUN = 9144 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9148.xsdst ! RUN = 9148 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9152.xsdst ! RUN = 9152 ! NEVT = 187
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9156.xsdst ! RUN = 9156 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9160.xsdst ! RUN = 9160 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/va0e/206.5/hzha03pyth6156_hzcq_206.5_90_12_9164.xsdst ! RUN = 9164 ! NEVT = 1000
