*
*   Nickname     : xs_evev_e206.7_m99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/EVEV/E206.7/MILANO/SUMT
*   Description  : ShortDST Grace  Extended Short DST simulation 99_a1 done at ecms=206.7 , Milano
*---
*   Comments     :  time stamp: Sat Jun 15 20:01:00 2002
*---
*
* 
*                * e+e- -> e v e v   
*                * GRC4F + JETSET 7.409 DELPHI tune ( TPC ok) 
*
*   Comments    : Post-Dst processing 
*
*
*
*          * (t-channel only - Crete definition)
*          * cross section =  .018 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/206.7/Y01757.25.al ! RUN = 1307-1312 ! NEVT=1800
*          * (s+t-channel cos |theta _e-| > .9999 )
*          * cross section =  .153 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/206.7/Y01757.37.al ! RUN = 1807-1812 ! NEVT=1800
*          * (t-channel only - Crete definition)
*          * cross section =  .051 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/206.7/EK5330.586.xsdst ! RUN = 3307-3312 ! NEVT=1800
*          * (s+t-channel cos |theta _e-| > .9999 )
*          * cross section =  .271 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/206.7/EK5330.589.xsdst ! RUN = 3807-3812 ! NEVT=1800
