*
*   Nickname     : xs_evev_e204.9_m99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/EVEV/E204.9/MILANO/SUMT
*   Description  : ShortDST Grace  Extended Short DST simulation 99_a1 done at ecms=204.9 , Milano
*---
*   Comments     :  time stamp: Sat Jun 15 20:01:00 2002
*---
*
* 
*                * e+e- -> e v e v   
*                * GRC4F + JETSET 7.409 DELPHI tune ( TPC ok) 
*
*   Comments    : Post-Dst processing 
*
*
*
*          * (t-channel only - Crete definition)
*          * cross section =  .017 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/204.9/Y01757.26.al ! RUN = 2301-2306 ! NEVT=1800
*          * (s+t-channel cos |theta _e-| > .9999 )
*          * cross section =  .153 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/204.9/Y01757.38.al ! RUN = 2801-2806 ! NEVT=1800
*          * (t-channel only - Crete definition)
*          * cross section =  .050 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/204.9/EK5330.587.xsdst ! RUN= 4301-4306 ! NEVT=1800
*          * (s+t-channel cos |theta _e-| > .9999 )
*          * cross section =  .271 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/204.9/EK5330.590.xsdst ! RUN= 4801-4806 ! NEVT=1800
*          * (s+t-channel cos |theta _e-| > .9999 )
*          * cross section =  .196 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/204.9/EK5330.591.xsdst ! RUN= 6801-6806 ! NEVT=1800
*          * (s+t-channel cos |theta _e-| > .9999 )
*          * cross section =  .200 pb
FILE=/castor/cern.ch/delphi/MCprod/milano/grc4f_postdst/evev/v99_4/204.9/EK5330.592.xsdst ! RUN= 7801-7806 ! NEVT=1800

