*
*   Nickname     : xs_hzha03pyth6156hgmm_e188.6_m42.5_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E188.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Dec  8 22:13:06 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_42.5_88259.xsdst ! RUN = 88259 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_42.5_88260.xsdst ! RUN = 88260 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_42.5_88261.xsdst ! RUN = 88261 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_42.5_88262.xsdst ! RUN = 88262 ! NEVT = 500
