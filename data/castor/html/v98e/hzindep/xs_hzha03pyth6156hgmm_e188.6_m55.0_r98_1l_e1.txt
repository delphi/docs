*
*   Nickname     : xs_hzha03pyth6156hgmm_e188.6_m55.0_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGMM/E188.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 1998 events in 4 files time stamp: Sat Dec  8 22:13:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_55.0_89509.xsdst ! RUN = 89509 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_55.0_89510.xsdst ! RUN = 89510 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_55.0_89511.xsdst ! RUN = 89511 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgmm_188.6_55.0_89512.xsdst ! RUN = 89512 ! NEVT = 498
