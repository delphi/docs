*
*   Nickname     : xs_hzha03pyth6156hcqq_e188.6_m100.0_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Jan 31 12:12:28 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_100.0_9891.xsdst ! RUN = 9891 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_100.0_9892.xsdst ! RUN = 9892 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_100.0_9893.xsdst ! RUN = 9893 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_100.0_9894.xsdst ! RUN = 9894 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_100.0_9895.xsdst ! RUN = 9895 ! NEVT = 400
