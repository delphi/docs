*
*   Nickname     : xs_hzha03pyth6156hcee_e188.6_m55.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E188.6/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 16 06:11:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_55.0_55504.xsdst ! RUN = 55504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_55.0_55505.xsdst ! RUN = 55505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_55.0_55506.xsdst ! RUN = 55506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_55.0_55507.xsdst ! RUN = 55507 ! NEVT = 497
