*
*   Nickname     : xs_hzha03pyth6156hgee_e188.6_m50.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E188.6/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1994 events in 4 files time stamp: Fri Nov 16 02:10:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_50.0_55008.xsdst ! RUN = 55008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_50.0_55009.xsdst ! RUN = 55009 ! NEVT = 496
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_50.0_55010.xsdst ! RUN = 55010 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_50.0_55011.xsdst ! RUN = 55011 ! NEVT = 500
