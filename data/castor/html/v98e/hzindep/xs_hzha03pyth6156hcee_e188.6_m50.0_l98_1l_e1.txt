*
*   Nickname     : xs_hzha03pyth6156hcee_e188.6_m50.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E188.6/LYON/SUMT/C001-3
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1499 events in 3 files time stamp: Fri Nov 16 02:10:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_50.0_55005.xsdst ! RUN = 55005 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_50.0_55006.xsdst ! RUN = 55006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_50.0_55007.xsdst ! RUN = 55007 ! NEVT = 500
