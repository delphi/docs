*
*   Nickname     : xs_hzha03pyth6156hsmm_e188.6_m102.5_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E188.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Dec  8 15:12:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_102.5_94251.xsdst ! RUN = 94251 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_102.5_94252.xsdst ! RUN = 94252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_102.5_94253.xsdst ! RUN = 94253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_102.5_94254.xsdst ! RUN = 94254 ! NEVT = 500
