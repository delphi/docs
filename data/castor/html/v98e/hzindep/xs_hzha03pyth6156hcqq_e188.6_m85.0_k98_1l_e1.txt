*
*   Nickname     : xs_hzha03pyth6156hcqq_e188.6_m85.0_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Jan 31 12:11:35 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_85.0_9861.xsdst ! RUN = 9861 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_85.0_9862.xsdst ! RUN = 9862 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_85.0_9863.xsdst ! RUN = 9863 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_85.0_9864.xsdst ! RUN = 9864 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_85.0_9865.xsdst ! RUN = 9865 ! NEVT = 400
