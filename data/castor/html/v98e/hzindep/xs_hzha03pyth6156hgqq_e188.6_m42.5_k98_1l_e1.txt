*
*   Nickname     : xs_hzha03pyth6156hgqq_e188.6_m42.5_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGQQ/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Jan 23 12:17:51 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgqq_188.6_42.5_8526.xsdst ! RUN = 8526 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgqq_188.6_42.5_8527.xsdst ! RUN = 8527 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgqq_188.6_42.5_8528.xsdst ! RUN = 8528 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgqq_188.6_42.5_8529.xsdst ! RUN = 8529 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgqq_188.6_42.5_8530.xsdst ! RUN = 8530 ! NEVT = 400
