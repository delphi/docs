*
*   Nickname     : xs_hzha03pyth6156hcnn_e188.6_m55.0_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCNN/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Tue Jan 22 18:16:47 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcnn_188.6_55.0_8396.xsdst ! RUN = 8396 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcnn_188.6_55.0_8397.xsdst ! RUN = 8397 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcnn_188.6_55.0_8398.xsdst ! RUN = 8398 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcnn_188.6_55.0_8399.xsdst ! RUN = 8399 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcnn_188.6_55.0_8400.xsdst ! RUN = 8400 ! NEVT = 400
