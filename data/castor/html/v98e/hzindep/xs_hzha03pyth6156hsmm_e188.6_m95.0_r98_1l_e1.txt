*
*   Nickname     : xs_hzha03pyth6156hsmm_e188.6_m95.0_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E188.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 1997 events in 4 files time stamp: Sat Dec  8 15:13:12 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_95.0_93501.xsdst ! RUN = 93501 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_95.0_93502.xsdst ! RUN = 93502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_95.0_93503.xsdst ! RUN = 93503 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_95.0_93504.xsdst ! RUN = 93504 ! NEVT = 498
