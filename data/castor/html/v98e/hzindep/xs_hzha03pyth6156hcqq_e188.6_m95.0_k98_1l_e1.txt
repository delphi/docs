*
*   Nickname     : xs_hzha03pyth6156hcqq_e188.6_m95.0_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Thu Jan 31 12:12:30 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_95.0_9881.xsdst ! RUN = 9881 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_95.0_9882.xsdst ! RUN = 9882 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_95.0_9883.xsdst ! RUN = 9883 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_95.0_9884.xsdst ! RUN = 9884 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_95.0_9885.xsdst ! RUN = 9885 ! NEVT = 400
