*
*   Nickname     : xs_hzha03pyth6156hcee_e188.6_m80.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E188.6/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1998 events in 4 files time stamp: Fri Nov 16 16:11:01 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_80.0_58004.xsdst ! RUN = 58004 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_80.0_58005.xsdst ! RUN = 58005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_80.0_58006.xsdst ! RUN = 58006 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_80.0_58007.xsdst ! RUN = 58007 ! NEVT = 500
