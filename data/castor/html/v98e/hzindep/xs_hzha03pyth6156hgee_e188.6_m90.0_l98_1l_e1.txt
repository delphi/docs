*
*   Nickname     : xs_hzha03pyth6156hgee_e188.6_m90.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E188.6/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1997 events in 4 files time stamp: Fri Nov 16 18:10:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_90.0_59008.xsdst ! RUN = 59008 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_90.0_59009.xsdst ! RUN = 59009 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_90.0_59010.xsdst ! RUN = 59010 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_90.0_59011.xsdst ! RUN = 59011 ! NEVT = 500
