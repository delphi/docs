*
*   Nickname     : xs_hzha03pyth6156hcee_e188.6_m47.5_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E188.6/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1996 events in 4 files time stamp: Thu Nov 15 18:12:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_47.5_54754.xsdst ! RUN = 54754 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_47.5_54755.xsdst ! RUN = 54755 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_47.5_54756.xsdst ! RUN = 54756 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_47.5_54757.xsdst ! RUN = 54757 ! NEVT = 499
