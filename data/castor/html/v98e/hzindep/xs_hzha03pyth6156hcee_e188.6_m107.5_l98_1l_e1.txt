*
*   Nickname     : xs_hzha03pyth6156hcee_e188.6_m107.5_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCEE/E188.6/LYON/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sat Nov 17 08:10:30 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_107.5_60754.xsdst ! RUN = 60754 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_107.5_60755.xsdst ! RUN = 60755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_107.5_60756.xsdst ! RUN = 60756 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcee_188.6_107.5_60757.xsdst ! RUN = 60757 ! NEVT = 499
