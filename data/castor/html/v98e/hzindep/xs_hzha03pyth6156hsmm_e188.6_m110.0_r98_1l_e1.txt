*
*   Nickname     : xs_hzha03pyth6156hsmm_e188.6_m110.0_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSMM/E188.6/RAL/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sat Dec  8 15:13:13 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_110.0_95001.xsdst ! RUN = 95001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_110.0_95002.xsdst ! RUN = 95002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_110.0_95003.xsdst ! RUN = 95003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsmm_188.6_110.0_95004.xsdst ! RUN = 95004 ! NEVT = 500
