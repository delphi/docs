*
*   Nickname     : xs_hzha03pyth6156hgnn_e188.6_m92.5_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 98e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Wed May 22 15:21:17 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119291.xsdst ! RUN = 119291 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119292.xsdst ! RUN = 119292 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119293.xsdst ! RUN = 119293 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119294.xsdst ! RUN = 119294 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119295.xsdst ! RUN = 119295 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119296.xsdst ! RUN = 119296 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119297.xsdst ! RUN = 119297 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119298.xsdst ! RUN = 119298 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119299.xsdst ! RUN = 119299 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_92.5_119300.xsdst ! RUN = 119300 ! NEVT = 500
