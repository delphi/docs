*
*   Nickname     : xs_hzha03pyth6156hsqq_e188.6_m80.0_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSQQ/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 1999 events in 5 files time stamp: Sun Jan 27 18:10:49 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsqq_188.6_80.0_9221.xsdst ! RUN = 9221 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsqq_188.6_80.0_9222.xsdst ! RUN = 9222 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsqq_188.6_80.0_9223.xsdst ! RUN = 9223 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsqq_188.6_80.0_9224.xsdst ! RUN = 9224 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsqq_188.6_80.0_9225.xsdst ! RUN = 9225 ! NEVT = 399
