*
*   Nickname     : xs_hzha03pyth6156hgnn_e188.6_m102.5_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGNN/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  : XShortDst HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Wed Jan 16 15:41:26 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_102.5_8181.xsdst ! RUN = 8181 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_102.5_8182.xsdst ! RUN = 8182 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_102.5_8183.xsdst ! RUN = 8183 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_102.5_8184.xsdst ! RUN = 8184 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgnn_188.6_102.5_8185.xsdst ! RUN = 8185 ! NEVT = 400
