*
*   Nickname     : xs_hzha03pyth6156hcqq_e188.6_m87.5_k98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCQQ/E188.6/KARLSRUHE/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , Karlsruhe
*---
*   Comments     : in total 2000 events in 5 files time stamp: Thu Jan 31 12:12:20 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_87.5_9866.xsdst ! RUN = 9866 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_87.5_9867.xsdst ! RUN = 9867 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_87.5_9868.xsdst ! RUN = 9868 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_87.5_9869.xsdst ! RUN = 9869 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcqq_188.6_87.5_9870.xsdst ! RUN = 9870 ! NEVT = 400
