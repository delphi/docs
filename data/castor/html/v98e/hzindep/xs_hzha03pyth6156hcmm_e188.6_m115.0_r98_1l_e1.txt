*
*   Nickname     : xs_hzha03pyth6156hcmm_e188.6_m115.0_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HCMM/E188.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Sat Dec  8 22:13:03 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcmm_188.6_115.0_95505.xsdst ! RUN = 95505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcmm_188.6_115.0_95506.xsdst ! RUN = 95506 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcmm_188.6_115.0_95507.xsdst ! RUN = 95507 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hcmm_188.6_115.0_95508.xsdst ! RUN = 95508 ! NEVT = 500
