*
*   Nickname     : xs_hzha03pyth6156hgee_e188.6_m107.5_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E188.6/LYON/SUMT/C001-3
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1499 events in 3 files time stamp: Sat Nov 17 10:11:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_107.5_60758.xsdst ! RUN = 60758 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_107.5_60759.xsdst ! RUN = 60759 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_107.5_60760.xsdst ! RUN = 60760 ! NEVT = 500
