*
*   Nickname     : xs_hzha03pyth6156hgee_e188.6_m40.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HGEE/E188.6/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1996 events in 4 files time stamp: Thu Nov 15 18:11:58 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_40.0_54008.xsdst ! RUN = 54008 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_40.0_54009.xsdst ! RUN = 54009 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_40.0_54010.xsdst ! RUN = 54010 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hgee_188.6_40.0_54011.xsdst ! RUN = 54011 ! NEVT = 499
