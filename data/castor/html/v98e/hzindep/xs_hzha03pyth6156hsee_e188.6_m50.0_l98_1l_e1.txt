*
*   Nickname     : xs_hzha03pyth6156hsee_e188.6_m50.0_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E188.6/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1996 events in 4 files time stamp: Thu Nov 15 18:11:54 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_50.0_55000.xsdst ! RUN = 55000 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_50.0_55001.xsdst ! RUN = 55001 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_50.0_55002.xsdst ! RUN = 55002 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_50.0_55003.xsdst ! RUN = 55003 ! NEVT = 497
