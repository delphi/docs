*
*   Nickname     : xs_hzha03pyth6156hsee_e188.6_m62.5_l98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HSEE/E188.6/LYON/SUMT/C001-4
*   Description  : XShortDst HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 98_e1 done at ecms=188.6 , Lyon
*---
*   Comments     : in total 1994 events in 4 files time stamp: Fri Nov 16 02:10:40 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_62.5_56250.xsdst ! RUN = 56250 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_62.5_56251.xsdst ! RUN = 56251 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_62.5_56252.xsdst ! RUN = 56252 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hsee_188.6_62.5_56253.xsdst ! RUN = 56253 ! NEVT = 499
