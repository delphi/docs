*   Nickname :     xs_hzha03pyth6156hzvc_e188.6_m4_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVC/E188.6/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu c c Extended Short DST simulation 98e  done at ecm=188.6 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Sat Aug 31 15:11:23 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hzvc_188.6_4_703.xsdst ! RUN = 703 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hzvc_188.6_4_704.xsdst ! RUN = 704 ! NEVT = 1000
