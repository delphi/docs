*   Nickname :     xs_hzha03pyth6156hzvg_e188.6_m20_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZVG/E188.6/CERN/SUMT/C001-2
*   Description :  HZHA03  e+e- --> hZ -> nu nu gluon gluon Extended Short DST simulation 98e  done at ecm=188.6 GeV , CERN
*---
*   Comments :     in total 2000 events in 2 files, time stamp: Fri Aug 30 22:10:42 2002
*---
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hzvg_188.6_20_2301.xsdst ! RUN = 2301 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hzvg_188.6_20_2302.xsdst ! RUN = 2302 ! NEVT = 1000
