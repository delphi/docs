*
*   Nickname     : xs_hzha03pyth6156ha4b_e188.6_m70_m90_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E188.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Nov 11 16:11:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100371.xsdst ! RUN = 100371 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100372.xsdst ! RUN = 100372 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100373.xsdst ! RUN = 100373 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100374.xsdst ! RUN = 100374 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100375.xsdst ! RUN = 100375 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100376.xsdst ! RUN = 100376 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100377.xsdst ! RUN = 100377 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100378.xsdst ! RUN = 100378 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100379.xsdst ! RUN = 100379 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_90_100380.xsdst ! RUN = 100380 ! NEVT = 499
