*
*   Nickname     : xs_hzha03pyth6156ha50_e188.6_m85_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA50/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Mon Oct  1 17:06:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58801.xsdst ! RUN = 58801 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58802.xsdst ! RUN = 58802 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58803.xsdst ! RUN = 58803 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58804.xsdst ! RUN = 58804 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58805.xsdst ! RUN = 58805 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58806.xsdst ! RUN = 58806 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58807.xsdst ! RUN = 58807 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58808.xsdst ! RUN = 58808 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58809.xsdst ! RUN = 58809 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha50_188.6_85_58810.xsdst ! RUN = 58810 ! NEVT = 500
