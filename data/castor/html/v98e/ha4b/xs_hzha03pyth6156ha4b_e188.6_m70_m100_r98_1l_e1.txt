*
*   Nickname     : xs_hzha03pyth6156ha4b_e188.6_m70_m100_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E188.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 5000 events in 10 files time stamp: Sun Nov 11 18:12:24 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100381.xsdst ! RUN = 100381 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100382.xsdst ! RUN = 100382 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100383.xsdst ! RUN = 100383 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100384.xsdst ! RUN = 100384 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100385.xsdst ! RUN = 100385 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100386.xsdst ! RUN = 100386 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100387.xsdst ! RUN = 100387 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100388.xsdst ! RUN = 100388 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100389.xsdst ! RUN = 100389 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_70_100_100390.xsdst ! RUN = 100390 ! NEVT = 500
