*
*   Nickname     : xs_hzha03pyth6156ha4b_e188.6_m50_m90_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E188.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 4995 events in 10 files time stamp: Tue Nov 13 04:14:26 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100261.xsdst ! RUN = 100261 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100262.xsdst ! RUN = 100262 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100263.xsdst ! RUN = 100263 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100264.xsdst ! RUN = 100264 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100265.xsdst ! RUN = 100265 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100266.xsdst ! RUN = 100266 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100267.xsdst ! RUN = 100267 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100268.xsdst ! RUN = 100268 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100269.xsdst ! RUN = 100269 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_90_100270.xsdst ! RUN = 100270 ! NEVT = 499
