*
*   Nickname     : xs_hzha03pyth6156ha4b_e188.6_m50_m130_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E188.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 4998 events in 10 files time stamp: Sun Nov 11 12:11:19 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100281.xsdst ! RUN = 100281 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100282.xsdst ! RUN = 100282 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100283.xsdst ! RUN = 100283 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100284.xsdst ! RUN = 100284 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100285.xsdst ! RUN = 100285 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100286.xsdst ! RUN = 100286 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100287.xsdst ! RUN = 100287 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100288.xsdst ! RUN = 100288 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100289.xsdst ! RUN = 100289 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_50_130_100290.xsdst ! RUN = 100290 ! NEVT = 500
