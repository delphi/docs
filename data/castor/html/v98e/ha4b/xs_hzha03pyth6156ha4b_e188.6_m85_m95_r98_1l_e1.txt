*
*   Nickname     : xs_hzha03pyth6156ha4b_e188.6_m85_m95_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4B/E188.6/RAL/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> hA -> 4b Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 4995 events in 10 files time stamp: Mon Nov 12 00:11:56 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100461.xsdst ! RUN = 100461 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100462.xsdst ! RUN = 100462 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100463.xsdst ! RUN = 100463 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100464.xsdst ! RUN = 100464 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100465.xsdst ! RUN = 100465 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100466.xsdst ! RUN = 100466 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100467.xsdst ! RUN = 100467 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100468.xsdst ! RUN = 100468 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100469.xsdst ! RUN = 100469 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4b_188.6_85_95_100470.xsdst ! RUN = 100470 ! NEVT = 498
