*   Nickname :     raw_kk2f4143mumu_e91.25_c98_1l_e
*   Generic Name : //CERN/DELPHI/P01_SIMD/RAW/KK2F4143MUMU/E91.25/CERN/SUMT/C001-20
*   Description :   raw data simulation raw data  done at ecm=91.25 GeV , CERN
*   Comments :     in total 200000 events in 20 files, time stamp: Sun Jul 11 12:41:8 2010
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50001.fadsim ! RUN = 50001 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50002.fadsim ! RUN = 50002 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50003.fadsim ! RUN = 50003 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50004.fadsim ! RUN = 50004 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50005.fadsim ! RUN = 50005 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50006.fadsim ! RUN = 50006 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50007.fadsim ! RUN = 50007 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50008.fadsim ! RUN = 50008 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50009.fadsim ! RUN = 50009 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50010.fadsim ! RUN = 50010 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50011.fadsim ! RUN = 50011 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50012.fadsim ! RUN = 50012 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50013.fadsim ! RUN = 50013 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50014.fadsim ! RUN = 50014 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50015.fadsim ! RUN = 50015 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50016.fadsim ! RUN = 50016 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50017.fadsim ! RUN = 50017 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50018.fadsim ! RUN = 50018 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50019.fadsim ! RUN = 50019 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v98e/91.25/kk2f4143_mumu_91.25_50020.fadsim ! RUN = 50020 ! NEVT = 10000
