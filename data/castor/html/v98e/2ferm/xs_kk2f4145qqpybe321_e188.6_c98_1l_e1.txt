*
*   Nickname     : xs_kk2f4145qqpybe321_e188.6_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4145QQPYBE321/E188.6/CERN/SUMT/C001-50
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 49980 events in 50 files time stamp: Sun Feb 17 22:11:42 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180001.xsdst ! RUN = 180001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180002.xsdst ! RUN = 180002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180003.xsdst ! RUN = 180003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180004.xsdst ! RUN = 180004 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180005.xsdst ! RUN = 180005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180006.xsdst ! RUN = 180006 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180007.xsdst ! RUN = 180007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180008.xsdst ! RUN = 180008 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180009.xsdst ! RUN = 180009 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180010.xsdst ! RUN = 180010 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180011.xsdst ! RUN = 180011 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180012.xsdst ! RUN = 180012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180013.xsdst ! RUN = 180013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180014.xsdst ! RUN = 180014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180015.xsdst ! RUN = 180015 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180016.xsdst ! RUN = 180016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180017.xsdst ! RUN = 180017 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180018.xsdst ! RUN = 180018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180019.xsdst ! RUN = 180019 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180020.xsdst ! RUN = 180020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180021.xsdst ! RUN = 180021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180022.xsdst ! RUN = 180022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180023.xsdst ! RUN = 180023 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180024.xsdst ! RUN = 180024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180025.xsdst ! RUN = 180025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180026.xsdst ! RUN = 180026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180027.xsdst ! RUN = 180027 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180028.xsdst ! RUN = 180028 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180029.xsdst ! RUN = 180029 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180030.xsdst ! RUN = 180030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180031.xsdst ! RUN = 180031 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180032.xsdst ! RUN = 180032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180033.xsdst ! RUN = 180033 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180034.xsdst ! RUN = 180034 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180035.xsdst ! RUN = 180035 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180036.xsdst ! RUN = 180036 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180037.xsdst ! RUN = 180037 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180038.xsdst ! RUN = 180038 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180039.xsdst ! RUN = 180039 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180040.xsdst ! RUN = 180040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180041.xsdst ! RUN = 180041 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180042.xsdst ! RUN = 180042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180043.xsdst ! RUN = 180043 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180044.xsdst ! RUN = 180044 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180045.xsdst ! RUN = 180045 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180046.xsdst ! RUN = 180046 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180047.xsdst ! RUN = 180047 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180048.xsdst ! RUN = 180048 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180049.xsdst ! RUN = 180049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v98e/188.6/kk2f4145_qqpybe321_188.6_180050.xsdst ! RUN = 180050 ! NEVT = 1000
