*
*   Nickname     : xs_qedbk23eegg_e188.0_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/QEDBK23EEGG/E188.0/SUMT
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.0 
*---
*   Comments     :  time stamp: Mon Nov  5 14:40:05 2001
*---
*
*
*         * e+ e- --> gamma + gamma (+ gamma) QED
*           Total cross-section = 114 pb
*           theta_1 > 15 degrees
*           visible cross-section = 10.68+-0.17 pb
*           QEDBK23: F.A.BERENDS AND R.KLEISS: "DISTRIBUTIONS FOR ELECTRON-
*           POSITRON ANNIHILATION INTO TWO AND THREE PHOTONS",
*           NUCL. PHYS. B186 (1981) 22.
*
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.22.xsdst ! RUN = -48435 -48436 -48437 -48438 -48439 -48440 -48441 -48442 -48443 -48444 -48445 ! NEVT = 6730  
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.23.xsdst ! RUN = -49519 -49520 -49521 -49522 -49523 -49524 -49525 -49526 -49527 -49528 -49529 -49530 -49531 -49532 -49533 ! NEVT = 7414 
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.24.xsdst ! RUN = -49534 -49535 -49536 -49537 -49538 -49539 -49540 -49541 -49542 -49543 -49544 -49545 -49546 -49547 -49548 ! NEVT = 9695
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.25.xsdst ! RUN = -49793 -49794 -49795 -49796 -49797 -49798 -49799 -49800 -49801 -49802 -49803  -49804 -49805 -49806 -49807 -49808 ! NEVT = 10938
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.26.xsdst ! RUN = -49809 -49810 -49811 -49812 -49813 -49814 -49815 ! NEVT = 4883
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.27.xsdst ! RUN = -422 -423 -424 -425 -426 -427 -428 -429 -430 -431 -432 -433 -434 ! NEVT = 7026
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.28.xsdst ! RUN = -435 -436 -437 -438 -439 -440 -441 -442 -443 -444 -445 -446 -447 ! NEVT = 8275 
FILE = /castor/cern.ch/delphi/MCprod/ral/qedbk23/eegg/v98e/188.0/EK5772.29.xsdst! RUN = -448 -449 -450 -451 -452 -453 -454 -455 -456 -457 -458 -459 -460 -461 ! NEVT = 7548 
