*
*   Nickname     : xs_teegg71cmpt_e189.0_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E189.0/SUMT
*   Description  : xshort dst Extended Short DST simulation  done at ecms=189.0 
*---
*   Comments     :  time stamp: Thu Jan 17 16:04:55 2002
*---
*
*           * Compton, e e --> e + e + gamma
*             theta_e > 10 degrees, theta_veto_e < 2 degrees,
*             theta_gamma > 10 degrees, E_e>1 GeV, E_gam > 1 GeV
*             x-sec = 53.43+-0.14pb  
* found 109576 events in 16 files
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891543.xsdst ! RUN =  -41383 -41384 -41385 -41386 -41387 -41388 -41389 -41390 -41391 -41392! NEVT = 6809
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891544.xsdst ! RUN =  -41393 -41394 -41395 -41396 -41397 -41398 -41399 -41400 -41401 -41402! NEVT = 6790
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891545.xsdst ! RUN =  -41403 -41404 -41405 -41406 -41407 -41408 -41409 -41410 -41411 -41412! NEVT = 6850
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891546.xsdst ! RUN =  -41413 -41414 -41415 -41416 -41417 -41418 -41419 -41420 -41421 -41422! NEVT = 6953
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891547.xsdst ! RUN =  -41423 -41424 -41425 -41426 -41427 -41428 -41429 -41430 -41431 -41432! NEVT = 6774
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891548.xsdst ! RUN =  -41433 -41434 -41435 -41436 -41437 -41438 -41439 -41440 -41441 -41442! NEVT = 6819
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891549.xsdst ! RUN =  -41443 -41444 -41445 -41446 -41447 -41448 -41449 -41450 -41451 -41452! NEVT = 6898
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891550.xsdst ! RUN =  -41453 -41454 -41455 -41456 -41457 -41458 -41459 -41460 -41461 -41462! NEVT = 6844
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891551.xsdst ! RUN =  -41463 -41464 -41465 -41466 -41467 -41468 -41469 -41470 -41471 -41472! NEVT = 6835
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891552.xsdst ! RUN =  -41473 -41474 -41475 -41476 -41477 -41478 -41479 -41480 -41481 -41482! NEVT = 6859
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891553.xsdst ! RUN =  -41483 -41484 -41485 -41486 -41487 -41488 -41489 -41490 -41491 -41492! NEVT = 6829
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891554.xsdst ! RUN =  -41493 -41494 -41495 -41496 -41497 -41498 -41499 -41500 -41501 -41502! NEVT = 6822
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891555.xsdst ! RUN =  -41503 -41504 -41505 -41506 -41507 -41508 -41509 -41510 -41511 -41512! NEVT = 6792
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891556.xsdst ! RUN =  -41513 -41514 -41515 -41516 -41517 -41518 -41519 -41520 -41521 -41522! NEVT = 6874
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891557.xsdst ! RUN =  -41523 -41524 -41525 -41526 -41527 -41528 -41529 -41530 -41531 -41532! NEVT = 6989
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v98e/189.0/TAP891558.xsdst ! RUN =  -41533 -41534 -41535 -41536 -41537 -41538 -41539 -41540 -41541 -41542! NEVT = 6839
