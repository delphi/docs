*
*   Nickname     : xs_kk2f4145qqpybe32_e91.25_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4145QQPYBE32/E91.25/RAL/SUMT/C001-50
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=91.25 , RAL
*---
*   Comments     : in total 148855 events in 50 files time stamp: Mon Feb 18 10:11:03 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17001.xsdst ! RUN = 17001 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17002.xsdst ! RUN = 17002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17003.xsdst ! RUN = 17003 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17004.xsdst ! RUN = 17004 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17005.xsdst ! RUN = 17005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17006.xsdst ! RUN = 17006 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17007.xsdst ! RUN = 17007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17008.xsdst ! RUN = 17008 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17009.xsdst ! RUN = 17009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17010.xsdst ! RUN = 17010 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17011.xsdst ! RUN = 17011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17012.xsdst ! RUN = 17012 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17013.xsdst ! RUN = 17013 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17014.xsdst ! RUN = 17014 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17015.xsdst ! RUN = 17015 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17016.xsdst ! RUN = 17016 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17017.xsdst ! RUN = 17017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17018.xsdst ! RUN = 17018 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17019.xsdst ! RUN = 17019 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17020.xsdst ! RUN = 17020 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17021.xsdst ! RUN = 17021 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17022.xsdst ! RUN = 17022 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17023.xsdst ! RUN = 17023 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17024.xsdst ! RUN = 17024 ! NEVT = 1868
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17025.xsdst ! RUN = 17025 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17026.xsdst ! RUN = 17026 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17027.xsdst ! RUN = 17027 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17028.xsdst ! RUN = 17028 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17029.xsdst ! RUN = 17029 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17030.xsdst ! RUN = 17030 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17031.xsdst ! RUN = 17031 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17032.xsdst ! RUN = 17032 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17033.xsdst ! RUN = 17033 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17034.xsdst ! RUN = 17034 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17035.xsdst ! RUN = 17035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17036.xsdst ! RUN = 17036 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17037.xsdst ! RUN = 17037 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17038.xsdst ! RUN = 17038 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17039.xsdst ! RUN = 17039 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17040.xsdst ! RUN = 17040 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17041.xsdst ! RUN = 17041 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17042.xsdst ! RUN = 17042 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17043.xsdst ! RUN = 17043 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17044.xsdst ! RUN = 17044 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17045.xsdst ! RUN = 17045 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17046.xsdst ! RUN = 17046 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17047.xsdst ! RUN = 17047 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17048.xsdst ! RUN = 17048 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17049.xsdst ! RUN = 17049 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/ral/kk2f4145/v98e/91.25/kk2f4145_qqpybe32_91.25_17050.xsdst ! RUN = 17050 ! NEVT = 3000
