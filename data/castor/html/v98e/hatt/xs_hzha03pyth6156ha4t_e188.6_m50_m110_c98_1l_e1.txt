*   Nickname :     xs_hzha03pyth6156ha4t_e188.6_m50_m110_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4T/E188.6/CERN/SUMT/C001-2
*   Description :   Extended Short DST simulation 98e  done at ecm=188.6 GeV , CERN
*   Comments :     in total 1999 events in 2 files, time stamp: Mon Nov 11 4:12:1 2002
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4t_188.6_50_110_6105.xsdst ! RUN = 6105 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ha4t_188.6_50_110_6106.xsdst ! RUN = 6106 ! NEVT = 1000
