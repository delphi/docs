*
*   Nickname     : xs_hzha03pyth6156hattbb_e188.6_m30_m30_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Mon Nov 12 22:24:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_30_30_39131.xsdst ! RUN = 39131 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_30_30_39132.xsdst ! RUN = 39132 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_30_30_39133.xsdst ! RUN = 39133 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_30_30_39134.xsdst ! RUN = 39134 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_30_30_39135.xsdst ! RUN = 39135 ! NEVT = 500
