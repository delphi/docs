*
*   Nickname     : xs_hzha03pyth6156habbtt_e188.6_m90_m90_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Tue Nov 13 02:14:10 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_90_90_38471.xsdst ! RUN = 38471 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_90_90_38472.xsdst ! RUN = 38472 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_90_90_38473.xsdst ! RUN = 38473 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_90_90_38474.xsdst ! RUN = 38474 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_90_90_38475.xsdst ! RUN = 38475 ! NEVT = 500
