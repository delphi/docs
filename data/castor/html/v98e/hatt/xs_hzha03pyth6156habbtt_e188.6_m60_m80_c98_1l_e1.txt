*
*   Nickname     : xs_hzha03pyth6156habbtt_e188.6_m60_m80_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2497 events in 5 files time stamp: Tue Nov 13 08:14:41 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_60_80_38311.xsdst ! RUN = 38311 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_60_80_38312.xsdst ! RUN = 38312 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_60_80_38313.xsdst ! RUN = 38313 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_60_80_38314.xsdst ! RUN = 38314 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_60_80_38315.xsdst ! RUN = 38315 ! NEVT = 500
