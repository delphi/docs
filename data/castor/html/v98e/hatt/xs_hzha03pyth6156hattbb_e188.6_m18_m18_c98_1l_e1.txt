*
*   Nickname     : xs_hzha03pyth6156hattbb_e188.6_m18_m18_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Mon Nov 12 18:15:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_18_18_39091.xsdst ! RUN = 39091 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_18_18_39092.xsdst ! RUN = 39092 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_18_18_39093.xsdst ! RUN = 39093 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_18_18_39094.xsdst ! RUN = 39094 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_18_18_39095.xsdst ! RUN = 39095 ! NEVT = 500
