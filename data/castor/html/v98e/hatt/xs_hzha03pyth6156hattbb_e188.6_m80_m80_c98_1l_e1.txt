*
*   Nickname     : xs_hzha03pyth6156hattbb_e188.6_m80_m80_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Tue Nov 13 00:16:59 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_80_39401.xsdst ! RUN = 39401 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_80_39402.xsdst ! RUN = 39402 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_80_39403.xsdst ! RUN = 39403 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_80_39404.xsdst ! RUN = 39404 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_80_39405.xsdst ! RUN = 39405 ! NEVT = 500
