*
*   Nickname     : xs_hzha03pyth6156hattbb_e188.6_m80_m90_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Tue Nov 13 12:15:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_90_39421.xsdst ! RUN = 39421 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_90_39422.xsdst ! RUN = 39422 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_90_39423.xsdst ! RUN = 39423 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_90_39424.xsdst ! RUN = 39424 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_80_90_39425.xsdst ! RUN = 39425 ! NEVT = 500
