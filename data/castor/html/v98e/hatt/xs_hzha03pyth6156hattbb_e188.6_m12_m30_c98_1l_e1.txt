*
*   Nickname     : xs_hzha03pyth6156hattbb_e188.6_m12_m30_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HATTBB/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Mon Nov 12 00:13:52 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_12_30_39011.xsdst ! RUN = 39011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_12_30_39012.xsdst ! RUN = 39012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_12_30_39013.xsdst ! RUN = 39013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_12_30_39014.xsdst ! RUN = 39014 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hattbb_188.6_12_30_39015.xsdst ! RUN = 39015 ! NEVT = 500
