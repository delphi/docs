*
*   Nickname     : xs_hzha03pyth6156habbtt_e188.6_m30_m40_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2499 events in 5 files time stamp: Mon Nov 12 22:25:22 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_40_38141.xsdst ! RUN = 38141 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_40_38142.xsdst ! RUN = 38142 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_40_38143.xsdst ! RUN = 38143 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_40_38144.xsdst ! RUN = 38144 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_40_38145.xsdst ! RUN = 38145 ! NEVT = 500
