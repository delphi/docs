*
*   Nickname     : xs_hzha03pyth6156habbtt_e188.6_m30_m50_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Mon Nov 12 22:24:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_50_38151.xsdst ! RUN = 38151 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_50_38152.xsdst ! RUN = 38152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_50_38153.xsdst ! RUN = 38153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_50_38154.xsdst ! RUN = 38154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_30_50_38155.xsdst ! RUN = 38155 ! NEVT = 500
