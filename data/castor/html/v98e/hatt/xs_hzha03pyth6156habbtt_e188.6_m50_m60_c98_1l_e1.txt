*
*   Nickname     : xs_hzha03pyth6156habbtt_e188.6_m50_m60_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HABBTT/E188.6/CERN/SUMT/C001-5
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 2500 events in 5 files time stamp: Tue Nov 13 00:17:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_50_60_38241.xsdst ! RUN = 38241 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_50_60_38242.xsdst ! RUN = 38242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_50_60_38243.xsdst ! RUN = 38243 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_50_60_38244.xsdst ! RUN = 38244 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_habbtt_188.6_50_60_38245.xsdst ! RUN = 38245 ! NEVT = 500
