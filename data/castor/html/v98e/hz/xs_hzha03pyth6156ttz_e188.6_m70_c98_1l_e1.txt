*
*   Nickname     : xs_hzha03pyth6156ttz_e188.6_m70_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Mon Oct  1 19:07:02 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47351.xsdst ! RUN = 47351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47352.xsdst ! RUN = 47352 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47353.xsdst ! RUN = 47353 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47354.xsdst ! RUN = 47354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47355.xsdst ! RUN = 47355 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47356.xsdst ! RUN = 47356 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47357.xsdst ! RUN = 47357 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47358.xsdst ! RUN = 47358 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47359.xsdst ! RUN = 47359 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_70_47360.xsdst ! RUN = 47360 ! NEVT = 500
