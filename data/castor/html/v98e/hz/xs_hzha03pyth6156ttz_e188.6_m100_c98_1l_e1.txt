*
*   Nickname     : xs_hzha03pyth6156ttz_e188.6_m100_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 4998 events in 10 files time stamp: Tue Oct  2 00:07:07 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50351.xsdst ! RUN = 50351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50352.xsdst ! RUN = 50352 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50353.xsdst ! RUN = 50353 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50354.xsdst ! RUN = 50354 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50355.xsdst ! RUN = 50355 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50356.xsdst ! RUN = 50356 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50357.xsdst ! RUN = 50357 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50358.xsdst ! RUN = 50358 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50359.xsdst ! RUN = 50359 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_100_50360.xsdst ! RUN = 50360 ! NEVT = 500
