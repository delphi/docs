*
*   Nickname     : xs_hzha03pyth6156ttz_e188.6_m60_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156TTZ/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Oct  1 14:06:36 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46351.xsdst ! RUN = 46351 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46352.xsdst ! RUN = 46352 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46353.xsdst ! RUN = 46353 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46354.xsdst ! RUN = 46354 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46355.xsdst ! RUN = 46355 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46356.xsdst ! RUN = 46356 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46357.xsdst ! RUN = 46357 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46358.xsdst ! RUN = 46358 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46359.xsdst ! RUN = 46359 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_ttz_188.6_60_46360.xsdst ! RUN = 46360 ! NEVT = 500
