*
*   Nickname     : xs_hzha03pyth6156hznu_e188.6_m40_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZNU/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar)(HZ+interference+fusion) Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Mon Oct  1 12:07:08 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44311.xsdst ! RUN = 44311 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44312.xsdst ! RUN = 44312 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44313.xsdst ! RUN = 44313 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44314.xsdst ! RUN = 44314 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44315.xsdst ! RUN = 44315 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44316.xsdst ! RUN = 44316 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44317.xsdst ! RUN = 44317 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44318.xsdst ! RUN = 44318 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44319.xsdst ! RUN = 44319 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hznu_188.6_40_44320.xsdst ! RUN = 44320 ! NEVT = 500
