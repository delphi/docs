*
*   Nickname     : xs_hzha03pyth6156hztt_e188.6_m85_c98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HZTT/E188.6/CERN/SUMT/C001-10
*   Description  : XShortDst HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 98_e1 done at ecms=188.6 , CERN
*---
*   Comments     : in total 4999 events in 10 files time stamp: Mon Oct  1 14:06:39 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48841.xsdst ! RUN = 48841 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48842.xsdst ! RUN = 48842 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48843.xsdst ! RUN = 48843 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48844.xsdst ! RUN = 48844 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48845.xsdst ! RUN = 48845 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48846.xsdst ! RUN = 48846 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48847.xsdst ! RUN = 48847 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48848.xsdst ! RUN = 48848 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48849.xsdst ! RUN = 48849 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hztt_188.6_85_48850.xsdst ! RUN = 48850 ! NEVT = 500
