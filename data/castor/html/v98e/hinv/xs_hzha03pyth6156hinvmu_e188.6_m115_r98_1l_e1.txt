*
*   Nickname     : xs_hzha03pyth6156hinvmu_e188.6_m115_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E188.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Sun Nov 25 12:14:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_115_51520.xsdst ! RUN = 51520 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_115_51521.xsdst ! RUN = 51521 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_115_51522.xsdst ! RUN = 51522 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_115_51523.xsdst ! RUN = 51523 ! NEVT = 500
