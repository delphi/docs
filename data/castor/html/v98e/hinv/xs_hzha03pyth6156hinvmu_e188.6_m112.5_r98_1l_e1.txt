*
*   Nickname     : xs_hzha03pyth6156hinvmu_e188.6_m112.5_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVMU/E188.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Tue Oct  9 20:20:10 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_112.5_51270.xsdst ! RUN = 51270 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_112.5_51271.xsdst ! RUN = 51271 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_112.5_51272.xsdst ! RUN = 51272 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvmu_188.6_112.5_51273.xsdst ! RUN = 51273 ! NEVT = 500
