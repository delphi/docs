*
*   Nickname     : xs_hzha03pyth6156hinvtt_e188.6_m70_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E188.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 1999 events in 4 files time stamp: Tue Oct  9 06:14:34 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_70_47030.xsdst ! RUN = 47030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_70_47031.xsdst ! RUN = 47031 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_70_47032.xsdst ! RUN = 47032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_70_47033.xsdst ! RUN = 47033 ! NEVT = 500
