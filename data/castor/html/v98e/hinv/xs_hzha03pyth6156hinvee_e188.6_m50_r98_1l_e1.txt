*
*   Nickname     : xs_hzha03pyth6156hinvee_e188.6_m50_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVEE/E188.6/RAL/SUMT/C001-10
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 4992 events in 10 files time stamp: Tue Oct  9 10:10:00 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45000.xsdst ! RUN = 45000 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45001.xsdst ! RUN = 45001 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45002.xsdst ! RUN = 45002 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45003.xsdst ! RUN = 45003 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45004.xsdst ! RUN = 45004 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45005.xsdst ! RUN = 45005 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45006.xsdst ! RUN = 45006 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45007.xsdst ! RUN = 45007 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45008.xsdst ! RUN = 45008 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvee_188.6_50_45009.xsdst ! RUN = 45009 ! NEVT = 498
