*
*   Nickname     : xs_hzha03pyth6156hinvqq_e188.6_m75_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVQQ/E188.6/RAL/SUMT/C001-9
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 4500 events in 9 files time stamp: Sun Nov 25 12:14:35 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47540.xsdst ! RUN = 47540 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47541.xsdst ! RUN = 47541 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47543.xsdst ! RUN = 47543 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47544.xsdst ! RUN = 47544 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47545.xsdst ! RUN = 47545 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47546.xsdst ! RUN = 47546 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47547.xsdst ! RUN = 47547 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47548.xsdst ! RUN = 47548 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvqq_188.6_75_47549.xsdst ! RUN = 47549 ! NEVT = 500
