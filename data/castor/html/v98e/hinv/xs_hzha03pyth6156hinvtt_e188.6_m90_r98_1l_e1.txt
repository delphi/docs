*
*   Nickname     : xs_hzha03pyth6156hinvtt_e188.6_m90_r98_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HINVTT/E188.6/RAL/SUMT/C001-4
*   Description  :  Extended Short DST simulation 98_e1 done at ecms=188.6 , RAL
*---
*   Comments     : in total 2000 events in 4 files time stamp: Tue Oct  9 20:20:17 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_90_49030.xsdst ! RUN = 49030 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_90_49031.xsdst ! RUN = 49031 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_90_49032.xsdst ! RUN = 49032 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/ral/hzha03pyth6156/v98e/188.6/hzha03pyth6156_hinvtt_188.6_90_49033.xsdst ! RUN = 49033 ! NEVT = 500
