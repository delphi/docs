*
*   Nickname     : xs_wwex202b1_e189_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WWEX202B1/CERN             JAN 2000 US/SUMT
*   Description  : Excalibur version 202 ww with full BE (MSTJ(53) = 0) Extended Short DST simulation 99_a1 189 , CERN             Jan 2000 US
*---
*   Comments     :  time stamp: Tue Feb 13 09:05:48 2001
*---
*
*                 MW = 80.35   
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11001.xsdst ! RUN = 11001 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11002.xsdst ! RUN = 11002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11003.xsdst ! RUN = 11003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11004.xsdst ! RUN = 11004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11005.xsdst ! RUN = 11005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11006.xsdst ! RUN = 11006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11007.xsdst ! RUN = 11007 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11008.xsdst ! RUN = 11008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11009.xsdst ! RUN = 11009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11010.xsdst ! RUN = 11010 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11011.xsdst ! RUN = 11011 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11012.xsdst ! RUN = 11012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11013.xsdst ! RUN = 11013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11014.xsdst ! RUN = 11014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11015.xsdst ! RUN = 11015 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11016.xsdst ! RUN = 11016 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11017.xsdst ! RUN = 11017 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11018.xsdst ! RUN = 11018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11019.xsdst ! RUN = 11019 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11020.xsdst ! RUN = 11020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11021.xsdst ! RUN = 11021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11022.xsdst ! RUN = 11022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11023.xsdst ! RUN = 11023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11024.xsdst ! RUN = 11024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11025.xsdst ! RUN = 11025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11026.xsdst ! RUN = 11026 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11027.xsdst ! RUN = 11027 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11028.xsdst ! RUN = 11028 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11029.xsdst ! RUN = 11029 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11030.xsdst ! RUN = 11030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11031.xsdst ! RUN = 11031 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11032.xsdst ! RUN = 11032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11033.xsdst ! RUN = 11033 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11034.xsdst ! RUN = 11034 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11035.xsdst ! RUN = 11035 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11036.xsdst ! RUN = 11036 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11037.xsdst ! RUN = 11037 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11038.xsdst ! RUN = 11038 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11039.xsdst ! RUN = 11039 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11040.xsdst ! RUN = 11040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11041.xsdst ! RUN = 11041 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11042.xsdst ! RUN = 11042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11043.xsdst ! RUN = 11043 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11044.xsdst ! RUN = 11044 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11045.xsdst ! RUN = 11045 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11046.xsdst ! RUN = 11046 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11047.xsdst ! RUN = 11047 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11048.xsdst ! RUN = 11048 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11049.xsdst ! RUN = 11049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b1/v99_4/189/run11050.xsdst ! RUN = 11050 ! NEVT = 1000
