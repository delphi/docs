*
*   Nickname     : xs_wwex202b2_e200_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WWEX202B2/CERN            JAN 2000 US/SUMT
*   Description  : Excalibur version 202 ww with BE (MSTJ(53) = 1) Extended Short DST simulation 99a1 200 , CERN            Jan 2000 US
*---
*   Comments     :  time stamp: Tue Feb 13 09:07:50 2001
*---
*
*                 MW = 80.35   
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11151.xsdst ! RUN = 11151 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11152.xsdst ! RUN = 11152 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11153.xsdst ! RUN = 11153 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11154.xsdst ! RUN = 11154 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11155.xsdst ! RUN = 11155 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11156.xsdst ! RUN = 11156 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11157.xsdst ! RUN = 11157 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11158.xsdst ! RUN = 11158 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11159.xsdst ! RUN = 11159 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11160.xsdst ! RUN = 11160 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11161.xsdst ! RUN = 11161 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11162.xsdst ! RUN = 11162 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11163.xsdst ! RUN = 11163 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11164.xsdst ! RUN = 11164 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11165.xsdst ! RUN = 11165 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11166.xsdst ! RUN = 11166 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11167.xsdst ! RUN = 11167 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11168.xsdst ! RUN = 11168 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11169.xsdst ! RUN = 11169 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11170.xsdst ! RUN = 11170 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11171.xsdst ! RUN = 11171 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11172.xsdst ! RUN = 11172 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11173.xsdst ! RUN = 11173 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11174.xsdst ! RUN = 11174 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11175.xsdst ! RUN = 11175 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11176.xsdst ! RUN = 11176 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11177.xsdst ! RUN = 11177 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11178.xsdst ! RUN = 11178 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11179.xsdst ! RUN = 11179 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11180.xsdst ! RUN = 11180 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11181.xsdst ! RUN = 11181 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11182.xsdst ! RUN = 11182 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11183.xsdst ! RUN = 11183 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11184.xsdst ! RUN = 11184 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11185.xsdst ! RUN = 11185 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11186.xsdst ! RUN = 11186 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11187.xsdst ! RUN = 11187 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11188.xsdst ! RUN = 11188 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11189.xsdst ! RUN = 11189 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11190.xsdst ! RUN = 11190 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11191.xsdst ! RUN = 11191 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11192.xsdst ! RUN = 11192 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11193.xsdst ! RUN = 11193 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11194.xsdst ! RUN = 11194 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11195.xsdst ! RUN = 11195 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11196.xsdst ! RUN = 11196 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11197.xsdst ! RUN = 11197 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11198.xsdst ! RUN = 11198 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11199.xsdst ! RUN = 11199 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wwex202b2/v99_4/200/run11200.xsdst ! RUN = 11200 ! NEVT = 998
