*   Nickname :     xs_bhwide101_e91.25_c99_1l_e1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/BHWIDE101/E91.25/CERN/SUMT/C001-50
*   Description :   Extended Short DST simulation 99e  done at ecm=91.25 GeV , CERN
*   Comments :     in total 149961 events in 50 files, time stamp: Sun Jul 11 12:41:7 2010
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50001.xsdst ! RUN = 50001 ! NEVT = 2996
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50002.xsdst ! RUN = 50002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50003.xsdst ! RUN = 50003 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50004.xsdst ! RUN = 50004 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50005.xsdst ! RUN = 50005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50006.xsdst ! RUN = 50006 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50007.xsdst ! RUN = 50007 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50008.xsdst ! RUN = 50008 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50009.xsdst ! RUN = 50009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50010.xsdst ! RUN = 50010 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50011.xsdst ! RUN = 50011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50012.xsdst ! RUN = 50012 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50013.xsdst ! RUN = 50013 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50014.xsdst ! RUN = 50014 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50015.xsdst ! RUN = 50015 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50016.xsdst ! RUN = 50016 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50017.xsdst ! RUN = 50017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50018.xsdst ! RUN = 50018 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50019.xsdst ! RUN = 50019 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50020.xsdst ! RUN = 50020 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50021.xsdst ! RUN = 50021 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50022.xsdst ! RUN = 50022 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50023.xsdst ! RUN = 50023 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50024.xsdst ! RUN = 50024 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50025.xsdst ! RUN = 50025 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50026.xsdst ! RUN = 50026 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50027.xsdst ! RUN = 50027 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50028.xsdst ! RUN = 50028 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50029.xsdst ! RUN = 50029 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50030.xsdst ! RUN = 50030 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50031.xsdst ! RUN = 50031 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50032.xsdst ! RUN = 50032 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50033.xsdst ! RUN = 50033 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50034.xsdst ! RUN = 50034 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50035.xsdst ! RUN = 50035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50036.xsdst ! RUN = 50036 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50037.xsdst ! RUN = 50037 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50038.xsdst ! RUN = 50038 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50039.xsdst ! RUN = 50039 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50040.xsdst ! RUN = 50040 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50041.xsdst ! RUN = 50041 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50042.xsdst ! RUN = 50042 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50043.xsdst ! RUN = 50043 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50044.xsdst ! RUN = 50044 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50045.xsdst ! RUN = 50045 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50046.xsdst ! RUN = 50046 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50047.xsdst ! RUN = 50047 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50048.xsdst ! RUN = 50048 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50049.xsdst ! RUN = 50049 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50050.xsdst ! RUN = 50050 ! NEVT = 3000
