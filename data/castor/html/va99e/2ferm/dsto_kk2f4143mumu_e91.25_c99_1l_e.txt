*   Nickname :     dsto_kk2f4143mumu_e91.25_c99_1l_e
*   Generic Name : //CERN/DELPHI/P01_SIMD/DSTO/KK2F4143MUMU/E91.25/CERN/SUMT/C001-20
*   Description :   Full DST/Delana output simulation 99e  done at ecm=91.25 GeV , CERN
*   Comments :     in total 199997 events in 20 files, time stamp: Sun Jul 11 12:41:6 2010
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50001.fadana ! RUN = 50001 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50002.fadana ! RUN = 50002 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50003.fadana ! RUN = 50003 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50004.fadana ! RUN = 50004 ! NEVT = 9999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50005.fadana ! RUN = 50005 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50006.fadana ! RUN = 50006 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50007.fadana ! RUN = 50007 ! NEVT = 9999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50008.fadana ! RUN = 50008 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50009.fadana ! RUN = 50009 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50010.fadana ! RUN = 50010 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50011.fadana ! RUN = 50011 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50012.fadana ! RUN = 50012 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50013.fadana ! RUN = 50013 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50014.fadana ! RUN = 50014 ! NEVT = 9999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50015.fadana ! RUN = 50015 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50016.fadana ! RUN = 50016 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50017.fadana ! RUN = 50017 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50018.fadana ! RUN = 50018 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50019.fadana ! RUN = 50019 ! NEVT = 10000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4143/v99e/91.25/kk2f4143_mumu_91.25_50020.fadana ! RUN = 50020 ! NEVT = 10000
