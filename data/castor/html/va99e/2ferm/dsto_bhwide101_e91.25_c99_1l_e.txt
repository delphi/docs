*   Nickname :     dsto_bhwide101_e91.25_c99_1l_e
*   Generic Name : //CERN/DELPHI/P01_SIMD/DSTO/BHWIDE101/E91.25/CERN/SUMT/C001-50
*   Description :   Full DST/Delana output simulation 99e  done at ecm=91.25 GeV , CERN
*   Comments :     in total 149961 events in 50 files, time stamp: Sun Jul 11 12:41:7 2010
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50001.fadana ! RUN = 50001 ! NEVT = 2996
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50002.fadana ! RUN = 50002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50003.fadana ! RUN = 50003 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50004.fadana ! RUN = 50004 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50005.fadana ! RUN = 50005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50006.fadana ! RUN = 50006 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50007.fadana ! RUN = 50007 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50008.fadana ! RUN = 50008 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50009.fadana ! RUN = 50009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50010.fadana ! RUN = 50010 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50011.fadana ! RUN = 50011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50012.fadana ! RUN = 50012 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50013.fadana ! RUN = 50013 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50014.fadana ! RUN = 50014 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50015.fadana ! RUN = 50015 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50016.fadana ! RUN = 50016 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50017.fadana ! RUN = 50017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50018.fadana ! RUN = 50018 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50019.fadana ! RUN = 50019 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50020.fadana ! RUN = 50020 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50021.fadana ! RUN = 50021 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50022.fadana ! RUN = 50022 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50023.fadana ! RUN = 50023 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50024.fadana ! RUN = 50024 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50025.fadana ! RUN = 50025 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50026.fadana ! RUN = 50026 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50027.fadana ! RUN = 50027 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50028.fadana ! RUN = 50028 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50029.fadana ! RUN = 50029 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50030.fadana ! RUN = 50030 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50031.fadana ! RUN = 50031 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50032.fadana ! RUN = 50032 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50033.fadana ! RUN = 50033 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50034.fadana ! RUN = 50034 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50035.fadana ! RUN = 50035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50036.fadana ! RUN = 50036 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50037.fadana ! RUN = 50037 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50038.fadana ! RUN = 50038 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50039.fadana ! RUN = 50039 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50040.fadana ! RUN = 50040 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50041.fadana ! RUN = 50041 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50042.fadana ! RUN = 50042 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50043.fadana ! RUN = 50043 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50044.fadana ! RUN = 50044 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50045.fadana ! RUN = 50045 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50046.fadana ! RUN = 50046 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50047.fadana ! RUN = 50047 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50048.fadana ! RUN = 50048 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50049.fadana ! RUN = 50049 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99e/91.25/bhwide101_91.25_50050.fadana ! RUN = 50050 ! NEVT = 3000
