# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
xs_kkmm414_e188.6_c99_1l_a1                     1998      v99a1      2FERM        100           99997      188.6          -          -       cern
xs_kkmm414_e200_c99_1l_a1                       1999      v99a1      2FERM        100           99996        200          -          -       cern
xs_kkqq414_e188.6_c99_1l_a1                     1998      v99a1      2FERM        100           99980      188.6          -          -       cern
xs_kkqq414_e200_c99_1l_a1                       1999      v99a1      2FERM        100           99984        200          -          -       cern
