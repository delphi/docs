*
*   Nickname     : dsto_hzha03pyth6156ha4g_e206_m120_m12_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99_a 206 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Sun Nov 11 09:30:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12121.fadana ! RUN = 12121 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12122.fadana ! RUN = 12122 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12123.fadana ! RUN = 12123 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12124.fadana ! RUN = 12124 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12125.fadana ! RUN = 12125 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12126.fadana ! RUN = 12126 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12127.fadana ! RUN = 12127 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_120_12_12128.fadana ! RUN = 12128 ! NEVT = 250
