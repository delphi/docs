*
*   Nickname     : xs_hzha03ha02_e202_m85_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03HA02/CERN/SUMT/C001-7
*   Description  :  Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     : in total 2299 events in 7 files time stamp: Mon Sep 10 18:10:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha0285_202_9509.xsdst ! RUN = 9509 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha0285_202_9510.xsdst ! RUN = 9510 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha0285_202_9511.xsdst ! RUN = 9511 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha0285_202_9512.xsdst ! RUN = 9512 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzha03_ha02_202_85_9519.xsdst ! RUN = 9519 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzha03_ha02_202_85_9520.xsdst ! RUN = 9520 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzha03_ha02_202_85_9521.xsdst ! RUN = 9521 ! NEVT = 500
