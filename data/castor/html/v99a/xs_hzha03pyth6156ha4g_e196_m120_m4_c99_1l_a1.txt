*
*   Nickname     : xs_hzha03pyth6156ha4g_e196_m120_m4_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Sun Nov 11 09:30:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12041.xsdst ! RUN = 12041 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12042.xsdst ! RUN = 12042 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12043.xsdst ! RUN = 12043 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12044.xsdst ! RUN = 12044 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12045.xsdst ! RUN = 12045 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12046.xsdst ! RUN = 12046 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12047.xsdst ! RUN = 12047 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_120_4_12048.xsdst ! RUN = 12048 ! NEVT = 250
