*
*   Nickname     : dsto_hzha03pyth6156ha4g_e206_m30_m12_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99_a 206 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Sun Nov 11 09:30:55 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3121.fadana ! RUN = 3121 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3122.fadana ! RUN = 3122 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3123.fadana ! RUN = 3123 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3124.fadana ! RUN = 3124 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3125.fadana ! RUN = 3125 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3126.fadana ! RUN = 3126 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3127.fadana ! RUN = 3127 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_30_12_3128.fadana ! RUN = 3128 ! NEVT = 250
