*
*   Nickname     : xs_hzha03pyth6156ha4g_e200_m130_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Sun Nov 11 09:30:55 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13121.xsdst ! RUN = 13121 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13122.xsdst ! RUN = 13122 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13123.xsdst ! RUN = 13123 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13124.xsdst ! RUN = 13124 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13125.xsdst ! RUN = 13125 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13126.xsdst ! RUN = 13126 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13127.xsdst ! RUN = 13127 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_130_12_13128.xsdst ! RUN = 13128 ! NEVT = 250
