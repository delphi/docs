*
*   Nickname     : dsto_hzha03pyth6156ha4g_e200_m170_m4_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99_a 200 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Sun Nov 11 09:30:43 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17041.fadana ! RUN = 17041 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17042.fadana ! RUN = 17042 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17043.fadana ! RUN = 17043 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17044.fadana ! RUN = 17044 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17045.fadana ! RUN = 17045 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17046.fadana ! RUN = 17046 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17047.fadana ! RUN = 17047 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_170_4_17048.fadana ! RUN = 17048 ! NEVT = 250
