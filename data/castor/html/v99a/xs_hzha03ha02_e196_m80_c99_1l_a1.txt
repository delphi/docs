*
*   Nickname     : xs_hzha03ha02_e196_m80_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03HA02/CERN/SUMT/C001-7
*   Description  :  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     : in total 2300 events in 7 files time stamp: Mon Sep 10 18:13:15 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha0280_196_9001.xsdst ! RUN = 9001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha0280_196_9002.xsdst ! RUN = 9002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha0280_196_9003.xsdst ! RUN = 9003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha0280_196_9004.xsdst ! RUN = 9004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzha03_ha02_196_80_9011.xsdst ! RUN = 9011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzha03_ha02_196_80_9012.xsdst ! RUN = 9012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzha03_ha02_196_80_9013.xsdst ! RUN = 9013 ! NEVT = 500
