*
*   Nickname     : dsto_hzha03pyth6156ha4g_e196_m50_m4_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99_a 196 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Sun Nov 11 09:30:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5041.fadana ! RUN = 5041 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5042.fadana ! RUN = 5042 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5043.fadana ! RUN = 5043 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5044.fadana ! RUN = 5044 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5045.fadana ! RUN = 5045 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5046.fadana ! RUN = 5046 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5047.fadana ! RUN = 5047 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_4_5048.fadana ! RUN = 5048 ! NEVT = 250
