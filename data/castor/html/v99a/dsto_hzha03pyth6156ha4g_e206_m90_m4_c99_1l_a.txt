*
*   Nickname     : dsto_hzha03pyth6156ha4g_e206_m90_m4_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99_a 206 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Sun Nov 11 09:30:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9041.fadana ! RUN = 9041 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9042.fadana ! RUN = 9042 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9043.fadana ! RUN = 9043 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9044.fadana ! RUN = 9044 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9045.fadana ! RUN = 9045 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9046.fadana ! RUN = 9046 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9047.fadana ! RUN = 9047 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA4g_206_90_4_9048.fadana ! RUN = 9048 ! NEVT = 250
