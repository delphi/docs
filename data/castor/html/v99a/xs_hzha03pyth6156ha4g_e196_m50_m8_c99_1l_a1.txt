*
*   Nickname     : xs_hzha03pyth6156ha4g_e196_m50_m8_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Sun Nov 11 09:30:44 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5081.xsdst ! RUN = 5081 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5082.xsdst ! RUN = 5082 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5083.xsdst ! RUN = 5083 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5084.xsdst ! RUN = 5084 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5085.xsdst ! RUN = 5085 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5086.xsdst ! RUN = 5086 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5087.xsdst ! RUN = 5087 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_50_8_5088.xsdst ! RUN = 5088 ! NEVT = 250
