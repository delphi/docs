*
*   Nickname     : xs_hzha03pyth6156ha4g_e196_m150_m8_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Sun Nov 11 09:30:47 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15081.xsdst ! RUN = 15081 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15082.xsdst ! RUN = 15082 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15083.xsdst ! RUN = 15083 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15084.xsdst ! RUN = 15084 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15085.xsdst ! RUN = 15085 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15086.xsdst ! RUN = 15086 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15087.xsdst ! RUN = 15087 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_150_8_15088.xsdst ! RUN = 15088 ! NEVT = 250
