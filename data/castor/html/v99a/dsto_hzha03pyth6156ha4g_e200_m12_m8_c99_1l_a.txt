*
*   Nickname     : dsto_hzha03pyth6156ha4g_e200_m12_m8_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99_a 200 , CERN
*---
*   Comments     : in total 1998 events in 8 files time stamp: Sun Nov 11 09:30:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1281.fadana ! RUN = 1281 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1282.fadana ! RUN = 1282 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1283.fadana ! RUN = 1283 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1284.fadana ! RUN = 1284 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1285.fadana ! RUN = 1285 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1286.fadana ! RUN = 1286 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1287.fadana ! RUN = 1287 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_12_8_1288.fadana ! RUN = 1288 ! NEVT = 250
