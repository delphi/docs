*
*   Nickname     : xs_hzha03ha02_e196_m100_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03HA02/CERN/SUMT/C001-7
*   Description  :  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     : in total 2300 events in 7 files time stamp: Mon Sep 10 18:13:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02100_196_11001.xsdst ! RUN = 11001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02100_196_11002.xsdst ! RUN = 11002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02100_196_11003.xsdst ! RUN = 11003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02100_196_11004.xsdst ! RUN = 11004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzha03_ha02_196_100_11011.xsdst ! RUN = 11011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzha03_ha02_196_100_11012.xsdst ! RUN = 11012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzha03_ha02_196_100_11013.xsdst ! RUN = 11013 ! NEVT = 500
