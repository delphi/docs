*
*   Nickname     : xs_hzha03ha02_e200_m85_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03HA02/CERN/SUMT/C001-7
*   Description  :  Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     : in total 2300 events in 7 files time stamp: Mon Sep 10 18:11:38 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0285_200_9505.xsdst ! RUN = 9505 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0285_200_9506.xsdst ! RUN = 9506 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0285_200_9507.xsdst ! RUN = 9507 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0285_200_9508.xsdst ! RUN = 9508 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzha03_ha02_200_85_9515.xsdst ! RUN = 9515 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzha03_ha02_200_85_9516.xsdst ! RUN = 9516 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzha03_ha02_200_85_9517.xsdst ! RUN = 9517 ! NEVT = 500
