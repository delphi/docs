*
*   Nickname     : dsto_hzha03pyth6156ha4g_e200_m80_m4_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA4G/CERN/SUMT/C001-4
*   Description  :  Full DST/Delana output simulation 99_a 200 , CERN
*---
*   Comments     : in total 999 events in 4 files time stamp: Sun Nov 11 09:30:53 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_80_4_8041.fadana ! RUN = 8041 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_80_4_8042.fadana ! RUN = 8042 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_80_4_8043.fadana ! RUN = 8043 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_80_4_8044.fadana ! RUN = 8044 ! NEVT = 250
