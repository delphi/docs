*
*   Nickname     : xs_hzha03pyth6156ha4g_e196_m100_m8_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Sun Nov 11 09:30:42 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10081.xsdst ! RUN = 10081 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10082.xsdst ! RUN = 10082 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10083.xsdst ! RUN = 10083 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10084.xsdst ! RUN = 10084 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10085.xsdst ! RUN = 10085 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10086.xsdst ! RUN = 10086 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10087.xsdst ! RUN = 10087 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA4g_196_100_8_10088.xsdst ! RUN = 10088 ! NEVT = 250
