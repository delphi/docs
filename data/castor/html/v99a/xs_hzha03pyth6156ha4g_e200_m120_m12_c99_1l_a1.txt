*
*   Nickname     : xs_hzha03pyth6156ha4g_e200_m120_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Sun Nov 11 09:30:57 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12121.xsdst ! RUN = 12121 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12122.xsdst ! RUN = 12122 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12123.xsdst ! RUN = 12123 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12124.xsdst ! RUN = 12124 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12125.xsdst ! RUN = 12125 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12126.xsdst ! RUN = 12126 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12127.xsdst ! RUN = 12127 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_12_12128.xsdst ! RUN = 12128 ! NEVT = 249
