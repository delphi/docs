*
*   Nickname     : xs_hzha03pyth6156ha4g_e200_m120_m8_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA4G/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Sun Nov 11 09:30:50 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12081.xsdst ! RUN = 12081 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12082.xsdst ! RUN = 12082 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12083.xsdst ! RUN = 12083 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12084.xsdst ! RUN = 12084 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12085.xsdst ! RUN = 12085 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12086.xsdst ! RUN = 12086 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12087.xsdst ! RUN = 12087 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA4g_200_120_8_12088.xsdst ! RUN = 12088 ! NEVT = 249
