*
*   Nickname     : xs_wphact211ncggtest_e206.5_m80.4_ca0_1l_e1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT211NCGGTEST/E206.5/CERN/SUMT/C001-40
*   Description  :  Extended Short DST simulation a0_e1 done at ecms=206.5 , CERN
*---
*   Comments     : in total 198957 events in 40 files time stamp: Sat Dec  8 15:36:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10001.xsdst ! RUN = 10001 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10002.xsdst ! RUN = 10002 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10003.xsdst ! RUN = 10003 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10004.xsdst ! RUN = 10004 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10005.xsdst ! RUN = 10005 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10006.xsdst ! RUN = 10006 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10007.xsdst ! RUN = 10007 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10008.xsdst ! RUN = 10008 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10009.xsdst ! RUN = 10009 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10010.xsdst ! RUN = 10010 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10011.xsdst ! RUN = 10011 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10012.xsdst ! RUN = 10012 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10013.xsdst ! RUN = 10013 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10014.xsdst ! RUN = 10014 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10015.xsdst ! RUN = 10015 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10016.xsdst ! RUN = 10016 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10017.xsdst ! RUN = 10017 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10018.xsdst ! RUN = 10018 ! NEVT = 3959
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10019.xsdst ! RUN = 10019 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10020.xsdst ! RUN = 10020 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10021.xsdst ! RUN = 10021 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10022.xsdst ! RUN = 10022 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10023.xsdst ! RUN = 10023 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10024.xsdst ! RUN = 10024 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10025.xsdst ! RUN = 10025 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10026.xsdst ! RUN = 10026 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10027.xsdst ! RUN = 10027 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10028.xsdst ! RUN = 10028 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10029.xsdst ! RUN = 10029 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10030.xsdst ! RUN = 10030 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10031.xsdst ! RUN = 10031 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10032.xsdst ! RUN = 10032 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10033.xsdst ! RUN = 10033 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10034.xsdst ! RUN = 10034 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10035.xsdst ! RUN = 10035 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10036.xsdst ! RUN = 10036 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10037.xsdst ! RUN = 10037 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10038.xsdst ! RUN = 10038 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10039.xsdst ! RUN = 10039 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact211/va0e/206.5/wphact211_ncggtest_206.5_80.4_10040.xsdst ! RUN = 10040 ! NEVT = 5000
