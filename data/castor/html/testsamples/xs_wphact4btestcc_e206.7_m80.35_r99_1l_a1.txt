*
*   Nickname     : xs_wphact4btestcc_e206.7_m80.35_r99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT4BTESTCC/E206.7/RAL/SUMT/C001-50
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , RAL
*---
*   Comments     : in total 49970 events in 50 files time stamp: Fri Aug 10 01:16:23 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10001.xsdst ! RUN = 10001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10002.xsdst ! RUN = 10002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10003.xsdst ! RUN = 10003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10004.xsdst ! RUN = 10004 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10005.xsdst ! RUN = 10005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10006.xsdst ! RUN = 10006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10007.xsdst ! RUN = 10007 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10008.xsdst ! RUN = 10008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10009.xsdst ! RUN = 10009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10010.xsdst ! RUN = 10010 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10011.xsdst ! RUN = 10011 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10012.xsdst ! RUN = 10012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10013.xsdst ! RUN = 10013 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10014.xsdst ! RUN = 10014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10015.xsdst ! RUN = 10015 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10016.xsdst ! RUN = 10016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10017.xsdst ! RUN = 10017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10018.xsdst ! RUN = 10018 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10019.xsdst ! RUN = 10019 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10020.xsdst ! RUN = 10020 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10021.xsdst ! RUN = 10021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10022.xsdst ! RUN = 10022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10023.xsdst ! RUN = 10023 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10024.xsdst ! RUN = 10024 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10025.xsdst ! RUN = 10025 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10026.xsdst ! RUN = 10026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10027.xsdst ! RUN = 10027 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10028.xsdst ! RUN = 10028 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10029.xsdst ! RUN = 10029 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10030.xsdst ! RUN = 10030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10031.xsdst ! RUN = 10031 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10032.xsdst ! RUN = 10032 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10033.xsdst ! RUN = 10033 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10034.xsdst ! RUN = 10034 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10035.xsdst ! RUN = 10035 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10036.xsdst ! RUN = 10036 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10037.xsdst ! RUN = 10037 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10038.xsdst ! RUN = 10038 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10039.xsdst ! RUN = 10039 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10040.xsdst ! RUN = 10040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10041.xsdst ! RUN = 10041 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10042.xsdst ! RUN = 10042 ! NEVT = 997
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10043.xsdst ! RUN = 10043 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10044.xsdst ! RUN = 10044 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10045.xsdst ! RUN = 10045 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10046.xsdst ! RUN = 10046 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10047.xsdst ! RUN = 10047 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10048.xsdst ! RUN = 10048 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10049.xsdst ! RUN = 10049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/ral/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10050.xsdst ! RUN = 10050 ! NEVT = 1000
