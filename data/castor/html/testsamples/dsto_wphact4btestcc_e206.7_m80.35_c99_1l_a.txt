*
*   Nickname     : dsto_wphact4btestcc_e206.7_m80.35_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/WPHACT4BTESTCC/E206.7/CERN/SUMT/C001-10
*   Description  :  Full DST/Delana output simulation 99_a done at ecms=206.7 , CERN
*---
*   Comments     : in total 9997 events in 10 files time stamp: Thu Aug 16 23:34:28 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10051.fadana ! RUN = 10051 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10052.fadana ! RUN = 10052 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10053.fadana ! RUN = 10053 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10054.fadana ! RUN = 10054 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10055.fadana ! RUN = 10055 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10056.fadana ! RUN = 10056 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10057.fadana ! RUN = 10057 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10058.fadana ! RUN = 10058 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10059.fadana ! RUN = 10059 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact4btest/v99_4/206.7/wphact4btest_cc_206.7_80.35_10060.fadana ! RUN = 10060 ! NEVT = 999
