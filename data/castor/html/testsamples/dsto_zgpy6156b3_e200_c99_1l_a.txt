*
*   Nickname     : dsto_zgpy6156b3_e200_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/ZGPY6156B3/CERN/SUMT/C001-100
*   Description  :  Full DST/Delana output simulation 99_a 200 , CERN
*---
*   Comments     : in total 99982 events in 100 files time stamp: Sat Jun 16 08:10:45 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20101.fadana ! RUN = 20101 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20102.fadana ! RUN = 20102 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20103.fadana ! RUN = 20103 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20104.fadana ! RUN = 20104 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20105.fadana ! RUN = 20105 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20106.fadana ! RUN = 20106 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20107.fadana ! RUN = 20107 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20108.fadana ! RUN = 20108 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20109.fadana ! RUN = 20109 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20110.fadana ! RUN = 20110 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20111.fadana ! RUN = 20111 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20112.fadana ! RUN = 20112 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20113.fadana ! RUN = 20113 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20114.fadana ! RUN = 20114 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20115.fadana ! RUN = 20115 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20116.fadana ! RUN = 20116 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20117.fadana ! RUN = 20117 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20118.fadana ! RUN = 20118 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20119.fadana ! RUN = 20119 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20120.fadana ! RUN = 20120 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20121.fadana ! RUN = 20121 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20122.fadana ! RUN = 20122 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20123.fadana ! RUN = 20123 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20124.fadana ! RUN = 20124 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20125.fadana ! RUN = 20125 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20126.fadana ! RUN = 20126 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20127.fadana ! RUN = 20127 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20128.fadana ! RUN = 20128 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20129.fadana ! RUN = 20129 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20130.fadana ! RUN = 20130 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20131.fadana ! RUN = 20131 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20132.fadana ! RUN = 20132 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20133.fadana ! RUN = 20133 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20134.fadana ! RUN = 20134 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20135.fadana ! RUN = 20135 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20136.fadana ! RUN = 20136 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20137.fadana ! RUN = 20137 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20138.fadana ! RUN = 20138 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20139.fadana ! RUN = 20139 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20140.fadana ! RUN = 20140 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20141.fadana ! RUN = 20141 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20142.fadana ! RUN = 20142 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20143.fadana ! RUN = 20143 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20144.fadana ! RUN = 20144 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20145.fadana ! RUN = 20145 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20146.fadana ! RUN = 20146 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20147.fadana ! RUN = 20147 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20148.fadana ! RUN = 20148 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20149.fadana ! RUN = 20149 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20150.fadana ! RUN = 20150 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20151.fadana ! RUN = 20151 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20152.fadana ! RUN = 20152 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20153.fadana ! RUN = 20153 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20154.fadana ! RUN = 20154 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20155.fadana ! RUN = 20155 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20156.fadana ! RUN = 20156 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20157.fadana ! RUN = 20157 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20158.fadana ! RUN = 20158 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20159.fadana ! RUN = 20159 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20160.fadana ! RUN = 20160 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20161.fadana ! RUN = 20161 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20162.fadana ! RUN = 20162 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20163.fadana ! RUN = 20163 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20164.fadana ! RUN = 20164 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20165.fadana ! RUN = 20165 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20166.fadana ! RUN = 20166 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20167.fadana ! RUN = 20167 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20168.fadana ! RUN = 20168 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20169.fadana ! RUN = 20169 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20170.fadana ! RUN = 20170 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20171.fadana ! RUN = 20171 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20172.fadana ! RUN = 20172 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20173.fadana ! RUN = 20173 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20174.fadana ! RUN = 20174 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20175.fadana ! RUN = 20175 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20176.fadana ! RUN = 20176 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20177.fadana ! RUN = 20177 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20178.fadana ! RUN = 20178 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20179.fadana ! RUN = 20179 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20180.fadana ! RUN = 20180 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20181.fadana ! RUN = 20181 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20182.fadana ! RUN = 20182 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20183.fadana ! RUN = 20183 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20184.fadana ! RUN = 20184 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20185.fadana ! RUN = 20185 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20186.fadana ! RUN = 20186 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20187.fadana ! RUN = 20187 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20188.fadana ! RUN = 20188 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20189.fadana ! RUN = 20189 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20190.fadana ! RUN = 20190 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20191.fadana ! RUN = 20191 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20192.fadana ! RUN = 20192 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20193.fadana ! RUN = 20193 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20194.fadana ! RUN = 20194 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20195.fadana ! RUN = 20195 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20196.fadana ! RUN = 20196 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20197.fadana ! RUN = 20197 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20198.fadana ! RUN = 20198 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20199.fadana ! RUN = 20199 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b3/v99_4/200/zgpy6156b3_200_20200.fadana ! RUN = 20200 ! NEVT = 1000
