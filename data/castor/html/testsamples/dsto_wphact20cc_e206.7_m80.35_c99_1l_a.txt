*
*   Nickname     : dsto_wphact20cc_e206.7_m80.35_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/WPHACT20CC/E206.7/CERN/SUMT/C001-2
*   Description  :  Full DST/Delana output simulation 99_a done at ecms=206.7 , CERN
*---
*   Comments     : in total 1998 events in 2 files time stamp: Mon Aug 20 02:22:51 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact20/v99_4/206.7/wphact20_cc_206.7_80.35_11840.fadana ! RUN = 11840 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact20/v99_4/206.7/wphact20_cc_206.7_80.35_11920.fadana ! RUN = 11920 ! NEVT = 998
