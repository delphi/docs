*
*   Nickname     : dsto_bhwide101_e200_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/BHWIDE101/CERN/SUMT
*   Description  :  Full DST/Delana output simulation 99_a 200 , CERN
*---
*   Comments     :  time stamp: Mon May 21 08:11:07 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10001.fadana ! RUN = 10001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10002.fadana ! RUN = 10002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10003.fadana ! RUN = 10003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10004.fadana ! RUN = 10004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10005.fadana ! RUN = 10005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10006.fadana ! RUN = 10006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10007.fadana ! RUN = 10007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10008.fadana ! RUN = 10008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10009.fadana ! RUN = 10009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10010.fadana ! RUN = 10010 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10051.fadana ! RUN = 10051 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10056.fadana ! RUN = 10056 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10021.fadana ! RUN = 10021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10013.fadana ! RUN = 10013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10032.fadana ! RUN = 10032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10033.fadana ! RUN = 10033 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10034.fadana ! RUN = 10034 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10042.fadana ! RUN = 10042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10073.fadana ! RUN = 10073 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10080.fadana ! RUN = 10080 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10011.fadana ! RUN = 10011 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10012.fadana ! RUN = 10012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10014.fadana ! RUN = 10014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10015.fadana ! RUN = 10015 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10029.fadana ! RUN = 10029 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10040.fadana ! RUN = 10040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10048.fadana ! RUN = 10048 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10054.fadana ! RUN = 10054 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10060.fadana ! RUN = 10060 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10069.fadana ! RUN = 10069 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10074.fadana ! RUN = 10074 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10075.fadana ! RUN = 10075 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10079.fadana ! RUN = 10079 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10087.fadana ! RUN = 10087 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10093.fadana ! RUN = 10093 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10022.fadana ! RUN = 10022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10023.fadana ! RUN = 10023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10024.fadana ! RUN = 10024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10025.fadana ! RUN = 10025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10030.fadana ! RUN = 10030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10036.fadana ! RUN = 10036 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10037.fadana ! RUN = 10037 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10038.fadana ! RUN = 10038 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10043.fadana ! RUN = 10043 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10044.fadana ! RUN = 10044 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10045.fadana ! RUN = 10045 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10047.fadana ! RUN = 10047 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10050.fadana ! RUN = 10050 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10052.fadana ! RUN = 10052 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10053.fadana ! RUN = 10053 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10057.fadana ! RUN = 10057 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10063.fadana ! RUN = 10063 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10065.fadana ! RUN = 10065 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10067.fadana ! RUN = 10067 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10070.fadana ! RUN = 10070 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10071.fadana ! RUN = 10071 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10072.fadana ! RUN = 10072 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10077.fadana ! RUN = 10077 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10078.fadana ! RUN = 10078 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10081.fadana ! RUN = 10081 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10083.fadana ! RUN = 10083 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10084.fadana ! RUN = 10084 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10085.fadana ! RUN = 10085 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10086.fadana ! RUN = 10086 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10096.fadana ! RUN = 10096 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10097.fadana ! RUN = 10097 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10026.fadana ! RUN = 10026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10027.fadana ! RUN = 10027 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10064.fadana ! RUN = 10064 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10066.fadana ! RUN = 10066 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10076.fadana ! RUN = 10076 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10088.fadana ! RUN = 10088 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10089.fadana ! RUN = 10089 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10092.fadana ! RUN = 10092 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10094.fadana ! RUN = 10094 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10095.fadana ! RUN = 10095 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10099.fadana ! RUN = 10099 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10100.fadana ! RUN = 10100 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10035.fadana ! RUN = 10035 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10039.fadana ! RUN = 10039 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10049.fadana ! RUN = 10049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10055.fadana ! RUN = 10055 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10058.fadana ! RUN = 10058 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10062.fadana ! RUN = 10062 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10082.fadana ! RUN = 10082 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10090.fadana ! RUN = 10090 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10028.fadana ! RUN = 10028 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10068.fadana ! RUN = 10068 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10091.fadana ! RUN = 10091 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10098.fadana ! RUN = 10098 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10059.fadana ! RUN = 10059 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10061.fadana ! RUN = 10061 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10018.fadana ! RUN = 10018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10016.fadana ! RUN = 10016 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10019.fadana ! RUN = 10019 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10041.fadana ! RUN = 10041 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10031.fadana ! RUN = 10031 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10020.fadana ! RUN = 10020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10017.fadana ! RUN = 10017 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/bhwide101/v99_4/200/bhwide101_200_10046.fadana ! RUN = 10046 ! NEVT = 1000
