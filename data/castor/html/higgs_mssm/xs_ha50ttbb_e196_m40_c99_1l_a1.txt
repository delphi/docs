*
*   Nickname     : xs_ha50ttbb_e196_m40_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:15 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35011.xsdst ! RUN = 35011 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35012.xsdst ! RUN = 35012 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35013.xsdst ! RUN = 35013 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35014.xsdst ! RUN = 35014 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35015.xsdst ! RUN = 35015 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35016.xsdst ! RUN = 35016 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35017.xsdst ! RUN = 35017 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35018.xsdst ! RUN = 35018 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35019.xsdst ! RUN = 35019 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb40_196_35020.xsdst ! RUN = 35020 ! NEVT = 200
