*
*   Nickname     : xs_ha02ttbb_e202_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA02TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA -> tautaubbbar, tan beta = 2 Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:28 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12231.xsdst ! RUN = 12231 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12232.xsdst ! RUN = 12232 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12233.xsdst ! RUN = 12233 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12234.xsdst ! RUN = 12234 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12235.xsdst ! RUN = 12235 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12236.xsdst ! RUN = 12236 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12237.xsdst ! RUN = 12237 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12238.xsdst ! RUN = 12238 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12239.xsdst ! RUN = 12239 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb12_202_12240.xsdst ! RUN = 12240 ! NEVT = 200
