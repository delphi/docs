*
*   Nickname     : xs_ha20ttbb_e191.6_m85_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20TTBB/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 20 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:47 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29501.xsdst ! RUN = 29501 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29502.xsdst ! RUN = 29502 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29503.xsdst ! RUN = 29503 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29504.xsdst ! RUN = 29504 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29505.xsdst ! RUN = 29505 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29506.xsdst ! RUN = 29506 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29507.xsdst ! RUN = 29507 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29508.xsdst ! RUN = 29508 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29509.xsdst ! RUN = 29509 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb85_191.6_29510.xsdst ! RUN = 29510 ! NEVT = 200
