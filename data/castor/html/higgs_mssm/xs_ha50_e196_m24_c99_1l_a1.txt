*
*   Nickname     : xs_ha50_e196_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:02 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32411.xsdst ! RUN = 32411 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32412.xsdst ! RUN = 32412 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32413.xsdst ! RUN = 32413 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32414.xsdst ! RUN = 32414 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32415.xsdst ! RUN = 32415 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32416.xsdst ! RUN = 32416 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32417.xsdst ! RUN = 32417 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32418.xsdst ! RUN = 32418 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32419.xsdst ! RUN = 32419 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha5024_196_32420.xsdst ! RUN = 32420 ! NEVT = 200
