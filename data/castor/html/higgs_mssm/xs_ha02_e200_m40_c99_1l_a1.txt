*
*   Nickname     : xs_ha02_e200_m40_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA02/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA -> 4b, tan beta = 2 Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:14 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14021.xsdst ! RUN = 14021 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14022.xsdst ! RUN = 14022 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14023.xsdst ! RUN = 14023 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14024.xsdst ! RUN = 14024 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14025.xsdst ! RUN = 14025 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14026.xsdst ! RUN = 14026 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14027.xsdst ! RUN = 14027 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14028.xsdst ! RUN = 14028 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14029.xsdst ! RUN = 14029 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha0240_200_14030.xsdst ! RUN = 14030 ! NEVT = 199
