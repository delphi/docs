*
*   Nickname     : xs_hzha03pyth6156ha6b_e199.5_m30_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03PYTH6156HA6B/E199.5/CERN/SUMT/C001-8
*   Description  :  Extended Short DST simulation 99a1 done at ecms=199.5 , CERN
*---
*   Comments     : in total 2000 events in 8 files time stamp: Thu Apr 18 22:10:09 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3126.xsdst ! RUN = 3126 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3127.xsdst ! RUN = 3127 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3128.xsdst ! RUN = 3128 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3129.xsdst ! RUN = 3129 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3130.xsdst ! RUN = 3130 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3131.xsdst ! RUN = 3131 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3132.xsdst ! RUN = 3132 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_30_12_3133.xsdst ! RUN = 3133 ! NEVT = 250
