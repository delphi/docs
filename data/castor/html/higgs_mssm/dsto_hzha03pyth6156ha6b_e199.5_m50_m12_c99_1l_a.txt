*
*   Nickname     : dsto_hzha03pyth6156ha6b_e199.5_m50_m12_c99_1l_a
*   Generic Name : //CERN/DELPHI/P02_SIMD/DSTO/HZHA03PYTH6156HA6B/E199.5/CERN/SUMT/C001-8
*   Description  :  Full DST/Delana output simulation 99a done at ecms=199.5 , CERN
*---
*   Comments     : in total 1999 events in 8 files time stamp: Thu Apr 18 22:10:09 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5126.fadana ! RUN = 5126 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5127.fadana ! RUN = 5127 ! NEVT = 249
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5128.fadana ! RUN = 5128 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5129.fadana ! RUN = 5129 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5130.fadana ! RUN = 5130 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5131.fadana ! RUN = 5131 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5132.fadana ! RUN = 5132 ! NEVT = 250
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/199.5/hzha03pyth6156_hA6b_199.5_50_12_5133.fadana ! RUN = 5133 ! NEVT = 250
