*
*   Nickname     : xs_ha50ttbb_e200_m40_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:23 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35021.xsdst ! RUN = 35021 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35022.xsdst ! RUN = 35022 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35023.xsdst ! RUN = 35023 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35024.xsdst ! RUN = 35024 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35025.xsdst ! RUN = 35025 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35026.xsdst ! RUN = 35026 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35027.xsdst ! RUN = 35027 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35028.xsdst ! RUN = 35028 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35029.xsdst ! RUN = 35029 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb40_200_35030.xsdst ! RUN = 35030 ! NEVT = 200
