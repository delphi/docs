*
*   Nickname     : xs_ha20_e196_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 20 Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:38 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25011.xsdst ! RUN = 25011 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25012.xsdst ! RUN = 25012 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25013.xsdst ! RUN = 25013 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25014.xsdst ! RUN = 25014 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25015.xsdst ! RUN = 25015 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25016.xsdst ! RUN = 25016 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25017.xsdst ! RUN = 25017 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25018.xsdst ! RUN = 25018 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25019.xsdst ! RUN = 25019 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha2050_196_25020.xsdst ! RUN = 25020 ! NEVT = 200
