*
*   Nickname     : xs_ha02_e191.6_m90_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA02/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA -> 4b, tan beta = 2 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:10 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19001.xsdst ! RUN = 19001 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19002.xsdst ! RUN = 19002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19003.xsdst ! RUN = 19003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19004.xsdst ! RUN = 19004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19005.xsdst ! RUN = 19005 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19006.xsdst ! RUN = 19006 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19007.xsdst ! RUN = 19007 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19008.xsdst ! RUN = 19008 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19009.xsdst ! RUN = 19009 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha0290_191.6_19010.xsdst ! RUN = 19010 ! NEVT = 200
