*
*   Nickname     : xs_ha02ttbb_e196_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA02TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA -> tautaubbbar, tan beta = 2 Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:24 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16011.xsdst ! RUN = 16011 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16012.xsdst ! RUN = 16012 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16013.xsdst ! RUN = 16013 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16014.xsdst ! RUN = 16014 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16015.xsdst ! RUN = 16015 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16016.xsdst ! RUN = 16016 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16017.xsdst ! RUN = 16017 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16018.xsdst ! RUN = 16018 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16019.xsdst ! RUN = 16019 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha02ttbb50_196_16020.xsdst ! RUN = 16020 ! NEVT = 200
