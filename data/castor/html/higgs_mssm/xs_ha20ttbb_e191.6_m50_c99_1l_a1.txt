*
*   Nickname     : xs_ha20ttbb_e191.6_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20TTBB/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 20 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:45 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26001.xsdst ! RUN = 26001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26002.xsdst ! RUN = 26002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26003.xsdst ! RUN = 26003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26004.xsdst ! RUN = 26004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26005.xsdst ! RUN = 26005 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26006.xsdst ! RUN = 26006 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26007.xsdst ! RUN = 26007 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26008.xsdst ! RUN = 26008 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26009.xsdst ! RUN = 26009 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha20ttbb50_191.6_26010.xsdst ! RUN = 26010 ! NEVT = 200
