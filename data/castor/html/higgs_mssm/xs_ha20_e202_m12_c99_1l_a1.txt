*
*   Nickname     : xs_ha20_e202_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 20 Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:41 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21231.xsdst ! RUN = 21231 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21232.xsdst ! RUN = 21232 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21233.xsdst ! RUN = 21233 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21234.xsdst ! RUN = 21234 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21235.xsdst ! RUN = 21235 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21236.xsdst ! RUN = 21236 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21237.xsdst ! RUN = 21237 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21238.xsdst ! RUN = 21238 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21239.xsdst ! RUN = 21239 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha2012_202_21240.xsdst ! RUN = 21240 ! NEVT = 200
