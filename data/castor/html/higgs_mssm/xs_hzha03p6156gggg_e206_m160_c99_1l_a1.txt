*
*   Nickname     : xs_hzha03p6156gggg_e206_m160_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156GGGG/CERN/SUMT
*   Description  : ShortDST HZHA03(Pythia 6.156)  e+e- --> hA ->4gluon, mh=160 GeV, different A  masses  Extended Short DST simulation 99_a1 206 , CERN
*---
*   Comments     :  time stamp: Mon Apr 23 13:48:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_15_16152.out.xsdst ! RUN = 16152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_15_16153.out.xsdst ! RUN = 16153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_15_16154.out.xsdst ! RUN = 16154 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_15_16155.out.xsdst ! RUN = 16155 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_20_16202.out.xsdst ! RUN = 16202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_20_16203.out.xsdst ! RUN = 16203 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_20_16204.out.xsdst ! RUN = 16204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_160_20_16205.out.xsdst ! RUN = 16205 ! NEVT = 499
