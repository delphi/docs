*
*   Nickname     : xs_ha50ttbb_e196_m80_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:19 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39011.xsdst ! RUN = 39011 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39012.xsdst ! RUN = 39012 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39013.xsdst ! RUN = 39013 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39014.xsdst ! RUN = 39014 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39015.xsdst ! RUN = 39015 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39016.xsdst ! RUN = 39016 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39017.xsdst ! RUN = 39017 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39018.xsdst ! RUN = 39018 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39019.xsdst ! RUN = 39019 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb80_196_39020.xsdst ! RUN = 39020 ! NEVT = 200
