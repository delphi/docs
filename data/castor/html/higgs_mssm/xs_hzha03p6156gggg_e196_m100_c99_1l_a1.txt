*
*   Nickname     : xs_hzha03p6156gggg_e196_m100_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156GGGG/CERN/SUMT
*   Description  : ShortDST HZHA03(Pythia 6.156)  e+e- --> hA ->4gluon, mh=100 GeV, different A  masses  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Apr 23 13:48:36 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_15_10152.out.xsdst ! RUN = 10152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_15_10153.out.xsdst ! RUN = 10153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_15_10154.out.xsdst ! RUN = 10154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_15_10155.out.xsdst ! RUN = 10155 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_20_10202.out.xsdst ! RUN = 10202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_20_10203.out.xsdst ! RUN = 10203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_20_10204.out.xsdst ! RUN = 10204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_20_10205.out.xsdst ! RUN = 10205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_25_10252.out.xsdst ! RUN = 10252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_25_10253.out.xsdst ! RUN = 10253 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_25_10254.out.xsdst ! RUN = 10254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_25_10255.out.xsdst ! RUN = 10255 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_30_10302.out.xsdst ! RUN = 10302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_30_10303.out.xsdst ! RUN = 10303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_30_10304.out.xsdst ! RUN = 10304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_30_10305.out.xsdst ! RUN = 10305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_40_10402.out.xsdst ! RUN = 10402 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_40_10403.out.xsdst ! RUN = 10403 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_40_10404.out.xsdst ! RUN = 10404 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_40_10405.out.xsdst ! RUN = 10405 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_50_10502.out.xsdst ! RUN = 10502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_50_10503.out.xsdst ! RUN = 10503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_50_10504.out.xsdst ! RUN = 10504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_50_10505.out.xsdst ! RUN = 10505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_60_10602.out.xsdst ! RUN = 10602 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_60_10603.out.xsdst ! RUN = 10603 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_60_10604.out.xsdst ! RUN = 10604 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_60_10605.out.xsdst ! RUN = 10605 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_70_10702.out.xsdst ! RUN = 10702 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_70_10703.out.xsdst ! RUN = 10703 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_70_10704.out.xsdst ! RUN = 10704 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_100_70_10705.out.xsdst ! RUN = 10705 ! NEVT = 497
