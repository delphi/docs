*
*   Nickname     : xs_hzha03p6156gggg_e206_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156GGGG/CERN/SUMT
*   Description  : ShortDST HZHA03(Pythia 6.156)  e+e- --> hA ->4gluon, mh=50 GeV, different A  masses  Extended Short DST simulation 99_a1 206 , CERN
*---
*   Comments     :  time stamp: Mon Apr 23 13:48:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_15_5152.out.xsdst ! RUN = 5152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_15_5153.out.xsdst ! RUN = 5153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_15_5154.out.xsdst ! RUN = 5154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_15_5155.out.xsdst ! RUN = 5155 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_20_5202.out.xsdst ! RUN = 5202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_20_5203.out.xsdst ! RUN = 5203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_20_5204.out.xsdst ! RUN = 5204 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_20_5205.out.xsdst ! RUN = 5205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_25_5252.out.xsdst ! RUN = 5252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_25_5253.out.xsdst ! RUN = 5253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_25_5254.out.xsdst ! RUN = 5254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_25_5255.out.xsdst ! RUN = 5255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_30_5302.out.xsdst ! RUN = 5302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_30_5303.out.xsdst ! RUN = 5303 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_30_5304.out.xsdst ! RUN = 5304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_30_5305.out.xsdst ! RUN = 5305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_40_5402.out.xsdst ! RUN = 5402 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_40_5403.out.xsdst ! RUN = 5403 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_40_5404.out.xsdst ! RUN = 5404 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_40_5405.out.xsdst ! RUN = 5405 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_50_5502.out.xsdst ! RUN = 5502 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_50_5503.out.xsdst ! RUN = 5503 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_50_5504.out.xsdst ! RUN = 5504 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_50_50_5505.out.xsdst ! RUN = 5505 ! NEVT = 500
