*
*   Nickname     : xs_ha50ttbb_e200_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:24 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38021.xsdst ! RUN = 38021 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38022.xsdst ! RUN = 38022 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38023.xsdst ! RUN = 38023 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38024.xsdst ! RUN = 38024 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38025.xsdst ! RUN = 38025 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38026.xsdst ! RUN = 38026 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38027.xsdst ! RUN = 38027 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38028.xsdst ! RUN = 38028 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38029.xsdst ! RUN = 38029 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha50ttbb70_200_38030.xsdst ! RUN = 38030 ! NEVT = 200
