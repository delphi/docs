*
*   Nickname     : xs_ha20ttbb_e200_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 20 Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:51 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23421.xsdst ! RUN = 23421 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23422.xsdst ! RUN = 23422 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23423.xsdst ! RUN = 23423 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23424.xsdst ! RUN = 23424 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23425.xsdst ! RUN = 23425 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23426.xsdst ! RUN = 23426 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23427.xsdst ! RUN = 23427 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23428.xsdst ! RUN = 23428 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23429.xsdst ! RUN = 23429 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha20ttbb24_200_23430.xsdst ! RUN = 23430 ! NEVT = 200
