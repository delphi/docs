*
*   Nickname     : xs_ha02ttbb_e202_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA02TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA -> tautaubbbar, tan beta = 2 Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:31 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18031.xsdst ! RUN = 18031 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18032.xsdst ! RUN = 18032 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18033.xsdst ! RUN = 18033 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18034.xsdst ! RUN = 18034 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18035.xsdst ! RUN = 18035 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18036.xsdst ! RUN = 18036 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18037.xsdst ! RUN = 18037 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18038.xsdst ! RUN = 18038 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18039.xsdst ! RUN = 18039 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha02ttbb70_202_18040.xsdst ! RUN = 18040 ! NEVT = 200
