*
*   Nickname     : xs_ha20_e191.6_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 20 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:34 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25001.xsdst ! RUN = 25001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25002.xsdst ! RUN = 25002 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25003.xsdst ! RUN = 25003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25004.xsdst ! RUN = 25004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25005.xsdst ! RUN = 25005 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25006.xsdst ! RUN = 25006 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25007.xsdst ! RUN = 25007 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25008.xsdst ! RUN = 25008 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25009.xsdst ! RUN = 25009 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2050_191.6_25010.xsdst ! RUN = 25010 ! NEVT = 200
