*
*   Nickname     : xs_ha50_e202_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 50 Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:08 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35031.xsdst ! RUN = 35031 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35032.xsdst ! RUN = 35032 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35033.xsdst ! RUN = 35033 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35034.xsdst ! RUN = 35034 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35035.xsdst ! RUN = 35035 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35036.xsdst ! RUN = 35036 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35037.xsdst ! RUN = 35037 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35038.xsdst ! RUN = 35038 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35039.xsdst ! RUN = 35039 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha5050_202_35040.xsdst ! RUN = 35040 ! NEVT = 200
