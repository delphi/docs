*
*   Nickname     : xs_hzha03p6156gggg_e196_m30_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156GGGG/CERN/SUMT
*   Description  : ShortDST HZHA03(Pythia 6.156)  e+e- --> hA ->4gluon, mh=30 GeV, different A  masses  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Apr 23 13:48:36 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_25_3252.out.xsdst ! RUN = 3252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_25_3253.out.xsdst ! RUN = 3253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_25_3254.out.xsdst ! RUN = 3254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_25_3255.out.xsdst ! RUN = 3255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_30_3302.out.xsdst ! RUN = 3302 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_30_3303.out.xsdst ! RUN = 3303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_30_3304.out.xsdst ! RUN = 3304 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hA_gggg_196_30_30_3305.out.xsdst ! RUN = 3305 ! NEVT = 500
