*
*   Nickname     : xs_ha50ttbb_e202_m30_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:27 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34031.xsdst ! RUN = 34031 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34032.xsdst ! RUN = 34032 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34033.xsdst ! RUN = 34033 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34034.xsdst ! RUN = 34034 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34035.xsdst ! RUN = 34035 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34036.xsdst ! RUN = 34036 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34037.xsdst ! RUN = 34037 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34038.xsdst ! RUN = 34038 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34039.xsdst ! RUN = 34039 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ha50ttbb30_202_34040.xsdst ! RUN = 34040 ! NEVT = 200
