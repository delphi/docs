*
*   Nickname     : xs_hzha03p6156gggg_e200_m110_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156GGGG/CERN/SUMT
*   Description  : ShortDST HZHA03(Pythia 6.156)  e+e- --> hA ->4gluon, mh=110 GeV, different A  masses  Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Apr 23 13:48:36 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_15_1.out.xsdst ! RUN = 11152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_15_2.out.xsdst ! RUN = 11153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_15_3.out.xsdst ! RUN = 11154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_15_4.out.xsdst ! RUN = 11155 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_20_1.out.xsdst ! RUN = 11202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_20_2.out.xsdst ! RUN = 11203 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_20_3.out.xsdst ! RUN = 11204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_20_4.out.xsdst ! RUN = 11205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_25_1.out.xsdst ! RUN = 11252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_25_2.out.xsdst ! RUN = 11253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_25_3.out.xsdst ! RUN = 11254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_25_4.out.xsdst ! RUN = 11255 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_30_1.out.xsdst ! RUN = 11302 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_30_2.out.xsdst ! RUN = 11303 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_30_3.out.xsdst ! RUN = 11304 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_30_4.out.xsdst ! RUN = 11305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_40_1.out.xsdst ! RUN = 11402 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_40_2.out.xsdst ! RUN = 11403 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_40_3.out.xsdst ! RUN = 11404 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_40_4.out.xsdst ! RUN = 11405 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_50_1.out.xsdst ! RUN = 11502 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_50_2.out.xsdst ! RUN = 11503 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_50_3.out.xsdst ! RUN = 11504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_50_4.out.xsdst ! RUN = 11505 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_60_1.out.xsdst ! RUN = 11602 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_60_2.out.xsdst ! RUN = 11603 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_60_3.out.xsdst ! RUN = 11604 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hA_gggg_200_110_60_4.out.xsdst ! RUN = 11605 ! NEVT = 498
