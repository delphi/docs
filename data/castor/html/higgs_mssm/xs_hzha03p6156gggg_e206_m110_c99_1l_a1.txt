*
*   Nickname     : xs_hzha03p6156gggg_e206_m110_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156GGGG/CERN/SUMT
*   Description  : ShortDST HZHA03(Pythia 6.156)  e+e- --> hA ->4gluon, mh=110 GeV, different A  masses  Extended Short DST simulation 99_a1 206 , CERN
*---
*   Comments     :  time stamp: Mon Apr 23 13:48:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_15_11152.out.xsdst ! RUN = 11152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_15_11153.out.xsdst ! RUN = 11153 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_15_11154.out.xsdst ! RUN = 11154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_15_11155.out.xsdst ! RUN = 11155 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_20_11202.out.xsdst ! RUN = 11202 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_20_11203.out.xsdst ! RUN = 11203 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_20_11204.out.xsdst ! RUN = 11204 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_20_11205.out.xsdst ! RUN = 11205 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_25_11252.out.xsdst ! RUN = 11252 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_25_11253.out.xsdst ! RUN = 11253 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_25_11254.out.xsdst ! RUN = 11254 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_25_11255.out.xsdst ! RUN = 11255 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_30_11302.out.xsdst ! RUN = 11302 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_30_11303.out.xsdst ! RUN = 11303 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_30_11304.out.xsdst ! RUN = 11304 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_30_11305.out.xsdst ! RUN = 11305 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_40_11402.out.xsdst ! RUN = 11402 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_40_11403.out.xsdst ! RUN = 11403 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_40_11404.out.xsdst ! RUN = 11404 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_40_11405.out.xsdst ! RUN = 11405 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_50_11502.out.xsdst ! RUN = 11502 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_50_11503.out.xsdst ! RUN = 11503 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_50_11504.out.xsdst ! RUN = 11504 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_50_11505.out.xsdst ! RUN = 11505 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_60_11602.out.xsdst ! RUN = 11602 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_60_11603.out.xsdst ! RUN = 11603 ! NEVT = 497
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_60_11604.out.xsdst ! RUN = 11604 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206/hzha03pyth6156_hA_gggg_206_110_60_11605.out.xsdst ! RUN = 11605 ! NEVT = 499
