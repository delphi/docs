*
*   Nickname     : xs_ha02ttbb_e191.6_m85_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA02TTBB/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA -> tautaubbbar, tan beta = 2 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:21 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19501.xsdst ! RUN = 19501 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19502.xsdst ! RUN = 19502 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19503.xsdst ! RUN = 19503 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19504.xsdst ! RUN = 19504 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19505.xsdst ! RUN = 19505 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19506.xsdst ! RUN = 19506 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19507.xsdst ! RUN = 19507 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19508.xsdst ! RUN = 19508 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19509.xsdst ! RUN = 19509 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha02ttbb85_191.6_19510.xsdst ! RUN = 19510 ! NEVT = 200
