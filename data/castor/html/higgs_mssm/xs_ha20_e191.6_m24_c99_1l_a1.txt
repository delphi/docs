*
*   Nickname     : xs_ha20_e191.6_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 20 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:32 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22401.xsdst ! RUN = 22401 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22402.xsdst ! RUN = 22402 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22403.xsdst ! RUN = 22403 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22404.xsdst ! RUN = 22404 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22405.xsdst ! RUN = 22405 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22406.xsdst ! RUN = 22406 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22407.xsdst ! RUN = 22407 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22408.xsdst ! RUN = 22408 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22409.xsdst ! RUN = 22409 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha2024_191.6_22410.xsdst ! RUN = 22410 ! NEVT = 200
