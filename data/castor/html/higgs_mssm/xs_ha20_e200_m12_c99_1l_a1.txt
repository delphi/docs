*
*   Nickname     : xs_ha20_e200_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA20/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->4b, tan beta = 20 Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:28:38 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21221.xsdst ! RUN = 21221 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21222.xsdst ! RUN = 21222 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21223.xsdst ! RUN = 21223 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21224.xsdst ! RUN = 21224 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21225.xsdst ! RUN = 21225 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21226.xsdst ! RUN = 21226 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21227.xsdst ! RUN = 21227 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21228.xsdst ! RUN = 21228 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21229.xsdst ! RUN = 21229 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ha2012_200_21230.xsdst ! RUN = 21230 ! NEVT = 200
