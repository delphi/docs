*
*   Nickname     : xs_ha50ttbb_e191.6_m90_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:12 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40001.xsdst ! RUN = 40001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40002.xsdst ! RUN = 40002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40003.xsdst ! RUN = 40003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40004.xsdst ! RUN = 40004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40005.xsdst ! RUN = 40005 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40006.xsdst ! RUN = 40006 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40007.xsdst ! RUN = 40007 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40008.xsdst ! RUN = 40008 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40009.xsdst ! RUN = 40009 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ha50ttbb90_191.6_40010.xsdst ! RUN = 40010 ! NEVT = 200
