*
*   Nickname     : xs_ha50ttbb_e196_m95_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HA50TTBB/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> hA ->tautaubbbar, tan beta = 50 Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:29:20 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40511.xsdst ! RUN = 40511 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40512.xsdst ! RUN = 40512 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40513.xsdst ! RUN = 40513 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40514.xsdst ! RUN = 40514 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40515.xsdst ! RUN = 40515 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40516.xsdst ! RUN = 40516 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40517.xsdst ! RUN = 40517 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40518.xsdst ! RUN = 40518 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40519.xsdst ! RUN = 40519 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ha50ttbb95_196_40520.xsdst ! RUN = 40520 ! NEVT = 200
