*
*   Nickname     : sh_lvqq_e091_r94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/LVQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; LVQQ cross-section = 0.094 +- 0.001 pb Short DST simulation 94_c2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Tue Nov 27 11:33:27 2001
*---
*
* 
*               * e+e- -> l nu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/lvqq/v94c/091.2/TAP891626.sdst ! RUN =  -41595 -41596 -41597      0      0      0      0      0      0      0! NEVT = 1050
