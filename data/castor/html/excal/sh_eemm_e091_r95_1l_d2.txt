*
*   Nickname     : sh_eemm_e091_r95_1l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/EEMM/E91.25/RAL/SUMT
*   Description  : ShortDST; EEMM cross-section = 2.78 +- 0.02 pb Short DST simulation 95_d2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Mon Jan  7 10:15:41 2002
*---
*
* 
*               * e+e- -> e e mu mu
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/eemm/v95d/091.2/TAP891715.sdst ! RUN =  -41681 -41682 -41683 -41684 -41685 -41686 -41687 -41688 -41689 ! NEVT = 3150
