*
*   Nickname     : sh_mmqq_e091_r92_2l_e2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/MMQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; MMQQ cross-section = 3.56 +- 0.02 pb Short DST simulation 92_e2 liq done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Fri Jan 11 10:12:14 2002
*---
*
* 
*               * e+e- -> mu mu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/mmqq/v92e/091.2/TAP891921.sdst ! RUN = -1875  -1876  -1877  -1878  -1879  -1880 ! NEVT = 2100
