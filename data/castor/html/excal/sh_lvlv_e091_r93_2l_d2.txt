*
*   Nickname     : sh_lvlv_e091_r93_2l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/LVLV/E91.25/RAL/SUMT
*   Description  : ShortDST; LVLV cross-section = 1.537 +- 0.011 pb Short DST simulation 93_d2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Wed Jan  9 09:13:50 2002
*---
*
* 
*               * e+e- -> l nu l nu
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/lvlv/v93d/091.2/TAP891786.sdst ! RUN =  -21738 -21739 -21740 ! NEVT = 1050
