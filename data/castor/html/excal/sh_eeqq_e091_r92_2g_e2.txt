*
*   Nickname     : sh_eeqq_e091_r92_2g_e2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/EEQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; EEQQ cross-section = 8.393 +- 0.045 pb Short DST simulation 92_e2 gas done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Fri Jan 11 11:25:21 2002
*---
*
* 
*               * e+e- -> e e q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/eeqq/v92e/091.2/TAP891871.sdst ! RUN = -1853  -1854  -1855  -1856  -1857  -1858  -1859  -1860  -1861 ! NEVT = 3149
