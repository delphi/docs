*
*   Nickname     : sh_vvqq_e091_r95_1l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/VVQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; VVQQ cross-section = 0.0269 +- 0.001 pb Short DST simulation 95_d2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Mon Jan  7 10:20:00 2002
*---
*
* 
*               * e+e- -> nu nu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/vvqq/v95d/091.2/TAP891713.sdst ! RUN = -41669 -41670 -41671 ! NEVT = 1050
