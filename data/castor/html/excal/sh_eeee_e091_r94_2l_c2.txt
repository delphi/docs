*
*   Nickname     : sh_eeee_e091_r94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/EEEE/E91.25/RAL/SUMT
*   Description  : ShortDST; EEEE cross-section = 2.54 +- 0.02 pb Short DST simulation 94_c2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Tue Nov 27 11:22:47 2001
*---
*
* 
*               * e+e- -> e e e e
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/eeee/v94c/091.2/TAP891622.sdst ! RUN =  -41559 -41560 -41561 -41562 -41563 -41564 -41565 -41566 -41567      0! NEVT = 3147
