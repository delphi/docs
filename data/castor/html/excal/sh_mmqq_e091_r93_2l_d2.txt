*
*   Nickname     : sh_mmqq_e091_r93_2l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/MMQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; MMQQ cross-section = 3.56 +- 0.02 pb Short DST simulation 93_d2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Wed Jan  9 09:07:45 2002
*---
*
* 
*               * e+e- -> mu mu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/mmqq/v93d/091.2/TAP891784.sdst ! RUN = -21723 -21724 -21725 -21726 -21727 -21728 -21729 -21730 -21731 ! NEVT = 2831
