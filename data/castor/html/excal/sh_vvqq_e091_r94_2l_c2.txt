*
*   Nickname     : sh_vvqq_e091_r94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/VVQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; VVQQ cross-section = 0.0269 +- 0.001 pb Short DST simulation 94_c2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Tue Nov 27 11:41:21 2001
*---
*
* 
*               * e+e- -> nu nu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/vvqq/v94c/091.2/TAP891631.sdst ! RUN =  -41616 -41617 -41618      0      0      0      0      0      0      0! NEVT = 1050
