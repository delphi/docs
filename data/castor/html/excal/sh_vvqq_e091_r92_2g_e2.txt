*
*   Nickname     : sh_vvqq_e091_r92_2g_e2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/VVQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; VVQQ cross-section = 0.0269 +- 0.001 pb Short DST simulation 92_e2 gas done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Fri Jan 11 10:55:01 2002
*---
*
* 
*               * e+e- -> nu nu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/vvqq/v92e/091.2/TAP891867.sdst ! RUN = -1823  -1824  -1826 ! NEVT = 1050
