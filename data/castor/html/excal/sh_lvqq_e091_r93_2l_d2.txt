*
*   Nickname     : sh_lvqq_e091_r93_2l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/LVQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; LVQQ cross-section = 0.0094 +- 0.0001 pb Short DST simulation 93_d2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Wed Jan  9 09:17:32 2002
*---
*
* 
*               * e+e- -> l nu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/lvqq/v93d/091.2/TAP891783.sdst ! RUN = -21720 -21721 -21722 ! NEVT = 1050
