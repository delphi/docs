*
*   Nickname     : sh_lvqq_e091_r92_2g_e2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/LVQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; LVQQ cross-section = 0.0094 +- 0.0001 pb Short DST simulation 92_e2 gas done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Fri Jan 11 09:57:29 2002
*---
*
* 
*               * e+e- -> l nu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/lvqq/v92e/091.2/TAP891862.sdst ! RUN =   -1794  -1795  -1796 ! NEVT = 1050
