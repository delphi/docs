*
*   Nickname     : sh_eeee_e091_r93_2l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/EEEE/E91.25/RAL/SUMT
*   Description  : ShortDST; EEEE cross-section = 2.54 +- 0.02 pb Short DST simulation 93_d2 done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Wed Jan  9 09:09:47 2002
*---
*
* 
*               * e+e- -> e e e e
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/eeee/v93d/091.2/TAP891789.sdst ! RUN = -21747 -21748 -21749 -21750 -21751 -21752 -21753 -21754 -21755 ! NEVT = 3145
