*
*   Nickname     : sh_ttqq_e091_r92_2l_e2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/TTQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; TTQQ cross-section = 0.538 +- 0.004 pb Short DST simulation 92_e2 liq done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Fri Jan 11 10:27:54 2002
*---
*
* 
*               * e+e- -> tau tau q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/ttqq/v92e/091.2/TAP891922.sdst ! RUN = -1881  -1882  -1883  -1884  -1885  -1886 ! NEVT = 2099
