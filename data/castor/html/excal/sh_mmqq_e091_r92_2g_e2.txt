*
*   Nickname     : sh_mmqq_e091_r92_2g_e2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/MMQQ/E91.25/RAL/SUMT
*   Description  : ShortDST; MMQQ cross-section = 3.56 +- 0.02 pb Short DST simulation 92_e2 gas done at ecms=91.25 , RAL
*---
*   Comments     :  time stamp: Fri Jan 11 10:14:55 2002
*---
*
* 
*               * e+e- -> mu mu q q
*               * EXCALIBUR & JETSET 7.409 Delphi Tune with Visible 4f cuts
*
*   Comments    : Post-Dst processing 
*
*
FILE = /castor/cern.ch/delphi/MCprod/ral/excal/mmqq/v92e/091.2/TAP891863.sdst ! RUN = -1797  -1798  -1799  -1800  -1801  -1802  -1803  -1804  -1805 ! NEVT = 3149
