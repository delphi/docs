# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
xs_wwex_e200_li99_fraga100_a1                   1999      v99a1      4FERM        289               0        200          -          -       live
xs_wwex_e200_li99_fraga700_a1                   1999      v99a1      4FERM        286               0        200          -          -       live
xs_wwex_e200_li99_lambda100_a1                  1999      v99a1      4FERM        279               0        200          -          -       live
xs_wwex_e200_li99_lambda500_a1                  1999      v99a1      4FERM        275               0        200          -          -       live
xs_wwex_e200_li99_qzero06534_a1                 1999      v99a1      4FERM        283               0        200          -          -       live
xs_wwex_e200_li99_qzero25_a1                    1999      v99a1      4FERM        282               0        200          -          -       live
xs_wwex_e200_li99_sigmaq200_a1                  1999      v99a1      4FERM        285               0        200          -          -       live
xs_wwex_e200_li99_sigmaq600_a1                  1999      v99a1      4FERM        276               0        200          -          -       live
xs_wwex_e200_li99_width1_a1                     1999      v99a1      4FERM        275               0        200          -          -       live
xs_wwex_e200_li99_width3_a1                     1999      v99a1      4FERM        249          124415        200          -          -       live
