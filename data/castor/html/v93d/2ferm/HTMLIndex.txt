# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
sh_apacic105_e91.25_wp93_2l_d2                  1993      v93d2      2FERM        300          870203      91.25          -          -       WUPP
sh_kk2f4146qqpy_e91.25_c93_2l_d2                1993      v93d2      2FERM        429         1235471      91.25          -          -       cern
sh_kk2f4146qqpy_e91.25_l93_2l_d2                1993      v93d2      2FERM         65          181081      91.25          -          -       lyon
sh_kk2f4146qqpybe321_e91.25_c93_2l_d2           1993      v93d2      2FERM        425         1233071      91.25          -          -       cern
sh_kk2f4146tthl_e91.25_c93_2l_d2                1993      v93d2      2FERM        110          541207      91.25          -          -       CERN
