*
*   Nickname     : xs_kk2f4144tthl_e91.25_c97_1l_g1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4144TTHL/E91.25/CERN/SUMT/C001-3
*   Description  :  Extended Short DST simulation 97_g1 done at ecms=91.25 , CERN
*---
*   Comments     : in total 14996 events in 3 files time stamp: Fri Jan 25 01:26:16 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v97g/91.25/kk2f4144_tthl_91.25_16001.xsdst ! RUN = 16001 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v97g/91.25/kk2f4144_tthl_91.25_16002.xsdst ! RUN = 16002 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4144/v97g/91.25/kk2f4144_tthl_91.25_16003.xsdst ! RUN = 16003 ! NEVT = 4998
