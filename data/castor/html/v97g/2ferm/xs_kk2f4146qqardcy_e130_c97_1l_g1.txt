*
*   Nickname     : xs_kk2f4146qqardcy_e130_c97_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4146QQARDCY/CERN/SUMT/C001-7
*   Description  :  Extended Short DST simulation 97g1 130 , CERN
*---
*   Comments     : in total 7000 events in 7 files time stamp: Thu Aug  1 22:12:10 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70000.xsdst ! RUN = 70000 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70001.xsdst ! RUN = 70001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70002.xsdst ! RUN = 70002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70003.xsdst ! RUN = 70003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70004.xsdst ! RUN = 70004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70005.xsdst ! RUN = 70005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/130/kk2f4146_qqardcy_130_70006.xsdst ! RUN = 70006 ! NEVT = 1000
