*
*   Nickname     : xs_koralz42kora_e183.5_l97_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KORALZ42KORA/E183.5/SUMT
*   Description  :  Extended Short DST simulation 97g1 done at ecms=183.5 
*---
*   Comments     :  time stamp: Mon Dec 17 14:59:17 2001
*---
*
*
*
*        * e+e- -> Z0 + gammas, Z0 -> tau+tau- KORALZ 4.2
*        * cross-section = (8.74+-0.02)pb
*          KORALZ, S. Jadach, B. Ward, Z. Was,
*          Comp Phys Comm (94)
*
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz42/kora/v97g/183.5/TAP891254.xsdst ! RUN =  -41230 -41231 -41232 -41233 -41234 -41235 -41236 -41237 ! NEVT = 6740
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz42/kora/v97g/183.5/TAP891255.xsdst ! RUN =  -41238 -41239 -41240 -41241 -41242 -41243 -41244 -41245 ! NEVT = 7776
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz42/kora/v97g/183.5/TAP891256.xsdst ! RUN =  -41246 -41247 -41248 -41249 -41250 -41251 -41252 -41253 ! NEVT = 7894
