*
*   Nickname     : xs_koralz402vvkz_e183.5_l97_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KORALZ402VVKZ/E183.5/SUMT
*   Description  :  Extended Short DST simulation 97g1 done at ecms=183.5 
*---
*   Comments     :  time stamp: Mon Dec 17 14:57:13 2001
*---
*
*
*         * e+e- -> nu nubar + gammas KORALZ 4.02
*         * cross-section = (11.74+-0.05)pb
*           KORALZ 4.02: S. Jadach, B. Ward, Z. Was,
*          Comp Phys Comm (94)   
*
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz402/vvkz/v97g/183.5/TAP891271.xsdst ! RUN =  -41257 -41258 -41259 -41260 -41261 -41262 -41263 ! NEVT = 13534
FILE = /castor/cern.ch/delphi/MCprod/ral/koralz402/vvkz/v97g/183.5/TAP891272.xsdst ! RUN =  -41264 -41265 -41266 -41267 -41268 -41269 -41270 ! NEVT = 13450
