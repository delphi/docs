*
*   Nickname     : xs_teegg71cmpt_e183.5_l97_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TEEGG71CMPT/E183.5/SUMT
*   Description  :  Extended Short DST simulation 97g1 done at ecms=183.5 
*---
*   Comments     :  time stamp: Mon Dec 17 15:03:46 2001
*---
*
*
*        * Compton, e e --> e + e + gamma
*            theta_e > 10 degrees, theta_veto_e < 2 degrees,
*            theta_gamma > 10 degrees, E_e>1 GeV, E_gam > 1 GeV
*            x-sec = (56.07+-0.10)pb
*            TEEGG7.1: Berends & Kleiss with m term
*            Nucl.Phys.B264(1986)265
*
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891375.xsdst ! RUN =  -41295 -41296 -41297 -41298 -41299 -41300 -41301 -41302 -41303 -41304 ! NEVT = 6982
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891376.xsdst ! RUN =  -41305 -41306 -41307 -41308 -41309 -41310 -41311 -41312 -41313 -41314 ! NEVT = 6996
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891377.xsdst ! RUN =  -41315 -41316 -41317 -41318 -41319 -41320 -41321 -41322 -41323 -41324 ! NEVT = 7021
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891378.xsdst ! RUN =  -41325 -41326 -41327 -41328 -41329 -41330 -41331 -41332 -41333 -41334 ! NEVT = 6957
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891379.xsdst ! RUN =  -41335 -41336 -41337 -41338 -41339 -41340 -41341 -41342 -41343 -41344 ! NEVT = 6975
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891380.xsdst ! RUN =  -41345 -41346 -41347 -41348 -41349 -41350 -41351 -41352 -41353 -41354 ! NEVT = 7058
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891381.xsdst ! RUN =  -41355 -41356 -41357 -41358 -41359 -41360 -41361 -41362 -41363 -41364 ! NEVT = 7012
FILE = /castor/cern.ch/delphi/MCprod/ral/teegg71/cmpt/v97g/183.5/TAP891382.xsdst ! RUN =  -41365 -41366 -41367 -41368 -41369 -41370 -41371 -41372 -41373 -41374 ! NEVT = 7037
