*
*   Nickname     : xs_bhwide101_e91.25_l97_1l_g1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/BHWIDE101/E91.25/LYON/SUMT/C001-34
*   Description  :  Extended Short DST simulation 97_g1 done at ecms=91.25 , Lyon
*---
*   Comments     : in total 33994 events in 34 files time stamp: Fri Dec  7 11:10:21 2001
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20101.xsdst ! RUN = 20101 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20102.xsdst ! RUN = 20102 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20103.xsdst ! RUN = 20103 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20104.xsdst ! RUN = 20104 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20105.xsdst ! RUN = 20105 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20106.xsdst ! RUN = 20106 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20107.xsdst ! RUN = 20107 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20108.xsdst ! RUN = 20108 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20109.xsdst ! RUN = 20109 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20110.xsdst ! RUN = 20110 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20111.xsdst ! RUN = 20111 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20112.xsdst ! RUN = 20112 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20113.xsdst ! RUN = 20113 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20114.xsdst ! RUN = 20114 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20115.xsdst ! RUN = 20115 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20116.xsdst ! RUN = 20116 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20117.xsdst ! RUN = 20117 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20118.xsdst ! RUN = 20118 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20119.xsdst ! RUN = 20119 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20120.xsdst ! RUN = 20120 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20121.xsdst ! RUN = 20121 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20122.xsdst ! RUN = 20122 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20123.xsdst ! RUN = 20123 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20124.xsdst ! RUN = 20124 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20125.xsdst ! RUN = 20125 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20126.xsdst ! RUN = 20126 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20127.xsdst ! RUN = 20127 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20128.xsdst ! RUN = 20128 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20129.xsdst ! RUN = 20129 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20130.xsdst ! RUN = 20130 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20131.xsdst ! RUN = 20131 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20132.xsdst ! RUN = 20132 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20133.xsdst ! RUN = 20133 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/lyon/bhwide101/v97g/91.25/bhwide101_91.25_20134.xsdst ! RUN = 20134 ! NEVT = 1000
