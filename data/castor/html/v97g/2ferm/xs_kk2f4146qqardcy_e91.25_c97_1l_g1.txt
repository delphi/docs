*
*   Nickname     : xs_kk2f4146qqardcy_e91.25_c97_1l_g1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4146QQARDCY/E91.25/CERN/SUMT/C001-70
*   Description  :  Extended Short DST simulation 97g1 done at ecms=91.25 , CERN
*---
*   Comments     : in total 202870 events in 70 files time stamp: Sun Jul 28 18:10:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61000.xsdst ! RUN = 61000 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61001.xsdst ! RUN = 61001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61002.xsdst ! RUN = 61002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61003.xsdst ! RUN = 61003 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61004.xsdst ! RUN = 61004 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61005.xsdst ! RUN = 61005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61006.xsdst ! RUN = 61006 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61007.xsdst ! RUN = 61007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61008.xsdst ! RUN = 61008 ! NEVT = 314
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61009.xsdst ! RUN = 61009 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61010.xsdst ! RUN = 61010 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61011.xsdst ! RUN = 61011 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61012.xsdst ! RUN = 61012 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61013.xsdst ! RUN = 61013 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61014.xsdst ! RUN = 61014 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61015.xsdst ! RUN = 61015 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61016.xsdst ! RUN = 61016 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61017.xsdst ! RUN = 61017 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61018.xsdst ! RUN = 61018 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61019.xsdst ! RUN = 61019 ! NEVT = 1312
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61020.xsdst ! RUN = 61020 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61022.xsdst ! RUN = 61022 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61023.xsdst ! RUN = 61023 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61024.xsdst ! RUN = 61024 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61025.xsdst ! RUN = 61025 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61026.xsdst ! RUN = 61026 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61027.xsdst ! RUN = 61027 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61028.xsdst ! RUN = 61028 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61029.xsdst ! RUN = 61029 ! NEVT = 1940
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61030.xsdst ! RUN = 61030 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61031.xsdst ! RUN = 61031 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61032.xsdst ! RUN = 61032 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61033.xsdst ! RUN = 61033 ! NEVT = 1318
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61034.xsdst ! RUN = 61034 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61035.xsdst ! RUN = 61035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61036.xsdst ! RUN = 61036 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61037.xsdst ! RUN = 61037 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61038.xsdst ! RUN = 61038 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61039.xsdst ! RUN = 61039 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61040.xsdst ! RUN = 61040 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61041.xsdst ! RUN = 61041 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61042.xsdst ! RUN = 61042 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61043.xsdst ! RUN = 61043 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61044.xsdst ! RUN = 61044 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61045.xsdst ! RUN = 61045 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61046.xsdst ! RUN = 61046 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61047.xsdst ! RUN = 61047 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61048.xsdst ! RUN = 61048 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61049.xsdst ! RUN = 61049 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61050.xsdst ! RUN = 61050 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61051.xsdst ! RUN = 61051 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61052.xsdst ! RUN = 61052 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61053.xsdst ! RUN = 61053 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61054.xsdst ! RUN = 61054 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61055.xsdst ! RUN = 61055 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61056.xsdst ! RUN = 61056 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61057.xsdst ! RUN = 61057 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61058.xsdst ! RUN = 61058 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61059.xsdst ! RUN = 61059 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61060.xsdst ! RUN = 61060 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61061.xsdst ! RUN = 61061 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61062.xsdst ! RUN = 61062 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61063.xsdst ! RUN = 61063 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61064.xsdst ! RUN = 61064 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61065.xsdst ! RUN = 61065 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61066.xsdst ! RUN = 61066 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61067.xsdst ! RUN = 61067 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61068.xsdst ! RUN = 61068 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61069.xsdst ! RUN = 61069 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v97g/91.25/kk2f4146_qqardcy_91.25_61070.xsdst ! RUN = 61070 ! NEVT = 3000
