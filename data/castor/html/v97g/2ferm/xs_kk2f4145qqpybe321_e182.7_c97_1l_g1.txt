*
*   Nickname     : xs_kk2f4145qqpybe321_e182.7_c97_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4145QQPYBE321/E182.7/CERN/SUMT/C001-50
*   Description  :  Extended Short DST simulation 97_g1 done at ecms=182.7 , CERN
*---
*   Comments     : in total 49369 events in 50 files time stamp: Fri Mar  1 12:14:15 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190001.xsdst ! RUN = 190001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190002.xsdst ! RUN = 190002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190003.xsdst ! RUN = 190003 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190004.xsdst ! RUN = 190004 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190005.xsdst ! RUN = 190005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190006.xsdst ! RUN = 190006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190007.xsdst ! RUN = 190007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190008.xsdst ! RUN = 190008 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190009.xsdst ! RUN = 190009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190010.xsdst ! RUN = 190010 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190011.xsdst ! RUN = 190011 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190012.xsdst ! RUN = 190012 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190013.xsdst ! RUN = 190013 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190014.xsdst ! RUN = 190014 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190015.xsdst ! RUN = 190015 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190016.xsdst ! RUN = 190016 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190017.xsdst ! RUN = 190017 ! NEVT = 385
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190018.xsdst ! RUN = 190018 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190019.xsdst ! RUN = 190019 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190020.xsdst ! RUN = 190020 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190021.xsdst ! RUN = 190021 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190022.xsdst ! RUN = 190022 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190023.xsdst ! RUN = 190023 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190024.xsdst ! RUN = 190024 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190025.xsdst ! RUN = 190025 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190026.xsdst ! RUN = 190026 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190027.xsdst ! RUN = 190027 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190028.xsdst ! RUN = 190028 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190029.xsdst ! RUN = 190029 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190030.xsdst ! RUN = 190030 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190031.xsdst ! RUN = 190031 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190032.xsdst ! RUN = 190032 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190033.xsdst ! RUN = 190033 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190034.xsdst ! RUN = 190034 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190035.xsdst ! RUN = 190035 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190036.xsdst ! RUN = 190036 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190037.xsdst ! RUN = 190037 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190038.xsdst ! RUN = 190038 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190039.xsdst ! RUN = 190039 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190040.xsdst ! RUN = 190040 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190041.xsdst ! RUN = 190041 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190042.xsdst ! RUN = 190042 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190043.xsdst ! RUN = 190043 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190044.xsdst ! RUN = 190044 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190045.xsdst ! RUN = 190045 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190046.xsdst ! RUN = 190046 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190047.xsdst ! RUN = 190047 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190048.xsdst ! RUN = 190048 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190049.xsdst ! RUN = 190049 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4145/v97g/182.7/kk2f4145_qqpybe321_182.7_190050.xsdst ! RUN = 190050 ! NEVT = 1000
