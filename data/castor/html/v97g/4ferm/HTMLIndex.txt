# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
dsto_wphact24cc_e182.7_mxxx_c97_1l_g            1997       v97g      4FERM        498          931662      182.7       80.4          -       CERN
xs_bdk01eeee_e182.7_r97_1l_g1                   1997      v97g1      4FERM        250          499278      182.7          -          -       ruth
xs_bdkeeeelt1gev_e182.7_c97_1l_g1               1997      v97g1      4FERM         10           50000      182.7          -          -       cern
xs_bdkrc02eemm_e182.7_r97_1l_g1                 1997      v97g1      4FERM        250         1796877      182.7          -          -       ruth
xs_bdkrc02eett_e182.7_r97_1l_g1                 1997      v97g1      4FERM        249         1477283      182.7          -          -       ruth
xs_bdkrceemmlt1gev_e182.7_c97_1l_g1             1997      v97g1      4FERM         10           50000      182.7          -          -       cern
xs_gpym6143wc0eeqq_e182.7_c97_1l_g1             1997      v97g1      4FERM         96          809046      182.7          -          -       cern
xs_wphact211ncgg_e182.7_mxxx_c97_1l_g1          1997      v97g1      4FERM        124         1010684      182.7       80.4          -       cern
xs_wphact21cc_e182.7_mxxx_l97_1l_g1             1997      v97g1      4FERM        489          488034      182.7       80.4          -       lyon
xs_wphact21nc4f_e182.7_mxxx_c97_1l_g1           1997      v97g1      4FERM        488          487616      182.7       80.4          -       cern
xs_wphact22ccbe32a_e182.7_mxxx_c97_1l_g1        1997      v97g1      4FERM         48           94879      182.7       80.4          -       cern
xs_wphact22ccbe32i_e182.7_mxxx_c97_1l_g1        1997      v97g1      4FERM         22           43988      182.7       80.4          -       cern
xs_wphact24cc_e182.7_mxxx_c97_1l_g1             1997      v97g1      4FERM        493          920121      182.7       80.4          -       CERN
