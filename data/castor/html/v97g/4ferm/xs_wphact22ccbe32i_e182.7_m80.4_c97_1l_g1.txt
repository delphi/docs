*
*   Nickname     : xs_wphact22ccbe32i_e182.7_m80.4_c97_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/WPHACT22CCBE32I/E182.7/CERN/SUMT/C001-22
*   Description  :  Extended Short DST simulation 97g1 done at ecms=182.7 , CERN
*---
*   Comments     : in total 43988 events in 22 files time stamp: Thu Mar  7 15:21:58 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190201.xsdst ! RUN = 190201 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190203.xsdst ! RUN = 190203 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190204.xsdst ! RUN = 190204 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190206.xsdst ! RUN = 190206 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190207.xsdst ! RUN = 190207 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190208.xsdst ! RUN = 190208 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190210.xsdst ! RUN = 190210 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190211.xsdst ! RUN = 190211 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190212.xsdst ! RUN = 190212 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190213.xsdst ! RUN = 190213 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190214.xsdst ! RUN = 190214 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190215.xsdst ! RUN = 190215 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190216.xsdst ! RUN = 190216 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190217.xsdst ! RUN = 190217 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190218.xsdst ! RUN = 190218 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190219.xsdst ! RUN = 190219 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190220.xsdst ! RUN = 190220 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190221.xsdst ! RUN = 190221 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190222.xsdst ! RUN = 190222 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190223.xsdst ! RUN = 190223 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190224.xsdst ! RUN = 190224 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact22/v97g/182.7/wphact22_ccbe32i_182.7_80.4_190225.xsdst ! RUN = 190225 ! NEVT = 2000
