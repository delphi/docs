*
*   Nickname     : xs_kk2f4146qqardcy_e91.25_c96_1l_g1
*   Generic Name : //CERN/DELPHI/P01_SIMD/XSDST/KK2F4146QQARDCY/E91.25/CERN/SUMT/C001-37
*   Description  :  Extended Short DST simulation 96g1 done at ecms=91.25 , CERN
*---
*   Comments     : in total 110990 events in 37 files time stamp: Sun Jul 28 06:13:32 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60000.xsdst ! RUN = 60000 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60001.xsdst ! RUN = 60001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60002.xsdst ! RUN = 60002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60003.xsdst ! RUN = 60003 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60004.xsdst ! RUN = 60004 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60005.xsdst ! RUN = 60005 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60006.xsdst ! RUN = 60006 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60007.xsdst ! RUN = 60007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60008.xsdst ! RUN = 60008 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60009.xsdst ! RUN = 60009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60010.xsdst ! RUN = 60010 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60011.xsdst ! RUN = 60011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60012.xsdst ! RUN = 60012 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60013.xsdst ! RUN = 60013 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60014.xsdst ! RUN = 60014 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60015.xsdst ! RUN = 60015 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60016.xsdst ! RUN = 60016 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60017.xsdst ! RUN = 60017 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60018.xsdst ! RUN = 60018 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60019.xsdst ! RUN = 60019 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60020.xsdst ! RUN = 60020 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60021.xsdst ! RUN = 60021 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60022.xsdst ! RUN = 60022 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60023.xsdst ! RUN = 60023 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60024.xsdst ! RUN = 60024 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60025.xsdst ! RUN = 60025 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60026.xsdst ! RUN = 60026 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60027.xsdst ! RUN = 60027 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60028.xsdst ! RUN = 60028 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60029.xsdst ! RUN = 60029 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60030.xsdst ! RUN = 60030 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60031.xsdst ! RUN = 60031 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60032.xsdst ! RUN = 60032 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60033.xsdst ! RUN = 60033 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60034.xsdst ! RUN = 60034 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60035.xsdst ! RUN = 60035 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/91.25/kk2f4146_qqardcy_91.25_60036.xsdst ! RUN = 60036 ! NEVT = 3000
