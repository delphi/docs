*
*   Nickname     : xs_kk2f4146qqardcy_e161.3_c96_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4146QQARDCY/E161.3/CERN/SUMT/C001-18
*   Description  :  Extended Short DST simulation 96g1 done at ecms=161.3 , CERN
*---
*   Comments     : in total 48826 events in 18 files time stamp: Sat Aug 10 06:14:00 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61000.xsdst ! RUN = 61000 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61001.xsdst ! RUN = 61001 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61002.xsdst ! RUN = 61002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61003.xsdst ! RUN = 61003 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61004.xsdst ! RUN = 61004 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61005.xsdst ! RUN = 61005 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61006.xsdst ! RUN = 61006 ! NEVT = 24
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61007.xsdst ! RUN = 61007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61008.xsdst ! RUN = 61008 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61009.xsdst ! RUN = 61009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61014.xsdst ! RUN = 61014 ! NEVT = 810
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61015.xsdst ! RUN = 61015 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61017.xsdst ! RUN = 61017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61018.xsdst ! RUN = 61018 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61019.xsdst ! RUN = 61019 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61020.xsdst ! RUN = 61020 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61021.xsdst ! RUN = 61021 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/161.3/kk2f4146_qqardcy_161.3_61022.xsdst ! RUN = 61022 ! NEVT = 2999
