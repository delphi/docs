*
*   Nickname     : xs_kk2f4146qqardcy_e172_c96_1l_g1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/KK2F4146QQARDCY/CERN/SUMT/C001-21
*   Description  :  Extended Short DST simulation 96g1 172 , CERN
*---
*   Comments     : in total 38992 events in 21 files time stamp: Tue Aug  6 11:14:44 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62000.xsdst ! RUN = 62000 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62001.xsdst ! RUN = 62001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62002.xsdst ! RUN = 62002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62003.xsdst ! RUN = 62003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62004.xsdst ! RUN = 62004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62005.xsdst ! RUN = 62005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62006.xsdst ! RUN = 62006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62007.xsdst ! RUN = 62007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62008.xsdst ! RUN = 62008 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62009.xsdst ! RUN = 62009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62010.xsdst ! RUN = 62010 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62011.xsdst ! RUN = 62011 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62014.xsdst ! RUN = 62014 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62015.xsdst ! RUN = 62015 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62016.xsdst ! RUN = 62016 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62017.xsdst ! RUN = 62017 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62018.xsdst ! RUN = 62018 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62019.xsdst ! RUN = 62019 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62020.xsdst ! RUN = 62020 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62021.xsdst ! RUN = 62021 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v96g/172/kk2f4146_qqardcy_172_62022.xsdst ! RUN = 62022 ! NEVT = 2999
