*
*   Nickname     : sh_scan_e091_s94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/SCAN/E91.25/SNAK/SUMT
*   Description  : ShortDST higgs  Short DST simulation 94_c2 done at ecms=91.25 , Snak
*---
*   Comments     :  time stamp: Wed Dec 12 10:03:56 2001
*---
*
* 
*                * e+e- -> hA (hA_ffff_Z0_mA_mh*)  
*                  where mA/mh - A/h mass, ffff - channel 
*
*   Comments    : Post-Dst processing 
*
*
*          ffff = ttbb (A->2tau, h->2b)
*             mA =< 12 => mh
*             mA=04,06,09,12 / mh=12,20,30,40,50,60,70
*
* mA=04, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.1.xsdst ! RUN =  -63161 -63160 -63174 -63163 -63170 -63164 -63162 -63171 -63173 -63172! NEVT =    1988
* mA=04, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.2.xsdst ! RUN =  -63182 -63186 -63184 -63180 -63181 -63189 -63188 -63187 -63185 -63183! NEVT =    1993
*mA=04, mh=30 
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.3.xsdst ! RUN =  -63192 -63191 -63195 -63194 -63190 -63196 -63193 -63197 -63198 -63199! NEVT =    1999
* mA=04, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.4.xsdst ! RUN =  -63200 -63208 -63202 -63206 -63207 -63204 -63201 -63209 -63205 -63203! NEVT =    1997
* mA=04, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.5.xsdst ! RUN =  -63210 -63218 -63219 -63211 -63212 -63213 -63216 -63217 -63215 -63214! NEVT =    1467
* mA=04, mh=60 
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.6.xsdst ! RUN =  -63222 -63226 -63225 -63227 -63228 -63229 -63224 -63220 -63221 -63223! NEVT =    1998
* mA=04, mh=70
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.7.xsdst ! RUN =  -63236 -63239 -63237 -63238 -63231 -63232 -63234 -63233 -63235 -63230! NEVT =    1176
* mA=06, mh=12 
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.8.xsdst ! RUN =  -63242 -63241 -63240 -63243 -63245 -63249 -63246 -63248 -63247 -63244! NEVT =    1994
* mA=06, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.9.xsdst ! RUN =  -63252 -63250 -63251 -63253 -63258 -63255 -63257 -63256 -63254 -63259! NEVT =    1996
* mA=06, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.10.xsdst ! RUN =  -63261 -63266 -63262 -63269 -63263 -63265 -63267 -63260 -63268 -63264! NEVT =    1997
* mA=06, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.11.xsdst ! RUN =  -63275 -63278 -63272 -63279 -63273 -63271 -63276 -63274 -63277 -63270! NEVT =    1515
* mA=06, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.12.xsdst ! RUN =  -63280 -63281 -63283 -63284 -63286 -63285 -63287 -63288 -63282 -63289! NEVT =    1977
* mA=06, mh=60
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.13.xsdst ! RUN =  -63290 -63298 -63291 -63294 -63295 -63292 -63296 -63297 -63293 -63299! NEVT =    1998
* mA=06, mh=70
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.14.xsdst ! RUN =  -63303 -63301 -63302 -63309 -63304 -63307 -63305 -63306 -63308 -63300! NEVT =    1552
* mA=09, mh=12 
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.15.xsdst ! RUN =  -63312 -63316 -63318 -63319 -63310 -63311 -63313 -63314 -63315 -63317! NEVT =    1991
* mA=09, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.16.xsdst ! RUN =  -63323 -63325 -63328 -63329 -63320 -63321 -63322 -63324 -63326 -63327! NEVT =    1998
* mA=09, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.17.xsdst ! RUN =  -63330 -63335 -63334 -63336 -63339 -63338 -63331 -63332 -63333 -63337! NEVT =    1999
* mA=09, mh=40 
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.18.xsdst ! RUN =  -63346 -63340 -63342 -63347 -63344 -63343 -63348 -63349 -63341 -63345! NEVT =    1870
* mA=09, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.19.xsdst ! RUN =  -63350 -63353 -63351 -63359 -63352 -63358 -63355 -63354 -63357 -63356! NEVT =    1832
* mA=09, mh=60
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.20.xsdst ! RUN =  -63360 -63363 -63367 -63361 -63362 -63365 -63364 -63366 -63369 -63368! NEVT =    1999
* mA=09, mh=70
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.21.xsdst ! RUN =  -63370 -63376 -63374 -63378 -63375 -63372 -63373 -63377 -63379 ! NEVT =    1666
* mA=12, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.22.xsdst ! RUN =  -63380 -63382 -63381 -63383 -63386 -63389 -63387 -63388 -63384 -63385! NEVT =    1986
* mA=12, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.23.xsdst ! RUN =  -63390 -63391 -63392 -63395 -63396 -63397 -63399 -63393 -63394 -63398! NEVT =    1998
* mA=12, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.24.xsdst ! RUN =  -63400 -63401 -63403 -63402 -63405 -63406 -63407 -63408 -63404 -63409! NEVT =    1999
* mA=12, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.25.xsdst ! RUN =  -63412 -63418 -63410 -63411 -63413 -63414 -63415 -63416 -63419 -63417! NEVT =    1622
* mA=12, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.26.xsdst ! RUN =  -63422 -63421 -63423 -63424 -63427 -63428 -63429 -63425 -63420 -63426! NEVT =    1995
* mA=12, mh=60 
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.27.xsdst ! RUN =  -63430 -63438 -63431 -63433 -63435 -63437 -63436 -63434 -63432 -63439! NEVT =    1834
* mA=12, mh=70
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.28.xsdst ! RUN =  -63446 -63448 -63441 -63443 -63440 -63442 -63445 -63444 -63447 -63449! NEVT =    1735
*
*          ee --> hZ* (hZ_ffff_Z0_mh*)
*                 where: mh - h mass, ffff - channel: 
*
*          ffff = ttqq (h->2tau, Z->qq) 
*             mh =< 12 
*             mh=04,06,09,12
*
* mh=04
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.29.xsdst ! RUN =  -63451 -63450 -63452 -63458 -63455 -63457 -63454 -63456 -63459 -63453! NEVT =    2000
* mh=06
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.30.xsdst ! RUN =  -63460 -63461 -63465 -63463 -63469 -63464 -63468 -63467 -63462 -63466! NEVT =    1954
* mh=09
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.31.xsdst ! RUN =  -63472 -63475 -63474 -63478 -63470 -63479 -63471 -63477 -63476 -63473! NEVT =    2000
* mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.32.xsdst ! RUN =  -63480 -63488 -63489 -63481 -63482 -63485 -63483 -63486 -63484 -63487! NEVT =    1830
*
*          ffff = bbqq (h->bb, Z->qq) 
*             mh >= 12 
*             mh=12,20,30,40,50
*
* mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.33.xsdst ! RUN =  -63492 -63495 -63494 -63496 -63490 -63491 -63493 -63497 -63498 -63499! NEVT =    1999
* mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.34.xsdst ! RUN =  -63500 -63503 -63506 -63507 -63508 -63509 -63502 -63501 -63504 -63505! NEVT =    2000
* mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.35.xsdst ! RUN =  -63510 -63511 -63513 -63512 -63514 -63518 -63516 -63515 -63519 -63517! NEVT =    2000
* mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.36.xsdst ! RUN =  -63521 -63526 -63523 -63520 -63522 -63525 -63524 -63527 -63529 -63528! NEVT =    2000
* mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.37.xsdst ! RUN =  -63533 -63538 -63539 -63532 -63531 -63530 -63534 -63536 -63537 -63535! NEVT =    2000
*
*          ee --> hA (hA_ffff_Z0_mA_mh*)
*                 where: mA/mh - A/h mass, ffff - channel: 
*
*          ffff = bbbb (A->2b, h->2b)
*          a) mA > mh >= 12 
*             mA=12 mh=20,30,40,50,60,70
*             mA=20 mh=   30,40,50,60
*             mA=30 mh=      40,50
*          b) mh > mA >= 12 
*             mh=12 mA=20,30,40,50,60,70
*             mh=20 mA=   30,40,50,60
*             mh=30 mA=      40,50
*          c) mA = mh >= 12
*             mh=mA=12,20,30,40 
*   
* mA=12, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.38.xsdst ! RUN =  -63540 -63541 -63543 -63544 -63546 -63547 -63545 -63548 -63549 -63542! NEVT =    1969
* mA=12, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.39.xsdst ! RUN =  -63551 -63552 -63553 -63550 -63554 -63556 -63557 -63558 -63559 -63555! NEVT =    1980
* mA=12, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.40.xsdst ! RUN =  -63560 -63561 -63562 -63563 -63564 -63566 -63567 -63568 -63569 -63565! NEVT =    1987
* mA=12, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.41.xsdst ! RUN =  -63571 -63573 -63574 -63575 -63577 -63570 -63572 -63576 -63578 -63579! NEVT =    1983
* mA=12, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.42.xsdst ! RUN =  -63583 -63585 -63589 -63588 -63581 -63584 -63580 -63582 -63586 -63587! NEVT =    1981
* mA=12, mh=60
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.43.xsdst ! RUN =  -63591 -63593 -63596 -63597 -63599 -63594 -63590 -63592 -63595 -63598! NEVT =    1979
* mA=12, mh=70
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.44.xsdst ! RUN =  -63602 -63601 -63605 -63603 -63606 -63600 -63607 -63604 -63608 -63609! NEVT =    1968
* mA=20, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.45.xsdst ! RUN =  -63611 -63613 -63615 -63616 -63617 -63618 -63619 -63610 -63612 -63614! NEVT =    1989
* mA=20, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.46.xsdst ! RUN =  -63620 -63621 -63622 -63623 -63624 -63625 -63626 -63627 -63629 -63628! NEVT =    1991
* mA=20, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.47.xsdst ! RUN =  -63631 -63636 -63634 -63638 -63635 -63639 -63632 -63633 -63637 -63630! NEVT =     901
* mA=20, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.48.xsdst ! RUN =  -63641 -63648 -63644 -63645 -63647 -63640 -63642 -63643 -63646 -63649! NEVT =    1429
* mA=20, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.49.xsdst ! RUN =  -63657 -63655 -63652 -63656 -63658 -63654 -63659 -63651 -63650 -63653! NEVT =     726
* mA=20, mh=60
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.50.xsdst ! RUN =  -63661 -63663 -63660 -63664 -63665 -63667 -63668 -63662 -63666 -63669! NEVT =    1989
* mA=30, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.51.xsdst ! RUN =  -63671 -63672 -63673 -63675 -63678 -63677 -63679 -63670 -63674 -63676! NEVT =    1984
* mA=30, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.52.xsdst ! RUN =  -63682 -63683 -63686 -63681 -63680 -63684 -63687 -63689 -63685 -63688! NEVT =    1991
* mA=30, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.53.xsdst ! RUN =  -63695 -63693 -63696 -63698 -63691 -63697 -63694 -63690 -63699 ! NEVT =    1241
* mA=30, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.54.xsdst ! RUN =  -63700 -63701 -63709 -63704 -63707 -63708 -63706 -63705 -63702 -63703! NEVT =    1303
* mA=30, mh=50
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.55.xsdst ! RUN =  -63716 -63711 -63713 -63715 -63714 -63717 -63719 -63718 -63710 -63712! NEVT =    1814
* mA=40, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.56.xsdst ! RUN =  -63720 -63721 -63723 -63724 -63726 -63727 -63722 -63728 -63729 -63725! NEVT =    1986
* mA=40, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.57.xsdst ! RUN =  -63732 -63737 -63736 -63730 -63733 -63735 -63739 -63738 -63734 -63731! NEVT =    1161
* mA=40, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.58.xsdst ! RUN =  -63741 -63742 -63743 -63744 -63746 -63747 -63748 -63749 -63740 -63745! NEVT =    1997
* mA=40, mh=40
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.59.xsdst ! RUN =  -63754 -63751 -63752 -63753 -63755 -63756 -63758 -63759 -63757 -63750! NEVT =    1468
* mA=50, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.60.xsdst ! RUN =  -63760 -63762 -63764 -63769 -63763 -63761 -63765 -63767 -63766 -63768! NEVT =    1983
* mA=50, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.61.xsdst ! RUN =  -63772 -63771 -63775 -63777 -63779 -63773 -63776 -63770 -63774 -63778! NEVT =    1996
* mA=50, mh=30
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.62.xsdst ! RUN =  -63782 -63785 -63784 -63789 -63780 -63781 -63783 -63786 -63787 -63788! NEVT =    1992
* mA=60, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.63.xsdst ! RUN =  -63791 -63792 -63794 -63795 -63796 -63797 -63790 -63793 -63799 -63798! NEVT =    1991
* mA=60, mh=20
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.64.xsdst ! RUN =  -63800 -63802 -63803 -63804 -63806 -63808 -63809 -63801 -63807 -63805! NEVT =    1991
* mA=70, mh=12
FILE= /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02905.65.xsdst ! RUN =  -63810 -63812 -63813 -63816 -63817 -63819 -63818 -63815 -63811 -63814! NEVT =    1987
*
*     ee --> hA (hA_ffff_Z0_mA_mh*)
*            where: mA/mh - A/h mass, ffff - channel: 
*
*            ffff = ttgg (A->2tau, h->2glu)
*               mA =< 12 =< mh
*               mA=04,06,09,12 / mh=12,20,30,40,50,60,70
*
* mA=04, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.1.xsdst ! RUN =  -65521 -65512 -65522 -65514 -65513 -65511 -65524 -65510 -65520 -65523! NEVT =    1782
* mA=04, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.2.xsdst ! RUN =  -65535 -65533 -65537 -65536 -65538 -65534 -65532 -65531 -65539 -65530! NEVT =    1998
* mA=04, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.3.xsdst ! RUN =  -65541 -65540 -65547 -65542 -65549 -65545 -65543 -65548 -65546 -65544! NEVT =    1643
* mA=04, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.4.xsdst ! RUN =  -65552 -65555 -65558 -65550 -65553 -65551 -65557 -65556 -65554 -65559! NEVT =    1999
* mA=04, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.5.xsdst ! RUN =  -65566 -65567 -65561 -65560 -65565 -65564 -65569 -65563 -65568 -65562! NEVT =    1579
* mA=04, mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.6.xsdst ! RUN =  -65571 -65572 -65570 -65578 -65577 -65579 -65576 -65575 -65574 -65573! NEVT =    1997
* mA=04, mh=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.7.xsdst ! RUN =  -65585 -65589 -65586 -65580 -65581 -65587 -65582 -65583 -65584 -65588! NEVT =    1998
* mA=06, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.8.xsdst ! RUN =  -65598 -65599 -65595 -65593 -65596 -65590 -65594 -65597 -65591 -65592! NEVT =    1927
* mA=06, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.9.xsdst ! RUN =  -65600 -65601 -65602 -65603 -65604 -65607 -65608 -65605 -65606 -65609! NEVT =    2000
* mA=06, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.10.xsdst ! RUN =  -65611 -65617 -65612 -65618 -65613 -65616 -65619 -65615      0      0! NEVT =    1600
* mA=06, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.11.xsdst ! RUN =  -65628 -65626 -65621 -65623 -65627 -65629 -65620 -65622 -65624 -65625! NEVT =    1999
* mA=06, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.12.xsdst ! RUN =  -65630 -65631 -65633 -65632 -65635 -65634 -65636 -65638 -65637 -65639! NEVT =    2000
* mA=06, mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.13.xsdst ! RUN =  -65646 -65640 -65645 -65641 -65643 -65642 -65644 -65648 -65647 -65649! NEVT =    2000
* mA=06, mh=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.14.xsdst ! RUN =  -65655 -65653 -65654 -65657 -65659 -65658 -65651 -65650 -65656 -65652! NEVT =    2000
* mA=09, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.15.xsdst ! RUN =  -65660 -65662 -65668 -65661 -65666 -65663 -65665 -65664 -65669 -65667! NEVT =    1472
* mA=09, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.16.xsdst ! RUN =  -65679 -65673 -65671 -65677 -65678 -65670 -65672 -65676 -65674 -65675! NEVT =    1816
* mA=09, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.17.xsdst ! RUN =  -65680 -65684 -65682 -65683 -65681 -65685 -65687 -65688 -65689 -65686! NEVT =    1998
* mA=09, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.18.xsdst ! RUN =  -65690 -65695 -65697 -65699 -65691 -65692 -65693 -65696 -65694 -65698! NEVT =    2000
* mA=09, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.19.xsdst ! RUN =  -65700 -65708 -65701 -65702 -65703 -65705 -65704 -65706 -65707 -65709! NEVT =    2000
* mA=09, mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.20.xsdst ! RUN =  -65711 -65714 -65716 -65717 -65718 -65710 -65712 -65713 -65715 -65719! NEVT =    2000
* mA=09, mh=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.21.xsdst ! RUN =  -65726 -65727 -65728 -65729 -65721 -65720 -65722 -65723 -65724 -65725! NEVT =    1999
* mA=12, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.22.xsdst ! RUN =  -65730 -65732 -65731 -65734 -65733 -65736 -65738 -65735 -65737 -65739! NEVT =    2000
* mA=12, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.23.xsdst ! RUN =  -65745 -65743 -65747 -65740 -65742 -65746 -65749 -65744 -65741 -65748! NEVT =    1545
* mA=12, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.24.xsdst ! RUN =  -65750 -65752 -65754 -65755 -65758 -65759 -65751 -65753 -65756 -65757! NEVT =    2000
* mA=12, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.25.xsdst ! RUN =  -65760 -65763 -65767 -65768 -65761 -65762 -65764 -65765 -65769 -65766! NEVT =    1998
* mA=12, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.26.xsdst ! RUN =  -65771 -65774 -65777 -65773 -65770 -65772 -65775 -65778 -65776 -65779! NEVT =    1720
* mA=12, mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.27.xsdst ! RUN =  -65780 -65781 -65784 -65782 -65788 -65783 -65785 -65789 -65787 -65786! NEVT =    1808
* mA=12, mh=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.28.xsdst ! RUN =  -65793 -65791 -65790 -65792 -65794 -65798 -65797 -65796 -65795 -65799! NEVT =    1735
*
*    ee --> hZ* (hZ_ffff_Z0_mh*)
*           where: mh - h mass, ffff - channel: 
*
*           hZ_ttnn_Z0_mh* :
*
*           ee -> hZ
*           Z -> nunu, h -> tau tau
*           * mh = 4, 6, 9, 12
*
* mh=04
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.29.xsdst ! RUN =  -65800 -65802 -65801 -65803 -65804 -65810 -65811 -65813 -65812 -65814! NEVT =    2000
* mh=06
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.30.xsdst ! RUN =  -65822 -65828 -65825 -65821 -65824 -65826 -65820 -65829 -65823 -65827! NEVT =    2000
* mh=09
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.31.xsdst ! RUN =  -65836 -65838 -65839 -65834 -65832 -65831 -65837 -65833 -65830 -65835! NEVT =    2000
* mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.32.xsdst ! RUN =  -65840 -65841 -65844 -65843 -65842 -65846 -65848 -65849 -65845 -65847! NEVT =    2000
*
*
*     hZ_ggnn_Z0_mh* : 
*
*     ee -> hZ
*     Z -> nunu, h -> gg
*     * mh = 4, 6, 9, 12, 20, 30, 40, 50
*
* mh=04
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.33.xsdst ! RUN =  -65851 -65850 -65853 -65859 -65852 -65856 -65855 -65858 -65857 -65854! NEVT =    2000
* mh=06
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.34.xsdst ! RUN =  -65865 -65868 -65861 -65862 -65866 -65863 -65869 -65864 -65860 -65867! NEVT =    2000
* mh=09
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.35.xsdst ! RUN =  -65878 -65876 -65877 -65873 -65872 -65875 -65870 -65871 -65874 -65879! NEVT =    2000
* mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.36.xsdst ! RUN =  -65880 -65884 -65882 -65881 -65883 -65885 -65886 -65887 -65888 -65889! NEVT =    2000
* mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.37.xsdst ! RUN =  -65891 -65897 -65890 -65893 -65894 -65892 -65895 -65899 -65898 -65896! NEVT =    2000
* mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.38.xsdst ! RUN =  -65903 -65905 -65908 -65909 -65900 -65904 -65901 -65902 -65907 -65906! NEVT =    2000
* mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.39.xsdst ! RUN =  -65911 -65915 -65914 -65919 -65913 -65917 -65912 -65910 -65916 -65918! NEVT =    2000
* mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.40.xsdst ! RUN =  -65920 -65927 -65922 -65925 -65921 -65923 -65926 -65924 -65928 -65929! NEVT =    2000
*
*
*    hZ_bbnn_Z0_mh* : 
*
*    ee -> hZ
*    Z -> nunu, h -> bb
*    * mh = 12, 20, 30, 40, 50
*
* mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.41.xsdst ! RUN =  -65935 -65937 -65931 -65933 -65934 -65930 -65932 -65936 -65938 -65939! NEVT =    1998
* mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.42.xsdst ! RUN =  -65941 -65943 -65942 -65946 -65947 -65949 -65940 -65944 -65945 -65948! NEVT =    1998
* mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.43.xsdst ! RUN =  -65950 -65951 -65957 -65952 -65954 -65956 -65953 -65959 -65955 -65958! NEVT =    2000
* mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.44.xsdst ! RUN =  -65961 -65963 -65967 -65966 -65968 -65962 -65960 -65964 -65965 -65969! NEVT =    2000
* mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02908.45.xsdst ! RUN =  -65970 -65972 -65975 -65974 -65976 -65979 -65971 -65973 -65977 -65978! NEVT =    2000
*
*
*    Yuk_A_bbbb_Z0_mA :  samples with pseudoscalar A
*
*    yukawa --> 4b
*    * mh = 11, 12, 15, 20, 30, 40, 50, 60, 70
*
* mA=11
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.1.xsdst ! RUN =  -66301 -66299 -66300 -66303 -66302 -66304 -66305 -66308 -66307 -66306! NEVT = 2499
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.2.xsdst ! RUN =  -66297 -66296 -66295 -66290 -66294 -66292 -66291 -66293 -66310 -66314! NEVT = 2500
* mA=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.3.xsdst ! RUN =  -66322 -66321 -66323 -66320 -66324 -66330 -66331 -66334 -66325 -66333! NEVT = 2499
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.4.xsdst ! RUN =  -66332 -66340 -66336 -66341 -66335 -66342 -66346 -66345 -66343 -66344! NEVT = 2500
* mA=15
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.5.xsdst ! RUN =  -66350 -66351 -66352 -66353 -66360 -66361 -66362 -66363 -66364 -66365! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.6.xsdst ! RUN =  -66355 -66354 -66366 -66370 -66371 -66372 -66374 -66373 -66376 -66375! NEVT = 2500
* mA=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.7.xsdst ! RUN =  -66380 -66381 -66382 -66383 -66384 -66385 -66390 -66391 -66392 -66393! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.8.xsdst ! RUN =  -66394 -66395 -66396 -66400 -66401 -66402 -66403 -66404 -66405 -66406! NEVT = 2500
* mA=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.9.xsdst ! RUN =  -66410 -66411 -66412 -66413 -66414 -66415 -66420 -66421 -66422 -66423! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.10.xsdst ! RUN =  -66424 -66425 -66426 -66430 -66431 -66432 -66433 -66434 -66435 -66436! NEVT = 2500
* mA=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.11.xsdst ! RUN =  -66444 -66443 -66456 -66445 -66453 -66461 -66441 -66462 -66454 -66442! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.12.xsdst ! RUN =  -66460 -66455 -66451 -66452 -66450 -66464 -66465 -66440 -66463 -66466! NEVT = 2500
* mA=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.13.xsdst ! RUN =  -66470 -66471 -66472 -66474 -66473 -66480 -66475 -66481 -66482 -66483! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.14.xsdst ! RUN =  -66484 -66485 -66486 -66490 -66492 -66495 -66496 -66494 -66493 -66491! NEVT = 2500
* mA=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.15.xsdst ! RUN =  -66501 -66503 -66500 -66504 -66510 -66502 -66511 -66512 -66513 -66505! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.16.xsdst ! RUN =  -66514 -66515 -66516 -66523 -66522 -66521 -66520 -66525 -66524 -66526! NEVT = 2500
* mA=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.17.xsdst ! RUN =  -66531 -66532 -66530 -66535 -66540 -66534 -66556 -66533 -66541 -66545! NEVT = 2341
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.18.xsdst ! RUN =  -66544 -66546 -66550 -66552 -66543 -66551 -66553 -66554 -66555 -66542! NEVT = 2500
*
*
*    Yuk_h_bbbb_Z0_mh :  samples with scalar h 
*
*    yukawa --> 4b
*    * mh = 11, 12, 15, 20, 30, 40, 50, 60, 70
*
* mh=11
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.19.xsdst ! RUN =  -66561 -66562 -66563 -66565 -66564 -66570 -66571 -66572 -66573 -66560! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.20.xsdst ! RUN =  -66575 -66576 -66574 -66580 -66581 -66582 -66584 -66585 -66583 -66586! NEVT = 2500
* mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.21.xsdst ! RUN =  -66590 -66591 -66592 -66593 -66594 -66600 -66601 -66602 -66603 -66604! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.22.xsdst ! RUN =  -66605 -66606 -66610 -66611 -66612 -66613 -66614 -66616 -66615 -66595! NEVT = 2500
* mh=15
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.23.xsdst ! RUN =  -66620 -66621 -66622 -66623 -66624 -66625 -66630 -66631 -66632 -66633! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.24.xsdst ! RUN =  -66634 -66635 -66636 -66640 -66641 -66643 -66642 -66644 -66645 -66646! NEVT = 2500
* mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.25.xsdst ! RUN =  -66650 -66652 -66651 -66654 -66653 -66662 -66664 -66660 -66661 -66663! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.26.xsdst ! RUN =  -66655 -66665 -66666 -66670 -66675 -66671 -66673 -66676 -66674 -66672! NEVT = 2499
* mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.27.xsdst ! RUN =  -66681 -66682 -66684 -66691 -66693 -66683 -66680 -66694 -66695 -66685! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.28.xsdst ! RUN =  -66692 -66696 -66700 -66701 -66703 -66702 -66690 -66705 -66706 -66704! NEVT = 2500
* mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.29.xsdst ! RUN =  -66714 -66710 -66713 -66712 -66715 -66721 -66711 -66720 -66722 -66723! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.30.xsdst ! RUN =  -66725 -66730 -66726 -66724 -66731 -66734 -66732 -66736 -66733 -66735! NEVT = 2500
* mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.31.xsdst ! RUN =  -66740 -66743 -66744 -66745 -66751 -66750 -66752 -66753 -66755 -66756! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.32.xsdst ! RUN =  -66741 -66754 -66760 -66742 -66761 -66762 -66764 -66765 -66763 -66766! NEVT = 2500
* mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.33.xsdst ! RUN =  -66770 -66771 -66772 -66773 -66774 -66781 -66775 -66782 -66784 -66785! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.34.xsdst ! RUN =  -66786 -66790 -66791 -66792 -66793 -66794 -66795 -66796 -66780 -66783! NEVT = 2500
* mh=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.35.xsdst ! RUN =  -66800 -66801 -66802 -66803 -66804 -66805 -66810 -66811 -66812 -66813! NEVT = 2500
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/Y02910.36.xsdst ! RUN =  -66814 -66815 -66816 -66820 -66821 -66822 -66824 -66825 -66823 -66826! NEVT = 2500
*
*    ee --> hA --> bbbb 
*
*               12 =< mA =< 70
*               mA=12,20,30,40,50,60,70 / mh=12,20,30,40,50,60,70
*
* mA=12, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.1.xsdst  ! RUN =  -67482 -67472 -67475 -67483 -67480 -67471 -67470 -67474 -67473 -67481 ! NEVT = 2462
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.2.xsdst  ! RUN =  -67494 -67484 -67491 -67486 -67490 -67492 -67485 -67493 -67495 -67496 ! NEVT = 2473
* mA=12, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.3.xsdst  ! RUN =  -67503 -67501 -67505 -67504 -67502 -67500 -67510 -67511 -67512 -67513 ! NEVT = 2482
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.4.xsdst  ! RUN =  -67520 -67514 -67515 -67516 -67522 -67521 -67524 -67526 -67523 -67525 ! NEVT = 2473
* mA=12, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.5.xsdst  ! RUN =  -67530 -67532 -67531 -67540 -67533 -67535 -67534 -67542 -67541 -67543 ! NEVT = 2475
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.6.xsdst  ! RUN =  -67544 -67545 -67546 -67550 -67551 -67553 -67552 -67555 -67554 -67556 ! NEVT = 2484
* mA=12, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.7.xsdst  ! RUN =  -67560 -67563 -67561 -67562 -67565 -67564 -67570 -67572 -67571 -67573 ! NEVT = 2484
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.8.xsdst  ! RUN =  -67574 -67575 -67576 -67582 -67581 -67580 -67583 -67586 -67584 -67585 ! NEVT = 2483
* mA=12, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.9.xsdst  ! RUN =  -67590 -67595 -67591 -67592 -67603 -67602 -67601 -67593 -67600 -67594 ! NEVT = 2475
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.10.xsdst ! RUN =  -67604 -67605 -67606 -67610 -67611 -67613 -67612 -67614 -67616 -67615 ! NEVT = 2484
* mA=12, mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.11.xsdst ! RUN =  -67622 -67623 -67620 -67621 -67625 -67630 -67624 -67631 -67632 -67633 ! NEVT = 2483
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.12.xsdst ! RUN =  -67640 -67634 -67635 -67636 -67641 -67642 -67644 -67643 -67646 -67645 ! NEVT = 2472
* mA=12, mh=70
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.13.xsdst ! RUN =  -67650 -67654 -67651 -67652 -67653 -67660 -67655 -67664 -67663 -67661 ! NEVT = 2467
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.14.xsdst ! RUN =  -67665 -67662 -67674 -67666 -67670 -67673 -67672 -67675 -67671 -67676 ! NEVT = 2466
* mA=20, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.15.xsdst ! RUN =  -67681 -67680 -67690 -67682 -67691 -67683 -67685 -67684 -67692 -67694 ! NEVT = 2484
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.16.xsdst ! RUN =  -67693 -67703 -67696 -67695 -67700 -67704 -67702 -67701 -67706 -67705 ! NEVT = 2485
* mA=20, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.17.xsdst ! RUN =  -66863 -66862 -66864 -66861 -66870 -66871 -66872 -66873 -66874 -66860 ! NEVT = 2489
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.18.xsdst ! RUN =  -67720 -67710 -67711 -67712 -67713 -67714 -67722 -67721 -67723 -67724 ! NEVT = 2488
* mA=20, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.19.xsdst ! RUN =  -66891 -66890 -66882 -66885 -66892 -66881 -66893 -66880 -66894 -66900 ! NEVT = 2492
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.20.xsdst ! RUN =  -66895 -66884 -66896 -66883 -66901 -66904 -66903 -66902 -66906 -66905 ! NEVT = 2496
* mA=20, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.21.xsdst ! RUN =  -66910 -66912 -66915 -66914 -66920 -66922 -66924 -66913 -66911 -66925 ! NEVT = 2494
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.22.xsdst ! RUN =  -66930 -66926 -66921 -66923 -66931 -66932 -66933 -66934 -66935 -66936 ! NEVT = 2492
* mA=20, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.23.xsdst ! RUN =  -66941 -66944 -66951 -66945 -66952 -66943 -66950 -66940 -66963 -66954 ! NEVT = 2491
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.24.xsdst ! RUN =  -66956 -66955 -66953 -66942 -66961 -66962 -66964 -66960 -66965 -66966 ! NEVT = 2491
* mA=20, mh=60
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.25.xsdst ! RUN =  -66970 -66971 -66972 -66973 -66974 -66982 -66975 -66981 -66983 -67010 ! NEVT = 2486
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.26.xsdst ! RUN =  -66984 -67011 -66985 -66980 -66986 -67016 -67015 -67012 -67013 -67014 ! NEVT = 2491
* mA=30, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.27.xsdst ! RUN =  -67020 -67031 -67025 -67030 -67023 -67024 -67022 -67034 -67021 -67033 ! NEVT = 2489  
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.28.xsdst ! RUN =  -67032 -67041 -67035 -67040 -67036 -67043 -67042 -67044 -67045 -67046 ! NEVT = 2497
* mA=30, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.29.xsdst ! RUN =  -67050 -67052 -67062 -67061 -67065 -67064 -67051 -67053 -67063 -67054 ! NEVT = 2493
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.30.xsdst ! RUN =  -67066 -67055 -67060 -67071 -67072 -67074 -67075 -67076 -67073 -67070 ! NEVT = 2495
* mA=30, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.31.xsdst ! RUN =  -67082 -67083 -67092 -67090 -67095 -67093 -67096 -67100 -67094 -67101 ! NEVT = 2492
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.32.xsdst ! RUN =  -67102 -67091 -67103 -67080 -67085 -67104 -67081 -67105 -67106 -67084 ! NEVT = 2496
* mA=30, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.33.xsdst ! RUN =  -67110 -67111 -67112 -67113 -67115 -67114 -67132 -67125 -67135 -67124 ! NEVT = 2494
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.34.xsdst ! RUN =  -67120 -67126 -67136 -67133 -67121 -67134 -67123 -67130 -67131 -67122 ! NEVT = 2494
* mA=30, mh=50
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.35.xsdst ! RUN =  -67141 -67140 -67143 -67142 -67145 -67150 -67151 -67144 -67152 -67153 ! NEVT = 2491  
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.36.xsdst ! RUN =  -67155 -67160 -67154 -67156 -67161 -67163 -67162 -67165 -67164 -67166 ! NEVT = 2497 
* mA=40, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.37.xsdst ! RUN =  -67170 -67171 -67173 -67172 -67183 -67175 -67185 -67174 -67182 -67184 ! NEVT = 2484
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.38.xsdst ! RUN =  -67190 -67186 -67192 -67180 -67181 -67191 -67195 -67196 -67194 -67193 ! NEVT = 2486
* mA=40, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.39.xsdst ! RUN =  -67201 -67200 -67203 -67202 -67204 -67205 -67210 -67211 -67220 -67214 ! NEVT = 2497
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.40.xsdst ! RUN =  -67216 -67213 -67212 -67221 -67222 -67223 -67224 -67225 -67226 -67215 ! NEVT = 2497
* mA=40, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.41.xsdst ! RUN =  -67230 -67231 -67232 -67235 -67234 -67240 -67233 -67241 -67242 -67245 ! NEVT = 2493
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.42.xsdst ! RUN =  -67244 -67243 -67251 -67250 -67252 -67246 -67255 -67253 -67254 -67256 ! NEVT = 2493
* mA=40, mh=40
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.43.xsdst ! RUN =  -67260 -67261 -67262 -67263 -67264 -67270 -67265 -67271 -67274 -67272 ! NEVT = 2493
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.44.xsdst ! RUN =  -67273 -67280 -67275 -67281 -67276 -67282 -67283 -67284 -67285 -67286 ! NEVT = 2495
* mA=50, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.45.xsdst ! RUN =  -67290 -67292 -67291 -67293 -67294 -67295 -67300 -67301 -67302 -67303 ! NEVT = 2484
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.46.xsdst ! RUN =  -67305 -67304 -67310 -67306 -67311 -67312 -67313 -67314 -67316 -67315 ! NEVT = 2484
* mA=50, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.47.xsdst ! RUN =  -67320 -67322 -67321 -67324 -67323 -67330 -67325 -67331 -67332 -67333 ! NEVT = 2490
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.48.xsdst ! RUN =  -67334 -67336 -67340 -67341 -67335 -67342 -67343 -67344 -67345 -67346 ! NEVT = 2493
* mA=50, mh=30
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.49.xsdst ! RUN =  -67350 -67351 -67352 -67355 -67353 -67360 -67361 -67354 -67362 -67363 ! NEVT = 2489
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.50.xsdst ! RUN =  -67364 -67370 -67365 -67366 -67371 -67372 -67374 -67373 -67376 -67375 ! NEVT = 2490
* mA=60, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.51.xsdst ! RUN =  -67380 -67381 -67382 -67385 -67390 -67384 -67383 -67394 -67391 -67393 ! NEVT = 2487
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.52.xsdst ! RUN =  -67392 -67395 -67400 -67396 -67401 -67404 -67402 -67403 -67406 -67405 ! NEVT = 2485
* mA=60, mh=20
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.53.xsdst ! RUN =  -67412 -67410 -67411 -67413 -67414 -67415 -67420 -67421 -67423 -67433 ! NEVT = 2488
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.54.xsdst ! RUN =  -67422 -67430 -67425 -67424 -67431 -67436 -67432 -67426 -67435 -67434 ! NEVT = 2492
* mA=70, mh=12
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.55.xsdst ! RUN =  -67440 -67441 -67442 -67443 -67444 -67460 -67445 -67450 -67452 -67451 ! NEVT = 2481
FILE = /castor/cern.ch/delphi/MCprod/snak/higgs/scan/v94c2/091.2/R07801.56.xsdst ! RUN =  -67456 -67461 -67453 -67454 -67455 -67462 -67463 -67466 -67464 -67465 ! NEVT = 2477 
