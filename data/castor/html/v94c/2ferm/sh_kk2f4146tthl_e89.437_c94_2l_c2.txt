*
*   Nickname     : sh_kk2f4146tthl_e89.437_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/KK2F4146TTHL/E89.437/CERN/SUMT/C001-3
*   Description  :  Short DST simulation 94c2 done at ecms=89.437 , CERN
*---
*   Comments     : in total 8999 events in 3 files time stamp: Wed Jul 31 18:12:07 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v94c/89.437/kk2f4146_tthl_89.437_2001.sdst ! RUN = 2001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v94c/89.437/kk2f4146_tthl_89.437_2002.sdst ! RUN = 2002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v94c/89.437/kk2f4146_tthl_89.437_2003.sdst ! RUN = 2003 ! NEVT = 2999
