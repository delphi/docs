*   Nickname :     sh_pythia5720yukabbtt_e91.2_m50_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKABBTT/E91.2/CERN/SUMT/C001-5
*   Description :   Short DST simulation 94c  done at ecm=91.2 GeV , CERN
*   Comments :     in total 10000 events in 5 files, time stamp: Mon Feb 10 19:22:32 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_50_501.sdst ! RUN = 501 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_50_502.sdst ! RUN = 502 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_50_503.sdst ! RUN = 503 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_50_504.sdst ! RUN = 504 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_50_505.sdst ! RUN = 505 ! NEVT = 2000
