*
*   Nickname     : sh_hzha03pythia6156tttt_e91.2_m50_m30_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/HZHA03PYTHIA6156TTTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Fri Aug  2 15:12:15 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41750.sdst ! RUN = 41750 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41751.sdst ! RUN = 41751 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41752.sdst ! RUN = 41752 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41753.sdst ! RUN = 41753 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41754.sdst ! RUN = 41754 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41755.sdst ! RUN = 41755 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41756.sdst ! RUN = 41756 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41757.sdst ! RUN = 41757 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41758.sdst ! RUN = 41758 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_50_30_41759.sdst ! RUN = 41759 ! NEVT = 500
