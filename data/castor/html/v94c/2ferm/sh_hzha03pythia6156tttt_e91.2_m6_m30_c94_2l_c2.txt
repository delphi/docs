*
*   Nickname     : sh_hzha03pythia6156tttt_e91.2_m6_m30_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/HZHA03PYTHIA6156TTTT/E91.2/CERN/SUMT/C001-9
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 4496 events in 9 files time stamp: Tue Aug  6 06:34:03 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41150.sdst ! RUN = 41150 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41151.sdst ! RUN = 41151 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41152.sdst ! RUN = 41152 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41153.sdst ! RUN = 41153 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41154.sdst ! RUN = 41154 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41155.sdst ! RUN = 41155 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41156.sdst ! RUN = 41156 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41157.sdst ! RUN = 41157 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_6_30_41159.sdst ! RUN = 41159 ! NEVT = 499
