*   Nickname :     sh_pythia5720yukabbtt_e91.2_m60_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKABBTT/E91.2/CERN/SUMT/C001-5
*   Description :   Short DST simulation 94c  done at ecm=91.2 GeV , CERN
*   Comments :     in total 9859 events in 5 files, time stamp: Tue Feb 11 4:12:8 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_60_601.sdst ! RUN = 601 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_60_602.sdst ! RUN = 602 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_60_603.sdst ! RUN = 603 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_60_604.sdst ! RUN = 604 ! NEVT = 1998
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_60_605.sdst ! RUN = 605 ! NEVT = 1862
