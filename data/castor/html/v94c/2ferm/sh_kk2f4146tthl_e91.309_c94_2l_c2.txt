*
*   Nickname     : sh_kk2f4146tthl_e91.309_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/KK2F4146TTHL/E91.309/CERN/SUMT/C001-3
*   Description  :  Short DST simulation 94c2 done at ecms=91.309 , CERN
*---
*   Comments     : in total 8995 events in 3 files time stamp: Wed Jul 31 22:10:35 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v94c/91.309/kk2f4146_tthl_91.309_3001.sdst ! RUN = 3001 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v94c/91.309/kk2f4146_tthl_91.309_3002.sdst ! RUN = 3002 ! NEVT = 2996
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v94c/91.309/kk2f4146_tthl_91.309_3003.sdst ! RUN = 3003 ! NEVT = 2999
