*
*   Nickname     : sh_hzha03pythia6156tttt_e91.2_m20_m9_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/HZHA03PYTHIA6156TTTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 5000 events in 10 files time stamp: Mon Aug  5 22:11:21 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41420.sdst ! RUN = 41420 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41421.sdst ! RUN = 41421 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41422.sdst ! RUN = 41422 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41423.sdst ! RUN = 41423 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41424.sdst ! RUN = 41424 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41425.sdst ! RUN = 41425 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41426.sdst ! RUN = 41426 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41427.sdst ! RUN = 41427 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41428.sdst ! RUN = 41428 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_20_9_41429.sdst ! RUN = 41429 ! NEVT = 500
