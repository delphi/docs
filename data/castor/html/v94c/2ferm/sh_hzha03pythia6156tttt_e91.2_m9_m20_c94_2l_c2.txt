*
*   Nickname     : sh_hzha03pythia6156tttt_e91.2_m9_m20_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/HZHA03PYTHIA6156TTTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 4996 events in 10 files time stamp: Tue Aug  6 06:34:05 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41240.sdst ! RUN = 41240 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41241.sdst ! RUN = 41241 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41242.sdst ! RUN = 41242 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41243.sdst ! RUN = 41243 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41244.sdst ! RUN = 41244 ! NEVT = 498
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41245.sdst ! RUN = 41245 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41246.sdst ! RUN = 41246 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41247.sdst ! RUN = 41247 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41248.sdst ! RUN = 41248 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_20_41249.sdst ! RUN = 41249 ! NEVT = 500
