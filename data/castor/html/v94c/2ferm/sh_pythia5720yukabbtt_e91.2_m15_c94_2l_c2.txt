*   Nickname :     sh_pythia5720yukabbtt_e91.2_m15_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKABBTT/E91.2/CERN/SUMT/C001-5
*   Description :   Short DST simulation 94c  done at ecm=91.2 GeV , CERN
*   Comments :     in total 8176 events in 5 files, time stamp: Mon Feb 10 19:22:32 2003
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_15_151.sdst ! RUN = 151 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_15_152.sdst ! RUN = 152 ! NEVT = 177
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_15_153.sdst ! RUN = 153 ! NEVT = 2000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_15_154.sdst ! RUN = 154 ! NEVT = 1999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_15_155.sdst ! RUN = 155 ! NEVT = 2000
