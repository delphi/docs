*
*   Nickname     : sh_hzha03pythia6156tttt_e91.2_m9_m9_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/HZHA03PYTHIA6156TTTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 4571 events in 10 files time stamp: Tue Aug  6 06:34:04 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41220.sdst ! RUN = 41220 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41221.sdst ! RUN = 41221 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41222.sdst ! RUN = 41222 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41223.sdst ! RUN = 41223 ! NEVT = 72
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41224.sdst ! RUN = 41224 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41225.sdst ! RUN = 41225 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41226.sdst ! RUN = 41226 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41227.sdst ! RUN = 41227 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41228.sdst ! RUN = 41228 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_tttt_91.2_9_9_41229.sdst ! RUN = 41229 ! NEVT = 500
