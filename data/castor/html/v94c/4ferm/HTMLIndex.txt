# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
sh_wphact23mmmm_e91.25_mxxx_c94_2l_c2           1994      v94c2      4FERM          4           19999      91.25       80.4          -       cern
sh_wphact23mmtt_e91.25_mxxx_c94_2l_c2           1994      v94c2      4FERM          5           24999      91.25       80.4          -       cern
sh_wphact23tttt_e91.25_mxxx_c94_2l_c2           1994      v94c2      4FERM          5           24996      91.25       80.4          -       cern
