*
*   Nickname     : sh_pythia5720yukabbtt_e91.2_m4_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKABBTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 9999 events in 10 files time stamp: Mon Apr 29 22:10:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_401.sdst ! RUN = 401 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_402.sdst ! RUN = 402 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_403.sdst ! RUN = 403 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_404.sdst ! RUN = 404 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_405.sdst ! RUN = 405 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_406.sdst ! RUN = 406 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_407.sdst ! RUN = 407 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_408.sdst ! RUN = 408 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_409.sdst ! RUN = 409 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_4_410.sdst ! RUN = 410 ! NEVT = 1000
