*
*   Nickname     : sh_pythia5720yukabbtt_e91.2_m10_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKABBTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 9998 events in 10 files time stamp: Mon Apr 29 22:10:24 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1001.sdst ! RUN = 1001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1002.sdst ! RUN = 1002 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1003.sdst ! RUN = 1003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1004.sdst ! RUN = 1004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1005.sdst ! RUN = 1005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1006.sdst ! RUN = 1006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1007.sdst ! RUN = 1007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1008.sdst ! RUN = 1008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1009.sdst ! RUN = 1009 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_bbtt_91.2_10_1010.sdst ! RUN = 1010 ! NEVT = 1000
