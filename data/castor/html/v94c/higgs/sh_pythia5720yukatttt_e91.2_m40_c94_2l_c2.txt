*
*   Nickname     : sh_pythia5720yukatttt_e91.2_m40_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKATTTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 9997 events in 10 files time stamp: Thu Jul 11 15:10:59 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4001.sdst ! RUN = 4001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4002.sdst ! RUN = 4002 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4003.sdst ! RUN = 4003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4004.sdst ! RUN = 4004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4005.sdst ! RUN = 4005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4006.sdst ! RUN = 4006 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4007.sdst ! RUN = 4007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4008.sdst ! RUN = 4008 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4009.sdst ! RUN = 4009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_40_4010.sdst ! RUN = 4010 ! NEVT = 999
