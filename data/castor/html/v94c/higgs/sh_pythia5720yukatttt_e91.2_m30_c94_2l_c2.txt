*
*   Nickname     : sh_pythia5720yukatttt_e91.2_m30_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/PYTHIA5720YUKATTTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 9997 events in 10 files time stamp: Thu Jul 11 15:10:59 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3001.sdst ! RUN = 3001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3002.sdst ! RUN = 3002 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3003.sdst ! RUN = 3003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3004.sdst ! RUN = 3004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3005.sdst ! RUN = 3005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3006.sdst ! RUN = 3006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3007.sdst ! RUN = 3007 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3008.sdst ! RUN = 3008 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3009.sdst ! RUN = 3009 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/pythia5720/v94c/91.2/pythia5720_yuka_tttt_91.2_30_3010.sdst ! RUN = 3010 ! NEVT = 999
