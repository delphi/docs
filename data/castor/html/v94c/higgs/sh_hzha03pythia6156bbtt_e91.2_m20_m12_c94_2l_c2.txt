*
*   Nickname     : sh_hzha03pythia6156bbtt_e91.2_m20_m12_c94_2l_c2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/HZHA03PYTHIA6156BBTT/E91.2/CERN/SUMT/C001-10
*   Description  :  Short DST simulation 94c2 done at ecms=91.2 , CERN
*---
*   Comments     : in total 1994 events in 10 files time stamp: Wed Apr 24 12:12:37 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_1.sdst ! RUN = 1 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_10.sdst ! RUN = 10 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_2.sdst ! RUN = 2 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_3.sdst ! RUN = 3 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_4.sdst ! RUN = 4 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_5.sdst ! RUN = 5 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_6.sdst ! RUN = 6 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_7.sdst ! RUN = 7 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_8.sdst ! RUN = 8 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pythia6156/v94c/91.2/hzha03pythia6156_bbtt_91.2_20_12_9.sdst ! RUN = 9 ! NEVT = 200
