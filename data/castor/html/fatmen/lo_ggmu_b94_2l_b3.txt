*
*  Fatmen Nickname: lo_ggmu_b94_2l_b3
*  Generic Name : /castor/cern.ch/delphi/tape/C001-10
*  Description    : LongDST gamma-gamma-->mu mu, no cuts;   (liq) ANA94B Fix 3  ; BAST
*   Comments     :    10 files, total size = 0.473 Gb, in 10 files
*
Y10049.[150,159-160].al ! NUM = 1-3
Y10046.[132-133,142,148,154-155].al ! NUM = 4-9
Y10050.212.al ! NUM = 10
