*
*  Fatmen Nickname: xs_wwex_e200_sa99_1l_a1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-204
*  Description    : XShortDST e+e- --> WW-like by EXCALIBUR 1.08 MW=80.35        SACL; 
*   Comments     :    204 files, total size = 33.723 Gb, in 204 files
*
Y02072.[1-20,36-55].al ! NUM = 1-40
Y02062.[1-40].al ! NUM = 41-80
Y02076.[41-44].al ! NUM = 81-84
Y02085.[1-20].al ! NUM = 85-104
Y00740.[5-52].al ! NUM = 105-152
Y02086.[1-12].al ! NUM = 153-164
Y15975.[47-66].al ! NUM = 165-184
Y15977.[41-60].al ! NUM = 185-204
