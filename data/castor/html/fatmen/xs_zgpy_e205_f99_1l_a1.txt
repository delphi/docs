*
*  Fatmen Nickname: xs_zgpy_e205_f99_1l_a1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-229
*  Description    : XShortDST  Z0Gamma, Z0 -> qqbar                              FARM; 
*   Comments     :    229 files, total size = 47.374 Gb, in 229 files
*
Y01446.[15-37].al ! NUM = 1-23
Y01447.[1-4].al ! NUM = 24-27
Y01446.38.al ! NUM = 28
Y01447.[5-39].al ! NUM = 29-63
Y01448.[1-39].al ! NUM = 64-102
Y01449.[1-20].al ! NUM = 103-122
R04504.[1-82].al ! NUM = 123-204
R04505.[1-25].al ! NUM = 205-229
