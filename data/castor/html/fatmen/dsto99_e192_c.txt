*
*  Fatmen Nickname: dsto99_e192_c
*  Generic Name : /castor/cern.ch/delphi/tape/C001-726
*  Description    : RTD or D only, all events after DELANA filter;99 data 3th proc.
*   Comments     :    726 files, total size = 111.518 Gb, in 726 files
*
Y15117.[1-261].al ! NUM = 1-261
Y15118.[1-208].al ! NUM = 262-469
Y15119.[1-179].al ! NUM = 470-648
Y15120.[1-78].al ! NUM = 649-726
