*
*  Fatmen Nickname: lo_zzpw_e161_b96_1l_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-4
*  Description    : LongDST e+ e- -> ZZ (low mass photon cutoff)   ANA96E Fix 1 ; BAST
*   Comments     :    4 files, total size = 0.233 Gb, in 4 files
*
Y13713.[136-139].al ! NUM = 1-4
