*
*  Fatmen Nickname: scan00_c1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-5992
*  Description    : RTXS, events after DAFNE tag for scanning; 2000 data,3rd proc. Fix 1
*   Comments     :    5992 files, total size = 23.479 Gb, in 5992 files
*
R02127.[1-992].al ! NUM = 1-992
R02128.[1-996].al ! NUM = 993-1988
R02129.[1-66].al ! NUM = 1989-2054
R02127.[993-994].al ! NUM = 2055-2056
R02129.[67-999].al ! NUM = 2057-2989
R02130.[1-994].al ! NUM = 2990-3983
R02131.[1-996].al ! NUM = 3984-4979
R02132.[1-494].al ! NUM = 4980-5473
R02133.[1-519].al ! NUM = 5474-5992
