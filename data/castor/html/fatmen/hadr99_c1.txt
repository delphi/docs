*
*  Fatmen Nickname: hadr99_c1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-32
*  Description    : XShort DSTs,hadronic events for analysis; 99 data, 3th proc. Fix 1
*   Comments     :    32 files, total size = 6.851 Gb, in 32 files
*
Y00811.[1-5].al ! NUM = 1-5
Y00838.[1-10].al ! NUM = 6-15
Y01420.[1-11].al ! NUM = 16-26
Y01413.[1-6].al ! NUM = 27-32
