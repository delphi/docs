*
*  Fatmen Nickname: dsto96_e161_g
*  Generic Name : /castor/cern.ch/delphi/tape/C001-984
*  Description    : DST-only, "OR" of the physics teams; 96 data, 7th proc.
*   Comments     :    984 files, total size = 70.346 Gb, in 984 files
*
R07556.[1-420].al ! NUM = 1-420
R07557.[1-252].al ! NUM = 421-672
R07558.[1-312].al ! NUM = 673-984
