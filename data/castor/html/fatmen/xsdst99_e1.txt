*
*  Fatmen Nickname: xsdst99_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-87
*  Description    : XShort DSTs, events after DELANA filter; 99 data, 5th proc.  Fix 2
*   Comments     :    87 files, total size = 21.043 Gb , in 87 files
*
R02125.[1-31].al ! NUM=1-31
R02360.[1-29].al ! NUM=32-60
R05096.[1-12].al ! NUM=61-72
R05107.[1-15].al ! NUM=73-87
