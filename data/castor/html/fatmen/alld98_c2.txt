*
*  Fatmen Nickname: alld98_c2
*  Generic Name : /castor/cern.ch/delphi/tape/C001-332
*  Description    : XShort DSTs, all events after DAFNE tag; 98 data, 3rd proc.  Fix 2
*   Comments     :    332 files, total size = 75.570 Gb, in 332 files
*
Y12302.[1-90].al ! NUM = 1-90
Y12326.[1-41,44-90].al ! NUM = 91-178
Y13763.[1-90].al ! NUM = 179-268
Y13764.[1-64].al ! NUM = 269-332
