*
*  Fatmen Nickname: xs_ha20_e188_sa98_1l_d2
*  Generic Name : /castor/cern.ch/delphi/tape/C001-27
*  Description    : XShortDST  e+e- --> hA --> 4b tanbeta = 20 HZHA  m(A)        SACL;
*   Comments     :    27 files, total size = 2.730 Gb, in 27 files
*
Y13742.[70-89].al ! NUM = 1-20
Y15976.[89-95].al ! NUM = 21-27
