*
*  Fatmen Nickname: lo_susy_e161_sa96_1l_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-4
*  Description    : LongDST e+ e- -> stop stopbar (stop --> s b)   ANA96E Fix 1 ; SACL
*   Comments     :    4 files, total size = 0.052 Gb, in 4 files
*
Y13713.[239-242].al ! NUM = 1-4
