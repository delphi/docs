*
*  Fatmen Nickname: alld99_e2
*  Generic Name : /castor/cern.ch/delphi/tape/C001-322
*  Description    : XShort DSTs, all events after DAFNE tag; 99 data, 5th proc.  Fix 2
*   Comments     :    322 files, total size = 79.568 Gb, in 322 files
*
R07918.[1-70].al ! NUM = 1-70
R07919.[1-42].al ! NUM = 71-112
R07922.[1-70].al ! NUM = 113-182
R07923.[1-37].al ! NUM = 183-219
R07926.[1-53].al ! NUM = 220-272
R07929.[1-50].al ! NUM = 273-322
