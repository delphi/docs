*
*  Fatmen Nickname: lo_ggqp_e172_r96_1l_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-20
*  Description    : LongDST gamma gamma -> hadrons (QPM)           ANA96E Fix 1 ; RUTH
*   Comments     :    20 files, total size = 1.157 Gb, in 20 files
*
Y13715.[17-22].al ! NUM = 1-6
Y13714.[322-324,328,249,253-261].al ! NUM = 7-20
