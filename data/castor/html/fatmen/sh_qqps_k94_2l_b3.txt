*
*  Fatmen Nickname: sh_qqps_k94_2l_b3
*  Generic Name : /castor/cern.ch/delphi/tape/C001-537
*  Description    : ShortDST QQbar (b life 1.6); Sim. of 94 (liq) ANA94B Fix 3  ; KARL
*   Comments     :    537 files, total size = 47.068 Gb, in 537 files
*
Y10042.[93-98,101-160].al ! NUM = 1-66
Y10043.[181-240].al ! NUM = 67-126
Y10044.[11-70,75-80,134-139,146-150,160,207-210,213-216,221-240].al ! NUM = 127-232
Y10045.[8-10,16-19,22,24-120,141-150,161-189,191-240].al ! NUM = 233-426
Y10046.[1-110,123].al ! NUM = 427-537
