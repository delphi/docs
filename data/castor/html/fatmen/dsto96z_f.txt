*
*  Fatmen Nickname: dsto96z_f
*  Generic name   : //CERN/DELPHI/P01_ALLD/DSTO/PHYS/Y96V06/SUMT/C1-21
*  Description    : DST-only, "OR" of the physics teams; 96 Z0 data, 6th  proc.
*   Comments     :    21 files, total size = 3.257 Gb, 
*
VID = ES0381, NUM = 1
VID = ES0381, NUM = 2
VID = ES0381, NUM = 3
VID = ES0381, NUM = 4
VID = ES0381, NUM = 5
VID = ES0381, NUM = 6
VID = ES0381, NUM = 7
VID = ES0381, NUM = 8
VID = ES0381, NUM = 9
VID = ES0381, NUM = 10
VID = ES0381, NUM = 11
VID = ES0381, NUM = 12
VID = ES0381, NUM = 13
VID = ES0381, NUM = 14
VID = ES0381, NUM = 15
VID = ES0381, NUM = 16
VID = ES0381, NUM = 17
VID = ES0381, NUM = 18
VID = ES0381, NUM = 19
VID = ES0381, NUM = 20
VID = ES0381, NUM = 21
