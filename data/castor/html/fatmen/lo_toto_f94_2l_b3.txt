*
*  Fatmen Nickname: lo_toto_f94_2l_b3
*  Generic Name : /castor/cern.ch/delphi/tape/C001-140
*  Description    : LongDST tau+tau- (KORALZ40); Sim. of 94 (liq) ANA94B Fix 3  ; FARM
*   Comments     :    140 files, total size = 13.914 Gb, in 140 files
*
Y10049.[1-120,123-130,140,158].al ! NUM = 1-130
Y10046.[129-131,143,147,163,170-171,183].al ! NUM = 131-139
Y10050.194.al ! NUM = 140
