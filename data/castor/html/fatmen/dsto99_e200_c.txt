*
*  Fatmen Nickname: dsto99_e200_c
*  Generic Name : /castor/cern.ch/delphi/tape/C001-1454
*  Description    : RTD or D only, all events after DELANA filter;99 data 3th proc.
*   Comments     :    1454 files, total size = 294.687 Gb, in 1454 files
*
Y15133.[24-29,34-171].al ! NUM = 1-144
Y15134.[1-162].al ! NUM = 145-306
Y15135.[1-169].al ! NUM = 307-475
Y15136.[1-176].al ! NUM = 476-651
Y15137.[1-175].al ! NUM = 652-826
Y15138.[1-155].al ! NUM = 827-981
Y15139.[1-147].al ! NUM = 982-1128
Y15140.[1-129].al ! NUM = 1129-1257
Y15141.[1-12,15-19,25-29,45-46].al ! NUM = 1258-1281
Y01409.[1-52].al ! NUM = 1282-1333
Y01425.[1-5].al ! NUM = 1334-1338
Y15142.[29-144].al ! NUM = 1339-1454
