*
*  Fatmen Nickname: alld98_d2
*  Generic Name : /castor/cern.ch/delphi/tape/C001-330
*  Description    : XShort DSTs, all events after DAFNE tag; 98 data, 4th proc.  Fix 2
*   Comments     :    330 files, total size = 75.506 Gb, in 330 files
*
Y12295.[1-87].al ! NUM = 1-87
Y12296.[1-48].al ! NUM = 88-135
Y12295.[88-89].al ! NUM = 136-137
Y12296.[49-90].al ! NUM = 138-179
Y12297.[1-90].al ! NUM = 180-269
Y12299.[1-61].al ! NUM = 270-330
