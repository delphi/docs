*
*  Fatmen Nickname: sh_qqps_m94_2l_b3
*  Generic Name : /castor/cern.ch/delphi/tape/C001-41
*  Description    : ShortDST QQbar (b life 1.6); Sim. of 94 (liq) ANA94B Fix 3  ; MILA
*   Comments     :    41 files, total size = 4.467 Gb, in 41 files
*
Y10042.92.al ! NUM = 1
Y10044.[127,140,155-159,161-166,171-179,181-182,191-196,201-206,211-212].al ! NUM = 2-39
Y10045.[15,23].al ! NUM = 40-41
