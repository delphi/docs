*
*  Fatmen Nickname: xs_zgpy_e188_f98_1l_d2
*  Generic Name : /castor/cern.ch/delphi/tape/C001-243
*  Description    : XShortDST Pythia qqbar at 188 GeV         ANA98D             FARM;
*   Comments     :    243 files, total size = 58.327 Gb, in 243 files
*
Y13741.[1-66].al ! NUM = 1-66
Y13742.[1-69].al ! NUM = 67-135
Y13743.[1-69].al ! NUM = 136-204
Y13744.[1-39].al ! NUM = 205-243
