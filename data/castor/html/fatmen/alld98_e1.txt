*
*  Fatmen Nickname: alld98_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-380
*  Description    : XShort DSTs, all events after DAFNE tag; 98 data, 5th proc.  Fix 1
*   Comments     :    380 files, total size = 92.178 Gb, in 380 files
*
R07134.[1-71].al ! NUM = 1-71
R07135.[1-72].al ! NUM = 72-143
R07136.[1-71].al ! NUM = 144-214
R07137.[1-72].al ! NUM = 215-286
R07138.[1-72].al ! NUM = 287-358
R07139.[1-22].al ! NUM = 359-380
