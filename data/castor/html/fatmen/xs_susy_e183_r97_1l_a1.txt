*
*  Fatmen Nickname: xs_susy_e183_r97_1l_a1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-59
*  Description    : XShortDST Susy signal                ; Sim. 97 ANA97A Fix 1 ; RUTH 
*   Comments     :    59 files, total size = 1.405 Gb, in 59 files
*
Y13705.[250-264,323-343,265-287].al ! NUM = 1-59
