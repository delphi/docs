*
*  Fatmen Nickname: xsdst98_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-83
*  Description    : XShort DSTs, events after DELANA filter; 98 data, 5th proc.  Fix 1
*   Comments     :    83 files, total size = 20.263 Gb, in 83 files
*
R05115.[1-79].al ! NUM = 1-79
R07143.[1-4].al ! NUM = 80-83
