*
*  Fatmen Nickname: scan00_u1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-1944
*  Description    : RTXS, events after DAFNE tag for scanning; 2000 data,5th proc. Fix 1
*   Comments     :    1944 files, total size = 8.816 Gb, in 1944 files
*
R02103.[1-994].al ! NUM = 1-994
R02104.[1-950].al ! NUM = 995-1944
