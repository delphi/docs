*
*  Fatmen Nickname: dsto99_e200_d
*  Generic Name : /castor/cern.ch/delphi/tape/C001-1458
*  Description    : RTD or D only, all events after DELANA filter;99 data 4th proc.
*   Comments     :    1458 files, total size = 314.122 Gb, in 1458 files
*
R02304.[1-137].al ! NUM = 1-137
R02305.[1-124].al ! NUM = 138-261
R02306.[1-114].al ! NUM = 262-375
R02307.[1-135].al ! NUM = 376-510
R02308.[1-132].al ! NUM = 511-642
R02309.[1-132].al ! NUM = 643-774
R02310.[1-124].al ! NUM = 775-898
R02311.[1-113].al ! NUM = 899-1011
R02312.[1-102].al ! NUM = 1012-1113
R02313.[1-108].al ! NUM = 1114-1221
R02314.[1-103].al ! NUM = 1222-1324
R02315.[1-103].al ! NUM = 1325-1427
R02316.[1-31].al ! NUM = 1428-1458
