*
*  Fatmen Nickname: xsdst97_d2
*  Generic Name : /castor/cern.ch/delphi/tape/C001-257
*  Description    : XShort DSTs, all events after DAFNE tag;   97 data,4rd proc. Fix 2
*   Comments     :    257 files, total size = 24.969 Gb, in 257 files
*
Y10183.[1-99].al ! NUM = 1-99
Y10184.[1-99].al ! NUM = 100-198
Y10185.[1-59].al ! NUM = 199-257
