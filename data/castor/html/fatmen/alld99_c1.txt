*
*  Fatmen Nickname: alld99_c1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-324
*  Description    : XShort DSTs, all events after DAFNE tag; 99 data, 3th proc.  Fix 1
*   Comments     :    324 files, total size = 78.592 Gb, in 324 files
*
Y15103.[1-49].al ! NUM = 1-49
Y15109.[1-89].al ! NUM = 50-138
Y15131.[1-21].al ! NUM = 139-159
Y01421.[1-35].al ! NUM = 160-194
Y01422.[1-35].al ! NUM = 195-229
Y01423.[1-35].al ! NUM = 230-264
Y01424.[1-9].al ! NUM = 265-273
Y01414.[1-35].al ! NUM = 274-308
Y01415.[1-16].al ! NUM = 309-324
