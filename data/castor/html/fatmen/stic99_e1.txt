*
*  Fatmen Nickname: stic99_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-64
*  Description    : XShort DSTs, STIC single arm events     ; 99 data, 5th proc. Fix 1
*   Comments     :    64 files, total size = 15.339 Gb, in 64 files
*
R02356.[1-22].al ! NUM = 1-22
R02434.[1-21].al ! NUM = 23-43
R05098.[1-11].al ! NUM = 44-54
R05109.[1-10].al ! NUM = 55-64
