*
*  Fatmen Nickname: dsto99_d
*  Generic Name : /castor/cern.ch/delphi/tape/C001-4269
*  Description    : RTD or D only, all events after DELANA filter;99 data 4th proc.
*   Comments     :    4269 files, total size = 883.507 Gb, in 4269 files
*
R02297.[1-185].al ! NUM = 1-185
R02298.[1-162].al ! NUM = 186-347
R02299.[1-155].al ! NUM = 348-502
R02300.[1-134].al ! NUM = 503-636
R02301.[1-90].al ! NUM = 637-726
R02304.[1-137].al ! NUM = 727-863
R02305.[1-124].al ! NUM = 864-987
R02306.[1-114].al ! NUM = 988-1101
R02307.[1-135].al ! NUM = 1102-1236
R02308.[1-132].al ! NUM = 1237-1368
R02309.[1-132].al ! NUM = 1369-1500
R02310.[1-124].al ! NUM = 1501-1624
R02311.[1-113].al ! NUM = 1625-1737
R02312.[1-102].al ! NUM = 1738-1839
R02313.[1-108].al ! NUM = 1840-1947
R02314.[1-103].al ! NUM = 1948-2050
R02315.[1-103].al ! NUM = 2051-2153
R02316.[1-31].al ! NUM = 2154-2184
R02324.[1-103].al ! NUM = 2185-2287
R02325.[1-110].al ! NUM = 2288-2397
R02326.[1-123].al ! NUM = 2398-2520
R02327.[1-114].al ! NUM = 2521-2634
R02328.[1-115].al ! NUM = 2635-2749
R02329.[1-97].al ! NUM = 2750-2846
R02335.[1-135].al ! NUM = 2847-2981
R02336.[1-133].al ! NUM = 2982-3114
R02337.[1-121].al ! NUM = 3115-3235
R02338.[1-133].al ! NUM = 3236-3368
R02339.[1-126].al ! NUM = 3369-3494
R02340.[1-124].al ! NUM = 3495-3618
R02341.[1-112].al ! NUM = 3619-3730
R02342.[1-114].al ! NUM = 3731-3844
R02343.[1-112].al ! NUM = 3845-3956
R02344.[1-71].al ! NUM = 3957-4027
R02335.[136-138].al ! NUM = 4028-4030
R02344.[72-111].al ! NUM = 4031-4070
R02345.[1-123].al ! NUM = 4071-4193
R02346.[1-76].al ! NUM = 4194-4269
