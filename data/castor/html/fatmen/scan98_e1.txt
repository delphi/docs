*
*  Fatmen Nickname: scan98_e1
*  Generic Name : /castor/cern.ch/delphi/tape/C001-4955
*  Description    : RTXS, events after DAFNE tag for scanning; 98 data,5th proc. Fix 1
*   Comments     :    4955 files, total size = 26.043 Gb, in 4955 files
*
R05116.[1-999].al ! NUM = 1-999
R05117.[1-999].al ! NUM = 1000-1998
R05118.[1-999].al ! NUM = 1999-2997
R05119.[1-999].al ! NUM = 2998-3996
R05120.[1-959].al ! NUM = 3997-4955
