*
*   Nickname     : xs_zgpy6156b2_e189_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/ZGPY6156B2/CERN/SUMT
*   Description  :  Extended Short DST simulation 99_a1 189 , CERN
*---
*   Comments     :  time stamp: Fri Feb  9 19:35:43 2001
*---
*
*
*   Comments     : only short dst has been kept
*                 Z0/Gamma with Pythia 6.156 with the std. DELPHI Jetset tuning
*                 plus BE32 model
*        	  special settings:
*        	       MSTJ 52 = 9
*        	       MSTJ 53 = 0
*        	       MSTJ 54 = 2
*        	   
*        	       PRJ92 = 1.35
*        	       PRJ93 = 0.34
*        	   
*        	       TUNE = 0                                                *
*
*                 Feb. 2000 , US 

FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10210.xsdst ! RUN = 10210 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10301.xsdst ! RUN = 10301 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10302.xsdst ! RUN = 10302 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10303.xsdst ! RUN = 10303 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10304.xsdst ! RUN = 10304 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10305.xsdst ! RUN = 10305 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10306.xsdst ! RUN = 10306 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10307.xsdst ! RUN = 10307 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10308.xsdst ! RUN = 10308 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10309.xsdst ! RUN = 10309 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10311.xsdst ! RUN = 10311 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10312.xsdst ! RUN = 10312 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10313.xsdst ! RUN = 10313 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10314.xsdst ! RUN = 10314 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10315.xsdst ! RUN = 10315 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10316.xsdst ! RUN = 10316 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10317.xsdst ! RUN = 10317 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10318.xsdst ! RUN = 10318 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10319.xsdst ! RUN = 10319 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10320.xsdst ! RUN = 10320 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10321.xsdst ! RUN = 10321 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10322.xsdst ! RUN = 10322 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10323.xsdst ! RUN = 10323 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10324.xsdst ! RUN = 10324 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10325.xsdst ! RUN = 10325 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10326.xsdst ! RUN = 10326 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10327.xsdst ! RUN = 10327 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10328.xsdst ! RUN = 10328 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10329.xsdst ! RUN = 10329 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10330.xsdst ! RUN = 10330 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10331.xsdst ! RUN = 10331 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10332.xsdst ! RUN = 10332 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10333.xsdst ! RUN = 10333 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10334.xsdst ! RUN = 10334 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10335.xsdst ! RUN = 10335 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10336.xsdst ! RUN = 10336 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10337.xsdst ! RUN = 10337 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10338.xsdst ! RUN = 10338 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10339.xsdst ! RUN = 10339 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10340.xsdst ! RUN = 10340 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10341.xsdst ! RUN = 10341 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10342.xsdst ! RUN = 10342 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10343.xsdst ! RUN = 10343 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10344.xsdst ! RUN = 10344 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10345.xsdst ! RUN = 10345 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10346.xsdst ! RUN = 10346 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10347.xsdst ! RUN = 10347 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10348.xsdst ! RUN = 10348 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10349.xsdst ! RUN = 10349 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10350.xsdst ! RUN = 10350 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10351.xsdst ! RUN = 10351 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10352.xsdst ! RUN = 10352 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10353.xsdst ! RUN = 10353 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10354.xsdst ! RUN = 10354 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10355.xsdst ! RUN = 10355 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10356.xsdst ! RUN = 10356 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10357.xsdst ! RUN = 10357 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10358.xsdst ! RUN = 10358 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10359.xsdst ! RUN = 10359 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10360.xsdst ! RUN = 10360 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10361.xsdst ! RUN = 10361 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10362.xsdst ! RUN = 10362 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10363.xsdst ! RUN = 10363 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10364.xsdst ! RUN = 10364 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10365.xsdst ! RUN = 10365 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10366.xsdst ! RUN = 10366 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10367.xsdst ! RUN = 10367 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10368.xsdst ! RUN = 10368 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10369.xsdst ! RUN = 10369 ! NEVT = 998
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10370.xsdst ! RUN = 10370 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10371.xsdst ! RUN = 10371 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10372.xsdst ! RUN = 10372 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10373.xsdst ! RUN = 10373 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10374.xsdst ! RUN = 10374 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10375.xsdst ! RUN = 10375 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10376.xsdst ! RUN = 10376 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10377.xsdst ! RUN = 10377 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10378.xsdst ! RUN = 10378 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10379.xsdst ! RUN = 10379 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10380.xsdst ! RUN = 10380 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10381.xsdst ! RUN = 10381 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10382.xsdst ! RUN = 10382 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10383.xsdst ! RUN = 10383 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10384.xsdst ! RUN = 10384 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10385.xsdst ! RUN = 10385 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10386.xsdst ! RUN = 10386 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10387.xsdst ! RUN = 10387 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10388.xsdst ! RUN = 10388 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10389.xsdst ! RUN = 10389 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10390.xsdst ! RUN = 10390 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10391.xsdst ! RUN = 10391 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10392.xsdst ! RUN = 10392 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10393.xsdst ! RUN = 10393 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10394.xsdst ! RUN = 10394 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10395.xsdst ! RUN = 10395 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10396.xsdst ! RUN = 10396 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10397.xsdst ! RUN = 10397 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10398.xsdst ! RUN = 10398 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10399.xsdst ! RUN = 10399 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/zgpy6156b2/v99_4/189/run10400.xsdst ! RUN = 10400 ! NEVT = 1000
