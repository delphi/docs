*
*   Nickname     : sh_kk2f4146qqardcy_e136.3_c95_1l_d2
*   Generic Name : //CERN/DELPHI/P02_SIMD/SHORT/KK2F4146QQARDCY/E136.3/CERN/SUMT/C001-6
*   Description  :  Short DST simulation 95d2 done at ecms=136.3 , CERN
*---
*   Comments     : in total 17991 events in 6 files time stamp: Fri Aug  9 15:13:04 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136.3/kk2f4146_qqardcy_136.3_51007.sdst ! RUN = 51007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136.3/kk2f4146_qqardcy_136.3_51008.sdst ! RUN = 51008 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136.3/kk2f4146_qqardcy_136.3_51009.sdst ! RUN = 51009 ! NEVT = 2997
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136.3/kk2f4146_qqardcy_136.3_51010.sdst ! RUN = 51010 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136.3/kk2f4146_qqardcy_136.3_51011.sdst ! RUN = 51011 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136.3/kk2f4146_qqardcy_136.3_51012.sdst ! RUN = 51012 ! NEVT = 2999
