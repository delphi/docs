*
*   Nickname     : sh_kk2f4146qqardcy_e136_c95_1l_d2
*   Generic Name : //CERN/DELPHI/P02_SIMD/SHORT/KK2F4146QQARDCY/CERN/SUMT/C001-13
*   Description  :  Short DST simulation 95d2 136 , CERN
*---
*   Comments     : in total 24557 events in 13 files time stamp: Wed Aug  7 06:14:25 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51000.sdst ! RUN = 51000 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51001.sdst ! RUN = 51001 ! NEVT = 563
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51002.sdst ! RUN = 51002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51003.sdst ! RUN = 51003 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51004.sdst ! RUN = 51004 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51005.sdst ! RUN = 51005 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51006.sdst ! RUN = 51006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51007.sdst ! RUN = 51007 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51008.sdst ! RUN = 51008 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51009.sdst ! RUN = 51009 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51010.sdst ! RUN = 51010 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51011.sdst ! RUN = 51011 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/136/kk2f4146_qqardcy_136_51012.sdst ! RUN = 51012 ! NEVT = 2999
