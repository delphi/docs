*
*   Nickname     : sh_kk2f4146qqardcy_e130_c95_1l_d2
*   Generic Name : //CERN/DELPHI/P02_SIMD/SHORT/KK2F4146QQARDCY/CERN/SUMT/C001-12
*   Description  :  Short DST simulation 95d2 130 , CERN
*---
*   Comments     : in total 21994 events in 12 files time stamp: Wed Aug  7 15:12:22 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50000.sdst ! RUN = 50000 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50001.sdst ! RUN = 50001 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50002.sdst ! RUN = 50002 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50003.sdst ! RUN = 50003 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50004.sdst ! RUN = 50004 ! NEVT = 999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50005.sdst ! RUN = 50005 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50006.sdst ! RUN = 50006 ! NEVT = 1000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50007.sdst ! RUN = 50007 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50008.sdst ! RUN = 50008 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50009.sdst ! RUN = 50009 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50010.sdst ! RUN = 50010 ! NEVT = 2999
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/130/kk2f4146_qqardcy_130_50012.sdst ! RUN = 50012 ! NEVT = 3000
