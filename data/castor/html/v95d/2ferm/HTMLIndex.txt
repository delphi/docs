# index of html fragments. Provides additional information for sorting files. DO NOT REMOVE.
#File Name                                       year processing   category      files          events     energy       mass      mass2       labo
sh_apacic105_e91.25_wp95_1l_d2                  1995      v95d2      2FERM        448         1316326      91.25          -          -       WUPP
sh_kk2f4146qqardcy_e130.3_c95_1l_d2             1995      v95d2      2FERM          6           14876      130.3          -          -       cern
sh_kk2f4146qqardcy_e130_c95_1l_d2               1995      v95d2      2FERM         12           21994        130          -          -       cern
sh_kk2f4146qqardcy_e136.3_c95_1l_d2             1995      v95d2      2FERM          6           17991      136.3          -          -       cern
sh_kk2f4146qqardcy_e136_c95_1l_d2               1995      v95d2      2FERM         13           24557        136          -          -       cern
sh_kk2f4146qqpy_e91.25_c95_1l_d2                1995      v95d2      2FERM        353         1031130      91.25          -          -       cern
sh_kk2f4146qqpy_e91.25_l95_1l_d2                1995      v95d2      2FERM         67          195447      91.25          -          -       lyon
sh_kk2f4146qqpybe321_e91.25_c95_1l_d2           1995      v95d2      2FERM        349         1017053      91.25          -          -       cern
sh_kk2f4146tthl_e89.437_c95_1l_d2               1995      v95d2      2FERM          3            8999     89.437          -          -       cern
sh_kk2f4146tthl_e91.25_c95_1l_d2                1995      v95d2      2FERM        129          490993      91.25          -          -       CERN
sh_kk2f4146tthl_e91.309_c95_1l_d2               1995      v95d2      2FERM          3            8998     91.309          -          -       cern
sh_kk2f4146tthl_e92.964_c95_1l_d2               1995      v95d2      2FERM          3            8998     92.964          -          -       cern
sh_qqps_e91.25_c95_1l_d2                        1995      v95d2      2FERM         76          226658      91.25          -          -       cern
