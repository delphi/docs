*
*   Nickname     : sh_kk2f4146tthl_e92.964_c95_1l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/KK2F4146TTHL/E92.964/CERN/SUMT/C001-3
*   Description  :  Short DST simulation 95d2 done at ecms=92.964 , CERN
*---
*   Comments     : in total 8998 events in 3 files time stamp: Thu Aug  1 01:12:34 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/92.964/kk2f4146_tthl_92.964_14001.sdst ! RUN = 14001 ! NEVT = 2998
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/92.964/kk2f4146_tthl_92.964_14002.sdst ! RUN = 14002 ! NEVT = 3000
FILE = /castor/cern.ch/delphi/MCprod/cern/kk2f4146/v95d/92.964/kk2f4146_tthl_92.964_14003.sdst ! RUN = 14003 ! NEVT = 3000
