*
*   Nickname     : sh_wphact23tttt_e91.25_m80.4_c95_1l_d2
*   Generic Name : //CERN/DELPHI/P01_SIMD/SHORT/WPHACT23TTTT/E91.25/CERN/SUMT/C001-5
*   Description  :  Short DST simulation 95d2 done at ecms=91.25 , CERN
*---
*   Comments     : in total 24996 events in 5 files time stamp: Sun Aug  4 18:14:03 2002
*---
*
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact23/v95d/91.25/wphact23_tttt_91.25_80.4_2101.sdst ! RUN = 2101 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact23/v95d/91.25/wphact23_tttt_91.25_80.4_2102.sdst ! RUN = 2102 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact23/v95d/91.25/wphact23_tttt_91.25_80.4_2103.sdst ! RUN = 2103 ! NEVT = 5000
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact23/v95d/91.25/wphact23_tttt_91.25_80.4_2104.sdst ! RUN = 2104 ! NEVT = 4999
FILE = /castor/cern.ch/delphi/MCprod/cern/wphact23/v95d/91.25/wphact23_tttt_91.25_80.4_2105.sdst ! RUN = 2105 ! NEVT = 4998
