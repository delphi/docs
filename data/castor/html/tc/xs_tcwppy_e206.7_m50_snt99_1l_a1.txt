*
*   Nickname     : xs_tcwppy_e206.7_m50_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/E206.7/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:54 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m50_r271501.xsdst ! RUN = 271501 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m50_r271502.xsdst ! RUN = 271502 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m50_r271503.xsdst ! RUN = 271503 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m50_r271504.xsdst ! RUN = 271504 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m50_r271505.xsdst ! RUN = 271505 ! NEVT = 400
