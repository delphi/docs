*
*   Nickname     : xs_tcwppy_e200_m90_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 200 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:53 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m90_r201901.xsdst ! RUN = 201901 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m90_r201902.xsdst ! RUN = 201902 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m90_r201903.xsdst ! RUN = 201903 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m90_r201904.xsdst ! RUN = 201904 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m90_r201905.xsdst ! RUN = 201905 ! NEVT = 400
