*
*   Nickname     : xs_tcpppy_e206.7_m70_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCPPPY/E206.7/SANTANDER/SUMT
*   Description  : e+e- ->PitPit  pitpit->bcbc (files 1-4) or pitpit->bc taunu (files 5-10) Extended Short DST simulation 99_a1 done at ecms=206.7 , Santander
*---
*   Comments     :  time stamp: Fri Apr 27 09:21:34 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_pitpit_m70_r270701.xsdst ! RUN = 270701 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_pitpit_m70_r270702.xsdst ! RUN = 270702 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_pitpit_m70_r270703.xsdst ! RUN = 270703 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_pitpit_m70_r270704.xsdst ! RUN = 270704 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_pitpit_m70_r270705.xsdst ! RUN = 270705 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_pitpit_207_70_370701.xsdst ! RUN = 370701 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_pitpit_207_70_370702.xsdst ! RUN = 370702 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_pitpit_207_70_370703.xsdst ! RUN = 370703 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_pitpit_207_70_370704.xsdst ! RUN = 370704 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_pitpit_207_70_370705.xsdst ! RUN = 370705 ! NEVT = 1000
