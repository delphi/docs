*
*   Nickname     : xs_tcpppy_e200_m50_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCPPPY/SANTANDER/SUMT
*   Description  : TechniColor signal samples, m=50GeV Extended Short DST simulation 99_a1 200 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:52 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m50_r200501.xsdst ! RUN = 200501 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m50_r200502.xsdst ! RUN = 200502 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m50_r200503.xsdst ! RUN = 200503 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m50_r200504.xsdst ! RUN = 200504 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m50_r200505.xsdst ! RUN = 200505 ! NEVT = 400
