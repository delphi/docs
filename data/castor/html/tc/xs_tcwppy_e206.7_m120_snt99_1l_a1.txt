*
*   Nickname     : xs_tcwppy_e206.7_m120_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/E206.7/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:54 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m120_r271201.xsdst ! RUN = 271201 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m120_r271202.xsdst ! RUN = 271202 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m120_r271203.xsdst ! RUN = 271203 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m120_r271204.xsdst ! RUN = 271204 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m120_r271205.xsdst ! RUN = 271205 ! NEVT = 400
