*
*   Nickname     : xs_tcwppy_e206.7_m125_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/E206.7/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:54 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m125_r271251.xsdst ! RUN = 271251 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m125_r271252.xsdst ! RUN = 271252 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m125_r271253.xsdst ! RUN = 271253 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m125_r271254.xsdst ! RUN = 271254 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m125_r271255.xsdst ! RUN = 271255 ! NEVT = 399
