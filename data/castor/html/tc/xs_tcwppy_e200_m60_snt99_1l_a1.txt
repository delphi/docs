*
*   Nickname     : xs_tcwppy_e200_m60_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 200 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:53 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m60_r201601.xsdst ! RUN = 201601 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m60_r201602.xsdst ! RUN = 201602 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m60_r201603.xsdst ! RUN = 201603 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m60_r201604.xsdst ! RUN = 201604 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m60_r201605.xsdst ! RUN = 201605 ! NEVT = 400
