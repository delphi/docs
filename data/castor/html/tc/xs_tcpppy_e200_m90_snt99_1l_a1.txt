*
*   Nickname     : xs_tcpppy_e200_m90_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCPPPY/SANTANDER/SUMT
*   Description  : TechniColor signal samples, m=90GeV Extended Short DST simulation 99_a1 200 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:52 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m90_r200901.xsdst ! RUN = 200901 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m90_r200902.xsdst ! RUN = 200902 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m90_r200903.xsdst ! RUN = 200903 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m90_r200904.xsdst ! RUN = 200904 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_pitpit_m90_r200905.xsdst ! RUN = 200905 ! NEVT = 400
