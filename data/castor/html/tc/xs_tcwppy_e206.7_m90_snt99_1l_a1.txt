*
*   Nickname     : xs_tcwppy_e206.7_m90_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/E206.7/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:54 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m90_r271901.xsdst ! RUN = 271901 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m90_r271902.xsdst ! RUN = 271902 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m90_r271903.xsdst ! RUN = 271903 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m90_r271904.xsdst ! RUN = 271904 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/206.7/tc_207_wpit_m90_r271905.xsdst ! RUN = 271905 ! NEVT = 399
