*
*   Nickname     : xs_tcwppy_e200_m80_snt99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TCWPPY/SANTANDER/SUMT
*   Description  :  Extended Short DST simulation 99_a1 200 , Santander
*---
*   Comments     :  time stamp: Thu Apr 26 14:41:53 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m80_r201801.xsdst ! RUN = 201801 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m80_r201802.xsdst ! RUN = 201802 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m80_r201803.xsdst ! RUN = 201803 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m80_r201804.xsdst ! RUN = 201804 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/sant/tc/v99_4/200/tc_200_wpit_m80_r201805.xsdst ! RUN = 201805 ! NEVT = 400
