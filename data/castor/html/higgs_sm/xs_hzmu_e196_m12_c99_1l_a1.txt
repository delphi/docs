*
*   Nickname     : xs_hzmu_e196_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZMU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:59 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2411.xsdst ! RUN = 2411 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2412.xsdst ! RUN = 2412 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2413.xsdst ! RUN = 2413 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2414.xsdst ! RUN = 2414 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2415.xsdst ! RUN = 2415 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2416.xsdst ! RUN = 2416 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2417.xsdst ! RUN = 2417 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2418.xsdst ! RUN = 2418 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2419.xsdst ! RUN = 2419 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzmu12_196_2420.xsdst ! RUN = 2420 ! NEVT = 200
