*
*   Nickname     : xs_hzmu_e202_m18_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZMU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:06 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3031.xsdst ! RUN = 3031 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3032.xsdst ! RUN = 3032 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3033.xsdst ! RUN = 3033 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3034.xsdst ! RUN = 3034 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3035.xsdst ! RUN = 3035 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3036.xsdst ! RUN = 3036 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3037.xsdst ! RUN = 3037 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3038.xsdst ! RUN = 3038 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3039.xsdst ! RUN = 3039 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu18_202_3040.xsdst ! RUN = 3040 ! NEVT = 200
