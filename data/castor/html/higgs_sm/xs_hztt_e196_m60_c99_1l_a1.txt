*
*   Nickname     : xs_hztt_e196_m60_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZTT/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:47 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7411.xsdst ! RUN = 7411 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7412.xsdst ! RUN = 7412 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7413.xsdst ! RUN = 7413 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7414.xsdst ! RUN = 7414 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7415.xsdst ! RUN = 7415 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7416.xsdst ! RUN = 7416 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7417.xsdst ! RUN = 7417 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7418.xsdst ! RUN = 7418 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7419.xsdst ! RUN = 7419 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt60_196_7420.xsdst ! RUN = 7420 ! NEVT = 200
