*
*   Nickname     : xs_hznu_e196_m115_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZNU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:15 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12611.xsdst ! RUN = 12611 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12612.xsdst ! RUN = 12612 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12613.xsdst ! RUN = 12613 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12614.xsdst ! RUN = 12614 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12615.xsdst ! RUN = 12615 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12616.xsdst ! RUN = 12616 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12617.xsdst ! RUN = 12617 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12618.xsdst ! RUN = 12618 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12619.xsdst ! RUN = 12619 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hznu115_196_12620.xsdst ! RUN = 12620 ! NEVT = 200
