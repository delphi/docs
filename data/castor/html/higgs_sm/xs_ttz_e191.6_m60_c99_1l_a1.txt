*
*   Nickname     : xs_ttz_e191.6_m60_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TTZ/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:27:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7501.xsdst ! RUN = 7501 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7502.xsdst ! RUN = 7502 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7503.xsdst ! RUN = 7503 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7504.xsdst ! RUN = 7504 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7505.xsdst ! RUN = 7505 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7506.xsdst ! RUN = 7506 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7507.xsdst ! RUN = 7507 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7508.xsdst ! RUN = 7508 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7509.xsdst ! RUN = 7509 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz60_191.6_7510.xsdst ! RUN = 7510 ! NEVT = 200
