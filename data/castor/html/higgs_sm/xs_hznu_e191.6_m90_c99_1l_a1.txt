*
*   Nickname     : xs_hznu_e191.6_m90_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZNU/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:14 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10101.xsdst ! RUN = 10101 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10102.xsdst ! RUN = 10102 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10103.xsdst ! RUN = 10103 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10104.xsdst ! RUN = 10104 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10105.xsdst ! RUN = 10105 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10106.xsdst ! RUN = 10106 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10107.xsdst ! RUN = 10107 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10108.xsdst ! RUN = 10108 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10109.xsdst ! RUN = 10109 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hznu90_191.6_10110.xsdst ! RUN = 10110 ! NEVT = 200
