*
*   Nickname     : xs_hzqq_e191.6_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZQQ/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:28 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8001.xsdst ! RUN = 8001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8002.xsdst ! RUN = 8002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8003.xsdst ! RUN = 8003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8004.xsdst ! RUN = 8004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8005.xsdst ! RUN = 8005 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8006.xsdst ! RUN = 8006 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8007.xsdst ! RUN = 8007 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8008.xsdst ! RUN = 8008 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8009.xsdst ! RUN = 8009 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq70_191.6_8010.xsdst ! RUN = 8010 ! NEVT = 200
