*
*   Nickname     : xs_hzee_e196_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:45 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6311.xsdst ! RUN = 6311 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6312.xsdst ! RUN = 6312 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6313.xsdst ! RUN = 6313 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6314.xsdst ! RUN = 6314 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6315.xsdst ! RUN = 6315 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6316.xsdst ! RUN = 6316 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6317.xsdst ! RUN = 6317 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6318.xsdst ! RUN = 6318 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6319.xsdst ! RUN = 6319 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee50_196_6320.xsdst ! RUN = 6320 ! NEVT = 200
