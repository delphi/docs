*
*   Nickname     : xs_hzee_e201.8_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E201.8/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=201.8 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:50 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2531.xsdst ! RUN = 2531 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2532.xsdst ! RUN = 2532 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2533.xsdst ! RUN = 2533 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2534.xsdst ! RUN = 2534 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2535.xsdst ! RUN = 2535 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2536.xsdst ! RUN = 2536 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2537.xsdst ! RUN = 2537 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2538.xsdst ! RUN = 2538 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2539.xsdst ! RUN = 2539 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee12_201.8_2540.xsdst ! RUN = 2540 ! NEVT = 200
