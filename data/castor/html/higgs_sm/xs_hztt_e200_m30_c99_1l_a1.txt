*
*   Nickname     : xs_hztt_e200_m30_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZTT/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:49 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4421.xsdst ! RUN = 4421 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4422.xsdst ! RUN = 4422 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4423.xsdst ! RUN = 4423 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4424.xsdst ! RUN = 4424 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4425.xsdst ! RUN = 4425 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4426.xsdst ! RUN = 4426 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4427.xsdst ! RUN = 4427 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4428.xsdst ! RUN = 4428 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4429.xsdst ! RUN = 4429 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hztt30_200_4430.xsdst ! RUN = 4430 ! NEVT = 200
