*
*   Nickname     : xs_hztt_e202_m40_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZTT/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:53 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5431.xsdst ! RUN = 5431 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5432.xsdst ! RUN = 5432 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5433.xsdst ! RUN = 5433 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5434.xsdst ! RUN = 5434 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5435.xsdst ! RUN = 5435 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5436.xsdst ! RUN = 5436 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5437.xsdst ! RUN = 5437 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5438.xsdst ! RUN = 5438 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5439.xsdst ! RUN = 5439 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt40_202_5440.xsdst ! RUN = 5440 ! NEVT = 200
