*
*   Nickname     : xs_hzmu_e200_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZMU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3621.xsdst ! RUN = 3621 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3622.xsdst ! RUN = 3622 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3623.xsdst ! RUN = 3623 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3624.xsdst ! RUN = 3624 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3625.xsdst ! RUN = 3625 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3626.xsdst ! RUN = 3626 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3627.xsdst ! RUN = 3627 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3628.xsdst ! RUN = 3628 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3629.xsdst ! RUN = 3629 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/hzmu24_200_3630.xsdst ! RUN = 3630 ! NEVT = 200
