*
*   Nickname     : xs_hzee_e201.8_m50_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E201.8/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=201.8 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:52 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6331.xsdst ! RUN = 6331 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6332.xsdst ! RUN = 6332 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6333.xsdst ! RUN = 6333 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6334.xsdst ! RUN = 6334 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6335.xsdst ! RUN = 6335 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6336.xsdst ! RUN = 6336 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6337.xsdst ! RUN = 6337 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6338.xsdst ! RUN = 6338 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6339.xsdst ! RUN = 6339 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee50_201.8_6340.xsdst ! RUN = 6340 ! NEVT = 200
