*
*   Nickname     : xs_hzqq_e202_m60_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZQQ/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:38 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7031.xsdst ! RUN = 7031 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7032.xsdst ! RUN = 7032 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7033.xsdst ! RUN = 7033 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7034.xsdst ! RUN = 7034 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7035.xsdst ! RUN = 7035 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7036.xsdst ! RUN = 7036 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7037.xsdst ! RUN = 7037 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7038.xsdst ! RUN = 7038 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7039.xsdst ! RUN = 7039 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq60_202_7040.xsdst ! RUN = 7040 ! NEVT = 200
