*
*   Nickname     : xs_hznu_e202_m18_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZNU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:22 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2931.xsdst ! RUN = 2931 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2932.xsdst ! RUN = 2932 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2933.xsdst ! RUN = 2933 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2934.xsdst ! RUN = 2934 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2935.xsdst ! RUN = 2935 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2936.xsdst ! RUN = 2936 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2937.xsdst ! RUN = 2937 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2938.xsdst ! RUN = 2938 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2939.xsdst ! RUN = 2939 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu18_202_2940.xsdst ! RUN = 2940 ! NEVT = 200
