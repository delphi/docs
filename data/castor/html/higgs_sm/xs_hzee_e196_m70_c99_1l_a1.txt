*
*   Nickname     : xs_hzee_e196_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:46 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8311.xsdst ! RUN = 8311 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8312.xsdst ! RUN = 8312 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8313.xsdst ! RUN = 8313 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8314.xsdst ! RUN = 8314 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8315.xsdst ! RUN = 8315 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8316.xsdst ! RUN = 8316 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8317.xsdst ! RUN = 8317 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8318.xsdst ! RUN = 8318 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8319.xsdst ! RUN = 8319 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hzee70_196_8320.xsdst ! RUN = 8320 ! NEVT = 200
