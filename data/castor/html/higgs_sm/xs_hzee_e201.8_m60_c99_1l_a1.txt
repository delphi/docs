*
*   Nickname     : xs_hzee_e201.8_m60_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E201.8/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=201.8 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:52 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7331.xsdst ! RUN = 7331 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7332.xsdst ! RUN = 7332 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7333.xsdst ! RUN = 7333 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7334.xsdst ! RUN = 7334 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7335.xsdst ! RUN = 7335 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7336.xsdst ! RUN = 7336 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7337.xsdst ! RUN = 7337 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7338.xsdst ! RUN = 7338 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7339.xsdst ! RUN = 7339 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/201.8/hzee60_201.8_7340.xsdst ! RUN = 7340 ! NEVT = 200
