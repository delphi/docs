*
*   Nickname     : xs_hztt_e191.6_m85_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZTT/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:43 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9901.xsdst ! RUN = 9901 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9902.xsdst ! RUN = 9902 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9903.xsdst ! RUN = 9903 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9904.xsdst ! RUN = 9904 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9905.xsdst ! RUN = 9905 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9906.xsdst ! RUN = 9906 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9907.xsdst ! RUN = 9907 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9908.xsdst ! RUN = 9908 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9909.xsdst ! RUN = 9909 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hztt85_191.6_9910.xsdst ! RUN = 9910 ! NEVT = 200
