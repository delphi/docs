*
*   Nickname     : xs_ttz_e200_m60_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TTZ/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:27:10 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7521.xsdst ! RUN = 7521 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7522.xsdst ! RUN = 7522 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7523.xsdst ! RUN = 7523 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7524.xsdst ! RUN = 7524 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7525.xsdst ! RUN = 7525 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7526.xsdst ! RUN = 7526 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7527.xsdst ! RUN = 7527 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7528.xsdst ! RUN = 7528 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7529.xsdst ! RUN = 7529 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz60_200_7530.xsdst ! RUN = 7530 ! NEVT = 200
