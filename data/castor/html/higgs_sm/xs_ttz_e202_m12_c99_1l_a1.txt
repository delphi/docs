*
*   Nickname     : xs_ttz_e202_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TTZ/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:27:11 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2731.xsdst ! RUN = 2731 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2732.xsdst ! RUN = 2732 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2733.xsdst ! RUN = 2733 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2734.xsdst ! RUN = 2734 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2735.xsdst ! RUN = 2735 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2736.xsdst ! RUN = 2736 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2737.xsdst ! RUN = 2737 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2738.xsdst ! RUN = 2738 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2739.xsdst ! RUN = 2739 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/ttz12_202_2740.xsdst ! RUN = 2740 ! NEVT = 200
