*
*   Nickname     : xs_hzqq_e202_m30_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZQQ/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4031.xsdst ! RUN = 4031 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4032.xsdst ! RUN = 4032 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4033.xsdst ! RUN = 4033 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4034.xsdst ! RUN = 4034 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4035.xsdst ! RUN = 4035 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4036.xsdst ! RUN = 4036 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4037.xsdst ! RUN = 4037 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4038.xsdst ! RUN = 4038 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4039.xsdst ! RUN = 4039 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzqq30_202_4040.xsdst ! RUN = 4040 ! NEVT = 200
