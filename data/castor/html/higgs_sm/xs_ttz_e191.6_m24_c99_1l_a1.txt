*
*   Nickname     : xs_ttz_e191.6_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TTZ/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:27:02 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3901.xsdst ! RUN = 3901 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3902.xsdst ! RUN = 3902 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3903.xsdst ! RUN = 3903 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3904.xsdst ! RUN = 3904 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3905.xsdst ! RUN = 3905 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3906.xsdst ! RUN = 3906 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3907.xsdst ! RUN = 3907 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3908.xsdst ! RUN = 3908 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3909.xsdst ! RUN = 3909 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/ttz24_191.6_3910.xsdst ! RUN = 3910 ! NEVT = 200
