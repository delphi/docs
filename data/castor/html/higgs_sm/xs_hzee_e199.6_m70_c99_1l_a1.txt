*
*   Nickname     : xs_hzee_e199.6_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E199.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=199.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:50 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8321.xsdst ! RUN = 8321 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8322.xsdst ! RUN = 8322 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8323.xsdst ! RUN = 8323 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8324.xsdst ! RUN = 8324 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8325.xsdst ! RUN = 8325 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8326.xsdst ! RUN = 8326 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8327.xsdst ! RUN = 8327 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8328.xsdst ! RUN = 8328 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8329.xsdst ! RUN = 8329 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee70_199.6_8330.xsdst ! RUN = 8330 ! NEVT = 199
