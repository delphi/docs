*
*   Nickname     : xs_hztt_e202_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZTT/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:54 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8431.xsdst ! RUN = 8431 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8432.xsdst ! RUN = 8432 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8433.xsdst ! RUN = 8433 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8434.xsdst ! RUN = 8434 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8435.xsdst ! RUN = 8435 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8436.xsdst ! RUN = 8436 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8437.xsdst ! RUN = 8437 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8438.xsdst ! RUN = 8438 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8439.xsdst ! RUN = 8439 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hztt70_202_8440.xsdst ! RUN = 8440 ! NEVT = 200
