*
*   Nickname     : xs_hzee_e191.6_m80_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:42 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9301.xsdst ! RUN = 9301 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9302.xsdst ! RUN = 9302 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9303.xsdst ! RUN = 9303 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9304.xsdst ! RUN = 9304 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9305.xsdst ! RUN = 9305 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9306.xsdst ! RUN = 9306 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9307.xsdst ! RUN = 9307 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9308.xsdst ! RUN = 9308 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9309.xsdst ! RUN = 9309 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzee80_191.6_9310.xsdst ! RUN = 9310 ! NEVT = 200
