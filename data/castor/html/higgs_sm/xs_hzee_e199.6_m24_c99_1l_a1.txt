*
*   Nickname     : xs_hzee_e199.6_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E199.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=199.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:48 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3721.xsdst ! RUN = 3721 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3722.xsdst ! RUN = 3722 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3723.xsdst ! RUN = 3723 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3724.xsdst ! RUN = 3724 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3725.xsdst ! RUN = 3725 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3726.xsdst ! RUN = 3726 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3727.xsdst ! RUN = 3727 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3728.xsdst ! RUN = 3728 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3729.xsdst ! RUN = 3729 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee24_199.6_3730.xsdst ! RUN = 3730 ! NEVT = 200
