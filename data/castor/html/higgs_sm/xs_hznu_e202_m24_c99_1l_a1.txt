*
*   Nickname     : xs_hznu_e202_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZNU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:22 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3531.xsdst ! RUN = 3531 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3532.xsdst ! RUN = 3532 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3533.xsdst ! RUN = 3533 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3534.xsdst ! RUN = 3534 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3535.xsdst ! RUN = 3535 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3536.xsdst ! RUN = 3536 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3537.xsdst ! RUN = 3537 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3538.xsdst ! RUN = 3538 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3539.xsdst ! RUN = 3539 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu24_202_3540.xsdst ! RUN = 3540 ! NEVT = 200
