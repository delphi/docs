*
*   Nickname     : xs_hzee_e199.6_m40_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZEE/E199.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z-> e+e-) Extended Short DST simulation 99_a1 done at ecms=199.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:24:48 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5321.xsdst ! RUN = 5321 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5322.xsdst ! RUN = 5322 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5323.xsdst ! RUN = 5323 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5324.xsdst ! RUN = 5324 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5325.xsdst ! RUN = 5325 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5326.xsdst ! RUN = 5326 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5327.xsdst ! RUN = 5327 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5328.xsdst ! RUN = 5328 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5329.xsdst ! RUN = 5329 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/199.6/hzee40_199.6_5330.xsdst ! RUN = 5330 ! NEVT = 200
