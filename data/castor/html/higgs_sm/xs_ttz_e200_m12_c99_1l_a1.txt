*
*   Nickname     : xs_ttz_e200_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TTZ/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:27:08 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2721.xsdst ! RUN = 2721 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2722.xsdst ! RUN = 2722 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2723.xsdst ! RUN = 2723 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2724.xsdst ! RUN = 2724 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2725.xsdst ! RUN = 2725 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2726.xsdst ! RUN = 2726 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2727.xsdst ! RUN = 2727 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2728.xsdst ! RUN = 2728 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2729.xsdst ! RUN = 2729 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/200/ttz12_200_2730.xsdst ! RUN = 2730 ! NEVT = 200
