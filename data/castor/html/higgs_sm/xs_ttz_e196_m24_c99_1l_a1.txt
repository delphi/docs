*
*   Nickname     : xs_ttz_e196_m24_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/TTZ/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  tau+tau- Z -> q qbar) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:27:06 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3911.xsdst ! RUN = 3911 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3912.xsdst ! RUN = 3912 ! NEVT = 198
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3913.xsdst ! RUN = 3913 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3914.xsdst ! RUN = 3914 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3915.xsdst ! RUN = 3915 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3916.xsdst ! RUN = 3916 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3917.xsdst ! RUN = 3917 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3918.xsdst ! RUN = 3918 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3919.xsdst ! RUN = 3919 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/ttz24_196_3920.xsdst ! RUN = 3920 ! NEVT = 200
