*
*   Nickname     : xs_hzqq_e191.6_m110_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZQQ/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:25 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12001.xsdst ! RUN = 12001 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12002.xsdst ! RUN = 12002 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12003.xsdst ! RUN = 12003 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12004.xsdst ! RUN = 12004 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12005.xsdst ! RUN = 12005 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12006.xsdst ! RUN = 12006 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12007.xsdst ! RUN = 12007 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12008.xsdst ! RUN = 12008 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12009.xsdst ! RUN = 12009 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq110_191.6_12010.xsdst ! RUN = 12010 ! NEVT = 200
