*
*   Nickname     : xs_hzha03p6156hzqq_e206.7_m105_l99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HZQQ/E206.7/LYON, 4999 EVENTS/SUMT
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , Lyon, 4999 events
*---
*   Comments     :  time stamp: Thu Apr 19 19:16:16 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*                 HZHA03 interfaced to Pythia 6.156
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20500.xsdst ! RUN = 20500 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20501.xsdst ! RUN = 20501 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20502.xsdst ! RUN = 20502 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20503.xsdst ! RUN = 20503 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20504.xsdst ! RUN = 20504 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20505.xsdst ! RUN = 20505 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20506.xsdst ! RUN = 20506 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20507.xsdst ! RUN = 20507 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20508.xsdst ! RUN = 20508 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20509.xsdst ! RUN = 20509 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20510.xsdst ! RUN = 20510 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20511.xsdst ! RUN = 20511 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20512.xsdst ! RUN = 20512 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20513.xsdst ! RUN = 20513 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20514.xsdst ! RUN = 20514 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20515.xsdst ! RUN = 20515 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20516.xsdst ! RUN = 20516 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20517.xsdst ! RUN = 20517 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20518.xsdst ! RUN = 20518 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20519.xsdst ! RUN = 20519 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20520.xsdst ! RUN = 20520 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20521.xsdst ! RUN = 20521 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20522.xsdst ! RUN = 20522 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20523.xsdst ! RUN = 20523 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq105_206.7_20524.xsdst ! RUN = 20524 ! NEVT = 200
