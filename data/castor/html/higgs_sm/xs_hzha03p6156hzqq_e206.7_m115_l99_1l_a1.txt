*
*   Nickname     : xs_hzha03p6156hzqq_e206.7_m115_l99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HZQQ/E206.7/LYON,  4997 EVENTS/SUMT
*   Description  :  Extended Short DST simulation 99_a1 done at ecms=206.7 , Lyon,  4997 events
*---
*   Comments     :  time stamp: Thu Apr 19 19:16:16 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*                 HZHA03 interfaced to Pythia 6.156
*
*
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21500.xsdst ! RUN = 21500 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21501.xsdst ! RUN = 21501 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21502.xsdst ! RUN = 21502 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21503.xsdst ! RUN = 21503 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21504.xsdst ! RUN = 21504 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21505.xsdst ! RUN = 21505 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21506.xsdst ! RUN = 21506 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21507.xsdst ! RUN = 21507 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21508.xsdst ! RUN = 21508 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21509.xsdst ! RUN = 21509 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21510.xsdst ! RUN = 21510 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21511.xsdst ! RUN = 21511 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21512.xsdst ! RUN = 21512 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21513.xsdst ! RUN = 21513 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21514.xsdst ! RUN = 21514 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21515.xsdst ! RUN = 21515 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21516.xsdst ! RUN = 21516 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21517.xsdst ! RUN = 21517 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21518.xsdst ! RUN = 21518 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21519.xsdst ! RUN = 21519 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21520.xsdst ! RUN = 21520 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21521.xsdst ! RUN = 21521 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21522.xsdst ! RUN = 21522 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21523.xsdst ! RUN = 21523 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/lyon/hzha03pyth6156/v99_4/206.7/hzqq115_206.7_21524.xsdst ! RUN = 21524 ! NEVT = 200
