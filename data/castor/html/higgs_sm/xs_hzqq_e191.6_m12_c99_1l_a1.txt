*
*   Nickname     : xs_hzqq_e191.6_m12_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZQQ/E191.6/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> q qbar) Extended Short DST simulation 99_a1 done at ecms=191.6 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:25 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2201.xsdst ! RUN = 2201 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2202.xsdst ! RUN = 2202 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2203.xsdst ! RUN = 2203 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2204.xsdst ! RUN = 2204 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2205.xsdst ! RUN = 2205 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2206.xsdst ! RUN = 2206 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2207.xsdst ! RUN = 2207 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2208.xsdst ! RUN = 2208 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2209.xsdst ! RUN = 2209 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/191.6/hzqq12_191.6_2210.xsdst ! RUN = 2210 ! NEVT = 200
