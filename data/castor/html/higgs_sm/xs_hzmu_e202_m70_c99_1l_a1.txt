*
*   Nickname     : xs_hzmu_e202_m70_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZMU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any  Z -> mu+mu-) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:08 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8231.xsdst ! RUN = 8231 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8232.xsdst ! RUN = 8232 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8233.xsdst ! RUN = 8233 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8234.xsdst ! RUN = 8234 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8235.xsdst ! RUN = 8235 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8236.xsdst ! RUN = 8236 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8237.xsdst ! RUN = 8237 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8238.xsdst ! RUN = 8238 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8239.xsdst ! RUN = 8239 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hzmu70_202_8240.xsdst ! RUN = 8240 ! NEVT = 200
