*
*   Nickname     : xs_hztt_e196_m80_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZTT/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any but tau Z -> tau+tau-) Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:48 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9411.xsdst ! RUN = 9411 ! NEVT = 199
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9412.xsdst ! RUN = 9412 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9413.xsdst ! RUN = 9413 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9414.xsdst ! RUN = 9414 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9415.xsdst ! RUN = 9415 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9416.xsdst ! RUN = 9416 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9417.xsdst ! RUN = 9417 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9418.xsdst ! RUN = 9418 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9419.xsdst ! RUN = 9419 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/196/hztt80_196_9420.xsdst ! RUN = 9420 ! NEVT = 200
