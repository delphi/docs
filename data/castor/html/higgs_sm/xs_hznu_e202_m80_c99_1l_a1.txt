*
*   Nickname     : xs_hznu_e202_m80_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZNU/CERN/SUMT
*   Description  : ShortDST HZHA03  e+e- --> HZ (H ->  any Z -> nu nubar) Extended Short DST simulation 99_a1 202 , CERN
*---
*   Comments     :  time stamp: Mon Feb 12 19:25:24 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9131.xsdst ! RUN = 9131 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9132.xsdst ! RUN = 9132 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9133.xsdst ! RUN = 9133 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9134.xsdst ! RUN = 9134 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9135.xsdst ! RUN = 9135 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9136.xsdst ! RUN = 9136 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9137.xsdst ! RUN = 9137 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9138.xsdst ! RUN = 9138 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9139.xsdst ! RUN = 9139 ! NEVT = 200
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03/v99_4/202/hznu80_202_9140.xsdst ! RUN = 9140 ! NEVT = 200
