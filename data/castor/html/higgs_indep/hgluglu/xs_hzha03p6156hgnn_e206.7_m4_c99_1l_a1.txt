*
*   Nickname     : xs_hzha03p6156hgnn_e206.7_m4_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGNN/E206.7/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_a1 done at ecms=206.7 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:03:11 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvgg_206.7_4_411.xsdst ! RUN = 411 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvgg_206.7_4_412.xsdst ! RUN = 412 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvgg_206.7_4_413.xsdst ! RUN = 413 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvgg_206.7_4_414.xsdst ! RUN = 414 ! NEVT = 500
