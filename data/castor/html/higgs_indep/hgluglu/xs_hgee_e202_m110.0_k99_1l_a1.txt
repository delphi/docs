*
*   Nickname     : xs_hgee_e202_m110.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee110.0_202._1.xsdst ! RUN = 14256 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee110.0_202._2.xsdst ! RUN = 14257 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee110.0_202._3.xsdst ! RUN = 14258 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee110.0_202._4.xsdst ! RUN = 14259 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee110.0_202._5.xsdst ! RUN = 14260 ! NEVT = 399
