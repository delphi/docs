*
*   Nickname     : xs_hgmm_e196_m90.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:38 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglmm90.0_196._1.xsdst ! RUN = 14606 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglmm90.0_196._2.xsdst ! RUN = 14607 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglmm90.0_196._3.xsdst ! RUN = 14608 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglmm90.0_196._4.xsdst ! RUN = 14609 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglmm90.0_196._5.xsdst ! RUN = 14610 ! NEVT = 400
