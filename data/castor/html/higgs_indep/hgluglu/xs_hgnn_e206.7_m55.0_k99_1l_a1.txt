*
*   Nickname     : xs_hgnn_e206.7_m55.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGNN/E206.7/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_a1 done at ecms=206.7 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:40 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglglnn55.0_206.7_1.xsdst ! RUN = 13831 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglglnn55.0_206.7_2.xsdst ! RUN = 13832 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglglnn55.0_206.7_3.xsdst ! RUN = 13833 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglglnn55.0_206.7_4.xsdst ! RUN = 13834 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglglnn55.0_206.7_5.xsdst ! RUN = 13835 ! NEVT = 400
