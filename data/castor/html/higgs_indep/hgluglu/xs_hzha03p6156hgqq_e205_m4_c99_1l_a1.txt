*
*   Nickname     : xs_hzha03p6156hgqq_e205_m4_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGQQ/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 205 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:02:50 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/205/hzha03pyth6156_hZ_qqgg_205_4_422.xsdst ! RUN = 422 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/205/hzha03pyth6156_hZ_qqgg_205_4_423.xsdst ! RUN = 423 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/205/hzha03pyth6156_hZ_qqgg_205_4_424.xsdst ! RUN = 424 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/205/hzha03pyth6156_hZ_qqgg_205_4_425.xsdst ! RUN = 425 ! NEVT = 500
