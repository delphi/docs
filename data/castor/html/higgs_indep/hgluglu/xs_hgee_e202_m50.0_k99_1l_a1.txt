*
*   Nickname     : xs_hgee_e202_m50.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee50.0_202._1.xsdst ! RUN = 14196 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee50.0_202._2.xsdst ! RUN = 14197 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee50.0_202._3.xsdst ! RUN = 14198 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee50.0_202._4.xsdst ! RUN = 14199 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglee50.0_202._5.xsdst ! RUN = 14200 ! NEVT = 400
