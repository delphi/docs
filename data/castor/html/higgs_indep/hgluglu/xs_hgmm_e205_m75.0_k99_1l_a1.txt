*
*   Nickname     : xs_hgmm_e205_m75.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_a1 205 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:39 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglmm75.0_205._1.xsdst ! RUN = 14786 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglmm75.0_205._2.xsdst ! RUN = 14787 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglmm75.0_205._3.xsdst ! RUN = 14788 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglmm75.0_205._4.xsdst ! RUN = 14789 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglmm75.0_205._5.xsdst ! RUN = 14790 ! NEVT = 399
