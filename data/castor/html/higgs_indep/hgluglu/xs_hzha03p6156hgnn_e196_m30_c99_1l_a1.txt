*
*   Nickname     : xs_hzha03p6156hgnn_e196_m30_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGNN/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:03:10 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvgg_196_30_3011.xsdst ! RUN = 3011 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvgg_196_30_3012.xsdst ! RUN = 3012 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvgg_196_30_3013.xsdst ! RUN = 3013 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvgg_196_30_3014.xsdst ! RUN = 3014 ! NEVT = 500
