*
*   Nickname     : xs_hzha03p6156hgqq_e200_m20_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGQQ/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 200 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:02:50 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hZ_qqgg_200_20_2022.xsdst ! RUN = 2022 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hZ_qqgg_200_20_2023.xsdst ! RUN = 2023 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hZ_qqgg_200_20_2024.xsdst ! RUN = 2024 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/200/hzha03pyth6156_hZ_qqgg_200_20_2025.xsdst ! RUN = 2025 ! NEVT = 500
