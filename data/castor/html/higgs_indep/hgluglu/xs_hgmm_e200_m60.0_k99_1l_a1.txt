*
*   Nickname     : xs_hgmm_e200_m60.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_a1 200 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:38 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglmm60.0_200._1.xsdst ! RUN = 14641 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglmm60.0_200._2.xsdst ! RUN = 14642 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglmm60.0_200._3.xsdst ! RUN = 14643 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglmm60.0_200._4.xsdst ! RUN = 14644 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglmm60.0_200._5.xsdst ! RUN = 14645 ! NEVT = 400
