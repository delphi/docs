*
*   Nickname     : xs_hgee_e205_m100.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->e+e-  Extended Short DST simulation 99_a1 205 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglee100.0_205._1.xsdst ! RUN = 14311 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglee100.0_205._2.xsdst ! RUN = 14312 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglee100.0_205._3.xsdst ! RUN = 14313 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglee100.0_205._4.xsdst ! RUN = 14314 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglglee100.0_205._5.xsdst ! RUN = 14315 ! NEVT = 400
