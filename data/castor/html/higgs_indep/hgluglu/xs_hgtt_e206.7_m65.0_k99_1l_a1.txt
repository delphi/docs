*
*   Nickname     : xs_hgtt_e206.7_m65.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGTT/E206.7/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->tau+tau- Extended Short DST simulation 99_a1 done at ecms=206.7 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:43 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglgltt65.0_206.7_1.xsdst ! RUN = 15341 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglgltt65.0_206.7_2.xsdst ! RUN = 15342 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglgltt65.0_206.7_3.xsdst ! RUN = 15343 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglgltt65.0_206.7_4.xsdst ! RUN = 15344 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hglgltt65.0_206.7_5.xsdst ! RUN = 15345 ! NEVT = 400
