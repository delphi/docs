*
*   Nickname     : xs_hgqq_e202_m105.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGQQ/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:41 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglqq105.0_202._1.xsdst ! RUN = 13251 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglqq105.0_202._2.xsdst ! RUN = 13252 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglqq105.0_202._3.xsdst ! RUN = 13253 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglqq105.0_202._4.xsdst ! RUN = 13254 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hglglqq105.0_202._5.xsdst ! RUN = 13255 ! NEVT = 399
