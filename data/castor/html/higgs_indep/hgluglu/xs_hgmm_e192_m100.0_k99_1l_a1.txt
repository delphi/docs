*
*   Nickname     : xs_hgmm_e192_m100.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->mu+mu-  Extended Short DST simulation 99_a1 192 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:38 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglmm100.0_192._1.xsdst ! RUN = 14551 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglmm100.0_192._2.xsdst ! RUN = 14552 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglmm100.0_192._3.xsdst ! RUN = 14553 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglmm100.0_192._4.xsdst ! RUN = 14554 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglmm100.0_192._5.xsdst ! RUN = 14555 ! NEVT = 400
