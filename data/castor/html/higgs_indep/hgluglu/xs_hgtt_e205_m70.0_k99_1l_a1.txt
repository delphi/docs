*
*   Nickname     : xs_hgtt_e205_m70.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGTT/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->tau+tau- Extended Short DST simulation 99_a1 205 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:43 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt70.0_205._1.xsdst ! RUN = 15281 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt70.0_205._2.xsdst ! RUN = 15282 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt70.0_205._3.xsdst ! RUN = 15283 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt70.0_205._4.xsdst ! RUN = 15284 ! NEVT = 398
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt70.0_205._5.xsdst ! RUN = 15285 ! NEVT = 400
