*
*   Nickname     : xs_hgtt_e196_m85.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGTT/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->tau+tau- Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:42 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglgltt85.0_196._1.xsdst ! RUN = 15101 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglgltt85.0_196._2.xsdst ! RUN = 15102 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglgltt85.0_196._3.xsdst ! RUN = 15103 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglgltt85.0_196._4.xsdst ! RUN = 15104 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglgltt85.0_196._5.xsdst ! RUN = 15105 ! NEVT = 399
