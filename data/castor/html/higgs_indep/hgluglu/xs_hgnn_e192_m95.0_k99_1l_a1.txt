*
*   Nickname     : xs_hgnn_e192_m95.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_a1 192 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:39 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglnn95.0_192._1.xsdst ! RUN = 13546 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglnn95.0_192._2.xsdst ! RUN = 13547 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglnn95.0_192._3.xsdst ! RUN = 13548 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglnn95.0_192._4.xsdst ! RUN = 13549 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hglglnn95.0_192._5.xsdst ! RUN = 13550 ! NEVT = 400
