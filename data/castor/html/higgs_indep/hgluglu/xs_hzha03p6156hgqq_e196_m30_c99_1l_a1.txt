*
*   Nickname     : xs_hzha03p6156hgqq_e196_m30_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGQQ/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:02:49 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_30_3021.xsdst ! RUN = 3021 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_30_3022.xsdst ! RUN = 3022 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_30_3023.xsdst ! RUN = 3023 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_30_3024.xsdst ! RUN = 3024 ! NEVT = 500
