*
*   Nickname     : xs_hgnn_e196_m90.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:39 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglnn90.0_196._1.xsdst ! RUN = 13606 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglnn90.0_196._2.xsdst ! RUN = 13607 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglnn90.0_196._3.xsdst ! RUN = 13608 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglnn90.0_196._4.xsdst ! RUN = 13609 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hglglnn90.0_196._5.xsdst ! RUN = 13610 ! NEVT = 400
