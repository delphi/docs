*
*   Nickname     : xs_hzha03p6156hgqq_e196_m15_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGQQ/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:02:49 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_15_1521.xsdst ! RUN = 1521 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_15_1522.xsdst ! RUN = 1522 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_15_1523.xsdst ! RUN = 1523 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqgg_196_15_1524.xsdst ! RUN = 1524 ! NEVT = 500
