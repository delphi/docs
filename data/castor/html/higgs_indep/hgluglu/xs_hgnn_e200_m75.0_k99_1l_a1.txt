*
*   Nickname     : xs_hgnn_e200_m75.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->nunubar  Extended Short DST simulation 99_a1 200 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:40 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglnn75.0_200._1.xsdst ! RUN = 13656 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglnn75.0_200._2.xsdst ! RUN = 13657 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglnn75.0_200._3.xsdst ! RUN = 13658 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglnn75.0_200._4.xsdst ! RUN = 13659 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglnn75.0_200._5.xsdst ! RUN = 13660 ! NEVT = 400
