*
*   Nickname     : xs_hzha03p6156hgqq_e206.7_m35_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HGQQ/E206.7/CERN/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 done at ecms=206.7 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:02:50 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqgg_206.7_35_3521.xsdst ! RUN = 3521 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqgg_206.7_35_3522.xsdst ! RUN = 3522 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqgg_206.7_35_3523.xsdst ! RUN = 3523 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqgg_206.7_35_3524.xsdst ! RUN = 3524 ! NEVT = 500
