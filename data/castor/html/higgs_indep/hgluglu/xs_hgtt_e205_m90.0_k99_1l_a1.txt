*
*   Nickname     : xs_hgtt_e205_m90.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGTT/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->tau+tau- Extended Short DST simulation 99_a1 205 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:43 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt90.0_205._1.xsdst ! RUN = 15301 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt90.0_205._2.xsdst ! RUN = 15302 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt90.0_205._3.xsdst ! RUN = 15303 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt90.0_205._4.xsdst ! RUN = 15304 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hglgltt90.0_205._5.xsdst ! RUN = 15305 ! NEVT = 400
