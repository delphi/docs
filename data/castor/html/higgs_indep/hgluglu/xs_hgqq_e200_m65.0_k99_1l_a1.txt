*
*   Nickname     : xs_hgqq_e200_m65.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HGQQ/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->gluglu, Z->qqbar  Extended Short DST simulation 99_a1 200 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:52:41 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglqq65.0_200._1.xsdst ! RUN = 13146 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglqq65.0_200._2.xsdst ! RUN = 13147 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglqq65.0_200._3.xsdst ! RUN = 13148 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglqq65.0_200._4.xsdst ! RUN = 13149 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hglglqq65.0_200._5.xsdst ! RUN = 13150 ! NEVT = 400
