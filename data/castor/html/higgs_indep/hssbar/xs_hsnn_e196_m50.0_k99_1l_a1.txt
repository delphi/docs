*
*   Nickname     : xs_hsnn_e196_m50.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:27 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn50.0_196._1.xsdst ! RUN = 16066 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn50.0_196._2.xsdst ! RUN = 16067 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn50.0_196._3.xsdst ! RUN = 16068 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn50.0_196._4.xsdst ! RUN = 16069 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn50.0_196._5.xsdst ! RUN = 16070 ! NEVT = 399
