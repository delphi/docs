*
*   Nickname     : xs_hsqq_e192_m85.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSQQ/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_a1 192 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:29 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssqq85.0_192._1.xsdst ! RUN = 15536 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssqq85.0_192._2.xsdst ! RUN = 15537 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssqq85.0_192._3.xsdst ! RUN = 15538 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssqq85.0_192._4.xsdst ! RUN = 15539 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssqq85.0_192._5.xsdst ! RUN = 15540 ! NEVT = 400
