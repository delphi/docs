*
*   Nickname     : xs_hsnn_e192_m55.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 192 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:27 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn55.0_192._1.xsdst ! RUN = 16006 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn55.0_192._2.xsdst ! RUN = 16007 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn55.0_192._3.xsdst ! RUN = 16008 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn55.0_192._4.xsdst ! RUN = 16009 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn55.0_192._5.xsdst ! RUN = 16010 ! NEVT = 400
