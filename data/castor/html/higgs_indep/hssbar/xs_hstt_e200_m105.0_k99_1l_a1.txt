*
*   Nickname     : xs_hstt_e200_m105.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSTT/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->tau+tau- Extended Short DST simulation 99_a1 200 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:35 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hsstt105.0_200._1.xsdst ! RUN = 17686 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hsstt105.0_200._2.xsdst ! RUN = 17687 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hsstt105.0_200._3.xsdst ! RUN = 17688 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hsstt105.0_200._4.xsdst ! RUN = 17689 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hsstt105.0_200._5.xsdst ! RUN = 17690 ! NEVT = 400
