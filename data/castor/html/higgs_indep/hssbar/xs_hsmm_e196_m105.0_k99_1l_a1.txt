*
*   Nickname     : xs_hsmm_e196_m105.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:23 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssmm105.0_196._1.xsdst ! RUN = 17121 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssmm105.0_196._2.xsdst ! RUN = 17122 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssmm105.0_196._3.xsdst ! RUN = 17123 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssmm105.0_196._4.xsdst ! RUN = 17124 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssmm105.0_196._5.xsdst ! RUN = 17125 ! NEVT = 400
