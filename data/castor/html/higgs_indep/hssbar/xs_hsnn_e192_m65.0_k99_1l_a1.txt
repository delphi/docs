*
*   Nickname     : xs_hsnn_e192_m65.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 192 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:27 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn65.0_192._1.xsdst ! RUN = 16016 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn65.0_192._2.xsdst ! RUN = 16017 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn65.0_192._3.xsdst ! RUN = 16018 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn65.0_192._4.xsdst ! RUN = 16019 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssnn65.0_192._5.xsdst ! RUN = 16020 ! NEVT = 400
