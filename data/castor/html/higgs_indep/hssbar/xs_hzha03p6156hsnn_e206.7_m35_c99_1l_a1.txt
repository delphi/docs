*
*   Nickname     : xs_hzha03p6156hsnn_e206.7_m35_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HSNN/E206.7/CERN/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 done at ecms=206.7 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:03:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvss_206.7_35_3531.xsdst ! RUN = 3531 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvss_206.7_35_3532.xsdst ! RUN = 3532 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvss_206.7_35_3533.xsdst ! RUN = 3533 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_vvss_206.7_35_3534.xsdst ! RUN = 3534 ! NEVT = 500
