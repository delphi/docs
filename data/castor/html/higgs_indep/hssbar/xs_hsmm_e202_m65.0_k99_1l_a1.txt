*
*   Nickname     : xs_hsmm_e202_m65.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:24 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssmm65.0_202._1.xsdst ! RUN = 17211 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssmm65.0_202._2.xsdst ! RUN = 17212 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssmm65.0_202._3.xsdst ! RUN = 17213 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssmm65.0_202._4.xsdst ! RUN = 17214 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssmm65.0_202._5.xsdst ! RUN = 17215 ! NEVT = 400
