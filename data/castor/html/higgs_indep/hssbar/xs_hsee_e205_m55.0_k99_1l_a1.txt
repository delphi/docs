*
*   Nickname     : xs_hsee_e205_m55.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_a1 205 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:22 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssee55.0_205._1.xsdst ! RUN = 16766 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssee55.0_205._2.xsdst ! RUN = 16767 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssee55.0_205._3.xsdst ! RUN = 16768 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssee55.0_205._4.xsdst ! RUN = 16769 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssee55.0_205._5.xsdst ! RUN = 16770 ! NEVT = 400
