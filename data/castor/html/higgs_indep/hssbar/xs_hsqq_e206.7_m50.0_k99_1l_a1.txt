*
*   Nickname     : xs_hsqq_e206.7_m50.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSQQ/E206.7/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_a1 done at ecms=206.7 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:33 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssqq50.0_206.7_1.xsdst ! RUN = 15826 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssqq50.0_206.7_2.xsdst ! RUN = 15827 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssqq50.0_206.7_3.xsdst ! RUN = 15828 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssqq50.0_206.7_4.xsdst ! RUN = 15829 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssqq50.0_206.7_5.xsdst ! RUN = 15830 ! NEVT = 400
