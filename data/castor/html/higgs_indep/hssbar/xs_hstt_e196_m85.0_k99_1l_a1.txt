*
*   Nickname     : xs_hstt_e196_m85.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSTT/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->tau+tau- Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:35 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hsstt85.0_196._1.xsdst ! RUN = 17601 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hsstt85.0_196._2.xsdst ! RUN = 17602 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hsstt85.0_196._3.xsdst ! RUN = 17603 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hsstt85.0_196._4.xsdst ! RUN = 17604 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hsstt85.0_196._5.xsdst ! RUN = 17605 ! NEVT = 400
