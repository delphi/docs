*
*   Nickname     : xs_hsee_e196_m80.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:21 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssee80.0_196._1.xsdst ! RUN = 16596 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssee80.0_196._2.xsdst ! RUN = 16597 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssee80.0_196._3.xsdst ! RUN = 16598 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssee80.0_196._4.xsdst ! RUN = 16599 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssee80.0_196._5.xsdst ! RUN = 16600 ! NEVT = 399
