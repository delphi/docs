*
*   Nickname     : xs_hstt_e206.7_m80.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSTT/E206.7/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->tau+tau- Extended Short DST simulation 99_a1 done at ecms=206.7 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:37 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hsstt80.0_206.7_1.xsdst ! RUN = 17856 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hsstt80.0_206.7_2.xsdst ! RUN = 17857 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hsstt80.0_206.7_3.xsdst ! RUN = 17858 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hsstt80.0_206.7_4.xsdst ! RUN = 17859 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hsstt80.0_206.7_5.xsdst ! RUN = 17860 ! NEVT = 400
