*
*   Nickname     : xs_hzha03p6156hsqq_e206.7_m25_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HSQQ/E206.7/CERN/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_a1 done at ecms=206.7 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:03:04 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqss_206.7_25_2541.xsdst ! RUN = 2541 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqss_206.7_25_2542.xsdst ! RUN = 2542 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqss_206.7_25_2543.xsdst ! RUN = 2543 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/206.7/hzha03pyth6156_hZ_qqss_206.7_25_2544.xsdst ! RUN = 2544 ! NEVT = 499
