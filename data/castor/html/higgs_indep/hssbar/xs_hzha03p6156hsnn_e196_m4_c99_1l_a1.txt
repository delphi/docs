*
*   Nickname     : xs_hzha03p6156hsnn_e196_m4_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HSNN/CERN/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:03:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvss_196_4_431.xsdst ! RUN = 431 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvss_196_4_432.xsdst ! RUN = 432 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvss_196_4_433.xsdst ! RUN = 433 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_vvss_196_4_434.xsdst ! RUN = 434 ! NEVT = 499
