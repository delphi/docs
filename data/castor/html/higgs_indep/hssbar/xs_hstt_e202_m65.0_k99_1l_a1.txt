*
*   Nickname     : xs_hstt_e202_m65.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSTT/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->tau+tau- Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:36 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hsstt65.0_202._1.xsdst ! RUN = 17711 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hsstt65.0_202._2.xsdst ! RUN = 17712 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hsstt65.0_202._3.xsdst ! RUN = 17713 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hsstt65.0_202._4.xsdst ! RUN = 17714 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hsstt65.0_202._5.xsdst ! RUN = 17715 ! NEVT = 398
