*
*   Nickname     : xs_hsee_e206.7_m55.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSEE/E206.7/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_a1 done at ecms=206.7 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:22 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssee55.0_206.7_1.xsdst ! RUN = 16831 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssee55.0_206.7_2.xsdst ! RUN = 16832 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssee55.0_206.7_3.xsdst ! RUN = 16833 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssee55.0_206.7_4.xsdst ! RUN = 16834 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/206.7/hssee55.0_206.7_5.xsdst ! RUN = 16835 ! NEVT = 399
