*
*   Nickname     : xs_hsee_e200_m80.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_a1 200 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:21 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssee80.0_200._1.xsdst ! RUN = 16661 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssee80.0_200._2.xsdst ! RUN = 16662 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssee80.0_200._3.xsdst ! RUN = 16663 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssee80.0_200._4.xsdst ! RUN = 16664 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssee80.0_200._5.xsdst ! RUN = 16665 ! NEVT = 400
