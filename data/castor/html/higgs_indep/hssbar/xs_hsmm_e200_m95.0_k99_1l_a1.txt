*
*   Nickname     : xs_hsmm_e200_m95.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_a1 200 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:24 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssmm95.0_200._1.xsdst ! RUN = 17176 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssmm95.0_200._2.xsdst ! RUN = 17177 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssmm95.0_200._3.xsdst ! RUN = 17178 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssmm95.0_200._4.xsdst ! RUN = 17179 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/200/hssmm95.0_200._5.xsdst ! RUN = 17180 ! NEVT = 400
