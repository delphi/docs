*
*   Nickname     : xs_hsee_e192_m110.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSEE/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->e+e-  Extended Short DST simulation 99_a1 192 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:19 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssee110.0_192._1.xsdst ! RUN = 16561 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssee110.0_192._2.xsdst ! RUN = 16562 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssee110.0_192._3.xsdst ! RUN = 16563 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssee110.0_192._4.xsdst ! RUN = 16564 ! NEVT = 399
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/192/hssee110.0_192._5.xsdst ! RUN = 16565 ! NEVT = 400
