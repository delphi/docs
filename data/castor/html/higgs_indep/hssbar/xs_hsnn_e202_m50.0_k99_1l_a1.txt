*
*   Nickname     : xs_hsnn_e202_m50.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:27 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssnn50.0_202._1.xsdst ! RUN = 16196 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssnn50.0_202._2.xsdst ! RUN = 16197 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssnn50.0_202._3.xsdst ! RUN = 16198 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssnn50.0_202._4.xsdst ! RUN = 16199 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssnn50.0_202._5.xsdst ! RUN = 16200 ! NEVT = 400
