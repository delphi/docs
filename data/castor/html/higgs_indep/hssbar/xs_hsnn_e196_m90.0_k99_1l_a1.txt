*
*   Nickname     : xs_hsnn_e196_m90.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSNN/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->nunubar  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:27 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn90.0_196._1.xsdst ! RUN = 16106 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn90.0_196._2.xsdst ! RUN = 16107 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn90.0_196._3.xsdst ! RUN = 16108 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn90.0_196._4.xsdst ! RUN = 16109 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssnn90.0_196._5.xsdst ! RUN = 16110 ! NEVT = 400
