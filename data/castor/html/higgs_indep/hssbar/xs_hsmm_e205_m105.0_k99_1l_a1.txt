*
*   Nickname     : xs_hsmm_e205_m105.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSMM/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->mu+mu-  Extended Short DST simulation 99_a1 205 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:26 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssmm105.0_205._1.xsdst ! RUN = 17316 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssmm105.0_205._2.xsdst ! RUN = 17317 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssmm105.0_205._3.xsdst ! RUN = 17318 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssmm105.0_205._4.xsdst ! RUN = 17319 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/205/hssmm105.0_205._5.xsdst ! RUN = 17320 ! NEVT = 400
