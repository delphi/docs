*
*   Nickname     : xs_hsqq_e196_m60.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSQQ/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_a1 196 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:29 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssqq60.0_196._1.xsdst ! RUN = 15576 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssqq60.0_196._2.xsdst ! RUN = 15577 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssqq60.0_196._3.xsdst ! RUN = 15578 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssqq60.0_196._4.xsdst ! RUN = 15579 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/196/hssqq60.0_196._5.xsdst ! RUN = 15580 ! NEVT = 400
