*
*   Nickname     : xs_hsqq_e202_m75.0_k99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HSQQ/KARLSRUHE/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_a1 202 , Karlsruhe
*---
*   Comments     :  time stamp: Mon Feb 12 15:51:32 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssqq75.0_202._1.xsdst ! RUN = 15721 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssqq75.0_202._2.xsdst ! RUN = 15722 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssqq75.0_202._3.xsdst ! RUN = 15723 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssqq75.0_202._4.xsdst ! RUN = 15724 ! NEVT = 400
FILE = /castor/cern.ch/delphi/MCprod/karlsruhe/hzha03/v99_4/202/hssqq75.0_202._5.xsdst ! RUN = 15725 ! NEVT = 400
