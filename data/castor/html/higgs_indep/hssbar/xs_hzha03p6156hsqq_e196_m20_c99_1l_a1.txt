*
*   Nickname     : xs_hzha03p6156hsqq_e196_m20_c99_1l_a1
*   Generic Name : //CERN/DELPHI/P02_SIMD/XSDST/HZHA03P6156HSQQ/CERN/SUMT
*   Description  : ShortDST HZHA03 H->ssbar, Z->qqbar  Extended Short DST simulation 99_a1 196 , CERN
*---
*   Comments     :  time stamp: Thu Apr 19 18:03:03 2001
*---
*
*   Comments     : only short dst has been kept           
*
*
*
*
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqss_196_20_2041.xsdst ! RUN = 2041 ! NEVT = 500
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqss_196_20_2042.xsdst ! RUN = 2042 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqss_196_20_2043.xsdst ! RUN = 2043 ! NEVT = 499
FILE = /castor/cern.ch/delphi/MCprod/cern/hzha03pyth6156/v99_4/196/hzha03pyth6156_hZ_qqss_196_20_2044.xsdst ! RUN = 2044 ! NEVT = 500
