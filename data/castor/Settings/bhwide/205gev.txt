1           USER'S DIRECTIVES TO RUN THIS JOB
            ----------------------------------


 ***** DATA CARD CONTENT     LIST                                                                            
 ***** DATA CARD CONTENT     C-- UNIT FOR BINARY OUTPUT                                                      
 ***** DATA CARD CONTENT     LUNOUT  18                                                                      
 ***** DATA CARD CONTENT     C-- Lab ID (used to generate random number seed)                                
 ***** DATA CARD CONTENT     LABO 'CERN'                                                                     
 ***** DATA CARD CONTENT     C-- Run number (used to generate random number seed)                            
 ***** DATA CARD CONTENT      NRUN 9996                                                                      
 ***** DATA CARD CONTENT     C-- Centre of mass energy                                                       
 ***** DATA CARD CONTENT     ECMS 205.                                                                       
 ***** DATA CARD CONTENT     C-- Number of events to generate                                                
 ***** DATA CARD CONTENT     NEVT  100000                                                                    
 ***** DATA CARD CONTENT     C-- Maximum acolinearity (0.0 = back-to-back)                                   
 ***** DATA CARD CONTENT     ACOL 179.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum acolinearity                                                        
 ***** DATA CARD CONTENT     ACLMIN 0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum acoplanarity                                                        
 ***** DATA CARD CONTENT     ACPMIN 0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum fermion polar angle                                                 
 ***** DATA CARD CONTENT     TMIN  9.0                                                                       
 ***** DATA CARD CONTENT     C-- Minimum electron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TEMIN 0.0                                                                       
 ***** DATA CARD CONTENT     C-- Maximum electron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TEMAX  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum positron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TPMIN  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Maximum positron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TPMAX  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum fermion energy                                                      
 ***** DATA CARD CONTENT     EMIN  0.05                                                                      
 ***** DATA CARD CONTENT     C-- Minimum electron PT                                                         
 ***** DATA CARD CONTENT     PEMIN  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum positron PT                                                         
 ***** DATA CARD CONTENT     PPMIN  0.0                                                                      
 ***** DATA CARD CONTENT     END                                                                             
 EXCXMP: Laboratory CERN recognized
 EXCXMP: Lab number set to         2000
MARran INITIALIZED: IJ,KL,IJKL,NTOT,NTOT2=       665      5466  20009996         1         1
 MARINI SKIPPING OVER      1000000000
 MARINI SKIPPING OVER               1


 ===========================================================================
 =                                                                         =
 =               BBB   B  B  B    B  B  BBB   BBBB                         =
 =               B  B  B  B  B    B  B  B  B  B                            =
 =               BBB   BBBB  B    B  B  B  B  BBB                          =
 =               B  B  B  B  B BB B  B  B  B  B                            =
 =               BBB   B  B  BB  BB  B  BBB   BBBB                         =
 =                                                                         =
 =                 *******************************                         =
 =                 *      BHWIDE version 1.01    *                         =
 =                 * M.C. for wide-angle Bhabha  *                         =
 =                 *   September 1995   (1.00)   *                         =
 =                 *   September 1996   (1.01)   *                         =
 =                 *           AUTHORS           *                         =
 =                 *    S. Jadach, W. Placzek,   *                         =
 =                 *          B.F.L. Ward        *                         =
 =                 *******************************                         =
 ===========================================================================



 ===========================================================================
 =                This program is based on papers                          =
 =                --------------------------------                         =
 =                UTHEP-95-1001; hep-ph/9608412.                           =
 =                Phys. Rev. D40 (1989) 3582.                              =
 =                Comp. Phys. Comm. 70 (1992) 305.                         =
 ===========================================================================


 Thank you for flying .... choosing A L I B A B A
                                    =============

 A (semi) Analytical Leading log Improved BhABhA scattering calculation.
 This program is meant for large angle Bhabha scattering [and for other
 fermion pair production (but then only in s channel)].

 *******************************************************************
 * Authors: W.J.P. Beenakker, F.A. Berends and S.C. van der Marck. *
 * Address: Instituut-Lorentz, University of Leiden                *
 *          P.o.b. 9506, 2300 RA Leiden, The Netherlands           *
 * Bitnet addresses: BEENAKKER @ HLERUL59                          *
 *                   BERENDS @ HLERUL5 or BERENDS @ HLERUL59       *
 *                   VANDERMARCK @ HLERUL59                        *
 *                   joint address: LORENTZ @ HLERUL5              *
 *******************************************************************
 * References:                                                     *
 * [1] W. Beenakker, F.A. Berends and S.C. van der Marck,          *
 *     "Large angle Bhabha scattering" and "Higher order           *
 *     corrections to the forward-backward asymmetry,"             *
 *     Leiden preprints 1990, for the treatment of the purely      *
 *     QED corrections and the incorporation of cuts on energy and *
 *     angle of both outgoing particles and their acollinearity.   *
 * [2] W. Beenakker and W. Hollik, ECFA workshop on LEP 200,       *
 *        CERN 87-08 p.185, ed. by A. Boehm and W. Hoogland;       *
 *     W. Hollik, DESY preprint 88-188, both for the treatment     *
 *     of the weak (non-QED) corrections.                          *
 *******************************************************************
 Version 2.0, August 1990

 The properties of the fermions:
    label       name     mass (GeV)  partial width of the Z (GeV)
      0      neutrino    0.0000000           0.1673613
      1      electron    0.0005110           0.0839799
      2          muon    0.1056584           0.0839792
      3           tau    1.7841000           0.0837868
      4      up quark    0.0414500           0.2995842
      5    down quark    0.0414600           0.3864207
      6   charm quark    1.5000000           0.2991584
      7 strange quark    0.1500000           0.3864167
      8     top quark  174.0000000           0.0000000
      9  bottom quark    4.5000000           0.3775833
     10       hadrons                        1.7491633

 For the bosons we have (everything in GeV):
  mass of the   Z   =   91.1887    total width of the Z =  2.5029931
  mass of the   W   =   80.4138    <==> sin**2(theta-w) =  0.2223592
  mass of the Higgs =  300.0000

 Some coupling strengths:
                    1/alfa =    137.036
 the QED correction factor =      1.0017421
               alfa-strong =      0.124
 the QCD correction factor =      1.0416593

    
 --------------  BHWIDE ----------------------
  100000.0 Events requested
  =======> Generation starts...
    
 ***************************************************************
 LABO LABNUM NRUN   NEVT ECMS 
 CERN 2000   9996 100000 205.0
   THEMN  THEMX  THPMN THPMX  ENMIN    ACOL  ACLMN  ACPMN  PTEMN  PTPMN
     9.0  171.0    9.0  171.0    0.1  179.0    0.0    0.0    0.0    0.0
 ***************************************************************
 =====================DUMPS====================
  P2   19.8779211200467  16.5057824836712  94.9500893772375  98.4027036034056
  Q2  -14.0242690680413 -11.7687736086095 -70.5986900027375  72.9339371315397
 PHO   -4.7055920162169  -3.9582115532607 -23.7232863363203  24.5072305558684
 PHO   -1.1476549134977  -0.7736962639727  -4.7942211527750   4.9900174519811
 PHO   -0.0004051222909  -0.0051010578282   4.1661081145953   4.1661112572052
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 205.0000000000000
  ievent =            1
 =====================DUMPS====================
  P2  -17.6760708322891  20.1494859079973  98.9126402465934 102.4800256793387
  Q2   17.6761849396483 -20.1495335070441 -98.9148054335509 102.4821445364538
 PHO   -0.0001139021837   0.0000474000341   0.0199972953016   0.0199976758612
 PHO   -0.0000002051755   0.0000001990127  -0.0178321083441   0.0178321083464
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 205.0000000000000
 =====================DUMPS====================
  P2   15.0014120923679  -6.7032739383430  99.4264199746602 100.7749434904145
  Q2  -15.0400465487799   6.7241096029171 -99.6737968913037 101.0261472943099
 PHO    0.2570930403492  -0.1159907604873   1.7017439076183   1.7249588444454
 PHO   -0.2184765132796   0.0951484699939  -1.4400411745250   1.4596245416285
 PHO    0.0000179293424   0.0000066259193  -0.0143258164497   0.0143258292016
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 205.0000000000000
  ievent =         1001
  ievent =         2001
  ievent =         3001
  ievent =         4001
  ievent =         5001
  ievent =         6001
  ievent =         7001
  ievent =         8001
  ievent =         9001
  ievent =        10001
  ievent =        11001
  ievent =        12001
  ievent =        13001
  ievent =        14001
  ievent =        15001
  ievent =        16001
  ievent =        17001
  ievent =        18001
  ievent =        19001
  ievent =        20001
  ievent =        21001
  ievent =        22001
  ievent =        23001
  ievent =        24001
  ievent =        25001
  ievent =        26001
  ievent =        27001
  ievent =        28001
  ievent =        29001
  ievent =        30001
  ievent =        31001
  ievent =        32001
  ievent =        33001
  ievent =        34001
  ievent =        35001
  ievent =        36001
  ievent =        37001
  ievent =        38001
  ievent =        39001
  ievent =        40001
  ievent =        41001
  ievent =        42001
  ievent =        43001
  ievent =        44001
  ievent =        45001
  ievent =        46001
  ievent =        47001
  ievent =        48001
  ievent =        49001
  ievent =        50001
  ievent =        51001
  ievent =        52001
  ievent =        53001
  ievent =        54001
  ievent =        55001
  ievent =        56001
  ievent =        57001
  ievent =        58001
  ievent =        59001
  ievent =        60001
  ievent =        61001
  ievent =        62001
  ievent =        63001
  ievent =        64001
  ievent =        65001
  ievent =        66001
  ievent =        67001
  ievent =        68001
  ievent =        69001
  ievent =        70001
  ievent =        71001
  ievent =        72001
  ievent =        73001
  ievent =        74001
  ievent =        75001
  ievent =        76001
  ievent =        77001
  ievent =        78001
  ievent =        79001
  ievent =        80001
  ievent =        81001
  ievent =        82001
  ievent =        83001
  ievent =        84001
  ievent =        85001
  ievent =        86001
  ievent =        87001
  ievent =        88001
  ievent =        89001
  ievent =        90001
  ievent =        91001
  ievent =        92001
  ievent =        93001
  ievent =        94001
  ievent =        95001
  ievent =        96001
  ievent =        97001
  ievent =        98001
  ievent =        99001
  =======> Generation finished!
    
 |||||||||||||||||||||||||||||||||||||||||||||||||||
 || Number of events        100000
 || Number of written events        100000
 Xsec_accep =      1.02159495 Nanob.
 error      =      0.00109963 Nanob.
 Xsec_write =      1.02159495 Nanob.
 || Random Number state    20009996   102824346           1
 |||||||||||||||||||||||||||||||||||||||||||||||||||
1768.72user 1.25system 29:49.11elapsed 98%CPU (0avgtext+0avgdata 0maxresident)k
0inputs+0outputs (207major+181minor)pagefaults 0swaps


 ===========================================================================
 =                                                                         =
 =               BBB   B  B  B    B  B  BBB   BBBB                         =
 =               B  B  B  B  B    B  B  B  B  B                            =
 =               BBB   BBBB  B    B  B  B  B  BBB                          =
 =               B  B  B  B  B BB B  B  B  B  B                            =
 =               BBB   B  B  BB  BB  B  BBB   BBBB                         =
 =                                                                         =
 =                 *******************************                         =
 =                 *      BHWIDE version 1.01    *                         =
 =                 * M.C. for wide-angle Bhabha  *                         =
 =                 *   September 1995   (1.00)   *                         =
 =                 *   September 1996   (1.01)   *                         =
 =                 *           AUTHORS           *                         =
 =                 *    S. Jadach, W. Placzek,   *                         =
 =                 *          B.F.L. Ward        *                         =
 =                 *******************************                         =
 ===========================================================================



 ===========================================================================
 =                This program is based on papers                          =
 =                --------------------------------                         =
 =                UTHEP-95-1001; hep-ph/9608412.                           =
 =                Phys. Rev. D40 (1989) 3582.                              =
 =                Comp. Phys. Comm. 70 (1992) 305.                         =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWIDE: INPUT PARAMETRES                                =
 =               *********************************                         =
 =                1                  OPTIONS   switch        KeyOpt     N1 =
 =                0                  weighting switch        KeyWgt        =
 =                1                  rand. numb. switch      KeyRnd        =
 =             1221                  RADIATION switch        KeyRad     N2 =
 =                1                  e-weak cor. switch      KeyEWC        =
 =                2                  EWRC Libary choice      KeyLib        =
 =                2                  QED mat. elm. type      KeyMod        =
 =                1                  vac_pol   switch        KeyPia        =
 =     205.00000000                  CMS energy   [GeV]      CMSENE     X1 =
 =    42025.000                      CMSENE^2   [GeV^2]      SVAR          =
 =       9.00000000                  theta_min_e+ [deg]      THMINP     X2 =
 =     171.00000000                  theta_max_e+ [deg]      THMAXP     X3 =
 =       9.00000000                  theta_min_e- [deg]      THMINE     X4 =
 =     171.00000000                  theta_max_e- [deg]      THMAXE     X5 =
 =       0.15707963                  theta_min_e+ [rad]      THMIRP        =
 =       2.98451302                  theta_max_e+ [rad]      THMARP        =
 =       0.15707963                  theta_min_e- [rad]      THMIRE        =
 =       2.98451302                  theta_max_e- [rad]      THMARE        =
 =       0.05000000                  Energy_min_e+[GeV]      ENMINP     X6 =
 =       0.05000000                  Energy_min_e-[GeV]      ENMINE     X7 =
 =       0.00048780                  E_min_e+/E_beam         XEMINP        =
 =       0.00048780                  E_min_e-/E_beam         XEMINE        =
 =     179.00000000                  Acollinearity[deg]      ACOLLI     X8 =
 =       3.12413936                  Acollinearity[rad]      ACOLLR        =
 =   0.63361458E-01                  trasf_min [GeV^2]       TRMIN         =
 =    42025.000                      trasf_max [GeV^2]       TRMAX         =
 =   0.15077087E-05                  xi_min=TRMIN/SVAR       XIMIN         =
 =    1.0000000                      xi_max=TRMAX/SVAR       XIMAX         =
 =   0.10000000E-03                  eps_CM infr. cut        EPSCMS     X9 =
 =   0.99999998E-06                  delta  infr. cut        DEL           =
 =      91.18869781                  Z-mass GeV              AMAZ          =
 =       2.50299314                  Z-width GeV             GAMMZ         =
 =       0.22235916                  weak mixing angle       SINW2         =
 =   0.93669240                      Born xsecion [nb]       BORNXS        =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWID1:        WINDOW A                                 =
 =               *********************************                         =
 =           100000                  Accepted total          NEVGEN     A1 =
 =          1957272                  Raw prior reject.       IEVENT     A2 =
 =    1.0215949      +- 0.00109963   Xsec M.C. [nb]          XSECMC     A3 =
 =       0.00107639                  relat. error            ERELMC     A4 =
 =       0.51057264  +- 0.00107639   weight  M.C.            AWT        A5 =
 =               45                  WT<0                    NEVNEG     A6 =
 =              114                  WT>WTMAX                NEVOVE     A7 =
 =      10.00000000                  Maximum WT              WWMX       A8 =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWID1:        WINDOW B                                 =
 =               *********************************                         =
 =       0.46904878  +- 0.00053806  WT1*WT2*T/TP*T/TQ                   B1 =
 =       0.99722781  +- 0.00003814  WT3 from KINO4                      B2 =
 =       2.11386587  +- 0.00013992  YFS formfac              WT         B4 =
 =       0.51057264  +- 0.00107639  TOTAL                               B5 =
 =       0.00109043  +- 0.00014601  xsec/xtot: WT>WTMAX      WT         B6 =
 =      -0.00000965  +--0.00000827  xsec/xtot: WT<0          WT         B7 =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                           WINDOW C                                      =
 =               Built-in average control weights.                         =
 =               Should equal one +- statist. err.                         =
 =               *********************************                         =
 =       0.99994180  +- 0.00062135  <WCTA1>                             C1 =
 =       0.99964944  +- 0.00062156  <WCTA2>                             C2 =
 =       0.99983801  +- 0.00102777  <WCTA1*WCTA2>                       C3 =
 =       0.99998025  +- 0.00025038  <WCTB1>                             C4 =
 =       1.00045239  +- 0.00024985  <WCTB2>                             C5 =
 =       1.00049128  +- 0.00036482  <WCTB1*WCTB2>                       C6 =
 ===========================================================================

