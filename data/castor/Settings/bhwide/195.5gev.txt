1           USER'S DIRECTIVES TO RUN THIS JOB
            ----------------------------------


 ***** DATA CARD CONTENT     LIST                                                                            
 ***** DATA CARD CONTENT     C-- UNIT FOR BINARY OUTPUT                                                      
 ***** DATA CARD CONTENT     LUNOUT  18                                                                      
 ***** DATA CARD CONTENT     C-- Lab ID (used to generate random number seed)                                
 ***** DATA CARD CONTENT     LABO 'CERN'                                                                     
 ***** DATA CARD CONTENT     C-- Run number (used to generate random number seed)                            
 ***** DATA CARD CONTENT      NRUN 9992                                                                      
 ***** DATA CARD CONTENT     C-- Centre of mass energy                                                       
 ***** DATA CARD CONTENT     ECMS 195.5                                                                      
 ***** DATA CARD CONTENT     C-- Number of events to generate                                                
 ***** DATA CARD CONTENT     NEVT  100000                                                                    
 ***** DATA CARD CONTENT     C-- Maximum acolinearity (0.0 = back-to-back)                                   
 ***** DATA CARD CONTENT     ACOL 179.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum acolinearity                                                        
 ***** DATA CARD CONTENT     ACLMIN 0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum acoplanarity                                                        
 ***** DATA CARD CONTENT     ACPMIN 0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum fermion polar angle                                                 
 ***** DATA CARD CONTENT     TMIN  9.0                                                                       
 ***** DATA CARD CONTENT     C-- Minimum electron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TEMIN 0.0                                                                       
 ***** DATA CARD CONTENT     C-- Maximum electron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TEMAX  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum positron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TPMIN  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Maximum positron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TPMAX  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum fermion energy                                                      
 ***** DATA CARD CONTENT     EMIN  0.05                                                                      
 ***** DATA CARD CONTENT     C-- Minimum electron PT                                                         
 ***** DATA CARD CONTENT     PEMIN  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum positron PT                                                         
 ***** DATA CARD CONTENT     PPMIN  0.0                                                                      
 ***** DATA CARD CONTENT     END                                                                             
 EXCXMP: Laboratory CERN recognized
 EXCXMP: Lab number set to         2000
MARran INITIALIZED: IJ,KL,IJKL,NTOT,NTOT2=       665      5462  20009992         1         1
 MARINI SKIPPING OVER      1000000000
 MARINI SKIPPING OVER               1


 ===========================================================================
 =                                                                         =
 =               BBB   B  B  B    B  B  BBB   BBBB                         =
 =               B  B  B  B  B    B  B  B  B  B                            =
 =               BBB   BBBB  B    B  B  B  B  BBB                          =
 =               B  B  B  B  B BB B  B  B  B  B                            =
 =               BBB   B  B  BB  BB  B  BBB   BBBB                         =
 =                                                                         =
 =                 *******************************                         =
 =                 *      BHWIDE version 1.01    *                         =
 =                 * M.C. for wide-angle Bhabha  *                         =
 =                 *   September 1995   (1.00)   *                         =
 =                 *   September 1996   (1.01)   *                         =
 =                 *           AUTHORS           *                         =
 =                 *    S. Jadach, W. Placzek,   *                         =
 =                 *          B.F.L. Ward        *                         =
 =                 *******************************                         =
 ===========================================================================



 ===========================================================================
 =                This program is based on papers                          =
 =                --------------------------------                         =
 =                UTHEP-95-1001; hep-ph/9608412.                           =
 =                Phys. Rev. D40 (1989) 3582.                              =
 =                Comp. Phys. Comm. 70 (1992) 305.                         =
 ===========================================================================


 Thank you for flying .... choosing A L I B A B A
                                    =============

 A (semi) Analytical Leading log Improved BhABhA scattering calculation.
 This program is meant for large angle Bhabha scattering [and for other
 fermion pair production (but then only in s channel)].

 *******************************************************************
 * Authors: W.J.P. Beenakker, F.A. Berends and S.C. van der Marck. *
 * Address: Instituut-Lorentz, University of Leiden                *
 *          P.o.b. 9506, 2300 RA Leiden, The Netherlands           *
 * Bitnet addresses: BEENAKKER @ HLERUL59                          *
 *                   BERENDS @ HLERUL5 or BERENDS @ HLERUL59       *
 *                   VANDERMARCK @ HLERUL59                        *
 *                   joint address: LORENTZ @ HLERUL5              *
 *******************************************************************
 * References:                                                     *
 * [1] W. Beenakker, F.A. Berends and S.C. van der Marck,          *
 *     "Large angle Bhabha scattering" and "Higher order           *
 *     corrections to the forward-backward asymmetry,"             *
 *     Leiden preprints 1990, for the treatment of the purely      *
 *     QED corrections and the incorporation of cuts on energy and *
 *     angle of both outgoing particles and their acollinearity.   *
 * [2] W. Beenakker and W. Hollik, ECFA workshop on LEP 200,       *
 *        CERN 87-08 p.185, ed. by A. Boehm and W. Hoogland;       *
 *     W. Hollik, DESY preprint 88-188, both for the treatment     *
 *     of the weak (non-QED) corrections.                          *
 *******************************************************************
 Version 2.0, August 1990

 The properties of the fermions:
    label       name     mass (GeV)  partial width of the Z (GeV)
      0      neutrino    0.0000000           0.1673613
      1      electron    0.0005110           0.0839799
      2          muon    0.1056584           0.0839792
      3           tau    1.7841000           0.0837868
      4      up quark    0.0414500           0.2995842
      5    down quark    0.0414600           0.3864207
      6   charm quark    1.5000000           0.2991584
      7 strange quark    0.1500000           0.3864167
      8     top quark  174.0000000           0.0000000
      9  bottom quark    4.5000000           0.3775833
     10       hadrons                        1.7491633

 For the bosons we have (everything in GeV):
  mass of the   Z   =   91.1887    total width of the Z =  2.5029931
  mass of the   W   =   80.4138    <==> sin**2(theta-w) =  0.2223592
  mass of the Higgs =  300.0000

 Some coupling strengths:
                    1/alfa =    137.036
 the QED correction factor =      1.0017421
               alfa-strong =      0.124
 the QCD correction factor =      1.0416593

    
 --------------  BHWIDE ----------------------
  100000.0 Events requested
  =======> Generation starts...
    
 ***************************************************************
 LABO LABNUM NRUN   NEVT ECMS 
 CERN 2000   9992 100000 195.5
   THEMN  THEMX  THPMN THPMX  ENMIN    ACOL  ACLMN  ACPMN  PTEMN  PTPMN
     9.0  171.0    9.0  171.0    0.1  179.0    0.0    0.0    0.0    0.0
 ***************************************************************
 =====================DUMPS====================
  P2   -8.5960968019322 -41.9803079941815  87.0560082288094  97.0308595680275
  Q2    8.5915040915928  41.9872175046065 -74.7596299409176  86.1727488565797
 PHO   -0.0021309959170  -0.0004761959698  -8.9713471234076   8.9713473891372
 PHO    0.0067237062565  -0.0064333144552  -3.3250311644842   3.3250441862555
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 195.5000000000000
  ievent =            1
 =====================DUMPS====================
  P2   14.9723305911491  -3.6040192088178  96.5292849965780  97.7500000000000
  Q2  -14.9723305911491   3.6040192088178 -96.5292849965780  97.7500000000000
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 195.4999999999999
 =====================DUMPS====================
  P2   24.7497879619085  18.9227690801320  92.6377095301120  97.7362185735976
  Q2  -24.7496243440810 -18.9231118190791 -92.1125864003339  97.2386581546182
 PHO   -0.0001636070497   0.0003428450745  -0.5081154743074   0.5081156163125
 PHO   -0.0000000107777  -0.0000001061275  -0.0170076554709   0.0170076554712
 SUM    0.0000000000000   0.0000000000000  -0.0000000000002 195.4999999999995
  ievent =         1001
  ievent =         2001
  ievent =         3001
  ievent =         4001
  ievent =         5001
  ievent =         6001
  ievent =         7001
  ievent =         8001
  ievent =         9001
  ievent =        10001
  ievent =        11001
  ievent =        12001
  ievent =        13001
  ievent =        14001
  ievent =        15001
  ievent =        16001
  ievent =        17001
  ievent =        18001
  ievent =        19001
  ievent =        20001
  ievent =        21001
  ievent =        22001
  ievent =        23001
  ievent =        24001
  ievent =        25001
  ievent =        26001
  ievent =        27001
  ievent =        28001
  ievent =        29001
  ievent =        30001
  ievent =        31001
  ievent =        32001
  ievent =        33001
  ievent =        34001
  ievent =        35001
  ievent =        36001
  ievent =        37001
  ievent =        38001
  ievent =        39001
  ievent =        40001
  ievent =        41001
  ievent =        42001
  ievent =        43001
  ievent =        44001
  ievent =        45001
  ievent =        46001
  ievent =        47001
  ievent =        48001
  ievent =        49001
  ievent =        50001
  ievent =        51001
  ievent =        52001
  ievent =        53001
  ievent =        54001
  ievent =        55001
  ievent =        56001
  ievent =        57001
  ievent =        58001
  ievent =        59001
  ievent =        60001
  ievent =        61001
  ievent =        62001
  ievent =        63001
  ievent =        64001
  ievent =        65001
  ievent =        66001
  ievent =        67001
  ievent =        68001
  ievent =        69001
  ievent =        70001
  ievent =        71001
  ievent =        72001
  ievent =        73001
  ievent =        74001
  ievent =        75001
  ievent =        76001
  ievent =        77001
  ievent =        78001
  ievent =        79001
  ievent =        80001
  ievent =        81001
  ievent =        82001
  ievent =        83001
  ievent =        84001
  ievent =        85001
  ievent =        86001
  ievent =        87001
  ievent =        88001
  ievent =        89001
  ievent =        90001
  ievent =        91001
  ievent =        92001
  ievent =        93001
  ievent =        94001
  ievent =        95001
  ievent =        96001
  ievent =        97001
  ievent =        98001
  ievent =        99001
  =======> Generation finished!
    
 |||||||||||||||||||||||||||||||||||||||||||||||||||
 || Number of events        100000
 || Number of written events        100000
 Xsec_accep =      1.12146006 Nanob.
 error      =      0.00120000 Nanob.
 Xsec_write =      1.12146006 Nanob.
 || Random Number state    20009992   103398712           1
 |||||||||||||||||||||||||||||||||||||||||||||||||||
2738.51user 0.75system 45:39.45elapsed 99%CPU (0avgtext+0avgdata 0maxresident)k
0inputs+0outputs (206major+181minor)pagefaults 0swaps


 ===========================================================================
 =                                                                         =
 =               BBB   B  B  B    B  B  BBB   BBBB                         =
 =               B  B  B  B  B    B  B  B  B  B                            =
 =               BBB   BBBB  B    B  B  B  B  BBB                          =
 =               B  B  B  B  B BB B  B  B  B  B                            =
 =               BBB   B  B  BB  BB  B  BBB   BBBB                         =
 =                                                                         =
 =                 *******************************                         =
 =                 *      BHWIDE version 1.01    *                         =
 =                 * M.C. for wide-angle Bhabha  *                         =
 =                 *   September 1995   (1.00)   *                         =
 =                 *   September 1996   (1.01)   *                         =
 =                 *           AUTHORS           *                         =
 =                 *    S. Jadach, W. Placzek,   *                         =
 =                 *          B.F.L. Ward        *                         =
 =                 *******************************                         =
 ===========================================================================



 ===========================================================================
 =                This program is based on papers                          =
 =                --------------------------------                         =
 =                UTHEP-95-1001; hep-ph/9608412.                           =
 =                Phys. Rev. D40 (1989) 3582.                              =
 =                Comp. Phys. Comm. 70 (1992) 305.                         =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWIDE: INPUT PARAMETRES                                =
 =               *********************************                         =
 =                1                  OPTIONS   switch        KeyOpt     N1 =
 =                0                  weighting switch        KeyWgt        =
 =                1                  rand. numb. switch      KeyRnd        =
 =             1221                  RADIATION switch        KeyRad     N2 =
 =                1                  e-weak cor. switch      KeyEWC        =
 =                2                  EWRC Libary choice      KeyLib        =
 =                2                  QED mat. elm. type      KeyMod        =
 =                1                  vac_pol   switch        KeyPia        =
 =     195.50000000                  CMS energy   [GeV]      CMSENE     X1 =
 =    38220.250                      CMSENE^2   [GeV^2]      SVAR          =
 =       9.00000000                  theta_min_e+ [deg]      THMINP     X2 =
 =     171.00000000                  theta_max_e+ [deg]      THMAXP     X3 =
 =       9.00000000                  theta_min_e- [deg]      THMINE     X4 =
 =     171.00000000                  theta_max_e- [deg]      THMAXE     X5 =
 =       0.15707963                  theta_min_e+ [rad]      THMIRP        =
 =       2.98451302                  theta_max_e+ [rad]      THMARP        =
 =       0.15707963                  theta_min_e- [rad]      THMIRE        =
 =       2.98451302                  theta_max_e- [rad]      THMARE        =
 =       0.05000000                  Energy_min_e+[GeV]      ENMINP     X6 =
 =       0.05000000                  Energy_min_e-[GeV]      ENMINE     X7 =
 =       0.00051151                  E_min_e+/E_beam         XEMINP        =
 =       0.00051151                  E_min_e-/E_beam         XEMINE        =
 =     179.00000000                  Acollinearity[deg]      ACOLLI     X8 =
 =       3.12413936                  Acollinearity[rad]      ACOLLR        =
 =   0.60425184E-01                  trasf_min [GeV^2]       TRMIN         =
 =    38220.250                      trasf_max [GeV^2]       TRMAX         =
 =   0.15809730E-05                  xi_min=TRMIN/SVAR       XIMIN         =
 =    1.0000000                      xi_max=TRMAX/SVAR       XIMAX         =
 =   0.10000000E-03                  eps_CM infr. cut        EPSCMS     X9 =
 =   0.99999998E-06                  delta  infr. cut        DEL           =
 =      91.18869781                  Z-mass GeV              AMAZ          =
 =       2.50299314                  Z-width GeV             GAMMZ         =
 =       0.22235916                  weak mixing angle       SINW2         =
 =    1.0297642                      Born xsecion [nb]       BORNXS        =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWID1:        WINDOW A                                 =
 =               *********************************                         =
 =           100000                  Accepted total          NEVGEN     A1 =
 =          1969056                  Raw prior reject.       IEVENT     A2 =
 =    1.1214601      +- 0.00120000   Xsec M.C. [nb]          XSECMC     A3 =
 =       0.00107003                  relat. error            ERELMC     A4 =
 =       0.50972593  +- 0.00107003   weight  M.C.            AWT        A5 =
 =               35                  WT<0                    NEVNEG     A6 =
 =              102                  WT>WTMAX                NEVOVE     A7 =
 =      10.00000000                  Maximum WT              WWMX       A8 =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWID1:        WINDOW B                                 =
 =               *********************************                         =
 =       0.46856289  +- 0.00053722  WT1*WT2*T/TP*T/TQ                   B1 =
 =       0.99725341  +- 0.00003784  WT3 from KINO4                      B2 =
 =       2.11171843  +- 0.00013973  YFS formfac              WT         B4 =
 =       0.50972593  +- 0.00107003  TOTAL                               B5 =
 =       0.00098202  +- 0.00013306  xsec/xtot: WT>WTMAX      WT         B6 =
 =      -0.00000910  +--0.00000567  xsec/xtot: WT<0          WT         B7 =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                           WINDOW C                                      =
 =               Built-in average control weights.                         =
 =               Should equal one +- statist. err.                         =
 =               *********************************                         =
 =       0.99903371  +- 0.00061853  <WCTA1>                             C1 =
 =       1.00047853  +- 0.00061748  <WCTA2>                             C2 =
 =       0.99932794  +- 0.00102174  <WCTA1*WCTA2>                       C3 =
 =       0.99989370  +- 0.00024958  <WCTB1>                             C4 =
 =       1.00002959  +- 0.00024948  <WCTB2>                             C5 =
 =       0.99995106  +- 0.00036400  <WCTB1*WCTB2>                       C6 =
 ===========================================================================

