1           USER'S DIRECTIVES TO RUN THIS JOB
            ----------------------------------


 ***** DATA CARD CONTENT     LIST                                                                            
 ***** DATA CARD CONTENT     C-- UNIT FOR BINARY OUTPUT                                                      
 ***** DATA CARD CONTENT     LUNOUT  18                                                                      
 ***** DATA CARD CONTENT     C-- Lab ID (used to generate random number seed)                                
 ***** DATA CARD CONTENT     LABO 'CERN'                                                                     
 ***** DATA CARD CONTENT     C-- Run number (used to generate random number seed)                            
 ***** DATA CARD CONTENT      NRUN 9997                                                                      
 ***** DATA CARD CONTENT     C-- Centre of mass energy                                                       
 ***** DATA CARD CONTENT     ECMS 208.                                                                       
 ***** DATA CARD CONTENT     C-- Number of events to generate                                                
 ***** DATA CARD CONTENT     NEVT  100000                                                                    
 ***** DATA CARD CONTENT     C-- Maximum acolinearity (0.0 = back-to-back)                                   
 ***** DATA CARD CONTENT     ACOL 179.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum acolinearity                                                        
 ***** DATA CARD CONTENT     ACLMIN 0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum acoplanarity                                                        
 ***** DATA CARD CONTENT     ACPMIN 0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum fermion polar angle                                                 
 ***** DATA CARD CONTENT     TMIN  9.0                                                                       
 ***** DATA CARD CONTENT     C-- Minimum electron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TEMIN 0.0                                                                       
 ***** DATA CARD CONTENT     C-- Maximum electron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TEMAX  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum positron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TPMIN  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Maximum positron polar angle, only relevant if TMIN=0                       
 ***** DATA CARD CONTENT     TPMAX  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum fermion energy                                                      
 ***** DATA CARD CONTENT     EMIN  0.05                                                                      
 ***** DATA CARD CONTENT     C-- Minimum electron PT                                                         
 ***** DATA CARD CONTENT     PEMIN  0.0                                                                      
 ***** DATA CARD CONTENT     C-- Minimum positron PT                                                         
 ***** DATA CARD CONTENT     PPMIN  0.0                                                                      
 ***** DATA CARD CONTENT     END                                                                             
 EXCXMP: Laboratory CERN recognized
 EXCXMP: Lab number set to         2000
MARran INITIALIZED: IJ,KL,IJKL,NTOT,NTOT2=       665      5467  20009997         1         1
 MARINI SKIPPING OVER      1000000000
 MARINI SKIPPING OVER               1


 ===========================================================================
 =                                                                         =
 =               BBB   B  B  B    B  B  BBB   BBBB                         =
 =               B  B  B  B  B    B  B  B  B  B                            =
 =               BBB   BBBB  B    B  B  B  B  BBB                          =
 =               B  B  B  B  B BB B  B  B  B  B                            =
 =               BBB   B  B  BB  BB  B  BBB   BBBB                         =
 =                                                                         =
 =                 *******************************                         =
 =                 *      BHWIDE version 1.01    *                         =
 =                 * M.C. for wide-angle Bhabha  *                         =
 =                 *   September 1995   (1.00)   *                         =
 =                 *   September 1996   (1.01)   *                         =
 =                 *           AUTHORS           *                         =
 =                 *    S. Jadach, W. Placzek,   *                         =
 =                 *          B.F.L. Ward        *                         =
 =                 *******************************                         =
 ===========================================================================



 ===========================================================================
 =                This program is based on papers                          =
 =                --------------------------------                         =
 =                UTHEP-95-1001; hep-ph/9608412.                           =
 =                Phys. Rev. D40 (1989) 3582.                              =
 =                Comp. Phys. Comm. 70 (1992) 305.                         =
 ===========================================================================


 Thank you for flying .... choosing A L I B A B A
                                    =============

 A (semi) Analytical Leading log Improved BhABhA scattering calculation.
 This program is meant for large angle Bhabha scattering [and for other
 fermion pair production (but then only in s channel)].

 *******************************************************************
 * Authors: W.J.P. Beenakker, F.A. Berends and S.C. van der Marck. *
 * Address: Instituut-Lorentz, University of Leiden                *
 *          P.o.b. 9506, 2300 RA Leiden, The Netherlands           *
 * Bitnet addresses: BEENAKKER @ HLERUL59                          *
 *                   BERENDS @ HLERUL5 or BERENDS @ HLERUL59       *
 *                   VANDERMARCK @ HLERUL59                        *
 *                   joint address: LORENTZ @ HLERUL5              *
 *******************************************************************
 * References:                                                     *
 * [1] W. Beenakker, F.A. Berends and S.C. van der Marck,          *
 *     "Large angle Bhabha scattering" and "Higher order           *
 *     corrections to the forward-backward asymmetry,"             *
 *     Leiden preprints 1990, for the treatment of the purely      *
 *     QED corrections and the incorporation of cuts on energy and *
 *     angle of both outgoing particles and their acollinearity.   *
 * [2] W. Beenakker and W. Hollik, ECFA workshop on LEP 200,       *
 *        CERN 87-08 p.185, ed. by A. Boehm and W. Hoogland;       *
 *     W. Hollik, DESY preprint 88-188, both for the treatment     *
 *     of the weak (non-QED) corrections.                          *
 *******************************************************************
 Version 2.0, August 1990

 The properties of the fermions:
    label       name     mass (GeV)  partial width of the Z (GeV)
      0      neutrino    0.0000000           0.1673613
      1      electron    0.0005110           0.0839799
      2          muon    0.1056584           0.0839792
      3           tau    1.7841000           0.0837868
      4      up quark    0.0414500           0.2995842
      5    down quark    0.0414600           0.3864207
      6   charm quark    1.5000000           0.2991584
      7 strange quark    0.1500000           0.3864167
      8     top quark  174.0000000           0.0000000
      9  bottom quark    4.5000000           0.3775833
     10       hadrons                        1.7491633

 For the bosons we have (everything in GeV):
  mass of the   Z   =   91.1887    total width of the Z =  2.5029931
  mass of the   W   =   80.4138    <==> sin**2(theta-w) =  0.2223592
  mass of the Higgs =  300.0000

 Some coupling strengths:
                    1/alfa =    137.036
 the QED correction factor =      1.0017421
               alfa-strong =      0.124
 the QCD correction factor =      1.0416593

    
 --------------  BHWIDE ----------------------
  100000.0 Events requested
  =======> Generation starts...
    
 ***************************************************************
 LABO LABNUM NRUN   NEVT ECMS 
 CERN 2000   9997 100000 208.0
   THEMN  THEMX  THPMN THPMX  ENMIN    ACOL  ACLMN  ACPMN  PTEMN  PTPMN
     9.0  171.0    9.0  171.0    0.1  179.0    0.0    0.0    0.0    0.0
 ***************************************************************
 =====================DUMPS====================
  P2   -8.0743426360566  31.4787997067146  98.7780391387460 103.9865897908801
  Q2    8.0743447314452 -31.4787979549045 -98.7917939676785 103.9996553799164
 PHO   -0.0000020953886  -0.0000017518100   0.0137548289324   0.0137548292036
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 208.0000000000000
  ievent =            1
 =====================DUMPS====================
  P2   24.4564233019658  10.8356898721603  92.8991966031417  96.6736238355683
  Q2  -24.3379304226202 -10.6515119057294 -92.4850500435798  96.2251216936559
 PHO   -0.0179456767378  -0.0123699977112  -7.7563361355405   7.7563667597432
 PHO   -0.1005472026077  -0.1718079687196   7.3421895759787   7.3448877110326
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 207.9999999999999
 =====================DUMPS====================
  P2   -5.4057046838478 -18.1811909912240 101.7899970877631 103.5421694594834
  Q2    5.3955825527210  18.1704233299746 -66.0010614021050  68.6688918036353
 PHO    0.0101221311268   0.0107676612494 -35.7889356856581  35.7889387368812
 SUM    0.0000000000000   0.0000000000000   0.0000000000000 208.0000000000000
  ievent =         1001
  ievent =         2001
  ievent =         3001
  ievent =         4001
  ievent =         5001
  ievent =         6001
  ievent =         7001
  ievent =         8001
  ievent =         9001
  ievent =        10001
  ievent =        11001
  ievent =        12001
  ievent =        13001
  ievent =        14001
  ievent =        15001
  ievent =        16001
  ievent =        17001
  ievent =        18001
  ievent =        19001
  ievent =        20001
  ievent =        21001
  ievent =        22001
  ievent =        23001
  ievent =        24001
  ievent =        25001
  ievent =        26001
  ievent =        27001
  ievent =        28001
  ievent =        29001
  ievent =        30001
  ievent =        31001
  ievent =        32001
  ievent =        33001
  ievent =        34001
  ievent =        35001
  ievent =        36001
  ievent =        37001
  ievent =        38001
  ievent =        39001
  ievent =        40001
  ievent =        41001
  ievent =        42001
  ievent =        43001
  ievent =        44001
  ievent =        45001
  ievent =        46001
  ievent =        47001
  ievent =        48001
  ievent =        49001
  ievent =        50001
  ievent =        51001
  ievent =        52001
  ievent =        53001
  ievent =        54001
  ievent =        55001
  ievent =        56001
  ievent =        57001
  ievent =        58001
  ievent =        59001
  ievent =        60001
  ievent =        61001
  ievent =        62001
  ievent =        63001
  ievent =        64001
  ievent =        65001
  ievent =        66001
  ievent =        67001
  ievent =        68001
  ievent =        69001
  ievent =        70001
  ievent =        71001
  ievent =        72001
  ievent =        73001
  ievent =        74001
  ievent =        75001
  ievent =        76001
  ievent =        77001
  ievent =        78001
  ievent =        79001
  ievent =        80001
  ievent =        81001
  ievent =        82001
  ievent =        83001
  ievent =        84001
  ievent =        85001
  ievent =        86001
  ievent =        87001
  ievent =        88001
  ievent =        89001
  ievent =        90001
  ievent =        91001
  ievent =        92001
  ievent =        93001
  ievent =        94001
  ievent =        95001
  ievent =        96001
  ievent =        97001
  ievent =        98001
  ievent =        99001
  =======> Generation finished!
    
 |||||||||||||||||||||||||||||||||||||||||||||||||||
 || Number of events        100000
 || Number of written events        100000
 Xsec_accep =      0.99184844 Nanob.
 error      =      0.00106409 Nanob.
 Xsec_write =      0.99184844 Nanob.
 || Random Number state    20009997   103160512           1
 |||||||||||||||||||||||||||||||||||||||||||||||||||
1748.76user 1.00system 29:21.77elapsed 99%CPU (0avgtext+0avgdata 0maxresident)k
0inputs+0outputs (207major+181minor)pagefaults 0swaps


 ===========================================================================
 =                                                                         =
 =               BBB   B  B  B    B  B  BBB   BBBB                         =
 =               B  B  B  B  B    B  B  B  B  B                            =
 =               BBB   BBBB  B    B  B  B  B  BBB                          =
 =               B  B  B  B  B BB B  B  B  B  B                            =
 =               BBB   B  B  BB  BB  B  BBB   BBBB                         =
 =                                                                         =
 =                 *******************************                         =
 =                 *      BHWIDE version 1.01    *                         =
 =                 * M.C. for wide-angle Bhabha  *                         =
 =                 *   September 1995   (1.00)   *                         =
 =                 *   September 1996   (1.01)   *                         =
 =                 *           AUTHORS           *                         =
 =                 *    S. Jadach, W. Placzek,   *                         =
 =                 *          B.F.L. Ward        *                         =
 =                 *******************************                         =
 ===========================================================================



 ===========================================================================
 =                This program is based on papers                          =
 =                --------------------------------                         =
 =                UTHEP-95-1001; hep-ph/9608412.                           =
 =                Phys. Rev. D40 (1989) 3582.                              =
 =                Comp. Phys. Comm. 70 (1992) 305.                         =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWIDE: INPUT PARAMETRES                                =
 =               *********************************                         =
 =                1                  OPTIONS   switch        KeyOpt     N1 =
 =                0                  weighting switch        KeyWgt        =
 =                1                  rand. numb. switch      KeyRnd        =
 =             1221                  RADIATION switch        KeyRad     N2 =
 =                1                  e-weak cor. switch      KeyEWC        =
 =                2                  EWRC Libary choice      KeyLib        =
 =                2                  QED mat. elm. type      KeyMod        =
 =                1                  vac_pol   switch        KeyPia        =
 =     208.00000000                  CMS energy   [GeV]      CMSENE     X1 =
 =    43264.000                      CMSENE^2   [GeV^2]      SVAR          =
 =       9.00000000                  theta_min_e+ [deg]      THMINP     X2 =
 =     171.00000000                  theta_max_e+ [deg]      THMAXP     X3 =
 =       9.00000000                  theta_min_e- [deg]      THMINE     X4 =
 =     171.00000000                  theta_max_e- [deg]      THMAXE     X5 =
 =       0.15707963                  theta_min_e+ [rad]      THMIRP        =
 =       2.98451302                  theta_max_e+ [rad]      THMARP        =
 =       0.15707963                  theta_min_e- [rad]      THMIRE        =
 =       2.98451302                  theta_max_e- [rad]      THMARE        =
 =       0.05000000                  Energy_min_e+[GeV]      ENMINP     X6 =
 =       0.05000000                  Energy_min_e-[GeV]      ENMINE     X7 =
 =       0.00048077                  E_min_e+/E_beam         XEMINP        =
 =       0.00048077                  E_min_e-/E_beam         XEMINE        =
 =     179.00000000                  Acollinearity[deg]      ACOLLI     X8 =
 =       3.12413936                  Acollinearity[rad]      ACOLLR        =
 =   0.64288703E-01                  trasf_min [GeV^2]       TRMIN         =
 =    43264.000                      trasf_max [GeV^2]       TRMAX         =
 =   0.14859630E-05                  xi_min=TRMIN/SVAR       XIMIN         =
 =    1.0000000                      xi_max=TRMAX/SVAR       XIMAX         =
 =   0.10000000E-03                  eps_CM infr. cut        EPSCMS     X9 =
 =   0.99999998E-06                  delta  infr. cut        DEL           =
 =      91.18869781                  Z-mass GeV              AMAZ          =
 =       2.50299314                  Z-width GeV             GAMMZ         =
 =       0.22235916                  weak mixing angle       SINW2         =
 =   0.90991286                      Born xsecion [nb]       BORNXS        =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWID1:        WINDOW A                                 =
 =               *********************************                         =
 =           100000                  Accepted total          NEVGEN     A1 =
 =          1963409                  Raw prior reject.       IEVENT     A2 =
 =   0.99184844      +- 0.00106409   Xsec M.C. [nb]          XSECMC     A3 =
 =       0.00107283                  relat. error            ERELMC     A4 =
 =       0.51032426  +- 0.00107283   weight  M.C.            AWT        A5 =
 =               52                  WT<0                    NEVNEG     A6 =
 =              102                  WT>WTMAX                NEVOVE     A7 =
 =      10.00000000                  Maximum WT              WWMX       A8 =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                 BHWID1:        WINDOW B                                 =
 =               *********************************                         =
 =       0.46892694  +- 0.00053720  WT1*WT2*T/TP*T/TQ                   B1 =
 =       0.99729036  +- 0.00003765  WT3 from KINO4                      B2 =
 =       2.11389066  +- 0.00014013  YFS formfac              WT         B4 =
 =       0.51032426  +- 0.00107283  TOTAL                               B5 =
 =       0.00100991  +- 0.00013417  xsec/xtot: WT>WTMAX      WT         B6 =
 =      -0.00001478  +--0.00001020  xsec/xtot: WT<0          WT         B7 =
 ===========================================================================



 ===========================================================================
 =               *********************************                         =
 =                           WINDOW C                                      =
 =               Built-in average control weights.                         =
 =               Should equal one +- statist. err.                         =
 =               *********************************                         =
 =       1.00058987  +- 0.00062030  <WCTA1>                             C1 =
 =       0.99933578  +- 0.00062121  <WCTA2>                             C2 =
 =       1.00044496  +- 0.00102651  <WCTA1*WCTA2>                       C3 =
 =       0.99986932  +- 0.00025009  <WCTB1>                             C4 =
 =       1.00003200  +- 0.00024993  <WCTB2>                             C5 =
 =       0.99992558  +- 0.00036468  <WCTB1*WCTB2>                       C6 =
 ===========================================================================

