Delphi Raw Data Diagram

DELPHI Raw Data Diagram
=======================

    *                                                               *
    *                                                               *
    *       The structure of the RAW data appears now as follows:   *
    *                                                               *
    *                -------------                                  *
    *                |    TOP    |                                  *
    *                -------------                                  *
    *                 -1  -5  -7       ______                       *
    *                  |   |   |------>| PA |                       *
    *                  |   |    ______ ------                       *
    *                  |   |--->| SE |                              *
    *                  |        ------                              *
    *           ---------------                                     *
    *           |     RAW     |          EMB (Event Master Bank)    *
    *           ---------------                                     *
    *            -1,,-6,,-n -5                                      *
    *                   |    |       -----------                    *
    *                   |    ------> | TRIGGER |                    *
    *                   |            -----------                    *
    *                   |               |   |                       *
    *                   |               |-3 |-2                     *
    *                   |               |   |     --------------    *
    *                   |               |   ----> | T1/T2 DATA |    *
    *                   |               |         --------------    *
    *                   |               |   -----------             *
    *                   |               ----| T3 DATA |             *
    *                   |                   -----------             *
    *                   |                                           *
    *          ---------------------                                *
    *          |       PMBn        |  1 PMB (Partition Master Bank) *
    *          ---------------------           per module           *
    *              |  |  |  |  |                                    *
    *         ... -5 -4 -3 -2 -1                                    *
    *              |  |  |  |  |                                    *
    *              |  |  |  |  |                                    *
    *              |  |  |  |  |    ----------                      *
    *              |  |  |  |  |--> | ERRORS |                      *
    *              |  |  |  |       ----------                      *
    *              |  |  |  |    ---------                          *
    *              |  |  |  |--> | T1/T2 |                          *
    *              |  |  |       ---------                          *
    *              |  |  |    ------                                *
    *              |  |  |--> | T3 |                                *
    *              |  |       ------                                *
    *              |  |    ------                                   *
    *              |  |--> | T4 |                                   *
    *              |       ------                                   *
    *              |    --------                                    *
    *              |--> | RAWD |                                    *
    *                   --------                                    *
    *                                                               *
    *                                                               *
