Post DST data sets
==================

-   **1991**
    -   *Real Data*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shrd91.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lord91.html)
    -   *Monte Carlo*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shmc91.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc91.html)
-   **1992**
    -   *Real Data*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shrd92.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lord92.html)
    -   *Monte Carlo*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shmc92.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc92.html)
-   **1993**
    -   *Real Data*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shrd93.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lord93.html)
    -   *Monte Carlo*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shmc93.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc93.html)
-   **1994**
    -   *Real Data*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shrd94.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lord94.html)
    -   *Monte Carlo*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shmc94.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc94.html)
-   **1995**
    -   *Real Data*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shrd95.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lord95.html)
    -   *Monte Carlo*
        -   [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shmc95.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc95.html)
-   **1996**
    -   *Real Data*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsrd96.html) ,
            [Short
            DST](http://delphiwww.cern.ch/spassoff/data/shrd96.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lord96.html)
    -   *Monte Carlo*
        -   Short DST , [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc96.html)
-   **1997**
    -   *Real Data*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsrd97.html)
    -   *Monte Carlo*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsmc97.html) ,
            [Long
            DST](http://delphiwww.cern.ch/spassoff/data/lomc97.html)
-   **1998**
    -   *Real Data*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsrd98.html)
    -   *Monte Carlo*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsmc98.html)
-   **1999**
    -   *Real Data*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsrd99.html)
    -   *Monte Carlo*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsmc99.html)
-   **2000**
    -   *Real Data*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsrd00.html)
    -   *Monte Carlo*
        -   [XShort
            DST](http://delphiwww.cern.ch/spassoff/data/xsmc00.html)

[]()

------------------------------------------------------------------------

*Last update 01 Apr 2000*

[]()[**Tzanko Spassoff**](http://consult.cern.ch/xwho/people/03692)\
[mail to Tzanko.Spassoff@cern.ch](mailto:Tzanko.Spassoff@cern.ch)
