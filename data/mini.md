DELPHI Mini DST

DELPHI Mini DST
===============

Mini DST data structure and available datasets

[Mini DST content
description](http://delphiwww.cern.ch/spassoff/des/minides.ps)

****

Mini DST datasets

****

------------------------------------------------------------------------

*This page is updated further on by*

[Hansjoerg
Klein](http://delonline.cern.ch/delphi$www/public/people/klein.html)

**For suggestions and corrections mail to KLEIN@VXCERN.CERN.CH**
