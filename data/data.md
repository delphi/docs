### ![DELPHI Collaboration](http://delphiwww.cern.ch/icons/dolphin.gif) DELPHI experiment at LEP ![DELPHI Collaboration](http://delphiwww.cern.ch/icons/dolphin.gif)

Physics Data Sets
=================

for Analysis of *Real Data and Monte Carlo*
-------------------------------------------

------------------------------------------------------------------------

### Tools for Datasets Identification

-   [FATFIND (find individual Datasets and search DELPHI
    Files contents)](http://delphiwww.cern.ch/offline/fatfind/fatfind.html)
-   [SCANFAT (find FATMEN nicknames and search DELPHI
    Files contents)](http://delphiwww.cern.ch/scanfat.html)

------------------------------------------------------------------------

### Lists of Datasets

-   [**Real Data and MC sets in CASTOR (DST, XShort, Short) for
    analysis**](http://delphiwww.cern.ch/offline/data/castor/html/index.html)
-   Old/obsolete pages:
    -   [Post DST's (XShort,Short and Long) for analysis
        (old page)](http://delphiwww.cern.ch/offline/data/postdst.html#end)
    -   [High Energy Hotline
        98](http://delphiwww.cern.ch/delfigs/delwww/hotline/hotline_189/hot_tapes98.html)
    -   [Raw data for all LEP periods](rawdata.html)

### DELANA software used for re-processings

-   [Processings in 1996 and
    1997](http://delphiwww.cern.ch/wickens/process9697.html)
-   [Processings in
    1998](http://delphiwww.cern.ch/wickens/process98.html)

### History of DELANA and DSTANA reprocessings

-   [DELANA, DSTANA and known
    problems](http://delphiwww.cern.ch/offline/prodsh.html)

### Additional informations on final LEP2 data set

-   [Final software, data sets and XSDST versions
    available](http://delphiwww.cern.ch/offline/data/Final_LEP2_data_set.html)

[Back to Delphi homepage](http://delphiwww.cern.ch/)

------------------------------------------------------------------------

*Last update 25 Sep 2002*
