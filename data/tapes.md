DELPHI Physics Data

DELPHI Data Cassettes
=====================

### Known to TMS (Tape Management System) at CERN

### *Located in the Central Tape Vault or in the Tape Archive*

### *(This is a list for technical and historical use only)*

------------------------------------------------------------------------

IBM 3480 cassettes with DST-data produced at DELFARM
----------------------------------------------------

### *ALL cassettes of type EA have been archived 28 November 1996*

-   EA0031 - EA0068 DSTO90 FANOUT
-   EA0345 - EA0541 DSTO91 DELANA D
-   EA0542 - EA0689 DSTO91 DELANA E
-   EA0801 - EA0838 DSTO90 DSTFIXED
-   EA0995 - EA1419 DSTO92 DELANA B
-   EA1420 - EA1708 DSTO92 DELANA C
-   EA1709 - EA1884 DSTO91 DELANA F
-   EA1885 - EA2169 DSTO92 DELANA D
-   EA2401 - EA2489 DSTO93 DELANA B
-   EA2541 - EA2620 DSTO93 DELANA B
-   EA2681 - EA2749 DSTO93 DELANA B
-   EA2761 - EA2763 DSTO93 DELANA B
-   EA2768 - EA2855 DSTO93 DELANA B
-   EA2861 - EA2865 DSTO93 DELANA B
-   EA2901 - EA2995 SHORT92 SDST D1
-   EA3001 - EA3098 SHORT92 SDST D2
-   EA3101 - EA3212 SHORT93 SDST B1
-   EA3213 - EA3257 SHORT91 SDST F1
-   EA3258 - EA3553 DSTO93 DELANA C
-   EA3601 - EA4429 DSTO94 DELANA A (archived)
-   EA4601 - EA5280 DSTO94 DELANA B
-   EA5281 - EA5431 SHORT93 SDST C1
-   EA5432 - EA5718 SHORT94 SDST B1
-   EA5721 - EA6131 DSTO95 DELANA A (P1+P2) Delana\_B in Robot SMCG
    I15501-I15584
-   EA6141 - EA6235 DSTO95 DELANA A (P3)
-   EA6236 - EA6333 DSTO95 DELANA B (P3)

IBM 3480 cassettes reserved for External copies
-----------------------------------------------

### *ALL cassettes of type EB have been archived 28 November 1996*

-   EB0001 - EB4500 IN2P3 LYON
-   EB4501 - EB5000 SACLAY
-   EB5001 - EB5200 RAL Rutherford
-   EB5201 - EB7000 IN2P3 LYON
-   EB7001 - EB7500 LIP (P.Abreu)
-   EB7501 - EB8000 HEPHY WIEN (W.Mitaroff)
-   EB8001 - EB9000 SACLAY
-   EB9001 - EB9999 SACLAY

IBM 3480 cassettes Test-beam data of detectors (ie. HPC)
--------------------------------------------------------

### *ALL cassettes of type EC have been archived 28 November 1996*

-   EC0001 - EC9999

IBM 3480 cassettes MDST's for 1989 pilot run (archived)
-------------------------------------------------------

-   ED0001 - ED0034 (data kept for historical reasons)

DLT2000 multifile cassettes rawdata copies
------------------------------------------

-   ED0001 - ED0004 1990 data (50 files each) team 4 selection
-   ED0005 - ED0017 1991 data (50 files each) DELANA selection
-   ED0018 - ED0104 1992 data (50 files each)
-   ED0105 - ED0150 1993 data (60 files each)
-   ED0151 - ED0238 1994 data (60 files each)

<!-- -->

-   ED0251 - ED1000 Raw data written via Central Data Recording [click
    here](http://delphiwww.cern.ch/offline/data/rawdata.html)

DLT2000 multifile cassettes Simulation Raw data
-----------------------------------------------

-   ED5001 - ED9999 allocated by Central Computing Group

DLT2000 multifile cassettes for external copies
-----------------------------------------------

-   EE0001 - EE9999 allocated by central computer group

EXAbyte 8500 multifile cassettes for external copies
----------------------------------------------------

-   EF0001 - EF9999 allocated by central computer group (Anita Bjorkebo)

EXAbyte 8500 multifile cassettes raw data copies (obsolete, see ED's)
---------------------------------------------------------------------

-   EG0001 - EG0007 1990 (input EN0001 - EN0171 (25 files each EXAbyte)
-   EG0008 - EG0033 1991 (input EN0301 - EN0941 (25 files each EXAbyte)
-   EG0034 - EG0206 1992 (input EP8013 - EQ2364 (25 files each EXAbyte)

IBM 3480 cassettes DST's of Simulation production
-------------------------------------------------

-   EH0001 - EH9999 look-up in SIMDST STATUS for details

IBM 3480 cassettes Short DST's of Simulation producted by DELfarm
-----------------------------------------------------------------

-   EI0001 - EI9999 look-up in SIMDST STATUS for details (Tz.Spassov)

IBM 3480 cassettes DST's of Simulation production (93\_1 vs onwards)
--------------------------------------------------------------------

-   EJ0001 - EJ9999 look-up in SIMDST STATUS for details

DLT2000 multifile cassettes possible backup data copies
-------------------------------------------------------

-   EK0001 - EK5000 allocated and used by Central Computing Group

DLT2000 multifile cassettes DST's for Simulation Production
-----------------------------------------------------------

-   EK5001 - EK9999 Numbers allocated by Central Computing Group

IBM 3480 cassettes Stream for Leptonics produced by DELfarm
-----------------------------------------------------------

-   EL0001 - EL9999 see Central Computer Group for details

IBM 3480 cassettes reserved for external copies
-----------------------------------------------

-   EM0001 - EM5000 MILANO
-   EM5001 - EM7500 VALENCIA
-   EM7501 - EM9999 KARLSRUHE

IBM 3480 cassettes copies of raw data (may be recycled)
-------------------------------------------------------

-   EN0001 - EN0171 Stripped raw data 90 (Team 4 selection). archived
-   EN0301 - EN0941 Stripped raw data 91 of DELANA events. archived

IBM 3480 cassettes Original Raw data written at DELPHI pit
----------------------------------------------------------

### The cassettes below have been copied to multifile DLT's [click here](http://delphiwww.cern.ch/offline/data/rawdata.html) and are archived

-   EP0001 - EP0969 LEP period 1989
-   EP1006 - EP3334 LEP period 1990
-   EP4001 - EP8000 LEP period 1991
-   EP8013 - EP9999 LEP period 1992
-   EQ0001 - EQ2364 LEP period 1992
-   EQ2401 - EQ5284 LEP period 1993
-   EQ5291 - EQ9999 LEP period 1994
-   ER0001 - ER0671 LEP period 1994

DLT MULTIFILE output
--------------------

-   ES0001 - ES.... [Hotline tapes, click
    here](http://delphiwww.cern.ch/offline/data/data.html)

IBM 3480 cassettes for TEAM DST's (see Tz.Spassov)
--------------------------------------------------

-   ET0001 - ET9999 Mainly Team strips. see FAT\_GROU NAMES G (CERNVM)

IBM 3480 cassettes Data processing selection streams
----------------------------------------------------

-   EV0001 - EV0036 Stripping V0's D\* ,etc events 93C processing
-   EV0041 - EV0776 Stripping STP's, V0's, D\* ,etc events 94B
    processing
-   EV0781 - EVxxxx Stripping STP's, V0's, D\* ,etc events 95A
    processing

IBM 3480 cassettes for MDST's and Simulation raw data
-----------------------------------------------------

-   EW0001 - EW0092 MDST's 1989 data} Half field (archived)
-   EW0093 - EW0146 MDST's 1989 data Full field (archived)
-   EW0147 - EW0173 MDST's 1989 data Half field recovered (archived)
-   EW0174 - EW0190 MDST's 1989 data Full field recovered (archived)
-   EW0191 - EW0191 MDST's 1989 data Half field recovered (archived)
-   EW0201 - EW0310 Simulation Raw data CERN-CSF (LEP200) archived
-   EW0601 - EW1000 Simulation Raw data CERN-DELFARM
-   EW1001 - EW1150 Simulation Raw data CERN-CSF (LEP200) archived
-   EW1151 - EW1300 Simulation Raw data CERN-CSF archived
-   EW1301 - EW2100 Simulation Raw data CERN-DELFARM
-   EW2101 - EW2900 Simulation Raw data CERN-CSF
-   EW2901 - EW9999 Simulation Raw data CERN-CSF (see A.Bjorkebo
    for details)

IBM 3480 cassettes Userpool for private use
-------------------------------------------

-   EX0301 - EX9999 Allocated by Central Computer Group

IBM 3480 cassettes DELfarm STP's for tests and other use
--------------------------------------------------------

-   EZ0001 - EZ9999 see A.Bjorkebo for details

------------------------------------------------------------------------

Tape copies in Central Robotics
===============================

SMCF robot with single file 3480 cassettes (200Mb)
--------------------------------------------------

*Since 1 Jan 1996 this robot has been stopped and all tapes are available through manual racks in the vault*
------------------------------------------------------------------------------------------------------------

-   I24061 - I24098 DSTO90 (copies of EA0031-EA0068)
-   I24101 - I24245 DSTO91\_D (copies of EA0345-EA0541)
-   I24251 - I24396 DSTO91\_E (copies of EA0542-EA0689)
-   EI0001 - EI9999 most of the simulation short DST's (Tz.Spassoff)

SMCG robot with multi file 3490 cassettes (1Gb)
-----------------------------------------------

*Since 1 Jan 1997 this robot has been stopped and all tapes are available through manual racks in the vault*
------------------------------------------------------------------------------------------------------------

-   I05001 - I05057 DSTO92\_C (copies of EA1420-EA1708)
-   I05065 - I05085 DSTO91\_F (copies of EA1709-EA1878)
-   I05090 - I05137 DSTO92\_D (copies of EA1885-EA2169)
-   I05145 - I05160 SHORT92\_D1 (copies of EA2901-EA2995)
-   I05161 - I05216 DSTO93\_B (copies of EA2401-EA2865)
-   I05231 - I05246 SHORT92\_D2 (copies of EA3001-EA3098)
-   I05247 - I05265 SHORT93\_B1 (copies of EA3101-EA3212)
-   I05293 - I05300 SHORT91\_F1 (copies of EA3213-EA3257)
-   I05301 - I05303 LONG91\_F1 (copies of EL8582-EL8598)
-   I05304 - I05311 LONG92\_D2 (copies of EL8475-EL8521)
-   I05312 - I05321 LONG93\_B1 (copies of EL8522-EL8581)
-   I05375 - I05377 MINI91\_F2
-   I05378 - I05384 MINI92\_D3
-   I05385 - I05391 MINI93\_B2
-   I05392 - I05398 MINI93\_C1
-   I05399 - I05412 MINI94\_B1
-   I05431 - I05455 SHORT93\_C1 (copies of EA5281-EA5431)
-   I05597 - I05604 LONG93\_C1 (copies of EL0151-EL0205)
-   I05646 - I05700 SHORT94\_B2
-   I05701 - I05717 MINI94\_B2
-   I05718 - I05737 LONG94\_B2
-   I15501 - I15584 DSTO95\_B (P1+P2)
-   I15585 - I16500 allocated to Delphi for use in production group
-   I02021 - I02320 allocated to Delphi for use in production group

All data may be accessed via FATFIND and will find it for you, whatever
happens.

Central Computer Group contacts concerning Data tapes
-----------------------------------------------------

-   [Anita Bjorkebo](http://consult.cern.ch/xwho/people/00358) DELFARM
    and Data tapes
-   [G. Grosdidier](http://consult.cern.ch/xwho/people/01640) Central
    Computing Group project leader,
-   [Tz. Spassov](http://consult.cern.ch/xwho/people/03692)
    XShort/Short/Long/Mini DST's , data tapes and Fatmen

------------------------------------------------------------------------

*Last Update 1 feb 1999*

[Hansjoerg Klein](http://delphiwww.cern.ch/offline/hkl.html)
