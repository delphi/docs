# DELPHI documentation
This repository contains various documents and manuals needed to understand how to run the software stack and how it works.

## CERNLIB
* [CERNLIB](doc/cernlib.ps.gz)
* [ARIADNE](doc/ariadne.ps.gz)
* [COMIS](doc/comis.ps.gz)
* [FATMEN (obsolete)](doc/fatmen.ps.gz)
* [GEANT 3](doc/geantall.ps.gz)
* [HIGZ](doc/higz.ps.gz)
* [KUIP](doc/kuip.ps.gz)
* [MINUIT](doc/minuit.ps.gz)
* [PAW](doc/paw.ps.gz)
* [ZEBRA](doc/zebra.ps.gz)
* [CSPACK](doc/cspack.ps.gz)
* [FFREAD](doc/ffread.ps.gz)
* [HBOOK](doc/hbook.ps.gz)
* [CERNLIB installation instructions](doc/install.ps.gz)
* [LHC++](doc/lhcpp.ps.gz)
* [Patchy](doc/p5refman.ps.gz)
* [Pythia](doc/pythia/lutp0030.ps.gz)
* [Pythia 6.2](doc/pythia/pythia6206.ps.gz)

## Database access

* [CARGO manual](ddb/cargo600.ps.gz)
* [DDAPP manual](ddb/ddapp.ps.gz)
* [DPLOT manual](ddb/dplot.ps.gz)

## Simulation
*   [running simulations](delsim/html/running_simana.html)
*   [production scheme setup at CERN](delsim/html/delphi_mass_production.html)

## Visualisation

*   [DELGRA manual](delgra/delgra.ps.gz)

## Data sample formats, DST structure

*   [LONG dst](dst/longdes.ps.gz)
*   [SHORT dst](dst/shortdes.ps.gz)
*   [(old) XSHORT dst](dst/xshortdes.ps.gz)

## Data analysis: dstana

*   [PHDST manual](dstana/phdst.ps.gz)
*   [SKELANA manual](dstana/skelana2.ps.gz)
*   [VECSUB manual](dstana/vecsub.ps.gz)
*   [basic btagging](dstana/btag.ps.gz)
*   [dstana manual](dstana/dstana2.ps.gz)

## Data and MonteCarlo samples and meta data
* [Note on LEP2 data sets](data/Final_LEP2_data_set.md)
* [DELPHI Physics Data Structures](data/format.md)
* [Samples on EOS/CASTOR](data/castor/index.html)
